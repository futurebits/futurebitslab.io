## Unit 20
# Science and Technology
### Ways with words

  

#### A. Match the words with their correct definitions.

  
**a. Freshman:** a first-year student at a university, college, or high school  
  
**b. Naive:** having a lack of experience or knowledge  
  
**c. Obliviously:** without conscious awareness  
  
**d. Brag:** says something in a boastful manner  
  
**e. Disposal:** action of throwing away something  
  
**f. Dorm:** dormitory, student residence hall or building  
  
**g. Suitemate:** someone who shares your bathroom/living room/kitchen  
  
**h. Incalculable:** not able to be calculated or estimated  
  

#### B. Replace the bold words in (a-h) selecting synonyms from the box.

a. Her story is **incredible** in the literal sense of the word.  
  
b. We often read the novels of the **potential** writers in the world.  
  
c. The Facebook users are **fragmented** but connected to each other through the Internet.  
  
d. Sometimes **unanticipated** events happen in our life.  
  
e. He paused, **scrutinizing** the faces of Anjana and Manju with his glittering eyes.  
  
f. I am sorry to say your handwriting is **indecipherable**.  
  
g. He is matured. He can **navigated** his own journey to make his career better.  
  
h. Gita’s heart swelled with **delight**, translating her confidence into power.  
  

#### C. Complete the sentences by choosing the correct word given in brackets.

a. Does television **affect** children? (affect/effect)  
  
b. Does television have an **effect** on children? (affect/effect)  
  
c. Could you **lend** me your book, please? (borrow/lend)  
  
d. Can I **borrow** your pen? (borrow/lend)  
  
e. Prices seem to **rise** every year. (raise/rise)  
  
f. You can **raise** your hand if you want to ask a question. (raise/rise)  
  
g. What did he **say** to you? (say/tell)  
  
h. I can’t **speak** Hindi. (speak/talk)  
  
i. I will **talk** to you on the phone. (speak/talk)  
  
j. I think that’s a very **sensible** idea. (sensible/sensitive)  
  
k. My teeth are very **sensitive** to cold. (sensible/sensitive)  
  
l. Our **principal** is a popular person. (principal/principle)  
  
m. I couldn’t understand the **principle** of gravity. (principal/principle)  
  
n. All friends, **expect** Nabina, came to the party. (accept/except)  
  
o. Will you **accept** my request? (accept/except)  
  
p. They were making too much **noise**. (noise/sound)  
  
q. All she could hear was the **sound** of the waves. (noise/sound)  
  
r. Did you give him any **advice** for his career? (advice/advise)  
  
s. My parents **advise** me to be a teacher. (advice/advise)  
  

 

### Comprehension

  

#### Answer these questions.

  

#### a. Why did the author feel that she was lucky to be so naïve of her freshman year at college?

The author felt that she was lucky to be so naïve of her freshman year at college because the very ignorance navigated her to the path of wealth and status. Despite of being foolish, she got an opportunity to explore new things. So she felt lucky.  
  

#### b. Why did she say that she went to college in the Stone Age?

She said that she went to college in the Stone Age because she compares her college year with her son’s college year she found a vast difference. Technology was not such a use at her time. Compared to the present time, she was not so lucky to use cell phones, electronic gadgets to the fullest like his son is doing now. Seeing her son, using electronic devices and technology almost everywhere, she says that her time was so primitive, hence she said she went to college in the stone age.  
  

#### c. What kinds of technological tools can Hayden use at his college life unlike at his mother’s time?

Hayeden uses every technological shortcuts to make his living easier. He uses mobile phone with Yale specific apps, Facebook, Twitter, Instagram and Youtube.  
  

#### d. How has the internet and social sites affected the lifestyle of the youths?

The Internet has made the youths rely on nearly everything either little or big. It has made the life of youths easier forgetting to experience struggle and hard labor. With no exertion they’ll plan undertaking work depending upon search engines and software hampering the extension of the skyline of pondering and innovativeness. Social sites have given them opportunities to be connected to different people but they are fragmented because they know each other virtually. Due to internet and social sites the youths have missed the actual pleasure of the discovering of a real world.  
  

#### e. What things about college life will Hayden really miss unlike his mother?

Hayden will miss real life interactions, usage of thinking capability and imagination, exploration and discoveries of various things in life dissimilar to his mom.  
  

#### f. The writer says, "I worry that students today are more connected and more fragmented". Isn’t this paradoxical? How?

The line "I worry that student today are more connected and more fragmented " is really conflicting and paradoxical. The paradoxical sentence is unreasonable in surface level. However, in profound level, there is its philosophical importance. There is no such thing as a fracture in affiliation apart from the above line implies youth are related to each other mainly by pleasant locations. But, in real world there are far to at least one one other. They do not have particular person to particular person connection so they’re divided. There is no openness to one another to acquire life’s critical minute.  
  

### Critical thinking

  

#### a. Do you think that advancements of technology can hinder the exposure students receive in school, and block them from gaining some of life’s most memorable moments? Give reasons in support of your answer.

Technology is the machines and tools we use to convert natural or mental resources into things we need, like energy or algorithms. Technology can be used in so many different ways, positive and negative, that the answers have become as convoluted and complicated as human interaction. It follows, therefore, that some technology is right for children in a learning and social environment, and some technology is not.  
  
Most of the schools and colleges are using modern devices like laptops and tablets as a means of teaching. So the students are also using the same devices to be on par with the teachings of the teachers. In the present day, social media has evolved leaps and bounds, with 90% of the world’s population using social media. So the devices which are provided to the students for studying are instead used to be active on social media. Students are not studying with the help of these devices, they are interested in checking the posts and status updates of their near and dear ones and many other things. This is how technology is becoming a huge distraction for the learners, thus increasing the gap between students and educators.  
  
In the present day, due to the huge advancement of technology, teachers are educating the students with different online tools instead of physically communicating with them, hence the students are unable to interact with the students and also cannot share problems with them. Teachers miserably fail to draw the attention of the student. It is recommended to use verbal communication with the students along with using online tools. So the students can learn the topics as well as they can share their problems with the teacher.  
  
Thus its true that advancements of technology can hinder the exposure students receive in school, and block them from gaining some of life’s most memorable moments.  
  

#### b. Kline’s essay focuses on the contrast between her son’s freshman college experience and her own, but she also establishes what they have in common. Explain.

The essay entitled “Taking my son to College, Where Technology has Replaced Serendipity” written by Christina Baker Kline is about the contrast between her college experience and her son’s experience. But the essay also includes the common things between her and her son. She talks about the experience of freshman college year in the same university. Although technology has replaced the way they do everything but still the education system was all same. All the old feelings and emotions were also the same. There were much more difficulty on her time comparing to her son’s age but still, friendships and team working has not changed. These things are all same in some way. Exploration about the new things ware limited to physical doings, but now at the time of his son, the exploration and research is still the same, but it is done via technology medium, via search engines, E-books rather than physical books. Relationship and friendship was the struggle for freshman at her time, still search for the companion was prominent. This thing still prevails in electronic form from facebook, instagram and social media.  
  

#### c. Has the internet aided to broadening or narrowing the critical thinking capacity of youths or readers? How?

Internet is a global network of interconnected networks of computers. It makes it easier and quick for people to access information sitting at any corner of the world. Just like any other technology, internet too has its own share of positive and negative effects on society. If internet is used to gain and explore knowledge, it is broadening of knowledge, but if you started relying on interent answers with blunt creativity, it is narrowing of the critical thinking capacity. The world of internet is so infinite that it can seem overwhelming. All forms of media don’t work in every setting. There needs to be a balance of modern technology in life, or you run the risk of losing out on developing fundamental skills. Internet is an excellent boost to your critical thinking skills, but you should be aware of some of the pitfalls that come along with it. Thus depending on how you’re using it, internet can be enhancing or damaging to critical thinking.  
  

### Grammar

  

#### A. Fill in the gaps with suitable articles where necessary.

a. Is he working as **a** university professor?  
  
b. My younger sister watches **the** television a lot.  
  
c.  
A: What did you get for your birthday?  
B: I got **a** lot of good presents.  
  
d. I’m going to **the** Dominican Republic for my winter vacation.  
  
e. I have to go to **a** bank today to deposit some money.  
  
f. Durga was injured in the accident and was taken to **the** nearest hospital.  
  
g. Every parent should visit  school to meet the teachers.  
  
h. Who is **the** woman in this photograph?  
  
i. There is **a** piano in the corner of the room.  
  
j.  
A: Do you think he is lying?  
B: No, he’s the kind of **a** guy that always tells the truth.