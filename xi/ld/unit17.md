## Unit 17
# Globalisation and Diaspora
### Ways with words

  

#### A. Choose the words for the following meanings.

  

#### a. the severely damaging or destructive effects of something

ravage  
  

#### b. someone forced to leave their country in order to escape war, persecution, or natural disaster

refugee  
  

#### c. to face a difficult situation

confront  
  

#### d. take a position of power or importance illegally or by force

usurp  
  

#### e. the process of becoming a part of a group, country, society, etc.

assimilation  
  

#### f. an area within a larger territory whose inhabitants are culturally or ethnically distinct

enclave  
  

#### C. Fill in the gaps with the correct word from the brackets.

  
a. I asked him if he was attending the ceremony and he **shook** his head ‘no’. _(shook/moved/ nodded)_  
  
b. They made a horrible decision which caused **irreparable** damage to our company. _(hopeless/inflexible/irreparable)_  
  
c. The leaders need to break down **barriers** so as to create favorable environment in the nation. _(barriers/obstacles/hindrances)_  
  
d. You ought to talk to the manager to **settle** the dispute. _(solve/settle/clear up)_  
  
e. Perhaps this issue will not get much media **coverage**. _(security/examination/ coverage)_  
  
f. This week is the first week of Joan as an in-charge. Everything is running **smoothly**. _(smoothly/calmly/easily)_  
  
g. I haven’t read the text thoroughly, but given a **quick** glance. _(fast/quick/rapid)_  
  
 

### Comprehension

  

#### Answer the following questions.

  

#### a. Why did millions of Palestinians leave their country?

Millions of Palestinians left their country because their country had denied basic human rights to find a better life and better economic opportunities.  
  

#### b. How is the global culture formed? Is cultural identity possible even in the global culture? Justify.

The global culture is formed by the youthful generations who have grown up in the midst of massive technological advancements.  
Yes, cultural identity is possible even in the global culture. Social media, street journalism, and global transnational businesses all play a role in catalysing new global culture formation processes. Globalization fosters cultural identity by creating a feeling of belonging. People are becoming increasingly worried about the uniqueness and particularity of their own culture in the current age of globalisation. Thus, even in a global society, cultural identity is conceivable.  
  

#### c. Why can moving from one side of the world to the other be an economic challenge for newcomers?

Moving from one side of the world to the other can be an economic challenge for newcomers because integration process needs time; starting from learning the language, integrating culturally, integration into the labour market.  
  

#### d. How can immigrants foster economy at different magnitudes of scale in their new homes?

Immigrants can foster economy at different magnitudes of scale in their new homes by starting their own companies or businesses.  
  

#### e. Can immigrants fully integrate in the new societies? Give reasons.

No, immigrants can’t fully integrate in the new societies because the society does not open its doors fully to allow them to integrate.  
  

#### f. Mention any two benefits of integrating into a new society for the immigrants.

– Migrants will have equal learning, engagement, and self-esteem possibilities.  
– The receiving nations can improve their economic prospects by integrating immigrants.  
  

#### g. Why should the immigrants run in concordance with global culture?

The immigrants should run in concordance with global culture because the cost of isolation and non-convergence is higher and more damaging than integration.  
  

### Critical Thinking

  

#### a. Do you think that the local is globalized and the global is localized? Can there be the global culture as well? Give examples.

Globalization is the process of increasing local businesses around the world whereas Localization is the adaptation of a resource or product to fit the demands of one specific culture or locale. I think local is globalized because local products, media, culture are now accessible all over the world. Similarly, the global is also localized because local people can benefit from the global product and the local culture. There can be a global culture among local peoples that is a by-product of globalization. People can feel world culture in different fields like education, sports, music, food, fashion, language, etc. Thus I think that local is globalized and the global is localized.  
  
Yes there can be the global culture as well. Some examples are listed below:  
1\. Creating a more homogenous world.  
2\. Attempt to promote a Western lifestyle and possibly Americanize the world.  
3\. Trend that will eventually make all of human experience and customs the same since all cultures are coming together into one.  
  

#### b. What is globalization? Discuss the effects of globalization on traditional cultures.

Globalization can be defined as the increased interconnectedness and interdependence of peoples and countries. It is generally understood to include two inter-related elements: the opening of international borders to increasingly fast flows of goods, services, finance, people and ideas; and the changes in institutions and policies at national and international levels that facilitate or promote such flows.  
  
A nation’s culture changes with economic development. One key fact is that people leave villages and agriculture for cities. This itself changes culture – agricultural festivals decline. In Nepal, there used to be bullock cart – but then tractors came in. People used to write letters – and then the phone came. When texting rose with smartphones, voice calls declined, and new cultural etiquette came up. When electricity came in, evening life changed. When air-conditioning came in, people stopped sitting on their stoops. When families had one TV, the family (and neighbors) sat around it. Now, everyone has their own screen.  
  
So, the answer is that technology, economic development and foreign cultures do change local behavior – but core elements of the culture may remain. So, we cannot say that globalization changes everything. But, some changes are inevitable.  
  

#### c. Discuss the impacts of globalization on the process and progress of education in Nepal.

Globalization of Education is the integration and application of similar education systems and knowledge throughout the world across the borders, improving the quality & deployment of education worldwide. The impacts of globalization on the process and progress of education in Nepal are as follows:  
  
1\. Globalization has improved the quality of education in Nepal. Due to globalization, Nepal got the opportunity to witness the best education systems worldwide and thus could replicate it.  
  
2\. New methods of learning such as e-learning, blended learning was quickly adopted by due to globalization.  
  
3\. Due to globalization, foreign universities were established in Nepal. These universities helped many students in getting a high-quality education. Moreover, foreign investments in the education sector also helped in improving the facilities and infrastructure.  
  
4\. Education should develop empathy and understanding in students. Globalization has enabled students to develop an understanding of other cultures, which is like a practical education.  
  
5\. Globalization made many people aware of human rights and the loopholes in the governance.  
  
Thus, Globalization has helped Nepal in improving it’s education systems and literacy rate. However, not everyone could benefit from the impact of globalization on education. Education inequalities must be bridged between Rich-poor & urban-rural areas so that everyone can utilize the opportunities created by globalization.  
  

### Writing

  

#### Write a news story to be published in a newspaper about a local festival/fair you have witnessed.

Dashain – Then and Now

**KATHMANDU, OCTOBER 26**  
  
Time brings change, and while that is mostly good and perhaps the only way forward, we wish something would remain the same.  
  
Dashain seems to evoke that sentiment.  
  
There was a time when Dashain was a highly anticipated festival, and while the fun is still there, the vibe isn’t the same. The memories of Dashain were more heart warming than how we celebrate the festival today.  
  
Dashain holidays were all about meeting cousins, playing games and eating a lot of meat. While we still do that, the charm and excitement have largely been lost because it’s not a new thing for today’s generation.  
  
Today it’s more of responsibility than fun.  
  
Before, eating rice and meat was a very big thing for the poor and used to happen only during the time of Dashain.  
  
We used to fly kites and try to cut the threads of those from our neighbourhood. We would run after the kites as they gently fell on the ground The first signs of Dashain were the kites appearing in the sky. Kite flying and also playing on the swings were the most important and fun activities of this festival.  
  
While we were kids, we used to sit in the lap of the elders on a large swing as we were scared. But now we can clearly see the change. It is no longer a big deal to have food on the table and we rarely see kites flying in the sky or the swings swaying in the fields.  
  
We used to get new clothes once in a year during the time of Dashain, but now we buy clothes every month. There is now no difference between the normal days and Dashain. Before Dashin created memories, but now it’s more like capturing the moments. Dashain is more for the photos. It’s not a bad thing, but only giving priority to photos to show off and not enjoy the moment is not good .  
  
Before, so much value was given to blessings bestowed to us by our elders, but today children are only interested in the money that is given as gifts. It’s not that today’s generation has completely forgotten the importance of Dashain. But slowly, but surely the charm of the biggest Hindu festival is waning. The vibes and that feel are going down.  
  
Lastly, change comes with time, and we should move accordingly, but we should never forgot our identity, our traditions and festivals, which create great memories.  
  

### Grammar

  

#### B. Complete each sentence using what/how/where/whether + one of these verbs:

a. Do you know **how to get** to Rama’s house?  
  
b. I don’t know **whether to apply** for the job or not.  
  
c. Have you decided **where to go** for your picnic?  
  
d. Can you show me **how to use** this camera?  
  
e. Ask Hari. He’ll tell you **how to ride** a bicycle.  
  
f. I was really astonished. I didn’t know **how to ride** on the horse.  
  

#### C. Paraphrase the following sentences using the verb in brackets as in the example.

  

#### a. Mahesh forgets to close the windows. (tend)

Mahesh tends to forget closing the windows  
  

#### b. Your car has broken down. (appear)

Your car appears to have broken down.  
  

#### c. Ashika is worried about her exam. (seem)

Ashika seems to be worried about her exam.  
  

#### d. They have developed the theory. (claim)

They claim to have developed the theory.  
  

#### e. He’s enjoying his new job. (pretend)

He pretends to be enjoying his new job.