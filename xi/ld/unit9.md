## Unit 9
# Democracy and Human Rights
### Ways with words

  

#### A. Find the words in the text that mean the same as the following. The first letters are given.

a. the system that completely separated black people from white people (**apartheid**)  
  
b. formal objection (**protest**)  
  
c. an ethnical group of people (**tribe**)  
  
d. an act undertaken to achieve a set goal (**campaign**)  
  
e. the formal beginning of any movement (**inauguration**)  
  
f. a strong feeling of excitement and happiness (**exhilaration**)  
  
g. being set free from legal, social, or political restrictions (**emancipation**)  
  

#### B. Find these words in a dictionary and write their meanings as they are used in the text.

  

#### a. liberty

the state of being free within society from oppressive restrictions imposed by authority on one’s way of life, behaviour, or political views.  
  

#### b. conflict

a serious disagreement or argument, typically a protracted one.  
  

#### c. ideology

a system of ideas and ideals, especially one which forms the basis of economic or political theory and policy.  
  

#### d. oppression

prolonged cruel or unjust treatment or exercise of authority.  
  

#### e. privilege

a special right, advantage, or immunity granted or available only to a particular person or group.  
  

#### f. dignity

the state or quality of being worthy of honour or respect.  
  

#### g. surrender

stop resisting to an enemy or opponent and submit to their authority.  
  

#### h. reconciliation

the restoration of friendly relations.  
  

#### C. The ‘d’ or ‘ed’ in the following verbs have different pronunciation. Put these verbs in the correct box.

_\[asked, killed, missed, ended, decided, washed, visited, lasted, watched, picked, smiled, fixed, walked, blessed, brushed, stopped, wanted, reached, laughed, enjoyed\]_  
  

#### /t/

asked, missed, washed, watched, picked, fixed, walked, blessed (v), brushed, stopped, reached, laughed  
  

#### /d/

killed, smiled  
  

#### /ɪd/

ended, decided, visited, lasted, blessed (adj), wanted, enjoyed  
  

#### D. Put these nouns into the correct box according to the pronunciation of the plural suffix: s/es.

_\[cats, dogs, horses, houses, books, roofs, boys, rooms, girls, noises, shops, trees, pages, babies, benches, classes\]_  
  

#### /s/

cats, books, roofs, shops  
  

#### /z/

dogs, boys, rooms, girls, trees,  
  

#### /ɪz/

horses, houses, noises, pages, babies, benches, classes  
  


### Comprehension

  

#### Answer these questions.

  

#### a. What were the restrictions imposed on the Blacks in South Africa?

The restrictions imposed on the Blacks in South Africa were Racial segregation, and political and economic discrimination.  
  

#### b. Why was Mandela arrested?

Mandela was arrested because he was charged with organizing an armed wing of the African National Congress (ANC).  
  

#### c. How did he describe racism and racial oppression?

He described racism and racial oppression as the pernicious ideology and practice.  
  

#### d. Why did he thank all the international guests?

He thanked all the international guests for coming to take possession about peace and justice with the people of his country.  
  

#### e. Why did he think that people in his country had achieved political emancipation?

He thought that people in his country had achieved political emancipation to liberate themselves from the continuing bondage of poverty, deprivation, suffering, gender and other discrimination.  
  

#### f. What is the main point of Mandela’s speech?

The main point of Mandela’s speech is that people of South Africa should build a society where there will be justice, peace and equality for all peoples.  
  
  

### Critical thinking

  

#### a. What does Mandela mean when he says – a rainbow nation at peace with itself and the world?

When Mandela says – a rainbow nation at peace with itself and the world he means that South Africans should build a society where both black and white will be able to walk, talk with each other without any fear. We know that rainbow is the combination of seven different colors. So by saying a rainbow nation he is trying to convey us a message that whenever all the peoples unite as like the colors in the rainbow then only there will be peace.  
  
In South Africans cultures, the rainbow symbolizes hope and bright future. South Africa is referred to a Rainbow Nation to describe the unity of various cultural, racial or ethnic groups in the country. During the segregation period, peoples were divided on the basis of skin color. Mandela wants them to live and work together in unity like the colors in a rainbow. He wished they could forget the ferocious and brutal past and put their hand in hand with each other and maintain a peaceful nation.  
  

#### b. Mandela should have avenged those who imprisoned him for such a long period. Instead, he followed the path of reconciliation. Why do you think he did so?

If Mandela has avenged those ho had prisoned him then he won’t be able to reach his destination. Mandela led his country wisely and promoted national reconciliation. He was a man of true words. He fought for his own people for a long period and was also imprisoned when he raised his voice against the authorities. However, after coming out of jail he did not avenge those who had put him in trouble. Because he believed that avenge and enmity cause disorder among people and by doing so he would lose the support of the majority.  
  

#### c. Why and how have societies struggled with segregation in the world? Do you find any evidence of segregation in your society? Discuss.

Segregation is the separation or isolation of a race, class, or ethnic group by enforced or voluntary residence in a restricted area, by barriers to social intercourse or separate educational facilities or by other discriminatory means. Many people of the world are struggling with social separation problem in the world.Racial segregation is one of many types of segregation, which can range from deliberate and systematic persecution through more subtle types of discrimination to self-imposed separation. Cast segregation is one of the most common forms of segregation in the Hindu culture. Any types of segregation can lead to chaos and voilence.  
  
Yes I have found the evidneces of segregation in my society. Although, the Nepalese Constitution Law doesn’t allow any segregation in terms of religion, caste and ethnicity but still it is prevalent in our society. However, since the unification of Nepal in the 18th century, Nepal’s various non-Hindu ethnic nationalities and tribes, previously called “Matwalis” (alcohol-drinkers) and now termed as “Adivasi/Janajati” (indigenous/nationalities), have been incorporated within the caste hierarchy to varying degrees of success. Despite the forceful integration by the state into the pan-Hindu social structure, the traditionally non-Hindu groups and tribes do not necessarily adhere to the customs and practices of the caste segregation.  
  
  

### Writing

  

#### a. Nepal has topsy-turvy political history. Many changes have been observed in different times. Write a short biography of any Nepali freedom fighter incorporating the changes brought under his/her leadership.

Ganesh Man Singh

Ganesh Man Singh was born on November 9, 1915 and was commander of the popular 1990 Janaandolan. Singh was also one of the main leaders of the Nepali Congress movement of 1951, which overthrew the Rana regime. More than anything else, Ganesh Man was a rebel and a freedom fighter. He was a popular hero and his exploits are legendary. He was a man of small proportions but charismatic, fearless, and physically very strong. He was never afraid of taking risks in his political career. He was a straightforward and frank leader in dealing with all.  
  
Ganesh Man was a rare breed amongst Nepali politicians—a man of integrity. He demonstrated his greatness through his leadership, rising above petty personal interests. Recognising his outstanding contribution to the field of democracy and human rights, Ganesh Man was honored by the United Nations with the ‘Human Rights Award’ in 1993. He was the first statesman from South Asia to receive this prestigious award. He also received the ‘United States Peace Run Prize’ in 1990 for his contribution to peace in Nepal and the world. He was also decorated with the ‘U Thant Peace Award’. He has become immortalised as a great leader that the nation and its citizens, without any political barriers, feel proud of. No other national leader has been able to fill the political vacuum after his death.  
  
  

### Grammar

  

#### B. Complete the sentences with should or shouldn’t. Use one of these verbs.

_\[drink, visit, leave, roam, quit\]_  
  
a. You have really done a wonderful job. I recommend you **shouldn’t leave** it.  
  
b. That’s a very dangerous area. Tourists **shouldn’t visit** there.  
  
c. I’m going to be late. Do you think I **should leave** now?  
  
d. Children **shouldn’t drink** sugary drinks. It’s not very healthy.  
  
e. I have lots of homework. I **shouldn’t roam** here and there today.  
  

#### C. Put in had better or should.

a. I think you **should** learn English to enroll a university course.  
  
b. It’s a great film. You **should** go and see it.  
  
c. I have to meet my friend in ten minutes. I **had better** go now or I’ll be late.  
  
d. These biscuits are delicious. You **should** try one.  
  
e. We **had better** get to the airport by 2 pm or else we may miss the flight.  
  
f. When people are driving, they **should** keep their eyes on the road.  
  
g. I **should** get up early tomorrow. I’ve got a lot to do.