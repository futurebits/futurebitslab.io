## Unit 6
# Health and Exercise
### Ways with words

  

#### A. Match the words with their meanings.

a. exhausted – tired  
b. apparent – clear  
c. vista – vision  
d. undulating – wavy  
e. destined – predetermined  
f. stuffy – suffocating, airless  
g. restrain – prevent, hinder  
  

#### B. Write the meaning and word class of the following words. Then use them in sentences of your own.

  
**1\. implore (verb):** beg someone earnestly or desperately to do something.  
I regretfully and respectfully implore you to help us.  
  
**2\. despair (noun):** the complete loss or absence of hop  
A few positive words can turn despair into hope.  
  
**3\. beseech (verb):** ask (someone) urgently and fervently to do something  
I will fall at his feet and beseech him.  
  
**4\. eloquent (adjective):** able to express a feeling  
He made an eloquent plea for peace.  
  
**5\. whirl (noun):** a state of confusion  
My head is starting to whirl.  
  
**6\. egoism (noun):** the fact of being excessively conceited in oneself  
His egoism stopped him from loving anyone but himself.  
  
**7\. agony (noun):** extreme physical or mental suffering  
She screamed again in agony as pain seared through her shoulder.  
  
**8\. delirious (adjective):** in a state of wild excitement or ecstasy  
The patient may be somewhat confused and delirious.  
  
**9\. delusive (adjective):** giving a false or misleading impression  
They were only coquetting a little with us, or bent on kindling delusive hopes.  
  
**10\. compensate (verb):** reduce the bad effect of loss  
His enthusiasm compensates for his lack of skill.  
  
**11\. mortgage (noun):** convey a property to a creditor as security on a loan  
She didn’t have enough money to pay the mortgage.  
  
**12\. brood (noun):** a large family of children  
He grew up among a lively brood of brothers and sisters.  
  
**13\. prelude (noun):** an action or event serving as an introduction to something more important  
A ceasefire had been agreed as a prelude to full peace negotiations.  
  

#### D. Choose the correct word.

  
a. Sarita was shocking/**shocked** to hear about earthquake.  
  
b. I think that rainy days in winter are **depressing**/depressed.  
  
c. The football match was very **exciting**/excited. I enjoyed it.  
  
d. The meals at Delight Café are **satisfying**/satisfied.  
  
e. I’ve got nothing to do. I’m boring/**bored**.  
  
f. Tanka is very good at telling funny stories. He can be very **amusing**/amused.  
  
g. The teacher’s explanation was **confusing**/confused. Most of the students didn’t understand it.  
  
h. He is such a **boring**/bored person. He never wants to go out.  
  
i. I will be surprising/**surprised** if she does well in her test.  
  
j. Are you interesting/**interested** in politics?  
  

#### E. Write the correct form of the adjective in the blanks as in the example.

  
a. They frustrate me but they don’t bore me. I never get **bored** when I study grammar.  
  
b. If teachers want to interest the students, they must use **interesting** materials.  
  
c. Certain stories interest almost everybody. For example, most students are **interested** in fairy tales.  
  
d. Certain things frighten me, but I never get **frightened** when I speak English.  
  
e. If I get a good grade, that excites me. And if I get more than ninety percent, I am really **excited**.  
  


### Understanding the text

  

#### Answer these questions.

  

#### a. Who was Nellie? What did she use to dream of?

Nellie was the daughter of a landowner and general, a young and pretty girl. She used to dream of getting married.  
  

#### b. What was she doing with the looking glass?

She was staring into the looking glass with exhausted, half-closed eyes.  
  

#### c. Why did she go to the doctor on one winter night?

She went to the doctor one night because her husband was ill.  
  

#### d. What was Stepan Lukitch doing when she reached his bedroom?

Stepan Lukitch was lying on his bed when she reached his bedroom.  
  

#### e. Why was the doctor not ready to go to see her husband?

The doctor was not ready to go to see her husband because the doctor was exhausted and ill himself seeing the typhus patients for last three days.  
  

#### f. Why did Stepan Lukitch suggest Nellie to go to the Zemstvo doctor?

Stepan Lukitch suggested Nellie to go to the Zemstvo doctor because he was ill himself and Nellie needed immediate treatment.  
  

#### g. Nellie said, “Come, perform that heroic deed! Have pity on us!" What was that pity to be done?

The pity to be done was to treat Nellie’s husband.  
  

#### h. When Nellie said, "I must have fallen asleep." What does it mean?

It means that Nellie stopped was awaken and was ready for facing the realities of life.  
  

#### i. What is the main theme of the story?

The main theme of the story is devotion, loneliness, escape, fear, love, dedication, hope, defeat and independence.  
  

### Critical thinking

  

#### a. “The looking glass (mirror)” is used as a symbol in the story. What does it symbolise?

In “Looking Glass,” the mirror symbolizes Nellie’s central character traits: her longing to be married and her habit of escaping her dull life into her elaborate fantasies. Protagonist Nellie is introduced dreamily gazing into her handheld mirror before falling asleep and seeing her “destined one” as clearly as if she were awake.  
  

#### b. Chekhov employs the magic trick in the story, using a very elegant transition from reality to imagination to reality sequence. Discuss its relevance to life of young people.

Anton Chekhov’s short story “The Looking Glass” might be relevant to the life of young people due to its fairy-tale quality. It’s as if Nellie turns herself into a distressed princess; her husband becomes a kind of afflicted prince charming. It’s interesting that her husband doesn’t have a name. The lack of a name connects to other fairy tales for young readers in which the male love interest is never given a clear name. In Cinderella, for instance, the prince has no name.  
  
Apart from a connection to fairy tales intended for young people, it could be argued that Chekov’s short story underscores the ways in which young people tend to dramatize life. It’s not uncommon for books, movies, and TV shows to portray young people as histrionic, overemotional, and hyperbolic. Nellie might represent the ways in which young people tend to glamorize love, suffering, and other things that might not be so enchanting in reality. Since some young people are fortunate enough to have not had to personally experience devastating hardship, they might be more inclined to invent their own. If their imagined scene grows too scary, they can, like Nellie, wake up, and it’ll be done with.  
  
When discussing the relevance of “The Looking to Glass” to young people, it might be insightful to note that all people, whatever their age, tend to be susceptible to fantasy, exaggeration, and flights of fancy. There are probably just as many overdramatic books, movies, and TV shows for adults as there are for young people. Furthermore, the sensational, slanted nature of social media and news outlets suggests that adults travel back and forth between reality and fantasy more often than they might like to admit.  
  
  

### Grammar

  

#### B. Match the expressions with their functions.

  
**a.  
A: What do you want to take?  
B: I’ll have tea, please.**  
Deciding  
  
**b.  
A: Are you free this evening?  
B: No, I’m going to meet my uncle.**  
Expressing a prior plan  
  
**c. The day will be lovely tomorrow.**  
Predicting a future action  
  
**d. There is no cloud in the sky.  
It’s going to be a lovely day.**  
Predicting with evidence  
  
**e. Don’t worry. I won’t tell anyone.  
**Promising  
  
**f. I’ll take you to the movies if you like.  
**Offering  
  
**g. I’ll tell your parents what you did.  
**Threatening  
  

#### C. Choose the correct answer.

a.  
A: Are you busy this evening?  
B: Yes, I **am going to** the movies. (will go/am going to)  
  
b.  
A: Where are you going for holiday this summer?  
B: Not sure yet. Maybe I **will go** to Ilam. (will go/am going to)  
  
c. I think you **will** like this movie. (will /are going to)  
  
d. I can’t join you at the party, I **am going to** be away for two weeks. (will /am going to)  
  
e. This exercise looks really hard. I **will** help you. (will/am going to)  
  
f.  
A: Hello. Can I speak to Sima, please?  
B: Just a minute. I **will** get her. (will/am going to)  
  
g. Perhaps she **will** pass the exam. (will/is going to)  
  
h. ‘I haven’t got my phone.’ ‘That’s OK. I **will** lend you mine.’ (will/am going to)  
  

#### D. Complete the sentences using will or be going to with the verbs.

a.  
Hari: Did you call Bina?  
Prem: Oh, I forgot. I **will call** her now. (call)  
  
b.  
Sunita: Have you got a ticket for the play?  
Hema: Yes, I **am going to watch** it on Saturday. (watch)  
  
c. ‘The alarm is ringing. It’s making an awful noise.’ ‘OK, I **will switch** it off.’ (switch)  
  
d. Do you think they **will like the** the presents we got for them? (like)  
  
e. ‘Lok is starting university tomorrow.’ ‘What **is he going to** study?’ (he/study)  
  
f. If I meet him, I **will tell** him the news. (tell)  
  
g. The phone is ringing. I **will answer** it. (answer)  
  
h. If you don’t stop bullying her, I **will tell** the teacher. (tell)