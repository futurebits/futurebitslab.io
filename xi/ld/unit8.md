## Unit 8
# Humour and Satire
### Ways with words

  

#### A. Match the following.

  
**transcendental** – spiritual, nonphysical or mystical  
**deplore** – to feel or express strong disapproval of (something)  
**absolve** – set free from blame, guilt, or responsibility; release  
**juju** – a charm or fetish used by some West African people  
**crude** – natural state  
**chronicle** – a written record of historical events  
  

#### B. Find the contextual meanings of the following words from the text and then use them in sentences of your own.

  
**a. condemn :** to express disapproval  
Don’t condemn him before you hear the evidence.  
  
**b. terror :** a feeling of extreme fear  
He lived in terror of being caught.  
  
**c. unbidden :** without being asked, invited or expected  
She shook her head to remove the unbidden thoughts.  
  
**d. persist** : to continue to exist  
The reporter persisted with his questioning.  
  
**e. devout :** having or showing strong religious feeling/ religious  
His grandfather is a devout Buddhist.  
  
**f. banish :** to make somebody/something go away  
The good wizard protected the temple with a spell to banish evil spirits.  
  
**g. creed :** a system of principles or religious beliefs  
Other countries have adopted this political creed enthusiastically.  
  
**h. hasten :** to make something happen more quickly  
The edge in his voice made her hasten her step.  
  
**i. sober :** serious and sensible  
He was as sober as a judge.  
  
**j. scorn :** feel or express contempt or disdain for  
He felt scorn for his working-class parents.  
  
**k. yearning :** a strong and emotional desire  
They had a deep yearning for their homeland.  
  
**l. aloof :** not friendly/ disinterested in other people  
His aloof response made her look up.  
  
**m. swarthy :** having dark skin  
My friend is skinny, with a swarthy complexion.  
  
**n. humbler :** having or showing a modest or low estimate of one’s importance  
The leader of a third troop took a humbler tone.  
  

#### C. Trace the origins of each of the following words finding such explanations in a dictionary or the Internet. Then make sentences by using each word.

  
**Minatory**: From Latin minatorius  
**Meaning** = threating  
The hate group left a **minatory** threat in the form of a burning cross on the couple’s lawn.  
  
**Placated**: From Latin Placast  
**Meaning** = make somebody less angry  
She was well **placated** when he did go on to choose Marley.  
  
**Cajoled**: From French Cajoler  
**Meaning** = pursuit somebody gently  
I managed to **cajole** her out of leaving too early.  
  
**Antedates**: From Latin ante  
**Meaning** = predate; put earlier date; occur earlier than something  
This event **antedates** the discovery of America by several centuries.  
  
**Proliferated**: From Latin prolifer  
Meaning = increase greatly; give birth  
Bead stores seem to have **proliferated** the American landscape.  
  
**Philter**: From Greek philtron  
Meaning = magical tonic; love potion  
He’s just full of magic **philter**.  
  

#### D. List any five words found in an English dictionary beginning with the prefix ‘super-.’ What common meaning do all of these words share? How do the words in your list change meaning if you eliminate the prefix?

  
**a.**  
**Superimpose** = lay over; place on the top  
**Impose** = enforce something; insist on something  
  
**b.**  
**Superman** = exceptional man; man with strength power  
**Man** = adult male human; person  
  
**c.**  
**Supernatural** = super human; beyond the nature  
**Natural** = related to nature; produced by nature  
  
**d.**  
**Supernumerary** = extra; exceeding; many of  
**Numerary** = number; of numbers  
  
**e.**  
**Superstar** = above other artist; more popular artist  
**Star** = mass of gas in space; popular artist  
  



### Comprehension

  

#### Answer these questions.

  

#### a. According to the author, what are the four types of superstition?

According to the author, the four types of superstition are Vain Observances, Divination, Idolatry and Improper Worship of the True God.  
  

#### b. Which language is the word ‘superstition’ derived from? What does it mean?

The word ‘superstition’ is derived from the Latin language ‘supersisto’. It means to stand in terror of the Deity.  
  

#### c. How do psychologists understand superstition?

Psychologists understand superstition as a compulsion that neurosis does not banish.  
  

#### d. How does superstition differ from religion?

The difference between Religion and Superstition is that they differ in the belief of a community. Religion is confined to worshipping Gods and devoting them. Superstition is just a belief that relies on rituals performed over them.  
  

#### e. What is the belief of some people in Middle Europe about sneezing?

Some people of Middle Europe believe that when a person sneezes, his/her soul is absent from body for a moment and they hasten to bless him.  
  

#### f. In the author’s view, why are people so fascinated about superstition?

In the author’s view, people are so fascinated about superstition because people’s desire to know their fate, and to have some hand in deciding it.  
  
  

### Critical Thinking

  

#### a. What is the key takeaway of this essay? Do you think that this essay is satirical? Why?

The essay aims to convey the message that we are following superstition knowingly and unknowingly. Every individual no matter how logical or rational he/she is, carries superstitions. The essayist believes that many superstitions are widespread and too old that must have been risen from a depth of human mind that is indifferent to any race or creed.  
  
The essay is satirical in the sense that it satirizes to those educated and uneducated people who are the victim of it. Throwing salt over the left shoulder after spilling it or avoiding walking under a ladder, resolving a matter related to university affairs by consulting the I Ching, placing jujus, lucky coins and other bringers of luck on the desks of the candidates in an examination hall, etc. are some of the superstitious activities he satires on.  
  

#### b. Can education bring change in the belief of superstition? Present your arguments to support your answer.

Education may or may not bring change in the belief of superstition. Education can make people less superstitious. At least educated people are more likely to send the sick to the hospital, than to call priests to cast out demons in order to cure the sick. Education provides an understanding of the diseases, the cause and the cure of the diseases. Superstitions are due to fear of the unknown. Once the unknown becomes known, there is no longer any superstition. Education has the potential to change superstitious beliefs. It enhances thinking abilities and intellect. People who are educated are less prone to be superstitious, and they may educate others about it too.  
  
Now the question is can education eradicate superstition? It is only possible when human beings gain complete knowledge of everything, then education is able to eradicate superstition. For the present, education does reduce a lot of superstitious beliefs around the world.  
  

### Writing

  

#### a. Write an essay on superstitions that exists in your community in about 250 words.

Superstitions in My Community

Superstitions are as old as man. The earliest men who had no scientific knowledge fell on easy prey to superstition. Thus, illiteracy and lack of knowledge and capacity to reason out are the hotbeds which generate and perpetuate superstition.  
  
Mahatma Buddha was probably the first great man to expound and explain the value and significance of reason which eliminated superstition altogether. He emphasized that everything should be thoroughly studied, judged and tested before being believed. Later, many other great men like Guru Nanak and Kabir exhorted the people to shun superstitions.  
  
Many people may believe that faith is also a form of superstition. But, as we can see if we think deeply, there is a difference. Faith is a positive factor whereas superstition is a negative factor.  
  
Earlier, superstition was rampant in villages. The belief in ghosts was common. It was believed that these ghosts operated at night and that they were visible to some people and invisible to others. Taking advantage of this many clever men burned into tan tricks and controllers of ghosts. They cheated the gullible villagers. Unfortunately, even at present, such clever men are at work.  
  
There are many kinds of superstitions which are observed by common people. The throbbing of eyes, a cat crossing our way, coming across a Brahmin-all these are believed to be inauspicious. The cawing of a crow indicated the possibility of a guest visiting our house that day. Similarly, if we come across a sweeper early in the morning, it is considered to be auspicious.  
  
We should try to develop a scientific spirit of mind and judge everything on the basis of reason.  
  

#### b. “Superstition is prevalent in every walk of life.” Argue for or against this statement.

Yes it is true that superstition is prevalent in every walk of life. The man started to believe in superstitions when he got a feeling that humans are at the mercy of natural elements. Similarly, some superstitions were also created because of social values. As a result, people worship forces of nature for a long time.  
  
The Greeks and Pagans used to worship elements of nature in the form of Gods and Goddesses. Same is the case with Nepalese tradition. People continue to worship the sun, moon, stars, planets, plants and more believing these things have the power to influence our lives. You might have heard ‘it is because of the impact of some evil star’ and more when a disease overtakes or disaster strikes. Even the people in the West have been believing in them. You will find instances in Shakespeare’s plays where he includes things like omens, witches and more.  
  
In fact, ever since a long time till date, people still consider the number 13 to be unlucky. Similarly, salt spilling over the dinner table is also an ill-omen. In Nepal, people consider the black cat crossing the way to be unlucky. Similar is the case of an owl hooting or a dog wailing.  
  
If we look at it closely, there is no logic as such behind the beliefs in superstitions. However, they have grown age-old and despite all the scientific advancement, they are not going anywhere soon and hence they are prevalent in every walk of life.  
  

### Grammar

  

#### B. Put the verb into the correct form, present simple or present continuous.

  
a. Nisha **speaks** (speak) English very well.  
  
b. Hurry up! We **are waiting** (wait) for you.  
  
c. Excuse me! **Do you speak** (you/speak) English?  
  
d. She **is having** (have) a shower in bathroom.  
  
e. How often **do you read** (you/read) a newspaper?  
  
f. I’m sorry, I **don’t understand** (not/understand). Can you speak more slowly?  
  
g. You can turn off the radio. I **am not listening** (not/listen) to it.  
  
h. I usually **get up** (get up) at 5 o’clock every morning.  
  
i. Look! The river **is flowing** (flow) very fast.  
  
j. Amrita **does not seem** (not/seem) very happy at the moment.  
  

#### C. Are the underlined verbs in the correct form? Correct them where necessary.

  

#### a. Water boils at 100° C.

Water boils at 100° C.  
  

#### b. The water boils. Can you turn it off?

The water is boiling. Can you turn it off?  
  

#### c. I must go now. It gets late.

I must go now. It is geting late.  
  

#### d. This sauce’s tasting really good.

This sauce tastes really good.  
  

#### e. I’m thinking this is your key.

I think this is your key.  
  

#### f. Are you believing in God?

Do you believe in God?  
  

#### g. I usually go to school on foot.

I usually go to school on foot.  
  

#### h. Look! That man tries to open the door of her car.

Look! That man is trying to open the door of her car.  
  

#### i. The moon is going round the earth.

The moon goes round the earth.  
  

#### j. I’m getting hungry. Let’s go and eat.

I’m getting hungry. Let’s go and eat.