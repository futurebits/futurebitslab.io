## Unit 13
# Career and Entrepreneurship
### Ways with words

  

#### A. Find the words from the text which mean the following. The first letter has been given.

  
a. making you feel physically relaxed; pleasant to wear, sit on, etc. (**comfortable**)  
  
b. having a degree from the university (**graduated**)  
  
c. without being connected with or influenced by something or by each other (**independently**)  
  
d. the introduction of new things, ideas or ways of doing something (**innovation**)  
  
e. familiar with something, having read, seen or experienced it (**acquainted**)  
  
f. a person who makes money by starting or running businesses, especially when this involves taking financial risks (**entrepreneur**)  
  
g. an opportunity or a place for somebody to express their opinions publicly or make progress in a particular area (**platform**)  
  
h. to use something, especially for a practical purpose (**utilize**)  
  

#### B.Write the plural forms of the following nouns.

a. calf : calves  
b. basis : bases  
c. sheaf : sheaves  
d. cactus : cacti / cactuses  
e. louse : lice  
f. crisis : crises  
g. person : people / persons  
h. formula : formulas  
i. ox: oxen  
j. fungus : fungi  
k. goose : geese  
l. oasis : oases  
m. analysis : analyses  
n. curriculum : curricula  
o. appendix : appendixes  
p. fungus : fungi  
  

 

### Comprehension

  

#### a. How did Mahabir Pun begin his lifelong journey in Nepal after coming back to Nepal from the US?

Mahabir Pun began his lifelong journey by staying in his home village, Nangi of Myagdi district to help villagers start a high school as a volunteer teacher after coming back to Nepal from the US.  
  

#### b. Why didn’t he want to work for other companies in the US or in Nepal?

He didn’t want to work for other companies in the US or in Nepal to have “so called” better living, work independently and do something on his own for the benefits of human beings.  
  

#### c. Why did he involve in innovation?

He involved in innovation to find solution for a problem.  
  

#### d. What are the income-generating programmes that he helped to start?

The income-generating programmes that he helped to start are Yak farming, camping ground for the trekkers, cheese making, jam making, paper making, bee keeping, vegetable farming, handicraft making, fish farming etc.  
  

#### e. Whose support is Pun getting to proceed with his campaign?

Pun is getting the support of communities to proceed with his campaign.  
  

#### f. How have the developed countries become prosperous?

The developed countries have become prosperous because they have given research and innovation topmost priority and they are investing huge amount of money for it.  
  

#### g. How can we keep the most talented and innovative people in Nepal?

We can keep the most talented and innovative people in Nepal by creating sound solid atmosphere and support system so that they could work here peacefully.  
  

### Critical thinking

  

#### a. Pun says, “We do not have culture of innovation that encourages young people to be innovative and creative”. Do you agree with him? Explain.

An innovation culture is part of the corporate culture and determines how much innovation is supported and promoted by management and employees. The culture of innovation is therefore the framework for every innovation activity. If the culture is not positive, it can hinder innovation, even if the idea and the innovation team are still perfect. It is thus a decisive factor in the success of innovation.  
  
Our country Nepal is said to be a developing country but development process is very slow here. Our education system is still based on paper and classrooms, and it hasn’t changed much over the years. There is no opportunities for young people who are likely to do something new since we do not have culture of encouraging them. Innovative and creative thinking are not encouraged in our educational system. Every year, thousands of students leave the country in pursuit of a higher education because of this. A research-based education is usually given the highest emphasis in industrialised nations. To do this, they invest a great deal of money and effort. Our culture discourages young people from taking risks, therefore there are no possibilities for them in Nepal.  
  

#### b. What qualities does an individual need to become an entrepreneur?

Entrepreneurs are innovative pioneers who venture opportunities and create new market at home and aboard. Becoming an entrepreneur requires more than just a creative idea. All entrepreneurs share commonalities in their attributes, abilities and qualities that empower them to beat the chances and pursue their objectives.  
  
Successful entrepreneurs need to see success. Many of the top businessmen around have seen a product and the associated success that could possibly come with it. If he/she don’t plan extensively about their new idea or concept, without thinking ahead, they might encounter unforeseen difficulties with management or cashflow. Perseverance is a key in becoming a full-time entrepreneur. A true entrepreneur will be able to prevent the feeling of being burnt out, if they know what their goals are, and if they love the task of always having to meet deadlines. Discipline is also the key in this field, as an entrepreneur must work hard in order to achieve.  
  
Entrepreneurs also need to manage their time properly. There’s no point in spending a whole day over a task, when in reality it will be cheaper and more effective to hire someone to help you with a certain task, thus enabling you to produce more profit. A successful entrepreneur must have an air or a grace about him that will demand respect from other individuals.  
  
Successful entrepreneurs are never secure in their field, although this may be the same with other bog-standard jobs, however the rewards that can be obtained from having the mentioned attributes can be really substantial. By working hard and never giving up, an individual is bound to become a successful entrepreneur.  
  

### Grammar

  

#### E. Use the correct tense of the verbs in brackets.

a. I don’t know where Muna is. Have **you seen** (you/see) her?  
  
b. Janak **wasn’t** (not/be) very well last week.  
  
c. Last night I **lost** (lose) my keys. So I stayed in my friend’s home.  
  
d. I **have lost** (lose) my keys. Can you help me look for them?  
  
e. I **have known** (know) Jamuna for three years. We still meet once a month.  
  
f. She **lived** (live) in Sikkim when she was a child.  
  
g.  
A: What’s wrong?  
B: I **have broken** (break) a glass.  
  
h.  
A: When **did you arrive** (you/arrive)?  
B: At 10 pm last night.  
  
i. How long **have you known** (you/know) Sarmila for?  
  
j. This is the first time I **have driven** (drive) a car.  
  

#### F. Use been or gone.

a. I’ve never **been** to Japan.  
  
b. Kalpana has **gone** to Korea. She may come back next year.  
  
c.  
A: Where’s Rachana?  
B: She has **gone** to the shops.  
  
d. Harina was here earlier but I think she has **gone** now.  
  
e. Have you ever **been** to London?