## Unit 10
# Home Life and Family Relationship
### Ways with words

  

#### A. Fill in the blanks with an appropriate words.

  
a. All my efforts to convince her for the tour were **futile**.  
  
b. The lost traveller **huddled up** under a shelter made of branches and leaves.  
  
c. Her rude behaviour was the main cause of **irritation** for him.  
  
d. She moved **awkwardly** in the room, thinking that I was watching.  
  
e. She is **mumbling** something, but I can’t hear her.  
  
f. When I met my sister after a long time, she talked to me in a **feeble** voice.  
  

#### B. Tick (√) the correct words that are similar in meaning to the underlined words.

  

#### a. Rupa studied Science reluctantly due to her father’s pressure.

ii. unwillingly  
  

#### b. She moved her fingers exasperatedly through her hair.

iv. annoyingly  
  

#### c. My grandmother is over eighty. She lifted her hands feebly.

iii. weakly  
  

#### d. She is wearing a tattered shawl.

ii. torn  
  

#### e. I have to work at two jobs to make ends meet in this expensive city.

iv. earn just enough money  
  

#### D. Syllable

  

#### c. Consult an English dictionary and find the number of syllables in the following words.

  
**bury** : bur-y  
2 syllables  
  
**development** : de-vel-op-ment  
4 syllables  
  
**syllabic** : syl-lab-ic  
3 syllables  
  
**Vowel** : vow-el  
2 syllables  
  
**education** : ed-u-ca-tion  
4 syllables  
  
**discipline** : dis-ci-pline  
3 syllables  
  
**examination** : ex-am-i-na-tion  
5 syllables  
  
**children** : chil-dren  
2 syllables  
  
**separately** : sep-a-rate-ly  
4 syllables  
  
**pronunciation** : pro-nun-cia-tion  
5 syllables  
  


### Comprehension

  

#### Answer the following questions.

  

#### a. Why didn’t the mother recognise her son Gopi?

The mother didn’t recognize her son Gopi because she was eighty years old and lost her memory.  
  

#### b. Why is the sister living with her mother?

The sister is living with her mother because she is a widow and there is nobody to take care of her mother.  
  

#### c. What is the sister’s attitude towards her brother?

She is serious towards her brother’s behaviour.  
  

#### d. Does the son love his mother very much? How do you know that?

Yes, the son love his mother very much. He shows his love by bending down and bringing his face close to his mother’s wrinkled cheeks.  
  

#### e. What does the mother actually need: a blanket or the warmth of her son’s love?

The mother actually needs the warmth of her son’s love. Here, the blanket symbolizes love and care.  
  

#### f. What does the phrase ‘the tattered blanket’ mean? Is it only the blanket that is tattered?

The phrase ‘the tattered blanket’ means the hurted feelings of his mother.  
  

#### g. The son says, ‘Amma can’t remember who I am’. Do you think that he remembered his mother? Why didn’t he answer when his sister asked, ‘Do you remember your Amma?’

He didn’t answer her question because he can’t do so as it had been a year since he wrote to his mother and more than five years since he saw her.  
  
  

### Critical thinking

  

#### a. What can be the expectations of the parents from their offspring at the old age?

Every human is different. Often times what one desires the other does not even see fit. Our parents are also one among the same crowd, like us. There must be some expectations they have from their children, our work is to understand and fulfill them.  
  
When people get old, they get sensitive and even the slightest of things hurt them. Same goes for the elderly parent. They have lived a full life, worked, earned respect, and fulfilled their responsibilities. So when they are old, they want us not to forget this and treat them with dignity and respect.  
  
After retirement the elderly people do not find purpose and meaning in their day, most of the time they do not have routine. So most of them stay usually alone at home. Often times we hear them complain that no one spends time with them, they miss company, they have nothing to do, no one to talk to, etc.  
  
Your father was the one taking care of everything before he retired, your mother was so busy all her life looking after her family and now because they are old they have near to nothing to do during their day. They want their children to involve them in conversations, decisions or any other big and small thing.  
  
These were just some common things that the elderly parents miss and want from you or want their children to know. But again every person and their wants are different, so we do the things according to our parent’s desire.  
  

#### b. Some children who live in a distance tend to be indifferent to the feelings of their parents. Why do you think they are like that? Discuss.

There are various reasons for children to live in a distance from their parents. The reason may be study, job, business and so on. For achieving their goal and fulfilling their self desires, they have to invest more time and efforts and hence their realtion with parents becomes weaker due to of their busy schedule. When children live in a distance, they gradually keep away relation from their parents. Finally, it creates a gap or distance between parents and their children. In todays world childrens think that money and success are more important than the parents’ feelings. They do hard work for earning and saving but not making the parents happy.  
  
Some children begin a new life with their family far from their parents. Their wife and children become more important than their old-age parents. That is why they have no time to call them and visit their parents. This indifference not only degrades the relationship between the parents and their children but also makes the parents live a lonely and miserable life.  
  

#### c. A mother’s love is everlasting and indispensable. Justify.

There is nothing that can come close to the love that a mother feels for her children. Women are inherently good mothers. Till birth women carry their young and then continue their love & affection throughout their childhood and even into adulthood. Every Mother always makes sure that their children are safe and happy throughout their childhood. It is the love for their child that a mother feels that drives these feelings.  
  
After birth, a child finds his mom as the first friend who plays with him along with extra care and nourishment. Without any expectation, a mother keeps on working for the betterment of her child. She plays all roles including mom like a mentor, a teacher, a friend, a caretaker. When a child born; it is the mother who easily understands the feelings or requirements of her child. She spends every second around her child for fulfilling his all needs. Since childhood, our mother keeps telling us what is wrong and what is right in a manner to build us as a good human being and also encourage us to do good things in life.  
  
We as a child always take our mother for granted but without her our life becomes worthless. Mother is a precious gift by God which we need to keep with love and care. She does her job of motherhood with a pure heart and complete devotion. The first teacher is a mother for any child and if he keeps learning life’s lessons under her guidance nothing can stop him in achieving the heights of success.  
  
  

### Grammar

  

#### B. Complete the following sentences with however, although or in spite of.

a. **In spite of,** the fact that he is an octogenarian; he still leads an active life.  
  
b. I still enjoyed the week **although,** the weather was bad.  
  
c. He has passed MA. **However,** he hasn’t got a job.  
  
d. **Although,** I had a headache, I enjoyed the movie.  
  
e. Ramila didn’t get the job, **in spite of,** the fact that she had all the necessary qualifications.  
  
f. **In spite of,** the fact that he had no money, he bought the car anyway.  
  
g. We can go to the park for lunch **although,** the weather report says it’s going to rain.  
  
h. **However,** I speak English well, my first language is actually Maithili.  
  

#### C. Combine the following sentences using the words given in brackets.

  

#### a. He was annoyed. He didn’t say anything. (although)

Although he was annoyed, he didn’t say anything.  
  

#### b. Playing the stock market is exciting. It can be risky. (however)

Playing the stock market is exciting, however, it can be risky.  
  

#### c. He works slowly. He never makes a mistake. (even though)

Even though he works slowly, he never makes a mistake.  
  

#### d. It was raining. We still went to the park. (in spite of)

In spite of rain, we still went to the park.  
  

#### e. Hark bought the watch. It was expensive. (despite)

Despite being expensive, Hark bought the watch.  
  

#### f. He is very poor. He wears expensive clothes. (but)

He is very poor but wears expensive clothes.  
  

#### D. Rewrite the following sentences using (a) although and (b) in spite of.

  

#### a. He had very little time, but he offered to help us.

– Although he had very little time, he offered to help us.  
– In spite of having very little time, he offered to help us.  
  

#### b. She is very poor, but she still wears expensive clothes.

– Although she is very poor, she still wears expensive clothes.  
– In spite of being very poor, she still wears expensive clothes.  
  

#### c. He’s a millionaire, but he lives in a very small flat.

– Although he’s a millionaire, he lives in a very small flat.  
– In spite of being a millionaire, he lives in a very small flat.  
  

#### d. They have a lot of money, but they are still not happy.

– Although they have a lot of money, they are still not happy.  
– In spite of having a lot of money, they are still not happy.  
  

#### e. The traffic was heavy, but we got there in time.

– Although the traffic was heavy, we got there in time.  
– In spite of the heavy traffic, we got there in time.