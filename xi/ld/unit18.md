## Unit 18
# Immigration and Identity
### Ways with words

  

#### A. Words from the text to solve the crossword puzzle:

  
**Across**  
2\. as is certain to happen – **inevitably**  
4\. famous and respected – **eminent**  
6\. a temporary stay – **sojourn**  
7\. knowing everything – **omniscient**  
  
**Down**  
1\. the state of being preoccupied – **preoccupation**  
3\. anxious or fearful that something bad will happen – **apprehensive**  
5\. a person of mixed white and black ancestry – **mulatto**  
  

#### B. Find the meanings of the following words in a dictionary as they are used in the text.

  

#### a. melancholy

a deep feeling of sadness that lasts for a long time and often cannot be explained  
  

#### b. elusive

difficult to find, define or achieve  
  

#### c. motif

a subject, an idea that is developed in a work of literature  
  

#### d. disdain

the feeling that somebody is not good enough to deserve your respect or attention  
  

#### e. fabricate

to produce something false to decieve someone  
  

#### f. intoxicate

to cause somebody to lose control of their behaviour or their physical and mental abilities  
  

#### g. resentment

a feeling of anger or unhappiness about something that you think is unfair Homophones  
  

#### D. Choose the right word to fill in the blanks.

a. Can you **bury** the box in the back garden? _(bury/berry)_  
  
b. Alex could not **break** the branch off the tree. _(break/brake)_  
  
c. **Whose** pencil is on the floor? _(Who’s/Whose)_  
  
d. We have got very **few** _(phew/few)_ tasks left.  
  
e. Some tribes worship their gods before they **prey**. _(prey/pray)_  
  
f. **Damn** it. Everything is messed up. _(Dam /Damn)_  
  
g. What a wonderful **lesson** the professor presented. _(lesson/lessen)_  
  

  

### Comprehension

  

#### Answer these questions.

  

#### a. How is Willie Chandran different from the rest of his family?

Willie Chandran is different from the rest of his family as he is insecure about himself and his own identity.  
  

#### b. Who is the main character of Half A Life? How is he described?

Willie Chandran is the main character of Half A Life. He was born in India to a Brahmin father and a poor mother 40-some years ago. His middle name comes from the name of an English writer called Somerest Maugham.  
  

#### c. Why does Willie leave India?

Willie leaves India because he had no identity in India being born as a child of a high caste father and a low caste mother. He is hateful to his father and manages to get a scholarship to a college in London.  
  

#### d. What is the revelation that Willie begin to feel in college and in London?

Willie begin to feel that he is secure and free unlike in India where racial discrimination plays a vital role. There he could remake himself, his past and his ancestry.  
  

#### e. Why does Willie accompany Ana?

Willie accompanies Ana because she has a kind of high status in her colonial world and he can also find a complete acceptance there.  
  

#### f. What is the central issue Naipal has raised in the novel?

In the novel Naipal has raised an issue of identity crisis regarding ethnicity and culture.  
  

### Critical Thinking

  

#### a. What kind of divided identity is depicted in the novel ‘Half a Life’? How do characters in the novel try to create new identities for themselves? Explain.

According to the review of the novel ‘Half A Life’, there are three characters having mixed racial or divided identities. They are Willie, Ana and a big light eyed man. Willie, the central character of the novel has divided identity because of his birth from a Brahmin father and a low caste mother. Because of racial discrimination that prevails in India, Willie feels unsecure and loss of identity. He is unhappy in India and to find his freedom and identity he migrates to London. Ana is another character having same kind of divided identity. She belongs to a mixed African identity (part Portuguese and part African) similar to the identity of Willie. Willie and Ana like each other. Ana has also come in London in search of her identity but later after meeting Willie, she feels that she may have a high status in her Portuguese African country even to be a second rank Portuguese than mimicking other’s behavior to hide her past. Willie also follows her to her home country in a hope of a complete acceptance there. But he also feels there lack of identity. He sees the plight of an illegitimate man illegally born out of and African mother and a Portuguese landowner father. Such people are abused by Portuguese people because of the same of their birth.  
  

#### b. Discuss the similaries between the author and the protagonist in the noivel?

Surprisingly, there is a startling amount of parallelism between Willie Chandran and the Nobel Prize-winning author V.S. Naipaul. Naipul is of Indian descent and was born in the country of Trinidad in 1932. He left his home country at the age of 18 to pursue a higher education and went to England on a scholarship grant in the year 1950. Naipul studied at University College of Oxford and after four years he realized he wanted to be an author. He took up the pen and paper and found himself, realizing that his occupation would forever be as a writer. His prowess as a writer is incredible. In addition to Half a Life, Naipaul has written A House for Mr. Biswas, A Bend in the River, Between Father and Son, A Way in the World, Beyond Belief, and many more, totaling more than 20 fiction and non-fiction books in his lifetime. As mentioned before, he is a Nobel Laureate and now lives his life in Wiltshire, England. The similarities of all these characteristics with Willie Chandran is so astounding that some would have guessed Naipaul was writing an autobiography. Willie is also of Indian descent and left his home country at the age of 18 to go study abroad; both of them were able to do this because of a scholarship. Willie went to England to study at a university for four years as well. He took up writing and published a book while he was studying in London and briefly pursued writing, which is Naipaul’s occupation. They both eventually find themselves living in a foreign country other than the one they were born in. It is clear that V.S. Naipaul used aspects of his own life to craft and mold a protagonist and a story that would be powerful and compelling enough to keep readers glued to the page.  
  

### Writing

  

#### B. Write a review of a book/film you have recently read or watched.

The Time Machine

**1\. Title of the Book:** The Time Machine  
  
**2\. Author of the Book:** H.G. Wells  
  
**3\. Country:** United Kingdom  
  
**4\. Language:** English  
  
**5\. First originally published by:** William Heinemann, London in 1895.  
  
**6\. Genre:** Science Fiction Novel  
  
**7\. Cost of the Book:** Rs. 300  
  
**8\. Name of the Publisher:** Dover Publications  
  
**9\. Edition and year of Copyright:** April 3, 1995  
  
**10\. No. of pages:** 80  
  
**11\. Writing style:** Narrative  
  
**12\. Characters:** The Narrator-Hillyer, Eloi, Morlocks, Weena  
  
**13\. Plot:** The story follows a Victorian scientist, who claims that he has invented a device that enables him to travel through time, and has visited the future, arriving in the year 802,701 in what had once been London. The narrator recounts the Traveler’s lecture to his weekly dinner guests that time is simply the fourth dimension and demonstrates a tabletop model machine for travelling through the fourth dimension. He reveals that he has built a machine capable of carrying a person through time and returns at dinner the following week to recount a remarkable tale, becoming the new narrator.  
  
**14\. Summary:** A group of men, including the narrator, is listening to the Time Traveler discussing his theory that time is the fourth dimension. The Time Traveler produces a miniature time machine and makes it disappear into thin air. The next week, the guests return, to find their host stumble in, looking disheveled and tired. They sit down after dinner, and the Time Traveler begins his story.  
  
**15\. My Impressions:** The time traveler’s machine is described in such sketchy terms that it can scarcely be believed as an instrument of science, and the time traveler’s account is similarly sketchy and bizarre. The very nature of time travel means that he’s away for only a short period of time, and the only “proof” of his travels is a crunched up flower. And given that the narrative is told in a twice-removed manner, the reader can’t help but wonder whether any of the novels is true at all. Did the time traveler truly engage in such chronological shenanigans, and did he experience what he claims? Or is he simply using an imagined future to provide a warning about the current state of society? But the reality is that neither the truth nor the journey matters: it’s only the outcome.  
  

### Grammar

  

#### Change the following into indirect speech.

  

#### a. She said, "While I was having dinner, the phone rang."

She said that while she was having dinner, the phone rang.  
  

#### b. My friend said, "Where are they staying?"

My friend asked where they were staying.  
  

#### c. Jamila said, "I travel a lot in my job."

Jamila said that she travelled a lot in her job.  
  

#### d. She said to me, "We lived in China for five years."

She told me that they had lived in China for five years.  
  

#### e. He said to me, "Do you like ice-cream?"

He told me if I had liked ice-cream.  
  

#### f. They said, "Hurray! We’ve won the match."

They exclaimed with delight that they had won the match.  
  

#### g. He said, "I’d tried everything without success, but this new medicine is great."

He said that he’d tried everything without success, but this new medicine was great.  
  

#### h. Sony said, "I go to the gym next to your house."

Sony said that she went to the gym next to my house.  
  

#### i. He said, "Be quiet after 10 o’clock."

He told me to be quiet after 10 o’clock.  
  

#### j. He said, "I don’t want to go to the party unless he invites me."

He said that he didn’t want to go to the party unless he invited him.  
  

#### k. He said to me, "I will see you tomorrow if you meet me."

He told me that he would see me the following day if I met him.  
  

#### l. She said, "If I were you, I would give up the work."

She said that if she were me, she would give up the work.