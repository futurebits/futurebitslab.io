## Unit 7
# Ecology and Development
### Ways with words

  

#### A. Match the words with their meanings.

  
**Constituency** – electoral district  
  
**Pursue** – to follow in an effort to overtake or capture  
  
**Infiltrate** – to enter or gain access to (an organization, place, etc.) secretly and gradually  
  
**Nomadic** – living the life of roaming  
  
**Anatomy** – art of studying the different parts of any organized body  
  
**Subversive** – seeking or intended to overthrow an established system or institution  
  

#### C. Make two other words by using each prefix given above and use them in sentences.

  

#### mal

Malignancy – This should be followed up to exclude a malignancy.  
Malware – Malware and viruses could infect their computers.  
  
**mis**  
Misspell – t’s difficult to see misspell in a sentence.  
Mismatch – Their marriage was a mismatch they had little in common.  
  
**un**  
Uncover – Erin never does get to uncover his true identity.  
Unsafe – If it weren’t so unsafe, he’d take her with him  
  
**in**  
Inedible – The food on the flight was totally inedible.  
Incapable – She’d been incapable of empathy or remorse.  
  
**pre**  
Prevent – I’m sorry to prevent you from seeing him.  
Preplan – Now, it is time to preplan new trip.  
  
**il**  
Illogical – Not being feminist to me is just totally illogical.  
Illusion – These will give the illusion of a cinched waist.  
  
**dis**  
Discomfort – Elisabeth’s discomfort was evident.  
Disengage – Overcharge the Plasma Pistol and disengage a vehicle.  
  
**im**  
Immature – Katie had always been immature, maybe even a little lazy.  
Immortal – The Dark One was going to turn her Immortal.  
  
**Re**  
Return – I have something to return to you.  
Rewrite – We can rewrite history as much as we like.  
  
**ir**  
Irremovable – Nowadays, most of smartphone have irremovable battery.  
Irrational – These are isolated cases of irrational numbers.  
  
**de**  
Deactivate – At this point your account will immediately be deactivated.  
Defrost – A basic microwave oven should be able to cook, defrost and reheat.  
  
**non**  
Nonstick – You’ll definitely need an nonstick cooking spray.  
Nonverbal – Their nonverbal exchange made me smile.  



### Comprehension

  

#### Answer these questions.

  

#### a. What did Maathai decide to do for the community?

Maathai decided to create jobs for the community.  
  

#### b. Mention the problems faced by women in Kenya.

The problems faced by women in Kenya were malnutrition, lack of food and adequate water, uemployment and soil erosion.  
  

#### c. Why did the speaker go to the women to talk about planting trees?

The speaker went to the women to talk about planting trees because the women in Kneya were most affected by the problems of firewood, malnutrition, lack of food and adequate water, unemployment and soil erosion, and were concerned about children and future.  
  

#### d. What is the most important achievement of the movement described in the text?

The most important achievement of the movement described in the text is women were independent, had acquired knowledge and techniques, had become empowered. They have been teaching each other.  
  

#### e. Why were the foresters’ ways not helpful to the women for planting trees?

The foresters’ ways were not helpful to the women for planting trees because they have complicated the ways of dealing with a very simple thing like looking for seeds and planting trees.  
  

#### f. When and how did she start The Green Belt Movement?

She started The Green Belt Movement on World Environment Day by planting the first seven trees  
  

#### g. Why do the donors want to provide money to the women?

The donors want to provide money to the women because their efforts were providing satisfactory results.  
  

#### h. What happened when the speaker criticized the political leadership?

The speaker has been portrayed as subversive when the speaker criticized the political leadership.  
  

#### i. Mention the agencies that supported her movement.

The agencies that supported her movement are as follows:  
– United Nations Development Fund for Women  
– Danish Voluntary Fund  
– Norwegian Agency for International Development  
– African Development Foundation  
  
  

### Critical thinking

  

#### a. Do you think that the title “Foresters without Diplomas” is suitable to the essay? How?

Yes I think that the essay’s title, “Foresters without Diplomas,” is appropriate.The writer and her team initially summoned the foresters to show how they grow trees to the ladies. Because they possessed degrees, the foresters proved to be very difficult to work with. They developed complicated ways for coping with outwardly basic tasks like as searching for seeds and growing trees. Finally, the writer and her colleagues instructed the ladies on how to do the task using common sense, which they accomplished. They were able to search the area for seeds and learn to identify seedlings as they germinate from seeds that had fallen to the ground. Women did not have the same opportunities as males.  
  

#### b. Can a person make a difference in a society? Discuss with an example from a person who has made a difference in your society.

It’s easy to feel like one can person can’t make a difference. The world has so many big problems, and they often seem impossible to solve. We know that while many common ways to do well, such as becoming a doctor, have less impact than you might first think; others have allowed certain people to achieve an extraordinary impact. In other words, one person can make a difference, but you might have to do something a little unconventional.  
  
Numerous folks have made outstanding and significant change, not only for our society, but the entirety of all people, the world over. Some of those examples are Dr. Jonas Salk found the vaccine that prevented people from contracting Polio, Thomas Edison discovered the light bulb, a major innovation to society, the world over. So we can see that a person can make a positive difference in the lives of people not in our society but throughout the world.  
  

#### c. Write an essay on ‘The Community Forest in Nepal’. Describe how these community forests have contributed to maintain ecology in our environment.

The Community Forest in Nepal

The Community Forestry Program in Nepal is a global innovation in participatory environmental governance that encompasses well-defined policies, institutions, and practices. The program addresses the twin goals of forest conservation and poverty reduction. As more than 70 percent of Nepal’s population depends on agriculture for their livelihood, community management of forests has been a critically important intervention.  
  
Through legislative developments and operational innovations over three decades, the program has evolved from a protection-oriented, conservation-focused agenda to a much more broad-based strategy for forest use, enterprise development, and livelihood improvement. By April 2009, one-third of Nepal’s population was participating in the program, directly managing more than one-fourth of Nepal’s forest area.  
  
The immediate livelihood benefits derived by rural households bolster strong collective action wherein local communities actively and sustainably manage forest resources. Community forests also became the source of diversified investment capital and raw material for new market-oriented livelihoods. Community forestry shows traits of political, financial, and ecological sustainability, including emergence of a strong legal and regulatory framework, and robust civil society institutions and networks.  
  
However, a continuing challenge is to ensure equitable distribution of benefits to women and marginalized groups. Lessons for replication emphasize experiential learning, establishment of a strong civil society network, flexible regulation to encourage diverse institutional modalities, and responsiveness of government and policymakers to a multistakeholder collaborative learning process.  
  
  

### Grammar

  

#### c. Underline the verb in each sentence and write whether it is transitive, intransitive or linking.

  
**a. His father looks handsome.**  
looks = linking verb  
  
**b. Bhawana drinks milk every day.**  
drinks = transitive verb  
  
**c. He became a watchman.**  
became = linking verb  
  
**d. This bread smells good.**  
smells = linking verb  
  
**e. The dog barked loudly.**  
barked = intransitive verb  
  
**f. He chased the dog.**  
chased = transitive verb  
  
**g. My sister swims fast.**  
intransitive verb  
  
**h. He painted a picture.**  
painted = transitive verb  
  
**i. Radhika always asks questions.**  
asks = transitive verb  
  
**j. Anjana has a long hair.**  
has = linking verb  
  
**k. Hungry lion roars.**  
intransitive verb  
  
**l. He tried again and again.**  
tried = intransitive verb  
  
**m. The weather is hot.**  
is = linking verb  
  
**n. Owls hide in the daytime.**  
hide = intransitive verb