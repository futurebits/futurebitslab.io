## Unit 3
# Media and Society
### Ways with words

  

#### A. Find the words from the text that have the following meanings.

  
a. a personal or corporate website – **blog**  
b. a job requiring expertise in a particular field – **profession**  
c. a feeling of sadness – **disappointment**  
d. put under custody – **detained**  
e. found guilty – **convicted**  
f. breaking or disregarding rules or system – **violating**  
g. advantages and disadvantages – **pros and cons**  
h. a non-statutory monetary penalty – **amercement**  
  

#### B. Use the following prepositional verbs in sentences.

  
**apologize to**  
She apologized to all of the patients she had kept waiting.  
  
**adapt to**  
It is really hard for Jim to adapt to the new environment.  
  
**devote to**  
They devote an hour every day to worship.  
  
**refer to**  
Her mother never referred to him again.  
  
**reply to**  
The company has replied to the recent protests by posting an ad in the local newspaper.  
  
**admire for**  
I admire her dedication to the job.  
  
**apply for**  
Only genuine refugees can apply for asylum.  
  
**pray for**  
I came up here to pray for the most important things.  
  
**scold for**  
You feel a little like a scold for wanting it to buckle down and concentrate.  
  
**vote for**  
In parliament, all members voted for the new tax.  
  
**aim at**  
In the long run men hit only what they aim at.  
  
**arrive at**  
I arrive at nine o’clock.  
  
**glance at**  
With a glance at the demon lord, she stretched to grab it and rose.  
  
**look at**  
He turned and looked at her.  
  
**bring up**  
She’s struggling to bring up a family alone.  
  
**agree on**  
We agree on the question.  
  
**comment on**  
She refused to comment on their decision.  
  
**concentrate on**  
She closed her eyes to concentrate hard on summoning the portal.  
  
**rely on**  
They rely on a well for all their water.  
  
**count on**  
I was counting on the money from the insurance to buy a new house.  
  
**grow up**  
It’s a wonderful place to grow up.  
  
**escape from**  
He made a daring escape from his kidnappers.  
  
**recover from**  
We prayed she would recover from her illness.  
  
**resign from**  
He was forced to resign from the board in protest.  
  
**suffer from**  
All children will tend to suffer from separation from their parents and siblings.  
  
**separate from**  
South America separated from Africa 200 million years ago.  
  
**worry about**  
She always worries about his weight.  
  
**argue about**  
My two elder sisters always argue each other about politics.  
  
**boast about**  
She is boasting about how wonderful her car is.  
  
**dream about**  
dreamed about Annie all last night.  
  
**acquaint with**  
Travel will acquaint us with new customs.  
  
**agree with**  
I know you agree with me.  
  
**charge with**  
It’s difficult to see charge with in a sentence.  
  
**trust with**  
My parents trust me with their life.  
  
**confuse with**  
People often confuse me and my twin brother.  
  
**believe in**  
I believe in working hard to achieve success.  
  
**involve in**  
The test will involve answering questions about a photograph.  
  
**succeed in**  
He wants to succeed in everything he does.  
  
**specialize in**  
The bakers specialize in catering for large parties.  
  
**absorb in**  
Plants absorb carbon in the form of carbon dioxide.  
    

### Comprehension

  

#### A. Fill in the gaps with appropriate words/phrases from the text.

  
a. People are using social media for individual as well as **marketing** purposes.  
b. Social media is not only a platform of sharing views, but also a platform of **marketing strategy**.  
c. The hospital wanted Prita to pay Rp. 100 million for**immaterial losses**.  
d. Some singers organized **clarified** to help Prita.  
e. The campaign to help Prita was able to collect Rp. **825 million**.  
  

#### B. Answer these questions.

  

#### a. How do you define social media?

Social media is computer based technology that facilitates the sharing of idea, thoughts and information through the building of virtual network and communities.  
  

#### b. Social media has been an integral part of modern life. How?

Social media has been an integral part of modern life as it allows people to interact in ways we could never imagine and we can easily access to the person we want to talk from any part of the world to any different part of the world.  
  

#### c. Why do people use Facebook and YouTube?

People use Facebook to exchange news through text and YouTube to watch videos.  
  

#### d. What is a blog? How can it be beneficial to us?

A blog is a regularly updated website or web page, typically one run by an individual or small group, that is written in an informal or conversational style.  
It can be beneficial to usbecause we can improve our writing skills, connect with people, learn new things, and so on.  
  

#### e. Was Prita’s intention bad when she sent a message to her friends?

No, Prita’s intention was not bad when she sent a message to her friends because she just wanted her friends to be more careful in accepting many kinds of services.  
  

#### f. How did Prita’s message become a big problem?

Prita’s message became a big problem after her friend shared it to a website (The People’s Forum).  
  

#### g. Why was she convicted by the court?

She was convicted by the court for violating the law about using the ICT (Information and Communication Technology).  
  

#### h. What was the purpose of the campaign, “Bantu Bebaskan Prita”?

The purpose of the campaign “Bantu Bebaskan Prita" was to collect money to help Prita so that she could pay the amercement.  
  

#### i. How are people stronger than social media?

People are stronger than social media as they can easily think before they act and think before they speak.  
  

### Critical thinking

  

#### a. Does social media have positive impact in the society? Discuss.

Social media is a tool that is becoming quite popular these days because of its user-friendly features. Social media platforms like Facebook, Instagram, Twitter and more are giving people a chance to connect with each other across distances. There are many positive ways in which social media can help to stay connected and support people’s wellbeing.  
  
When we look at the positive aspect of social media, we find numerous advantages. The most important being a great device for education. All the information one requires is just a click away. Students can educate themselves on various topics using social media.  
  
Furthermore, as more and more people are distancing themselves from newspapers, they are depending on social media for news. You are always updated on the latest happenings of the world through it. A person becomes more socially aware of the issues of the world.  
  
In addition, it strengthens bonds with your loved ones. Distance is not a barrier anymore because of social media. For instance, you can easily communicate with your friends and relatives overseas.  
  
Most importantly, it also provides a great platform for young budding artists to showcase their talent for free. You can get great opportunities for employment through social media too.  
  

#### b. How do you compare virtual communication and face-to-face communication?

Communication can be done by both face to face and online methods. Face to face communication enhances the quality of a person’s life, whereas virtual communication has a new domain of possibilities for communicating with people.  
  
The difference between face to face communication and virtual communication is that in the face to face communication, you can feel and see the presence of the other party, whereas, in virtual communication, you cannot see the other party to whom you are communicating with.  
  
Face to face communication is a traditional method of communication that allows you of being able to see the opposite party you are talking to. This allows betterment in exchange for communication and information both for a person and to whom he is communicating. Even though the technology today is remarkable many still follow this kind of communication for clarity. Face to face communication is also an informal type of discussion between friends, family, colleagues, etc.  
  
Virtual communication means when a person is communicating with another person via computer using the internet through email, etc. Due to technology development in a remarkable way, technology provides people with a significant way to communicate with other people and carry their business. We can talk and text anyone we want through mail anytime we want without having any boundaries for a person, whether the opposite person is online.  
  

### Writing

  

#### b. What is cyber bullying? What are its effects? How can it be stopped?

The use of electronic communication to bully a person, typically by sending messages of an intimidating or threatening nature is known as cyber bullying. Cyber bullying is a common occurrence in which children, teenagers, and even adults are targeted, humiliated, tortured, or threatened online. Rumors, threats, posting sexual comments, sharing the victim’s personal information, and hate speech are all examples of harmful bullying behavior. Bullying is commonly found on social networking sites, SMS, email, and online gaming.  
  
Cyberbullying is a multi-faced issue. Victims of cyberbullying feel unsafe even when they are at home. It is extremely difficult to avoid. It causes mental, emotional, and physical harm to the person because its effects can last for a long time. Low self-esteem, even suicidal tendencies, and a variety of negative emotional reactions may be experienced by the victim.  
  
Cyberbullying prevention is the need of the hour. It needs to be monitored and put an end to. There are various ways to tackle cyberbullying. Awareness is the key to prevent online harassment. We should make the children aware from an early age so they are always cautious. Moreover, parents must monitor their children’s online activities and limit their usage. Most importantly, cyberbullying must be reported instantly without delay. This can prevent further incidents from taking place.  
  

### Grammar

  

#### B. Complete the following sentences using the correct prepositions of direction: to, toward, onto, or into.

  
a. Prem drove Milan **to** the airport.  
b. The plane landed **onto** the runway.  
c. The kids climbed **onto** the monkey bars.  
d. Manish and Richa moved the table **into** the dining room.  
e. Ganesh almost fell **into** the river.  
f. Lalit and Sarita took the bus that was heading **toward** the university.  
  

#### C. Complete the sentences with the correct preposition from the brackets.

  
a. While we were hiking **through** the forest, we saw a mountain lion. (across/through/along/under)  
  
b. The leopards walked in a circle **round** the baby giraffe before they attacked.(into/towards/round/through)  
  
c. Go **out of** the building and turn left. (into/up/off/out of)  
  
d. She ran **away from** home when she was eighteen. (towards/away from/down/across)  
  
e. Raindrops ran **down** the windscreen making it difficult to see the road.(into/up/down/over)  
  
f. Hemanta put the plate **onto** the table and began to eat his dinner. (onto/into/up/off)  
  
g. The frightened deer disappeared **into** the forest. (up/onto/into/toward)  
  
h. We were driving **towards** the City Centre when we had an accident. (up/into/towards/along)  
  
i. The smoke from the fire went **up** into the sky. (into/up/to/onto)