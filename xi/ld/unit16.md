## Unit 16
# Critical Thinking
### Ways with words

  

#### A. The words in the crossword puzzle are from the text. Find them from the text to solve the puzzle based on the meaning clues given below.

  

#### 1\. unquestionable, impossible to doubt

indubitable  
  

#### 2\. never done or known before

unprecedented  
  

#### 3\. emergence or origination

emanation  
  

#### 4\. obviously or clearly

evidently  
  

#### 5\. impossible to achieve or overcome

insuperable  
  

#### 6\. moved with a violent, irregular action

agitated  
  

#### 7\. appropriate or suitable

apt  
  

#### 8\. force back

repel  
  

#### 9\. conservative

orthodox  
  

#### 10\. up to now

hitherto  
  

#### B. Find the words from the text that mean the following.

a. misleading or illusionary (**deceptive**)  
  
b. in a natural state; not yet processed or refined (**crude**)  
  
c. a mystical horse like animal with a single straight horn projecting from its forehead (**unicorn**)  
  
d. never dying or decaying (**immortal**)  
  
e. come to an end; stop (**cease**)  
  
f. to activate or put into motion (**actuate**)  
  

 

### Comprehension

  

#### Answer these questions.

  

#### a. What’s the difference between the body and the soul?

The body exists in time and space whereas the soul exists in time only. In simple words, body is visible entity where as soul is not visible.  
  

#### b. What do you understand by the psychologists’ saying that there is no such thing as mind?

Psychologists believe that there is no existence of mind as they study mental processes and human behaviors by observing, interpreting, and recording how people relate to one another and the environment.  
  

#### c. How can a mental activity be reduced to a physical activity?

Mental activity can be reduced to a physical activity by saying mind is just an emation of body which is really an elaborate scientific construction.  
  

#### d. How are mind and body related?

Mind and body are related as they are merely convenient ways of collecting and organizing events inside a man’s head into two groups according to causal relations.  
  

#### e. What is the relation between mind and memory? Does memory survive a person’s death?

Memory is the most essential characteristic of mind. No, memory doesn’t survive a person’s death because it is connected with certain kind of brain structure which decays at death. Thus, memory also dies.  
  

#### f. How is our power on the earth’s surface entirely dependent upon the sun?

Our power on the earth’s surface is entirely dependent upon the sun as it warms our seas, regulates our weather patterns, and gives energy to the growing green plants that provide the food and oxygen for life on Earth.  
  

### Critical Thinking

  

#### a. Do you believe that soul really exists? Write your arguments in support of your answer.

I believe that souls do exist.  
  
Have you ever imagined about your own death? Have you ever thought that what happens after the incident that the world calls death? Could you ever convince yourself that your existence on this planet earth would be finished once you die?  
  
I couldn’t ever convince myself with the fact that I’ll be finished once I die. Death would cause the degradation of my physical body but it’s just unimaginable that I will too get over once the death occurs. This “I” is nothing but the soul.  
  
I know that the science that we’re familiar with hasn’t proved the existence of souls yet. But we shouldn’t be hopeless. The boundary of science is expanding day by day. Even the fans and lights that we use everyday were once unimaginable but we know that today they exist. There are new fields added to science everyday. Some of the fields that have tried to progress in this topic are metaphysics, para psychology etc.  
  
As far as the ancient science is concerned, it had proved the existence of soul long ago. It also developed methods by which one can experience one’s soul. But anyways, I hope that there would be a day when the modern science will too prove the existence of soul.  
  

#### b. If you want to change your body, you first have to change your mind. Discuss.

I support the statement that “If you want to change your body, you first have to change your mind.” Psychologists say our “self talk” or “internal dialogue” can make or break a fitness routine.  
  
The problem is that many people simply aren’t aware of how destructive their thoughts are. The thing that precedes our behavior is a thought, and we sometimes aren’t good at getting in touch with our thoughts. Let me take my fitness example. The first step in charting the right course to fitness is to recognize how my thoughts are undermining my exercise plans. The second step is to challenge the negative thinking – and there’s certainly no shortage of that when it comes to exercise.  
  
It can be tough for beginners to master the skills necessary to adhere to an exercise plan, but give it time. If I could stick it out for six months, it starts to become a part of who I am versus what I do. Plus, I’ll probably see some positive results that can motivate me to keep at it. Among them: better muscle tone, feeling stronger and more fit, weight loss, improved mood and better sleep.  
  

### Writing

  

#### Write critically on the given topics.

  

#### a. The existence of God

The existence of God is foundational to the study of theology. The Bible does not seek to prove God’s existence, but rather takes it for granted. Scripture expresses a strong doctrine of natural revelation: the existence and attributes of God are evident from the creation itself, even though sinful human beings suppress and distort their natural knowledge of God. The dominant question in the Old and New Testaments is not whether God is, but rather who God is. Philosophers both Christian and non-Christian have offered a wide range of arguments for God’s existence, and the discipline of natural theology (what can be known or proven about God from nature alone) is flourishing today.  
  
Some philosophers, however, have proposed that belief in God is rationally justified even without theistic arguments or evidences. Meanwhile, professing atheists have offered arguments against God’s existence; the most popular is the argument from evil, which contends that the existence and extent of evil in the world gives us good reason not to believe in God. In response, Christian thinkers have developed various theodicies, which seek to explain why God is morally justified in permitting the evils we observe.  
  
If theology is the study of God and his works, then the existence of God is as foundational to theology as the existence of rocks is to geology. Two basic questions have been raised regarding belief in God’s existence: (1) Is it true? (2) Is it rationally justified (and if so, on what grounds)? The second is distinct from the first because a belief can be true without being rationally justified (e.g., someone might irrationally believe that he’ll die on a Thursday, a belief that turns out by chance to be true). Philosophers have grappled with both questions for millennia. In this essay, we will consider what the Bible says in answer to these questions, before sampling the answers of some influential Christian thinkers.  
  

### Grammar

  

#### A. Rewrite the following sentences using ‘used to’. You can make an affirmative/ negative statement or a question.

  

#### a. I/live in a flat when I was a child.

I used to live in a flat when I was a child.  
  

#### b. She/love eating chocolate but now she hates it.

She used to love eating chocolate but now she hates it.  
  

#### c. He/go to fishing in the summer?

Did he use to go fishing in the summer?  
  

#### d. My sister/play tennis when she was at school.

My sister used to play tennis when she was at school.  
  

#### e. He/play football every weekend?

Did he use to play football every weekend?  
  

#### f. My grandfather/speak five languages.

My grandfather used to speak five languages.  
  

#### g. I/not hate school from the beginning.

I did not use to hate school from the beginning.  
  

#### h. You/live in Kathmandu?

Did you use to live in Kathmandu?  
  

#### i. He/play Dandibiyo when he was a small child.

He used to play Dandibiyo when he was a small child.  
  

#### j. She/wear a frock when she was small but nowadays she wears jeans.

She used to wear a frock when she was small but nowadays she wears jeans.  
  

#### B. Write a short paragraph describing your past habits.

For five years, I lived with my uncle. I used to begin my regular tasks in the mornings at my uncle’s home. Despite having a full-time job at home, I used to strive for first place in my class. My instructors utilised locally accessible materials to make their lessons successful, despite the fact that my school lacked enough resources. They’d cheer us up in class by being nice. Project work was assigned by our English instructor. It proved to be very beneficial to our academic endeavours.