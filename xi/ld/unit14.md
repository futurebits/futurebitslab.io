## Unit 14
# Power and Politics
### Ways with words

  

#### A. Choose the correct meaning of the underlined word.

  
a. Many portraits of Napoleon show him with his right hand placed inside his coat.  
**sketch**  
  
b. Napoleon won one victory after another, defeating the Austrians in eighteen battles.  
**beating**  
  
c. Portrait painters thought this pose made men look more dignified.  
**good-looking**  
  
d. They announced France a republic.  
**officially declared**  
  
e. Napoleon conquered Austria in 1805.  
**triumphed over**  
  
f. There was no place to house his soldiers in the bitter Russian winter.  
**Cold**  
  
g. Napoleon was humiliated when he was defeated.  
**shamed**  
  

#### B. Guess the meanings to these words from the text and make sentences of your own.

  
**1\. violent :** causing hurt  
The love that is too violent will not last long.  
  
**2\. execute :** kill somebody as a legal punishment  
He was captured and executed for murder.  
  
**3\. ancient :** very old  
The pyramids were built in ancient times.  
  
**4\. alliance :** a union between people, groups, countries, etc.  
The two parties were still too much apart to form an alliance.  
  
**5\. brilliant :** very intelligent or skillful  
Brilliant students are favourite of teachers.  
  
**6\. genius :** very great and rare natural ability or skill  
You don’t have to be a genius to see that this plan will never work.  
  
**7\. consul :** an official chosen by a government to live in a foreign city  
The Consul spoke slowly and with great gravity.  
  
**8\. invading :** to enter a place in order to take control by military force  
During the Second World War the island was invaded by the Axis powers.  
  
  

### Comprehension

  

#### Answer these questions.

  

#### a. Where was Napoleon from?

Napoleon was from France.  
  

#### b. Why did poor and middle-class people declare France a republic?

Poor and middle-class people declared France a republic because they were tired of paying heavy taxes for the luxurious life of the king and his nobles.  
  

#### c. When did Napoleon declare himself emperor of France?

Napoleon declared himself emperor of France in 1804.  
  

#### d. What did he do when he ruled France?

When he ruled France, he restored and reorganized the French government and the Bank of France, built beautiful cities with wide streets, fine bridges, and beautiful buildings and monuments, such as the Arc de Triomphe.  
  

#### e. Which countries did he rule when he was the emperor?

He ruled Italy, Switzerland, and Germany when he was the emperor.  
  

#### f. What was the main cause of his destruction?

Invasion of Russia was the main cause of his destruction.  
  

#### g. How did his rule as emperor end in Europe?

His rule as emperor ended in Europe after the Europeans gathered their armies against him.  
  

#### h. How could Napoleon have been an even greater ruler?

Napoleon could have been an even greater ruler if he had not been driven by his love of power.  
  

### Critical thinking

  

#### a. What can be the qualities of a great leader? Can a great leader remain in power for long in a country? Discuss.

Leadership refers to the quality of leading people. Probably, it is one of the most important aspects of life. Above all, Leadership by a good leader has led to the progress of human civilization. Without a good leader, no organization or group can succeed. Furthermore, not everyone has this quality. This is because effective Leadership requires certain important characteristics.  
  
First of all, confidence is the most quality. A leader must have strong self-confidence. A person lacking in confidence can never be a good leader. A person must be confident enough to ensure others follow him. A good leader must certainly inspire others. A leader must be a role model for his followers. Furthermore, he must motivate them whenever possible. Also, in difficult situations, a leader must not lose hope. Honesty is another notable quality of a leader. Honesty and Integrity are important to earn the love of followers. Probably, every Leadership which loses trust is bound to fail. People will not work with full effort due to an immoral leader. Another important quality is good communication and decision making. Above all, if a leader makes poor communications and poor decisions then other qualities will not matter. Furthermore, good decision making ensures the success of the entire group. If the leader makes poor decisions, then the efforts of followers won’t matter.  
  
In conclusion, a good leader must be an excellent innovator. He must display a creative attitude in his work. Most noteworthy, innovation is a guarantee of survival of a group or innovation. Without creative thinking, progress is not possible.  
  
If a leader is good, capable and have all the above traits then he/she can easily remain in power for long in a country.  
  

#### b. The 16th president of the USA, Abraham Lincoln said democracy is government of the people for the people and by the people. Do you think it is perfectly applicable in the present context of Nepal? Explain.

The literal meaning of \`democracy’ is \`rule by the people’, or, in other words, a political system in which ultimate authority is shared equally by all members of the community. In Nepal as elsewhere such a system is widely endorsed as a desirable state of affairs but there is no precise, shared understanding of what it implies in practice. In Nepal, even more than in most other places, there is also a sense of deep disillusionment with what a system describing itself as democratic has actually achieved.  
  
The political system presented by Abraham Lincoln does not apply to Nepal, as corruption had paralyzed constitutional and government agencies. The Constitution guarantees the idea of ​​collective decision-making that includes all people equally, but it does not actually apply. Democratic norms and values ​​depend on the proper functioning of constitutional and state institutions. But it has been crippled by a group of certain people. The role of bureaucrats is important to keep the administration running smoothly so that development strategies can be easily implemented. However, corrupt bureaucracy does not do its job without bribes and is endorsed by political leaders. Nepalese people have never experienced a responsible, accountable government. State institutions are victims of nepotism and discrimination. Thus, in a country like Nepal, institutionalizing democracy is more difficult. To strengthen the principles of democracy, all constitutional agencies, government agencies and citizens must be responsible.  
  

### Writing

  

#### a. Write an essay on Power and Politics in about 500 words.

Power and Politics

Power is usually defined as the ability to influence people’s behavior by getting people to do what the person wants. Politically, having the ability to influence people is a huge responsibility. Being a leader means having the abilities to not only convince people, but also to inspire them and influence their decision by making them see the sense that the person with power is acting in their best interest. Therefore, political power is about not only persuasion, but also manipulation. Politics is the act of persuasion, acquisition, and exercise of political power.  
  
Political power shapes and control people’s attitude towards the leader and the governing system. The leader guides the behavior of his followers in a direction he desires to achieve the common political objectives. Occupying a political position means having the power and the ability to effect the desired change of behavior of other people involved through persuasion or manipulation. For example, if an individual has the political power over the other person, the degree in which a leader can motivate, incite, inspire, stimulate, and makes other people modify their political behavior. This process is termed as having the political power.  
  
Illegitimate political power implies exercising powers that violate the existing rules; these may include sabotage, protests, and whistle blowing. Extreme illegitimate forms of political power pose a risk of loss of membership. Since most political organization is made up of individuals with different values and interest, this presents the potential for conflict over resources. A leader in power uses his position to attain group goals. However, power is applied in different forms. Coercive power is based on fear. Coercive power includes sanctions, restrictions, and control by force.  
  
Power plays an important role in politics because its consequences can yield positive or negative results. Power in politics is a two-way relationship depending on how people interact based on the resources or values they hold or are in control of. Conflict arises due to pressure to achieve specific goals that might not be realistic. That is why leaders manipulate his followers to serve their needs at the expense of others. In a democratic system, use of power should not involve force; instead of obtaining power should be more of influence that is known as soft power.  
  

#### b. Write a couple of paragraphs about a national hero who fought bravely in the Anglo-Nepal War.

Balbhadra Kunwar Chhetri

Balbhadra Kunwar Chhetri (30 January 1789 – 13 March 1823) was a Nepalese military commander and administrator in the Kingdom of Nepal. He is one of the National heroes of Nepal. He was highly praised for his military skill for the defence of the Nalapani fort in the Anglo-Nepalese War (1814–1816). He was Captain in the Nepalese military and was tasked as commander to protect the forts of Dehradun.  
  
As commander of the Gorkhali forces in Dheradun, Capt. Balbhadra Kunwar was handed the responsibility of defending the area. The expanding Nepali/Gorkhali State had since the mid-late 18th century expanded the nation’s border on all sides, which eventually led to conflict with the British East India Company and a war followed. Realizing he could not defend the town of Dehradun, Capt. Balbhadra Kunwar withdrew to the strategic hill fort of Khalanga with an army strength of 600 including women and children against the British East India Company British stronghold of 3000–3500 troops. He turned down an incentive proposal of the British who would make him Governor of the Western Garhwal should he surrender or leave Nepal.  
  

### Grammar

  

#### B. Fill in the blanks with may, must or can’t.

a. Matthew **must** be at home. I can see his bike in front of his home.  
  
b. They **may** be coming tomorrow.  
  
c. She **can’t** speak French very well. She’s only lived in Paris for two weeks.  
  
d. My key is not in my pocket or on my desk so it **must** be in the drawer.  
  
e. I saw him yesterday. He **can’t** be abroad.  
  
f. You got the job? That’s great. You **must** be very delighted.  
  
g. I **may** finish it by tomorrow if I stay at work all night, but I’m not sure.  
  
h. Somebody is knocking on the door. It **must** be Sabina – she promised to come today.  
  

#### C. Rewrite the following sentences using may/might, must or can’t.

  

#### a. I’m sure he’s not going to the cinema today.

Ans: He can’t be going to the cinema today.  
  

#### b. Perhaps she knows the answer.

Ans: She may know the answer.  
  

#### c. I’m sure he has a car.

Ans: He must have a car.  
  

#### d. I doubt if it rains later on.

Ans: It may rain later on.  
  

#### e. Perhaps she wants to be alone.

Ans: She may want to be alone.  
  

#### f. I’m sure Harina is in her office.

Ans: Harina must be in her office.