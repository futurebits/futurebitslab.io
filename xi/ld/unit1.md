## Unit 1
# Education and Humanity
### Ways with words

  

#### A. Find the words from the text which mean the following.

  
a. a messenger or representative, especially one on a diplomatic mission  
**envoy**  
  
b. the state or quality of being worthy of honor or respect  
**dignity**  
  
c. harm done to someone in response to harm  
**revenge**  
  
d. a person who holds extreme views in political or religious matters  
**extremist**  
  
e. sympathetic pity and concern for the sufferings or misfortunes of others  
**compassion**  
  
f. a person who is believed to speak for God  
**prophet**  
  
g. the study of the nature of knowledge, reality and existence  
**philosophy**  
  
h. the use of physical force so as to injure, abuse, damage or destroy  
**violence**  
  
i. an unreasonable dislike of a particular group of people or things  
**prejudice**  
  

#### B. Match the words on the left with their opposite meanings on the right.

honour – disgrace  
innocent – guilty  
brutality – kindness  
forgiveness – punishment  
illiteracy – literacy  
  

#### C. Using dictionary

  

#### a. Study the dictionary entry above and answer these questions.

  

#### i. What is the headword in the first entry?

The headword in the first entry is Humanity.  
  

#### ii. How many meanings of the word ‘humanity’ are given?

Four meanings of the word ‘humanity’ are given.  
  

#### iii. What do the abbreviations U, OPP, pl, and sth stand for?

U, OPP, pl and sth stand for uncountable, opposite, plural and something respectively.  
  

#### iv. What is the British English spelling of ‘humanize’?

The British English spelling of ‘humanize’ is humanise.  
  

#### v. How is the word ‘humanize’ pronounced?

The word ‘humanize’ is pronounced as /ˈhjuːmənaɪz /.  
  

#### vi. If we say Every person should have the sense of humanity, which meaning of ‘humanity’ is applied?

If we say Every person should have the sense of humanity, the quality of being kind to people and animals by making sure that they do not suffer more than is necessary of ‘humanity’ is applied.  
  

#### b. Arrange the following words in alphabetical order.

  

#### i. advance analysis amuse assure allergy attain aid anxiety acute agreement

acute, advance, agreement, aid, allergy, amuse, analysis, anxiety, assure, attain  
  

#### ii. smoke small smart speaking smelling smoothly smuggler smashed smearing smallpox

small, smallpox, smart, smashed,speaking, smearing, smelling, smoke, smoothly, smuggler,  
  

#### iii. terminal terminate terminology termite terms terrace terrible terribly

terminal, terminate, terminology, terms, terrace, terrible, territory, terror  
   

### Comprehension

  

#### Answer the following questions.

  

#### a. Why did the speaker receive thousands of good-wishes cards and gifts from all over the world?

The speaker was shot by Taliban on the left side of her forehead and was hospitalized for her treatment and recovery. So the speaker received thousands of good wishes cards and gifts from all over the world.  
  

#### b. According to the speaker, what are hundreds of human rights activists and social workers struggling for?

According to the speaker, hundreds of human activist and social workers are struggling for achieving their goals of education, peace and equality.  
  

#### c. What has she learnt from Gandhi?

She learnt the value of nonviolence from Gandhi.  
  

#### d. In what sense is peace necessary for education?

Peace is very necessary for qualitative education. Children can obtain fruitful education only if there is peaceful environment. If not then the children are afraid to go to school and furthermore they cannot focus on their study. Hence peace is very necessary for education.  
  

#### e. According to the speaker, what are the main problems faced by both men and women?

According to the speaker, poverty, ignorance, injustice, racism and deprivation of basic rights are the main problems faced by both men and women.  
  

#### f. What is Malala calling upon all governments?

Malala is calling upon all governments to ensure free education for all children all over the world, to fight against terrorism and violence and to protect them from brutality and harm.  
  

#### g. What is the main message of this speech?

The main message of this speech is that education is such a weapon that eradicate the problems like illiteracy, terrorism, violence and harassment of fundamentals rights of human. So, providing education to every children in this world should be the first priority of all governments.  
  
  

### Critical thinking

  

#### a. ‘All children have the right to quality education. How can we ensure this right to every child? Discuss the role of the government and the parents to make sure that every child can attend school.’

Quality education is the demand of 21st century. There is different aspects related to get the quality education. The role of parents, government and schools are the main stakeholders related to a student’s quality education.  
  
**Role of Government**  
– The government can subsidize education in several forms including school fees for low-income parents.  
– Providing equality in education whereby the government ensures that the public schools offer a high quality of knowledge to the learners.  
– A government should ensure that teachers are well paid so that they are motivated in the provision of their services.  
– A government should put in place rules that could check to eliminate outdated syllabus since it misdirects many teachers and learners in schools.  
  
**Roles of Parents**  
– Kids would be easily inspired by what their parents do. So it would be good to be a role model in their learning phase.  
– Parents should talk to their kid about how their school life and different memories associated with it.  
– Parents should help them to be more organized with their daily routine and find enough time for the lessons.  
– Parents should make sure that the kids are provided with a peaceful and pleasant atmosphere at home.  
  

#### b.’Do you think that there is still discrimination between sons and daughters in terms of providing education in our country? What strategies do you suggest to overcome such discrimination against girls?’

Education in Nepal is still in its background phase. While advancements have been made to provide free education through government schools, this education is widely considered to be insufficient in comparison to the education provided by private schools. In comparison to private schools, government schools are less funded and provide a poorer quality of education. Despite this, a large number of girls are enrolled in government schools, while their brothers receive the opportunity for quality, or at least better, education in private schools. This disparity can be traced back to a history of gender based marginalization in the Nepalese society. More significantly, girls are discriminated in terms of education because their parents think that they go to their new homes after their marriage and cannot care their parents when they get older. Finally people still believe that males are physically, mentally and politically strong so that parents send their sons to high quality schools and girls to lower one.  
  
A number of strategies should be applied by all to afford education to the girls to bring about the prosperity of the nation. Some of them are listed below:  
  
1\. Gender biases should be ended.  
2\. Women should be given platforms to be in power and achieve economic success.  
3\. Violence and sexual assault against women should be ended.  
4\. The preference for the son should be removed from the people’s psychology.  
5\. Money given in the name of dowry should be invested in the girl’s education.  
  

#### c. ‘A Chinese philosopher Confucius said, “If your plan is for one year, plant rice; if your plan is for ten years, plant trees; if your plan is for one hundred years, educate children.” What is the meaning of this saying? Elaborate this with examples.’

The sole meaning of above statement is that our destination decide what type of efforts should be applied. If our set our goals and destination big then surely it will take a long time to achieve it. One of the philosophical doctrines presented by the Chinese philosopher before Christ, confusion is the key factor for bringing prolonged dimensional change in the life of people. It is not like planting rice and planting trees whose output appears within a year or within a decade. We have to wait for almost a century to get better result.  
  
Actually the above statement is explaining about the importance of education. At present time people without education is just like a pen without ink. Education shapes our future. It makes us well reputed in the society. Furthermore, without education thinking of a developed country is just like building castle in air. If today’s children are provided a quality education then they will play a crucial role in the development of the nation and create civilized society in future. Therefore, children should be provided with the best possible education as an investment so that it will benefit the whole country in the future.  
  
  

### Grammar

#### Classify the underlined words into different word classes.

  

#### a. The man who is wearing glasses is my uncle’s friend.

who – pronoun  
wearing – verb  
my – determiner  
  

#### b. I bought a round table in the supermarket.

round – adjective  
the – determiner  
  

#### c. Alas, she is dead.

Alas – interjection  
  

#### d. Hari works very hard all the time but his wife is very lazy.

hard – adverb  
wife – noun  
very – adverb  
  

#### e. I have never been to Japan.

never – adverb  
  

#### f. Ann drove a car safely.

Ann – Noun  
Safely – Adverb  
  

#### g. Nobody has claimed it.

nobody – pronoun  
it – pronoun  
  

#### h. She cut her hand with a knife.

her – determiner  
with – preposition  
  

#### i. They have postponed the program because of rain.

have – verb  
because of – preposition  
rain – noun  
  

#### j. Everybody comes to the party.

everybody – pronoun  
  

#### k. All such people ought to be avoided.

all – determiner  
such – determiner  
  

#### l. All of the food has gone.

all – pronoun  
  

#### m. What was that noise?

what – pronoun  
  

#### n. One must not boast of one’s own success.

one – pronoun  
  

#### o. Nobody was there to rescue the child.

nobody – pronoun  
  

#### p. Neither of the accusations is true.

neither – pronoun  
  

#### q. Neither answer is correct.

neither – determiner  
  

#### r. Here is the book that you lent me.

here – adverb  
that – pronoun  
  

#### s. Without health there is no happiness.

without – proposition  
health – noun  
happiness – noun