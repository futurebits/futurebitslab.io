## Unit 11
# Arts and Creations
### Ways with words

  

#### A. Circle the correct meanings of the underlined words.

  
a. Martin Luther started a revolt against the conventions of the Roman Catholic Church.  
**creations**  
  
b. It blended classical styles with new ideas.  
**mixed together**  
  
c. The dome marks the beginning of Renaissance architecture.  
**serves as a sign of**  
  
d. The arts flourished during the Renaissance.  
**grew and improved**  
  
e. The new passion for learning also led to amazing discoveries in science.  
**enthusiasm**  
  

#### C. What do the following idioms mean? Use them in sensible sentences.

  
**a. a hot potato :** a controversial situation  
The racial discrimination issue is a political hot potato.  
  
**b. once in a blue moon :** not very often or rarely  
Dad is working in Japan and he visits home once in a blue moon.  
  
**c. a bed of roses :** easy or comfortable situation or activity  
It’s to be remembered that life is not a bed of roses.  
  
**d. when pigs fly :** used to say that something will never happen  
He plans to clean his house every week, but he will probably do it only when pigs fly.  
  
**e. miss the boat :** miss an opportunity  
If you don’t buy now, you may find that you’ve missed the boat.  
  
**f. zip your lip :** shut up  
She zipped her lip at the meeting yesterday, as it was pointless saying anything.  
  
**g. fight tooth and nail :** to fight with great ferocity  
I want this job so I’m going to fight tooth and nail for it.  
  
**h. when life gives you lemon, make lemonade :** change something negative into a positive  
My grandfather lost his job due to the coronavirus, but he decided to keep busy and active. He says, “When life gives you lemons, make lemonade.”  
  
**i. goose egg :** a score of zero  
We had a good game, but the score was goose egg.  
  

#### D. Match the following phrasal verbs with their meanings.

  
**a. break down :** to stop functioning  
  
**b. check out :** to leave a hotel  
  
**c. fed up :** tired of something or someone  
  
**d. fill out :** to complete a form  
  
**e. get away :** to escape  
  
**f. give away :** to give something to someone for free  
  
**g. give up :** to quit a habit  
  
**h. look forward to :** to wait anxiously for something or an event  
  
**i. make up :** to invent a story or lie  
  
**j. pass away :** to die  
  
**k. put out :** to extinguish  
  
**l. take off :** to remove clothes or shoes from the body, to depart as in airplane  
  

 

### Comprehension

  

#### A. Answer these questions.

  

#### a. What does the word Renaissance mean? Which language is it derived from?

The word Renaissance means ‘rebirth’. It is derived from French word ‘renaitre’.  
  

#### b. What did the Greek scholars do in their new locations after leaving the Greek city?

After leaving the Greek city, Greek scholars taught Greek and shared their precious books in their new locations.  
  

#### c. How did ‘new learning’ teach people to think in different ways?

‘New Learning’ taught people to think in different ways by encouraging them to paint pictures, make statues, buildings and write great literatures.  
  

#### d. Describe the artistic developments of Renaissance in brief.

The artistic developments of the Renaissance first happened in Florence; then they spread to other Italian cities. Trade and banking made people living in cities like Florence, Venice and Milam rich. They spent their money and time to enjoy music, art and poetry. Greatest artists such as Leonardo da Vinci, Michelangelo, Raphel, Filipo, Brunelleshchi, Shakespeare, and Cervantes produced their master pieces. William Shakespeare became one of the greatest literary figure at that time.  
  

#### e. Name the three Renaissance scientists who made great discoveries?

Name of the three Renaissance scientists are Galileo Galilei, Nicolas Copernicus and Sir Isaac Newton.  
  

#### f. Who developed the printing press? Which country was he from?

Johannes Gutenberg developed the printing press. He was from Germany.  
  

#### g. Why is the development of the printing press considered as one of the gifts of Renaissance?

Before development of the printing press, books were hand written and expensive. The development of the printing press is considered as one of the gifts of Renaissance because it helped writers to publish their books at low price.  
  

#### h. Name the five famous explorers of the Renaissance?

The five famous explorers of the Renaissance were Christopher Columbus, Vasco de Gama, John Cabot, Ferdinand Magellan and Sir Francis Drake.  
  

#### i. Why do some people think of the Renaissance as the beginning of modern history?

The Renaissance brought revolution in art, architecture, literature, culture, navigation, science etc. That is why people think thank that the Renaissance is the beginning of modern history.  
  

#### B. What do you infer from the reading? Tick (√) the best answer.

  
**a. Before the Renaissance…**  
education was limited to scholars and privileged  
  
**b. Which statement is true?**  
The Greeks had a strong influence on Renaissance thinking  
  
**c. What did the Renaissance do?**  
It caused people to make changes in their lives  
  
**d. What were the major causes of the Renaissance?**  
printing, reading and learning  
  

### Critical Thinking

  

#### a. Do you agree with Machiavelli’s view that a good leader can do bad and dishonest things in order to preserve his power and protect his government? Explain.

Yes I agree with Machiavelli’s view that a good leader can do bad and dishonest things in order to preserve his power and protect his government. Machiavelli was a politician and writer. Machiavelli reveals several facts that how a good leader can turn into immoral and evil. He says that in order to maintain power and protect nation or territory, a leader makes a number of mistakes.  
  
It’s the duty of a leader to protect his government. So he can go beyond the rules and perform dishonest things to preserve his power. But the leaders whose goal is the pursuit of power, unlimited wealth, or the fame that accompanies success often appear self-centered. This desire is so strong that the leaders violate the moral standards that once governed their conduct. Many leaders reach the top by imposing their will on others, or even destroying those who stand in their way. When they reach the top, they may suspect that others are trying to knock them off their base and hence perform some out of the rules things to protect their leadership.  
  

#### b. Do you think that art and literature are important assets of a country? Give reasons.

Art and literature are the foundation of life. It places an emphasis on many topics from human tragedies to tales of the ever-popular search for love. While it is physically written in words, these words come alive in the imagination of the mind, and its ability to comprehend the complexity or simplicity of the text.  
  
Art and literature serves the people, and help them in their struggle for a better life, by arousing the people’s emotions against oppression and injustice and increasing their sensitivity regarding the people’s sufferings. Our country is facing the tremendous challenge of abolishing poverty, unemployment, inflation, ignorance, casteism, communalism, and other social evils, and hence artists and writers must join the ranks of those who are struggling for a better Nepal, they must inspire the people by their writings, and write against oppression and injustice. Thus we can say that art and literature are important assets of a country.  
  

### Writing

  

#### a. Write an essay on “Literature is the reflection of society.”

Literature is the Reflection of Society

Since the dawn of man, the art of storytelling was utilized to pass on critically deemed information about society, life, and everything. During the early days, much of our history was transposed orally through song and spoken word. Literature, like many art forms, is expressionism. It’s taking a thought and converting it into a tangible object that can be interpreted by others.  
  
Many authors have been represented social, political, ecological, historical, scientific phenomenon in their works. All our glorious past has secured in the literary pages so that today’s generation gain knowledge about the myth of The Ramayana, The Mahabharata, The Old Testament and The Bible, etc. it is only possible by the literary sources that present generation know who were our forefathers and how human came into existence.  
  
Thus, society creates literature because all traditions, customs, religious stories reflect through the literary works, but the excellence and natural surroundings of the reflection hinge on a writer’s approach of mind, whether he is enlightened in his outlook or conservative. That’s why literature is the mirror of society because it stuffed with all human emotions and activities including good and bad and social and political happenings.  
  

#### b. It is said that today’s reader is tomorrow’s leader. Do you agree with this statement? Explain.

Yes, I agree with the statement that today’s reader is tomorrow’s leader. Undoubtedly, reading is essential for all-round development of the personality. Reading is an essential basic skills building activity. One’s language fluency is determined by the quality of reading. Besides, vocabulary enrichment, ideas collection, familiarization with different types of writing formats, speaking fluency, etc, all depend upon reading.  
  
It is true a person who has the reading habit, never feels lonely and bored. The books open up a magical world for him or her and he lives among angels in the pages of the books. Books never let go the readers astray. They make them wise and pragmatic. We must endeavour to develop this fabulous habit of reading.  
  
Thus, one thing is absolutely clear, if we want to become great leaders in future, we must be great readers in the present.  
  

#### c. Write a short biography of a national literary, artistic or historical figure you appreciate most.

Bhanubhakta Acharya

The first Nepali poet who also translated the great epic ‘Ramayana’ from Sanskrit to Nepali, Bhanubhakta Acharya was born on 29 Ashar, 1814 in Tanahu district of Nepal. Acharya was born to a Brahmin family and received excellent education with a strong leaning towards religion from his grandfather at home. He is honored with the title Adikabi for the contributions he has made in the field of poetry and Nepali literature and every year, his birthday is celebrated as a festival of Bhanujayanti (13 July) by conducting various programs, usually academics and poem recitation.  
  
He wrote two masterpieces in his life among which, one is the Bhanubhaktey Ramayan and the other is a letter he wrote in verse form to the Prime Minister while he was in prison. He was made a scapegoat and sent to prison due to some misunderstanding in signing the papers. His letter became one of his great works. He not only won his freedom with his poem but was also given a bag of money. When he died in 1868, he did not know he would one day be one of the most revered poets of Nepal. His creation, however, was not published and he died without receiving credit for his contribution. His works were published by Motiram Bhatta in 1887. Although he is one of the most celebrated and revered poets of Nepal, his works are not as famous as other poets in the history of Nepali literature.  
  

### Grammar

  

#### B. Complete the following sentences with correct relative pronouns and write whether the clauses are defining or non-defining.

a. That’s the house where I was born.  
  
b. My aunt Nita, whoa journalist, is coming to visit next week.  
  
c. People wholike outdoor activities will love our holidays.  
  
d. The early 1960s, when the Beatles first started, was a very exciting time in pop culture.  
  
e. My essay on Shakespeare, which I found quite difficult, got a really good mark in the end.  
  
f. The Mayans, who lived in Central America, built many stunning temples.  
  
g. That’s the girl whose brother is in your class.  
  
h. The snake that was in the garden last week belongs to our next-door neighbor.  
  

#### C. Join the following pairs of sentences with an appropriate relative clauses.

  

#### a. He is a musician. His albums have sold millions.

He is a musician whose albums have sold millions.  
  

#### b. Amelia speaks English and Chines fluently. She is from Shanghai.

Amelia who is from Shanghai speaks English and Chines fluently.  
  

#### c. That’s the stadium. Real Madrid plays there.

That’s the stadium where Real Madrid plays there.  
  

#### d. Dublin is my favourite city. It is the capital of Ireland.

Dublin is my favourite city which is the capital of Ireland.  
  

#### e. The person was really helpful. They spoke to him.

The person to whom they spoke was really helpful.  
  

#### f. This smartphone takes great photos. I bought it last week.

This smartphone takes great photos whioch I bought it last week.