## Unit 4
# History and Culture
### Ways with words

  

#### A. Match the words with their meanings.

  
**Brevity** ➜ lasting only for a short time  
  
**Conquest** ➜ victory over a place or people by use of military force  
  
**Resemblance** ➜ the state of being alike  
  
**Barbarian** ➜ a member of an uncivilized group of people of culture  
  
**Ally** ➜ one state united to another by a treaty or a league for a military purpose  
  
**Nobility** ➜ state of being noble in character, quality, or rank  
  
**Ruthlessness** ➜ character of having no pity or compassion  
  
**Decimation** ➜ the killing or destruction of a large number of a population  
  
**Harsh** ➜ cruel  
  
**Elixir** ➜ liquid that is believed to cure all ills  
  

#### B. Find the following words in the text and use them in sentences of your own.

  
**Invasion**  
The people live under a constant threat of invasion.  
  
**Disregard**  
The present article will disregard this distinction.  
  
**Paramount**  
His happiness was of paramount importance.  
  
**Populace**  
They had the populace on their side.  
  
**Revolt**  
The peasants’ revolt was crushed by the king.  
  
**Assassination**  
He was in constant fear of assassination and distrusted all around him.  
  
**Serfdom**  
It has reduced us politically to serfdom.  
  

#### D. Consult a dictionary and prepare a list of ten homographs.

  
**Bass** – a type of fish/low, deep voice  
**Bat** – a piece of sports equipment/an animal  
**Evening** – smoothing out/after sunset  
**Bear** – to endure/animal  
**Close** – connected/lock  
**Lean** – thin/rest against  
**Bow** – bend forward/front of a ship  
**Lead** – metal/start off in front  
**Skip** – jump/miss out  
**Fair** – appearance/reasonable  
  

### Comprehension

  

#### B. Answer these questions.

  

#### a. Why is the Zhou Age called a feudal age?

The Zhou Age is called a feudal age because Zhou period had followed the rule of feudalism.  
  

#### b. What is the location advantage of the Qin?

The location advantage of Qin is the territory or area of dynasty is well bounded by mountains.  
  

#### c. What contributed to the success in the warfare as described in the text?

The location of the state, their size of the army, and their expert use of the chariot contributed to their success in warfare.  
  

#### d. Why did the Qin invite the foreign advisor, Shang Yang?

The Qin invited the foreign advisor, Shang Yang due to the lack of intellectual peoples in Qin.  
  

#### e. What were the key features of the Qin political system?

The key features of the Qin political system were fear and control.  
  

#### f. How were the people treated during the Qin Period?

The people were treated almost like slave in that period. People were supposed to contribute in all projects for welfare of Nation and were deprived from the approach of education.  
  

#### g. Why did Shi Huangdi stop educating ordinary people?

Shi Huangdi stopped educating ordinary people because he believed that illiterate people can be controlled easily.  
  

#### h. What did the Qin achieve by the legalism in practice?

The Qin achieved superior army, disciplined populace, obedient peoples, the great wall, great canal etc. by the help of legalism.  
  

#### i. Why did Shi Huangdi never sleep in the same room for two consecutive nights?

Shi Huangdi never slept in the same room for two consecutive nights because of constant fear of assassination.  
  

#### j. What are the everlasting marks of the Qin Dynasty?

The everlasting marks of the Qin Dynasty are as follows:  
1\. The Terracotta Army  
2\. Concept of Legalism  
3\. The Great Wall of China  
4\. The Grand Canal  
5\. Roads.  
  

### Critical thinking

  

#### a. The Great Wall and the Taj Mahal are the creation of the autocratic rulers. Present your view for or against this statement.

Yes, I believe that the Great Wall of China and the Taj Mahal were created by autocratic rulers. The Great Wall of China was built to protect the northern empire of China from enemy attacks. There are nineteen walls. The first wall was built in the 7th century BC.  
  
The Great Wall of China cannot be built by individual effort. As punishment, people were forced to undertake such projects by autocratic rulers. The text mentions that people throughout the empire must share responsibility with each other. If a person did not behave according to the rules, others had to contact them. If they did not, they were dismembered or beheaded. Those who made the greatest contribution to the state were highly rewarded, while those whose lives were deemed inconsequential were used as slaves in Shi Huangdi’s construction projects such as the Great Wall of China, the Grand Canal, and the streets that grew up in the simple trade and travel.  
  
Likewise, Taj Mahal is the result of the autocratic ruler of the Mughal Emperor of India, Shah Jahan. It is believed that he built it to immortalize his wife Mumtaz. Shah Jahan is said to have cut off the artists’ hands to prevent them from recreating the same type of structure. Historical facts show that the king was more inconsiderate than romantic. It took 20,000 workers to build the Taj Mahal over 20 years.  
  
Everyone remembers the names of the emperors who built the structures, but no one is eager to explore the reality behind the structures. Therefore, The Great Wall and the Taj Mahal can be considered as the creation of the autocratic rulers.  
  

#### b. How do you describe the pros and cons of feudalism?

Feudalism is defined as a dominant social system that existed in Europe during the Middle Ages in which people worked and fought for nobles who gave them protection and the use of land in return.  
  
**Pros of feudalism**  
Feudalism helped protect communities from the violence and warfare that broke out after the fall of Rome and the collapse of strong central government in Western Europe. Feudalism secured Western Europe’s society and kept out powerful invaders. The Lords were the major persons who keep away people’s worries. Feudalism helped people to develop the qualities like loyalty, bravery, generosity, and humility.  
  
**Cons of feudalism**  
The concept of feudalism used to divide poor and rich people into two classes. It created class divisions among people. Common and weak people had to be dependent upon the Lords. Easy environment for powerful people to impose their power over others. Lords could easily do whatever they wanted. Peasants had to survive under the lords and they were not allowed to leave their lords’ land. They were restricted in movement and even daily activities without their lords’ permission.  
  

### Writing

  

#### a. Write an email to your friend living abroad stating the contributions of Prithvi Narayan Shah in the unification of Nepal.

  
**To:** ariana23@gmail.com  
  
**From:** austinadam56@gmail.com  
  
**Subject:** Contributions of Prithivi Narayan Shah in the Unification of Nepal  
  
Following the recent discussions on the above subject, here are more clarifications on the immeasurable contributions that Prithivi Narayan Shah brought to ensure that Nepal is what it is today.  
  
Shah ended the divisions and infighting that existed between the four major Nepalese principalities of Gorkha, Malla, Patan, and Bhadgaon. Though the prince was a very ambitious member of the Shah ruling family of Gorkha, his personal involvement brought the wars among Malla, Patan, and Bhadgaon to an end.  
  
He did not stop there. He resolved their long-term differences and consolidated them to become modern-day Nepal. Shah also worked hard to strengthen the unity of Nepal. This unity ended the many years when foreigners could easily invade any of the small states to conquer them.  
  
No doubt, you will appreciate that this was not a mere achievement. While others were dividing nations, he united. While others conquered nations for their selfish interests, he strengthened Nepal for national unity.  
  
From this brief email, you will appreciate that Shah made lasting contributions to the Nepal we can call home today.  
  
From your friend,  
Austin  
  

### Grammar (Past Tense)

  

#### B. Complete the texts below using the correct past forms of the verbs from the bracket.

a. The Maya established a very advanced civilisation in the jungles of the Yucatan. However, their culture **disappeared** (disappear) by the time Europeans first **arrived** (arrive) in the New World.  
  
b. When I **turned** (turn) on the radio yesterday, I **heard** (hear) a song that was popular when I **was** (be) at the basic level of my study. It **took** (take) me back to some old memories.  
  
c. I was looking for a job. I **applied** (apply) for a job last week. Fortunately, I **got** (get) it and now I am a job holder.  
  
  

#### C. Choose the correct words from the list and complete the sentences with the correct verb form.

a. Newton **saw** an apple falling from the tree.  
  
b. Ramesh Bikal **wrote** many popular stories.  
  
c. My jacket is expensive. It **cost** me Rs 5000.  
  
d. She **got up** early in the morning yesterday.  
  
e. When I was small, my father **taught** me at home.  
  
f. Why did you **throw** the cap away?  
  

#### D. Put the verb into the correct form and complete the sentences.

a. I went to see the film, but I **did not** it. (not/enjoy)  
  
b. I **met** Rojina in town in a few days ago. (meet)  
  
c. It was very warm, so I **took off** my coat. (take off)  
  
d. Though the bed was very comfortable, I **did not sleep** very well. (not/sleep)  
  
e. I **started** new job last week. (start)  
  
g. He **was** too busy in the office yesterday. (be)  
  
h. Nita **invited** her to the party, but she didn’t come. (invite)