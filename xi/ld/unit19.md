## Unit 19
# Travel and Tourism
### Ways with words

  

#### A. Find the words form the text which mean the following.

a. became fuller and softer by shaking – **fluffed**  
  
b. sound mental health – **sanity**  
  
c. consisting of or easily breaking into small pieces – **crumbly**  
  
d. neither very hot nor very cold – **temperate**  
  
e. land that does not have enough water to support the growth of plants – **arid region**  
  
f. only slightly warm – **lukewarm**  
  

#### B. Find the meanings of the following words in an English dicitonary, write their word classes adn use them in your own sentences.

  

#### a. hazy (adjective) :

not clear, especially because of heat  
The mountains were hazy in the distance.  
  

#### b. frontier (noun) :

a line that separates two countries, etc.; the land near this line  
Many people travelling across the frontier were illegal immigrants.  
  

#### c. lush (adjective) :

covered in healthy grass and plants  
He glanced at the lush green hills.  
  

#### d. flipflops (noun) :

open shoes  
She took off her flipflops before crossing the stream.  
  

#### e. altitude (noun) :

height above sea level  
My friends are flying at an altitude of 10000 m.  
  

#### f. fatal (adjective) :

causing death  
If her sister gets ill again it could prove to be fatal.  
  

#### g. magnificent (adjective) :

extremely attractive and impressive  
Pashupati temple looks magnificent in the evening.  
  

#### C. make a list of the Nepali words used in the text and write their English equivalents.

bideshi : foreign  
Tatopani : lukewarm water  
raksi : wine  
bahini : younger sister  
mathi : up  
tala : down  
sutne : sleep  
panipuri : water balls/water bowels  
pani – water  
  
 

### Comprehension

  

#### Answer these questions.

  

#### a. How did the author feel when she saw a colourful rooster in the western Himalayas?

The author felt amused when she saw a colourful rooster in the western Himalayas. Furthermore she felt the roster glancing her for a brief moment and winked at her.  
  

#### b. How does she describe the houses on the trails of western Nepal?

She describe the house on the trails of western Nepal as made by the stones under a rock over a river. They looked too crumble to live in but people were comfortable with them.  
  

#### c. What does she mean when she says "I walked towards a village15 kilometres further as the crow flies?"

When she says "I walked towards a village 15 kilometres further as the crow flies" she means the distance she had to cover was measured between two places.  
  

#### d. Describe the village Thankur in brief.

The village Thankur have had only two houses. The village is located in an arid region. There is not much human mobility. Shiny and rainbow colored large birds called Himalayan monal are found there.  
  

#### e. What happened when she reached Jumla and how was she relieved?

When she reached Jumla she was ill with an unknown virus and a bad tummy. She was relieved after having medicine and pani puri.  
  

#### f. How does the author describe the bucket water in Khali Lagna?

The water surface on the bucket was frozen. When she cracked it, she felt satisfied as the water underneath was surprisingly lukewarm.  
  

#### g. How was Rara Lake on the day she reached there?

The water in Rara Lake was glistening in its dark blue glory when she reached there.  
  

### Critical Thinking

  

#### a. It is said that travelling a place equals to reading three books. Do you agree? Why?

I totally agree with the saying that travelling a place equals to reading three books. The world is a great book, of which they who never stir from home read only a page. It is a well established fact now that travelling gives a character to our knowledge. While travelling we have firsthand knowledge about the place we are travelling. We actually see the place with our own eyes, and its memory stays with us throughout our life. For example reading about a place may be entertaining and appealing, but visiting it in person will surely be thousand times more entertaining and gratifying.  
  
Traveling is wonderful in many ways. As we travel we meet more new people, cultures, and lifestyles. With all the newness in our life, we are also opened to new insights, outlook, and perception of the world and living, which often gives us a new purpose for our lives. While travelling, we have to be prepared for unplanned emergencies and schedules. We have to make arrangements for our travelling, lodging and boarding. This makes us more organized and self-reliant. Thus, travelling makes the inhabitants interconnected. We start feeling ourselves as members of one big family of mankind living on one home planet. This makes life more wonderful.  
  
In conclusion it can be said that travelling is one of the most precious gifts of life, and we must relish it and enjoy its sundry life-transforming benefits.  
  

#### b. The author spent five-week long walk along the western Himalayas. Do you think it was adventurous? Why?

Yes, it was adventurous because she had covered a distance that totals up to 25 percent of the Nepal Himalaya via the lower Great Himalayan Trail, along with some funky detours. She experienced places where few tourists go. She chose the Dhaulagiri Circuit. She witnessed and experienced as medieval and raw. She learnt to survive the wild western region with primal instincts, human intuition, and oneness with nature. She was able to break the monotony of life and work. Life is a mad rush but she became to able to escape it to participate in adventure from one place to another, from one activity to another, trying to gather as much as possible. Trekking through glaciers, crossing rivers, walking through arid regions, spending cold nights at high altitude, walking in the snow being frozen is a challenging task. That’s why this journey is really and adventurous one.  
  

### Writing

  

#### A. Write a travelogue of your recent visit to a natural/religious place in about 300 words.

With the objective to learn through observation rather than rote learning, we the students of grade-11 visited Pashupati area enlisted in the world heritage site. Pashupati area is located on the bank of the holy river Bagmati in the eastern part of Kathmandu district. There are places like Gaushala, Pingalastahan, Jay bagesori, Guhesori, Deupatan, Gaurighat, etc including the holy Pashupatinath temple in this area. There are about 500 different temples, shrines and 1000 shivalingas in this area. The main temple of Pashupatinath has been constructed in Pagoda style.  
  
The panchamukhi jyotirlinga kept inside Pashupati temple was established by the person called Nep during the Gopal dynasty. After the king Prachanda dev, Dharma dutta, Queen of shiva singh; Ganga devi and Prapta malla constructed and reconstructed the temple. Evidence is found that even the Shah kings and Rana prime ministers also contributed in its reconstructions.  
  
Large fair is held on the occasion of Shivaratri Teej, Bala Chaturdashi, Janai Purnima as well as every Monday of Shrawan. It is found that thousands of pilgrims visit this temple during these occasions from inside and outside the country. The number of pilgrim is not less on other days as well. They visit the Pashupatinath temple as it is considered the holiest temple of Hindus.  
  
The housings, huts, factories and industries constructed encroaching the land of Pashupati trust should be removed from their while afforestation program and construction of religious places should be launched. It is necessary to pay special attention in environmental sanitation. The government has to take responsibility in keeping the record of the income and expenditure of the temple. Besides, if the sewage can be managed on the bank of Bagmati river the importance of this area would increase even more.  
  

#### B. Write an essay in about 500 words on ‘Importance of Tourism in Nepal’.

Nepal is known for being one of the most famous travel destinations in the world. With thousands of tourists visiting the county every year, Tourism in Nepal has soared to new heights. The capital city of the country, Kathmandu and the city of lakes, Pokhara are the top cities in the country to see a sharp increase in the number of tourists in recent years. From UNESCO world heritage sites to different ancient landmarks, from a diverse ethnic community to experiencing different cultures and traditions, all of which have been successful to catch the interest of foreigners to visit the country of Nepal.  
  
Tourism heavily influences the working of Nepal’s government. Tourism is the number one source to generate foreign currency in Nepal. As the tourism sector grows and more foreigners spend their time in Nepal, the inflow of the currency directly uplifts the working of the country. As Nepal imports more goods than exporting its local products, the foreign currency is the highest number two source to generate money for the country to compete internationally.  
  
Foreigners take a keen interest in handicraft goods made from the local products using raw material available in the country. Goods like carpers, status, portraits are at a high demand by travelers. This helps the community to grow. It also expands the popularity of the Nepalese people’s handiwork throughout the world.  
  
When foreigners visit the country, they get to experience the local culture and traditions. Historical site visits and observing the art and architecture of famous UNESCO world heritage sites and landmarks help the country to spread its religious norms and values throughout the world. Tourism helps us to spread our influence in the world. Names of sites like Pashupatinath, Boudhanath, Muktinath, Swayambhunath, Lumbini, Kathmandu Durbar Square, Bhaktapur Durbar Square, Patan Durbar Square, Davis Falls, Phewa Lake, and more have been successfully spread in the world. This has led to an increase in tourist inflow to watch visit these places and its surroundings. This helps to generate easy income for people around the landmarks.  
  

### Grammar

  

#### B. Complete these sentences using the verbs given in the brackets.

a. Letters **are delivered** by the postman every day. (deliver)  
  
b. This bag **was found** in the bus yesterday. (find)  
  
c. The gate **is locked** at 7:00 pm every evening. (lock)  
  
d. I **was invited** to the party last week. (invite)  
  
e. The telephone **was invented** by Graham Bell. (invent)  
  
f. Muna Madan **was written** by Devkota. (write)  
  
g. How much money **was stolen** in the robbery? (steal)  
  
h. Do you know cheese **is made** from milk? (make)  
  
i. I was born in Kathmandu, but **was grown** in Dhangadhi. (grow)  
  

#### C. Change the following sentences into passive.

  

#### a. I didn’t fix the problem.

The problem wasn’t fixed by me.  
  

#### b. Police protect the town.

The town is protected by police.  
  

#### c. John’s mother raised him in a small town.

He was raised in a small town by John’s mother.  
  

#### d. Someone painted the building last year.

The building was built last year.  
  

#### e. Alexander Fleming discovered penicillin in 1928.

Penicillin was discovered in 1928 by Alexander Fleming.  
  

#### f. Some students study grammar on the Internet.

Grammar is studied on the internet by some students.  
  

#### g. Someone had broken the window by 3:00 p.m.

The window had been broken by 3: p.m.  
  

#### h. A strange man was watching us.

We were being watched by a strange man.  
  

#### i. Tokyo will hold the Olympics in 2020.

The Olympics will be held in 2020 by Tokyo.  
  

#### j. We are working on the report right now.

The report is being worked in right now by us.  
  

#### k. My manager has told him to arrive earlier.

He has been told to arrive earlier by my manager.  
  

#### l. They could not have made the mistake.

The mistake could not have been made by them.  
  

#### m. I hope they are going to hire me soon.

I hope I am going to be hired soon by them.  
  

#### n. I don’t like people staring at me.

I don’t like being statered at.  
  

#### o. She likes people waving at her.

She likes being waved at.  
  

#### p. Who told you the story?

By whom were you told the story ?  
  

#### q. Is he repairing the bicycle?

Is the bicycle being repaired by him?