## Unit 12
# Fantasy
### Ways with words

  

#### A. Find the meanings of the following words and phrases from a dictionary and make sentences by using them.

  
**a. peep into :** to get a quick look into something  
He peeped into the box and saw that his wife had gotten him a new watch.  
  
**b. pop down :** to go somewhere quickly  
I’ll try to pop down tomorrow evening after dinner.  
  
**c. remarkable :** unusual, surprising, excellent or worthy of notice  
She has made remarkable progress.  
  
**d. hedge :** a row of bushes or small trees planted close together along the edge of a garden, field or road  
Buying a house is the best hedge against inflation.  
  
**e. wonder :** desire to know something  
I wonder what the world is doing now.  
  
**f. tumble :** to fall quickly without control  
He took a tumble in the hay with the farmers’ daughter.  
  
**g. doze off :** to sleep, especially during the day  
He was just beginning to doze off when the telephone rang.  
  
**h. earnestly :** seriously  
Even when I studied most earnestly it seemed more like play than work.  
  
**i. tiny :** very small  
He made a tiny hole in the paper.  
  
**j. creep :** to move slowly, quietly and carefully in order to avoid being heard or noticed  
She crept toward the edge of the roof and looked over.  
  

#### B. Match the words below with their opposites.

a. beginning = ending  
b. stupid = clever  
c. natural = artificial  
d. disappointment = happiness  
e. ignorant = educated  
f. anxiously = calmly  
  


### Comprehension

  

#### A. Answer these questions.

  

#### a. What did Alice do while her sister was reading a book?

Alice felt asleep while her sister was reading a book.  
  

#### b. Why did Alice run across the field after the Rabbit?

Alice ran across the field after the Rabbit because of her curiosity to know about the rabbit which she had never seen before.  
  

#### c. Why didn’t she like to drop the jar? What did she do with it?

She didn’t like to drop the jar because she was afraid of killing somebody underneath. She put it onto one of the cupboards.  
  

#### d. What idea came to her mind when she saw a tiny golden key?

When she saw a tiny golden key, the idea of opening one of the doors of the hall came to her mind.  
  

#### e. What was written on the bottle that she found? Did she follow what it said?

“DRINK ME” was written on the bottle that she found. Yes, she followed what it said.  
  

#### f. Alice was fond of pretending to be two people. Who were they?

They were Alice and herself.  
  

#### g. Why did she want to eat the cake that she found?

She wanted to eat the cake that she found because it may make her grow bigger.  
  

#### B. Put these sentences in the right order as they happen in the story.

f. Alice saw a White Rabbit and ran after him.  
c. Alice fell down a rabbit hole.  
b. Alice found a small key and unblocked a very small door.  
d. Alice drank something from a bottle and got very small door.  
e. Alice tried to climb a table leg to get the key again.  
a. Alice ate a small cake, which said “EAT ME”  
  

### Critical thinking

  

#### a. “Down the rabbit hole” is a sort of writing called fantasy. On the basis of your reading of the story point out some special elements of this kind of writing?

The word fantasy refers to an illusion or an imaginative idea. The fantastic, a related word, also describes something that is imaginary and unrealistic. As its name suggests, the literary genre of fantasy refers to stories filled with imaginary and unrealistic elements.  
  
Events in a fantasy operate outside the laws of the real universe and typically involve supernatural elements, like magic or magical creatures. Unlike science fiction, which is generally set in the future, fantasy is commonly set in the past. Fantasy stories often contain elements of medieval life, such as castles, knights, kings, magical swords, and references to ancient spells. Characters in fantasy stories often live in a pre-industrial setting with limited technology, other than the advantages of magical power.  
  
The basic elements of fantasy have existed for centuries, and fantasy has roots in ancient myths, legends, and fairy tales. The course toward modern fantasy, however, began in the Victorian era when writers began developing their own distinct fantastical worlds, rather than building on established cultural tradition.  
  

#### b. Is it good to imagine of things which are not possible to achieve in reality? Explain.

Imagination is the ability to form mental images of things, ideas or stimulation with no input from the five senses. It’s your imagination that allows you to fantasize or dream of something you want that is not in your reality yet.  
  
Most poeple use their imagination to build pictures of a better lifestyle or what they want to hace in life and to predict something positive or negative that may happen. As we practice deliberately using our imagination, it will enhance our creative abilities, which means we will have better thoughts about our life. It helps to develop visions of what is possible, which means it will give clearer picture of our needs and the life we want to live. It will build up more self-confidence and improve our health and wellness by reducing stress. It helps to cut through mental clutter and noise.  
  
Using imagination makes it possible to experience reality in a world inside our mind. A well-developed imagination will make us a powerful creator and allow us to shape the future that we want. Hence it is good to imagine of things which are not possible to achieve in reality.  
  

#### c. Do you talk to yourself when you are in a trouble? If yes, how does it help you?

Yes I talk to myself when I am in a trouble. I talk to myself everyday! I don’t know exactly since how long I have been doing that but all past years that I recall I admit I have done it. And guess what! I love to.  
  
It helps me in many ways. I have so much to tell myself and maybe things that I wouldn’t say to anyone. Nobody understands me better than myself. I can just say anything and everything and not be judged! I think self-talk is good as it doesn’t require any effort unlike maintaining diaries etc. Self-talk happens mostly when I’m thinking through ideas, when debating decisions, or when I need of a pep talk. I feel better doing self-talk as it gives me feel of presence of someone around me. It helps me to make decisions more easily and motivate me to do things I may be putting off. To sum up, I do self-talk to work through extreme emotions – including anger, sadness, confusion and stress – and to sort out personal conundrums.  
  

### Writing

  

#### b. Describe a strange dream that you have seen recently.

I was 10 when this happened. One night, when I had gone to bed, I saw a dream. In a dream I woke up to hear a noise and looked to my open bedroom door, where I saw a dark figure staring at me with red eyes. Thinking that it was my mother, I had called out to her multiple times, each time getting louder and louder. By the third time I called out to her, her red eyes did not move and I was getting frantic, screaming out to my mom and hiding beneath my covers.  
  
Come to find out my mother was in the living room fast asleep and had woken up from my calls, running into my room. When I looked at her, the black figure had gone and I have not seen it since. But I guess I had always been afraid after that of what might be watching me or threatening to harm me at any given moment. I know that was a demon it was too dark and felt too evil.  
  

### Grammar

  

#### B. Express your wishes in the following situations in three different ways. Use I wish/If only…….

  

#### a. You don’t have a mobile phone (You need one).

– I wish I talked to my father about it.  
– I wish I could buy one myself.  
– I wish somebody would gift me one.  
  

#### b. You don’t know the answer of a question from the lesson.

– I wish my frend told me the answer.  
– I wish my teacher would help me.  
– I wish I could read the lesson properly.  
  

#### c. You can’t play the guitar.

– I wish I brought a guitar lesson book.  
– I wish someone would teach me guitar chords.  
– I wish I could go to the guitar expert’s.  
  

#### d. It’s cold.

– I wish I drank hot water.  
– I wish my mother would burn some firewood.  
– I wish I could stay inside the house.  
  

#### e. You are feeling sick.

– I wish I were healthy.  
– I wish somebody would call a doctor.  
– I wish I could go to hospital.  
  

#### f. You live in a crowded city.

– I wish I lived in the country.  
– I wish the city would be peaceful.  
– I wish I could spend a couple of weeks far from the city.  
  

#### g. You feel lonely.

– I wish I listened to my favourite music.  
– I wish my friend would phone me.  
– I wish I could do the household works.  
  

#### C. Rewrite the following sentences making correction if necessary.

  

#### a. I wish my father bought me a bike.

I wish my father bought me a bike.  
  

#### b. I wish I would write poems.

I wish I could write poems.  
  

#### c. I wish I could remember her name.

I wish I could remember her name.  
  

#### d. I wish I had a god job.

I wish I had a god job.  
  

#### e. I wish I would be rich.

I wish I were rich.