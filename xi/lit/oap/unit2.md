## Unit 2
# A Sunny Morning
### Characters

Dona Laura, a handsome old lady of 70  
Don Gonzalo, an old man of 70  
Petra, maid of Laura  
Juanito, servant of Gonzalo  
Place: A park of Mardid (Spain)  
  

### Introduction

The one-act play, “A Sunny Morning – A Comedy of Madrid” is a hilarious comic play written by Serafin and Joaquin Alveraz Quintero, popularly known as the ‘Golden Boys of Madrid Theatre’. The entire play revolves around two major characters Don Gonzalo and Dona Laura, who in their youth were passionate lovers but estranged by the cruelty of the fate. After several years of their separation now in their 70’s both Don Gonzalo and Dona Laura accidentally meet in a park of Madrid but they are not able to recognise each other. As they begin to talk they slowly drifted to recall their past and soon they realise individually that they were passionate lovers of the past. However, they are unwilling to disclose their real identity and introduced themselves as friends of those lovers.  
  

### Summary

The play opens in the setting of a park where Dona Laura enters the park with the help of her maid, Petra and finds place to sit on a bench to feed the birds with breadcrumbs. Meanwhile, Don Gonzalo also enters the park with his servant, Juanito and hesitates to sit on other side of the bench, which was already occupied by Dona Laura. He even curses the priests for being occupied his bench on which he usually sits in every sunny morning. He looks negatively at every aspect of his life and goes on complaining whereas Laura expresses her grievances when others disturb her. In the beginning both of them are reluctant at each other but soon they tried to understand each other in the progress of their conversation.  
  
Sharing a pinch of snuff between them softens their tone into friendliness. Soon their conversation stumbles upon reading books. This paves the way for the next level of their story. Dona Laura reads a poem from a book given by Don Gonzalo, which surprises them, that they were the lovers of several decades before and now talking about themselves. However, they do not want to disclose their true identities since they have lost seen each other in early youth. So, they spin fictitious stories where Laura identifies herself as a friend of ‘The Silver Maiden’ Laura Llorente while Don Gonzalo identifies himself as the cousin of Don Gonzalo.  
  
Laura Llorente lived at Maricela in Valencia. She was known as ‘The Silver Maiden’ in her locality. Gonzalo would pass by on his horseback every morning under Laura’s house window and would toss a bouquet of flowers up to her balcony. In the afternoon, while he would return by the same path and catch a bouquet of flowers that she would toss him down. When Laura’s parents wanted to marry her to a merchant a duel was followed and the merchant was badly wounded by Gonzalo. Don Gonzalo fled away fearing of the consequences. Laura waited for days and months and not hearing from him for a long time she left her home one afternoon and went to the beach. While she was engrossed in Don Gonzalo’s thoughts she was washed away by the waves.  
  
Don Gonzalo also tried to spin his version of a story about his supposed cousin. Gonzalo also loved Laura intensely. After injuring the merchant seriously, fearing the consequences, he took refuge in Seville and Madrid. He wrote many letters to her but her parents seized them. As there was no reply, in despair, he joined the army and went to Africa where he died in one of the trenches holding the flag of Spain and muttering the name of his love, Laura.  
  
In reality, after two years of their separation, Laura married someone and settled down in her life. Similarly, Gonzalo disappointed over his lost love, three months later he too married a ballet dancer and settled down in Paris. Though, they were separated, in their hearts the yearning for the romantic love continued. When they met in the park after nearly 50 years both of them were able to recall their intense romantic affair. Although they came to know about each other in reality, they did not want to reveal, as they had lost their charming youth. Thus the play “A Sunny Morning entertains the audience.  
  

 

### Understanding the text

  

#### Answer the following questions.

  

#### a. What makes Dona Laura think that Don Gonzalo is an ill-natured man? Why do neither Dona Laura nor Don Gonzalo reveal their true identities?

Gonzalo looks at everything negatively. He is reluctant to sit on the bench of Laura and insists that he wants his own bench. When he sees his usual bench occupied by the priests already, he is angry and he curses them. He asks his servant, Juanito to drive them away. Gonzalo scares the pigeons that Laura was feeding. Because of all these reasons, Laura thinks that Gonzalo is an ill-natured man.  
They are both ashamed of their failed love affair. Now they have lost their youth and taken different courses in life. That’s why talking about their past relationship directly is meaningless. Exposing their identities simply makes them unhappy and sad. That’s why in order to avoid bitter feelings, they don’t reveal their identity.  
  

#### b. At what point of time, do you think, Laura and Gonzalo begin to recognise each other?

When both of them start talking about the failed love affair of Laura Llorente and a gallant lover, they slowly recognize each other. However, Laura says that woman was her best friend and Gonzalo says that the lover was his cousin. Both of them know the events in detail. It makes them sure that they were the old lovers.  
  

#### c. When does Dona Laura realise that Don Gonzalo was her former lover?

When Laura says that the silver maiden was very unfortunate because of her sad love affair, Gonzalo also answers in the same way as if he knows about that affair. It makes Laura clear that Gonzalo was her lover, otherwise he would not have known the personal matters of others.  
  

#### d. Why do Dona Laura and Don Gonzalo spin fictitious stories about themselves?

They spin fictious stories to prove that they were really serious in their love, but because of providence of destiny, their love could not be successful. They wanted to hide their weaknesses and prove each other really helpless. They do it to collect sympathy.  
  

#### e. How do Dona Laura and Don Gonzalo feel about each other?

When they know each other as old-time lovers, they feel good. The emotional tides again bind them together. A new friendship starts and they promise to meet again at the park. Someone has rightly said about lover and wine-the older they are the better they taste and feel.  
  

### Reference to the context

  

#### a. Look at the extract below and answer the questions that follow:

“Yes, you are only twenty. (She sits down on the bench.) Oh, I feel more tired today than usual. (Noticing Petra, who seems impatient.) Go, if you wish to chat with your guard.”  
  

#### i. Who is the speaker?

Dona Laura is the speaker.  
  

#### ii. Who does ‘you’ refer to?

‘You’ refers to Dona Laura’s maid Petra.  
  

#### iii. Who is the ‘guard’ the speaker is talking to?

The ‘guard’ is the ‘park’s guard’ the speaker is talking to.  
  

#### b. Read the extract dialogue from the play and answer the questions that follow:

_DONA LAURA: (Indignantly.) Look out!  
DON GONZALO: Are you speaking to me, senora?  
DONA LAURA: Yes, to you.  
DON GONZALO: What do you wish?  
DONA LAURA: You have scared away the birds who were feeding on my crumbs.  
DON GONZALO: What do I care about the birds?  
DONA LAURA: But I do.  
DON GONZALO: This is a public park._  
  

#### c. Who is Dona addressing by saying “Look out”?

Dona is addressing Don Gonzalo by saying "Look out"  
  

#### d. What was Dona doing?

Dona was feeding breadcrumbs to pigeons in the park.  
  

#### e. Who scared the birds? Are they pet birds?

Don Gonzalo scared the birds. No, they aren’t pet birds.  
  

#### f. Where are the speakers at the time of the conversation?

The speakers are in a park at the time of conversation.  
  

#### g. What is the effect of flashback in the play when Dona Laura and Don Gonzalo knew that they were the lovers in the past?

The flashback makes the drama more meaningful and effective. That gives clear information to the readers about their unsuccessful love affair of the past. The past memory is still fresh in their minds and that unites them again as new friends. After, flashback, they recognize each other and feel emotionally close. That makes the readers clear that they were separated because of some causes.  
  

#### h. Discuss how the play is built around humour and irony.

The play is full of irony. Two old time lovers hide their identity and talk to each other as strangers. However, they know that they are old-time lovers. Laura calls herself as the best friend of the Silver Maiden and Gonzalo says that it was his cousin. From inside they are lovers from outside they are strangers. Similarly, that irony creates humour. Laura makes Gonzalo speechless in many situations because of her witty statements. They try to fool each other by fabricating false stories, but from inside they know they are lying. They know they lie too much. but appear as if they don’t know each other personally. When Gonzalo says he had visited America when he was six, Laura says he might have gone with Columbus. Similarly, when Gonzalo cleans shoes with his handkerchief, Laura asks whether he uses his shoe brush as handkerchief. Gonzalo makes up a heroic ending of himself and Laura shows emotional death of the Silver Maiden.  
  

#### i. How is the title ‘A Sunny Morning’ justifiable? Discuss.

This play is set in a park in Madrid, Spain on a sunny morning. In this setting, the entire play has been presented. A sunny morning has provided an ideal setting for a reunion of two former lovers in their golden years. They argue, know each other, conceal their identities, admire, tell fabricated stories about their deaths, prefer to meet again the next sunny morning, and so on. From start to finish, the entire play continues to make us laugh hysterically while remaining in this setting of a sunny morning. As a result, the title ‘A Sunny Morning’ is quite appropriate, as it depicts a reconciliation of former lovers in a park with great humour.  
  

### Reference beyond the text

  

#### a. What do you predict will happen in the next meeting between Dona Laura and Don Gonzalo? Discuss.

I predict that Dona Laura and Don Gonzalo will be much more excited in the next meeting. They will talk and share their feelings through fake means this time too. Both of them will try to make each other happy. Don Gonzalo will certainly be much frank this time. He will try to show his respect towards her. The readers will get a chance to see their shyness. They will surely get much more fun through their pretentious acts.  
  

#### b. Was it wise for Dona Laura and Don Gonzalo to keep their identities secret? How might their secrets affect future meetings?

Yes, it was wise for Dona Laura and Don to keep their identities secret. At the age of seventy, they both realized that they were former lovers who separated due to their ill-fate. But, they did a good job of not revealing their real identities. Both of them were quite old with weak physical conditions. They were not happy due to their old looks. At this age, the revelation of their reality would be vain. They thought it better to hide their identities and pretend to be unfamiliar. They chose the best way to enjoy their sweet youthful memories through fake means. Their secrets might affect them in their future meetings if they keep on lying in such a way. There was a high risk of the revelation of secrets due to their old age. If they reveal their realities unknowingly, they won’t get to meet with each other in their upcoming future. Hence, it was wise for them to keep their identities secret.