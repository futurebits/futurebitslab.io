## Unit 7
# An Astrologer’s Day
### Summary

  
The short story ‘An Astrologer’s Day’ by R. K. Narayan is a thriller and suspense short story which deals with a single day in the life of an ordinary astrologer who shrewdly tries to dupe people and escape from his guilt. The story not only exposes the fake astrologer but also highlights the gullible and superstitious people who approach him. His day begins like any other day but the day ends with unexpected events. When he is about to wind up his business, he meets a rogue character, Guru Nayak who is a part of the past life of the astrologer. Towards the end, as readers, we receive a shock that Guru Nayak and the astrologer belong to the same native towns. They were once upon a time good friends and had a quarrel one day. The result was that both were into bad company and had a fight. The astrologer tried to kill Guru Nayak by attacking him with a knife and when Guru Nayak fainted, he threw him into a nearby well.  
  
Fortunately, a passerby saved Guru Nayak. The astrologer left his native village forever and became an astrologer. Thus suddenly he confronts his past unexpectedly but smartly tackles the situation.  
  
The surrounding darkness seems to offer a refuge to the astrologer. There is an unexpected twist in the tale with the arrival of Guru Nayak on the scene. Gradually the mystery that is hidden in the darkness is unveiled by his questions. Guru Nayak challenges the astrologer’s knowledge. He refuses to go away without getting a satisfactory answer to his questions.  
  
However, the astrologer who is at his wit’s end now decides to face the situation. He displays accurate knowledge about Guru Nayak’s past and is successful in convincing him. In answering the question of Guru Nayak, the astrologer has not only deceived him but also saved himself from his own fate. The author superbly evokes the atmosphere of suspense and irony in the story. The story reveals how appearances are often deceptive. It shows the witty astrologer’s encounter and escape from his former enemy.  
  

 

### Understanding the text

  

#### Answer the following questions.

  

#### a. How does the astrologer’s appearance help him attract customers? How does he help the customers satisfy their needs?

His eyes are assumed to have a prophetic light by his customers. He wears a saffron turban. He presents himself so perfectly that he is a point of attraction for all the people. He helps the customers satisfy their needs with his working analysis of their troubles like marriage, money etc.  
  

#### b. How do you characterize the astrologer’s attitude toward the stranger?

The astrologer sees the stranger before him and perceives him to be his possible customer.  
  

#### c. What details does the astrologer give the stranger about his past?

The astrologer tells the stranger that he was stabbed by a knife and thrown into a well to die, and some people passing by saw him and saved from dying.  
  

#### d. Why does he advise the stranger to go home immediately?

He advises the stranger to go home immediately to get rid of danger in his life.  
  

#### e. What is your reaction to the conversation between the astrologer and his wife?

When the astrologer gets home, he tells his wife that in the past he thought he had killed a man. He was now happy to know that the man he helped today was not dead. It was a clever plot twist to throw in the man he thought he had killed, as a potential scam for him.  
  
  

### Reference to the context

  

#### a. Suspense is the feeling of anticipation you may have as you read. In this story, what details contributed to your feelings of suspense and surprise? Explain.

This story is a nice combination of suspense and surprise. Here the feeling of suspense is created about the personal and past life of the astrologer. The reader is told that he was not intended to be an astrologer. He left his village without any previous plan. The readers are also told that astrology is not his family business. This creates a curiosity in the mind of the reader about the reasons why he broke this ancestral cycle and was forced to leave his home all of a sudden. The sense of suspense continues with the astrologers encounter with the stranger. The astrologer catches a glimpse of the stranger’s face in the flash of light created by the matchstick and immediately disagrees to accept the stranger’s challenge. At the end of the story, the readers are surprised with revelation of the fact that the astrologer was the person who stabbed the stranger and left for dead when he was drunk during one of his days as youngster.  
  

#### b. Analyze the conflicts in “An Astrologer’s Day.” Explain how the conflicts are resolved and what they reveal about the characters involved in the story.

In the story conflicts arises when a stranger appears as a client of the astrologer on the site to consult him. The astrologer is packing up his things and is ready to call him one day. The stranger challenges the astrologer to provide specific answers for his questions. As the stranger light his cheroot, he catches a glimpse of his face to tell the stranger something that will satisfy him. The stranger is surprised to be told about his past life by the astrologer and agrees to give up his search for his enemy who was declared to have been crushed under a lorry. The astrologer ensures a safe and secure life to himself here after.  
  

#### c. “All right. I will speak. But will you give me a rupee if what I say is convincing? Otherwise I will not open my mouth, and you may do what you like.”

  

#### i. Who is the speaker?

The astrologer is the speaker.  
  

#### ii. Who is he speaking to?

He is speaking to Guru Nayak.  
  

#### iii. What does the expression ‘open my mouth’ mean?

The expression "open your mouth" means conveying convincing information about the stranger.  
  

#### d. Description helps readers visualize what is happening in a story. What details and techniques does the author use to describe the astrologer?

The astrologer is an impressive character. He is able to develop a new personality and survive in a densely populated urban environment by using his intelligence. The astrologer lives by his wits. Despite having no mystical knowledge he knows how to put on a show to attract passers-by. It is obvious that he must sit for long hours in order to collect enough to keep himself and his family alive from day to day. In addition to his intelligence, he is courageous and determined. When he is dealing with Guru Nayak and his life is in danger, he still insists on bargaining for money. He brings every single anna home to his wife so that she can buy food for the family. He is a devoted husband and father.  
  
The story has the mode of third-person omniscience. The use of dialogue throughout the story serves the function of providing multiple points of view without changing the overall authority of the narrator. The story is set in the Town Hall Park, in the late evening.  
  

#### e. Irony is a contrast between appearances and reality. What is ironic about Guru Nayak’s meeting with the astrologer?

The irony of the situation centers around the meeting of Guru Nayak’s meeting with the astrologer. He comes to the astrologer for help in finding and killing the man he is talking about. The man whom Guru Nayak is looking for is none other than the astrologer. He makes Guru Nayak wait and bargain for money. When he finally calls the stranger by his name and tells him about the incident that happened in the village, he has Guru Nayak in the palm of his hand.  
  

#### f. How does the astrologer’s manner of dress suit his character?

The main character here in this story is an astrologer. He wears a saffron turban. He presents himself so perfectly that he is a point of attraction for all the people. He helps the customers satisfy their needs with his working analysis of their troubles like marriage, money etc. Due to his dressing sense, many peoples are attracted toward him. His eyes are assumed to have a prophetic light by his customers and everyone thinks him as a wise astrologer.  
  
  

### Reference beyond the text

  

#### a. The astrologer attracts many customers in the street who are pleased and astonished by what he tells them. What does this tell you about the people of the town walking in the street?

The main character here in this story is an astrologer. His astrology business depends on his character and the way he look. Due to his dressing sense, many peoples are attracted toward him. His eyes are assumed to have a prophetic light by his customers. This tells us that the people of the town walking in the street are really fascinated and pleased with the astrologer and his looks due to which they take him as a wise astrologer.  
  

#### b. Why do most people want to know their future? Do you think astrologers can really help them know their future?

Most people want to know their future because they are very curious about what is going to happen ahead in their life. They want to know their happiness, the problems ahead, and their safety. Yes, real astrologers with real astrological knowledge can help you see your future.  
  

#### c. Is astrology a good practice of fortune telling or is it just a blind faith? Give reasons.

Yes, astrology is a good practice of fortune telling. Astrology is practiced in different life events. People believe a lot in astrology in Eastern cultures. People find the right solutions to their problems through astrology. Astrological prophecies are also proved to be correct in many cases.