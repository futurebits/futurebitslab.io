## Unit 2
# The Oval Portrait
### Summary

  
‘The Oval Portrait’ (1842) is one of the shortest tales Edgar Allan Poe ever wrote. In just a few pages, he offers a powerful story about the relationship between art and life, through the narrator’s encounter with the oval portrait of a young woman in a chateau in the Apennines. The story repays close analysis because of the way Poe offers his story as a subtle commentary on link between life and art.  
  
First, a brief summary of this briefest of stories. The narrator, wounded and delirious, has sought shelter in an old mansion with his valet or manservant, Pedro. He holes up in one of the rooms, and contemplates the strange paintings adorning the walls of the room, and reads a small book he had found on the pillow of the bed, which contains information about the paintings. At around midnight, he adjusts the candelabrum in the room and his eye catches a portrait he hadn’t previously noticed, in an oval-shaped frame, depicting a young girl on the threshold of womanhood.  
  
The narrator is captivated by this portrait, which seems so life-like; but he soon becomes appalled by it. He turns to the book and reads the entry detailing the history behind the oval portrait. The woman depicted in it was the young bride of the painter, and was a perfect wife in every respect – except that she was jealous of her husband’s art that distracted him from her. The artist paints a portrait of his wife, and becomes more and more obsessed with capturing her likeness, until he ends up spending all of his time gazing at the portrait of his wife, and hardly any time looking at her. She becomes weaker and weaker, dispirited from losing her husband’s love as he stops paying her attention and becomes more and more preoccupied with the painting. When he has just about finished the portrait and turns to regard his wife, it is to find that she has died.  
  

 

### Understanding the text

  

#### Answer these questions.

  

#### a. Where did the narrator and his servant make forcible entrance?

The narrator and his servant made a forcible entrance in a desolate Chateau which was located in the Apennines of central Italy.  
  

#### b. Which special picture did the narrator notice in the room?

The narrator noticed an oval-shaped picture of a young girl in the room.  
  

#### c. Describe the portrait that the narrator saw in the room.

The portrait was in an oval-shaped frame. It revealed the head and shoulders of the young woman. The arms, the bosom, and even the ends of the radiant hair dissolved untraceably into the unclear yet deep shadow which formed the background of the whole.  
  

#### d. What is the relationship between the portrait painter and its subject?

The relationship between the portrait painter and its subject is of Husband and wife.  
  
  

### Reference to the context

  

#### a. What is the central theme of the story? Who is the woman depicted in the oval portrait?

The central theme of the story “The Oval Portrait” presents the confusing relationship between art and life. This story has shown the destructive power or the addiction of art and love. Both art and love can lead anyone towards perfection whereas they can bring destructive results in anyone’s life too. Although the artist of this story has become successful through his artistic work but his wife became a victim. She lost her husband so extremely that she doesn’t reveal her pains and sufferings and lose her life. This story also suggests that a women’s beauty collapsed her to death.  
  
The woman depicted in the oval portrait is the wife of a passionate painter who has painted this oval portrait.  
  

#### b. "The Oval Portrait" is a short horror story by Edgar Allan Poe involving the disturbing circumstances surrounding a portrait in a chateau. Elaborate.

This short story has presented a terrible and gloomy setting of a barren Chateau in one of the mountains range (Apennines) of central Italy. The Chateau in this story is completely deserted and very old fashioned. The Chateau itself drowned in darkness in a desolated place. The apartments inside Chateau were so unmanaged and dirty though it was richly decorated previously. The walls were full of tapestries and panting creating a gloomy atmosphere. The life-like portrait has been placed in one of the dark corners. The surrounding of the lifelike portrait is so disturbing due to the unmanaged condition everywhere. Thus it looked quite like a horror story.  
  

#### c. "The Oval Portrait" suggests that the woman’s beauty condemns her to death. Discuss.

The women in the portrait is the one who is of rarest beauty sleeping into womanhood and who gets married to a painter who is obsessed with a passion of painting rather than appreciating the beautiful wife. Once he desires to paint a portrait of his beautiful wife. Despite her hatred towards the passion of her husband, she offers herself as a model to pose for the painting for many weeks uncomplainingly. Obsessed with his artistic task, he doesn’t notice the deteriorating health condition of his wife until she passes away. In this way, her beauty condemns her to own death.  
  

#### d. Discuss the story as a frame narrative (a story within a story).

The frame narrative is a literary technique that reveals about a story within a story told by the main or the supporting character. A character starts telling a story to other characters or he sits down to write a story, telling the details to the audience.  
  
Here the story starts with an unnamed narrator and his entering in a Chateau with a different plot and setting. But once this place is lit with candles, a painting is revealed and the next story starts with the description of the oval portrait. In this way the author switch to a new story within a story.  
  

#### e. The story is told in a descriptive style, with plenty of imagery and symbolism. Which images and symbols do you find in the story?

  
**Frame:**  
– It symbolizes the physical character of the young girl.  
– It is trying to capture all the physical beauty of a young girl in the frame only.  
  
**Image of young girl:**  
– It symbolizes the beauty of artist’s wife that he tried to depict through the portrait.  
  

#### f. What does the expression “She was dead!” mean?

The expression “She was dead” means the shocking moment of the death of the painter’s wife on the chair. The painter was quite obsessed with his painting that he forget about the surrounding and even his beautiful wife. Hence the obsession of artist towards painting took his wife life.  
  
  

### Reference beyond the text

  

#### a. Do you think there is life in art?

“The Oval Portrait” is a frame story that is based on life and art. The outer story follows an anonymous narrator who spends a night in a chateau in the Apennines. While he was there, he admires the impressive paintings but was more obsessed with a portrait of a beautiful young woman which was enclosed in an oval frame. Then he examines the underlying narrative of a lovely girl whose husband had a lot of obsession with his work. He became detached from reality. For him, his canvas became more real than the reality itself which proves beneficial for his art but disastrous for his real life. However, in a literary sense, the artist tries to find life within it but with a general view, art is just a creation that is created to display and has significance related to different things. Thus I don’t think that there is life in art.  
  

#### b. As a thing of art nothing could be more admirable than the painting itself. Explain.

I totally agree with the above statement. Painting is really a very admirable form of art. The painters really work hard and spend much time to create quality paintings with meanings. A picture can paint a thousand words. The painting itself speaks in its literal manner. Here in this story, the oval portrait is one of the finest examples which has surprised the narrator. He feels completely puzzled to see that painting and start describing about it. True arts baffles all formulas and it’s true that as a thing of art nothing could be more admirable than the painting itself.  
  

#### c. A more intense look at the painting reveals the illusion. Have you noticed any such painting?

Yes, I have noticed such painting which reveals the illusion. A painting can’t be judged at a single look. An artist spends days to make an art. Every objects in an art possess special meaning. The painting becomes more meaningful when it is watched by the viewer who is passionate about paintings. But, for those viewers like me who don’t have an idea about paintings find illusion all the time. In my case, I watched a typical painting in a museum near my house for a long time but I didn’t get meanings about it.