## Unit 1
# The Selfish Giant
### Summary

  
The story starts with the children playing in the garden of the Giant every afternoon after coming from school. The garden was lovely, large, with soft grass, and fruit trees. The trees bore rich fruits and birds sang sweetly sitting on them.  
  
One day after seven years the giant came back. He was staying with his friend, the Cornish Ogre. The children were scared to see him. He saw the children playing and said that he would not allow anyone to play here as it was his own garden. He built a wall around it and also put a notice board. The notice board displayed a warning on it, ‘Trespassers will be prosecuted’.  
  
The children became sad as they had no other place to play. They would wander around the high walls of the garden and remember the beautiful garden inside them.  
  
The spring season came and there were blossoms and little birds all around. But it was winter in the giant’s garden and there were frost and snow. In the absence of children birds also did not sing. Once a flower bloomed out of the grass but after seeing the notice board, it also went back to sleep. Then came the North Wind and the hailstorm. Due to the giant’s selfishness, autumn’s golden fruits also did not come to his garden.  
  
Then one morning, the giant heard sweet and lovely music. It was a linnet singing outside his window. The hail and the North Wind stopped and he could feel the spring. He saw that the children came into his garden through a little hole. The children were sitting on the branches of trees and the trees were blossoming. He also saw the birds flying and hear them chirping. The flowers had also come up.  
  
But, to his surprise, in one corner there was still winter. He saw that there a young boy was standing and he was not able to reach the branches of trees. The tree lowered its branches but still, he could not climb.  
  
At this scene, his heart melted. He realized that he was really very selfish. He decided to put that boy on the top of the tree, pull down the walls, and allow children to play here forever. But when the children saw him, they ran away and the garden became winter again. However, that little boy did not run as he was weeping. The giant put him on the top of the tree and the tree blossomed at once. He kissed the giant.  
  
The other children realizing that the giant is not wicked came back. The spring came back with them. The giant used to play with the children ever afternoon but that little boy was nowhere to be seen. As the years went by, he grew very weak. One winter morning, he saw a lovely tree with white blossoms in a corner. The branches of the tree were golden and the little boy stood under it.  
  
The boy was wounded which made the giant very angry. He told the boy that he will slay the man who has harmed him. The boy told him that these were the wounds of love. The boy smiled and asked the giant to come to his garden. Later, the children found the giant dead under the tree covered with white blossoms  
  


### Understanding the text

  

#### a. Where did the children use to play?

The children used to play in the Giant’s garden.  
  

#### b. What did the Snow and the Frost do to the garden?

The snow covered the grass with its large white cloak and Frost painted all the silver trees.  
  

#### c. What did the giant hear when he was lying awake in bed?

The giant heard a beautiful music when he was lying awake in bed.  
  

#### d. Why do you think spring season never came to the giant’s garden?

I think the spring season never came to the giant’s garden because he was cruel to the chidren.  
  

#### e. How did the giant realise his mistake?

The giant realised his mistake when his garden was covered by snow and frost as he stopped children from visiting the garden.  
  
  

### Reference to the context

  

#### A. Read the extracts given below and Ans the questions that follow.

  

#### a. “How happy we were there!” they said to each other.

  

#### i. Where does ‘there’ refer to?

‘There’ refers to the Giant’s garden.  
  

#### ii. What does ‘they’ refer to?

‘They’ refers to the children who used to play in the Giant’s garden.  
  

#### iii. Why are they saying so?

They are saying so because they aren’t allowed to play in the giant’s garden anymore as the giant had chased them out of his garden.  
  

#### b. “I have many beautiful flowers,” he said; “but the children are the most beautiful flowers of all.”

  

#### i. Who is the speaker?

The speaker is the Giant.  
  

#### ii. Who is he speaking to?

He is speaking to himself.  
  

#### iii. Who are ‘the children’ that the speaker is referring to?

‘The Children’ that the speaker is referring to are the small and innocent school kids who used to play in the Giant’s garden.  
  

#### iv. Why is the speaker saying that ‘the children are the ……’?

The speaker says ‘children are the most……..’ because the children bring joy and happiness in the garden as like the flowers.  
  

#### c. When the little child smiled at the Giant, and said to him, "You let me play once in your garden, today you shall come with me to my garden, which is Paradise," shortly afterwards, the happy giant dies. What is the coincidence of this event? Describe it in relation to this fairy story.

The coincidence of this event once, the giant let the little child play in his garden and make him happy and today the same child wants to take him to his garden paradise to make him happy in return, which means the final time of the gaint has come and the little child is a messenger/ fairy who wants him to the heaven because of his noble works.  
  

#### B. The story makes use of personification as one of the main figures of speech. Cite three examples of personification from the story. What is the significance of the seasons personified in the story?

The practice of representing the objects, qualities etc. as the human beings in art and literature is called personification. Three examples of personification from the story are the beautiful little flowers who slips back into the garden after seeing the notice board, the snow and the frost who says that the spring has forgotten this garden.  
  
The story has the significance of the seasons personified. At the beginning, the seasons bless the garden with pleasant weather until the Giant sends the children away from his garden. Because of the Giant’s unkindness to the children, the spring season leaves the garden. The seasons disfavor indicates that the Giant’s selfishness goes against the natural order and thus deserves punishment. Thus the significance of the seasons personification is to show that if someone does bad then he/she will deserve the bad.  
  

#### C. This story can be read as a fairy story, where the children, the seasons, the tree, the corner of the garden, the snow, the wind and the frost are all used as symbolism. Interpret those symbols.

The story ‘The Selfish Giant’ is a fairy tale. Here, the children symbolize innocence, love, compassion and blessings. Likewise the seasons represents life and death. The tree symbolizes patience and hope. The corner of the garden depicts the paradise. Finally the snow, the wind and the frost symbolize pain, suffering and discomfort.  
  

#### D. Which figure of speech is used for ‘winter, frost, snow, north wind, hail and little child’? Who is the little child compared to?

As a fairy tale, the story is full of figures of speech. The figure of speech is used for ‘winter, frost, snow, north wind, hail and little child’ is personification. The little child is compared to the God Almighty (Paradise).  
  
  

### Reference beyond text

  

#### a. What is the main theme of the story?

The main theme of this story is selfishness and love. It is the story of transformation of selfish giant into selfless giant. At the beginning of the story, the giant was very selfish and wicked towards the children and he closes the gate of beautiful garden for them. But later he realizes his mistake when the children stop coming to the garden and the garden never experiences the spring season until and unless he allows them to play in his garden. At this point he realizes the meaning of love and sharing. This story is very much relatable to the nature of human beings. We hardly share our belongings to anyone if somebody is needy and we are selfish too. This story teaches us to share love and compassion to get love in return.  
  

#### b. Does God punish those who are cruel to children and very selfish?

Yes, God punishes those who are cruel to children and very selfish. As in the story, Giant has really been so cruel to the children and selfish and he didn’t let them play on the garden because of which spring never came in his garden. In the absence of children birds also did not sing. Once a flower bloomed out of the grass but after seeing the notice board, it also went back to sleep. Then came the North Wind and the hailstorm. Due to the giant’s selfishness, autumn’s golden fruits also did not come to his garden. In this way god punishes to those who are cruel to children and very selfish.