## Unit 6
# Two Little Soldiers
### Summary

  
Luc and Jean are two soldiers who habitually spend their free time on Sundays away from the barracks, out in the countryside. Their day off has taken on the character of a ritual. Every Sunday, they bring food for breakfast to the same spot in the woods and lie back to enjoy the food, wine, and sights of an area that reminds them of home.  
  
Eventually, their ritual comes to include a bit of innocent ogling of a young village girl who brings her cow out to pasture every week at the same time. One Sunday, however, the girl speaks to them on her way to the pasture, and when she returns later, she shares the cow’s milk with them and leaves them with a promise to meet the following Sunday.  
  
The next weekend, Jean suggests that they bring something for her. They settle on candy as an appropriate present, but when the girl arrives, both are too shy to tell her that they have brought something. Finally, Luc tells the girl of the treat, and Jean, who always carries the provisions, give the bonbons to her.  
  
As the weeks pass, the girl becomes the topic of conversation for these soldiers as they spend time at the barracks, and the three become fast friends. The girl begins to share their Sunday breakfast meal and appears to devote equal attention to the two recruits.  
  
Then, in an uncharacteristic move, Luc seeks leave on a Tuesday, and again the following Thursday. He borrows money from Jean on that day but offers no explanation for his behavior. Jean lends the money.  
  
The following Sunday, when the girl appears with the cow, she immediately rushes up to Luc and they embrace ardently. Jean is hurt because he is left out and does not understand why the girl has suddenly turned all of her attention to Luc. Luc and the girl go off to care for the cow and disappear into the woods for a long time. Jean is stupefied. When they return, the lovers kiss again, and the girl offers Jean a kind “Good evening” before going away.  
  
Neither soldier speaks of the incident, but as they return to their barracks they stop momentarily on the bridge over the Seine. Jean leans over toward the water, farther than he should in Luc’s judgment, then suddenly tumbles into the torrent. Luc can do nothing; he watches in anguish as his good friend drowns.  
  

 

### Understanding the text

  

#### Answer these questions.

  

#### a. Why do the two soldiers spend their free time on Sundays away from the barrack out in the countryside?

The two soldiers spend their free time on Sundays away from the barracks out in the countryside because they have found a spot which reminds them of home and they do not feel happy elsewhere.  
  

#### b. Why does the girl become the topic of conversation for these soldiers?

The girl becomes the topic of conversation of these soldiers because both of them have fallen in love with her.  
  

#### c. Why does deception enter into their friendship?

Deception enters into their friendship because Luc used to secretly leave the barracks to meet the girl without informing his friends.  
  

#### d. Do you think that Luc is a betrayer of friendship?

Yes I think that Luc is a betrayer of friendship as he has broken the faith of his friend Jean. He leaves the barracks without telling anything to his friend so that he can meet the countryside girl whom Jean loves equally.  
  

#### e. What is the cause of suicide of Jean? Do you think that it was the only release of his love?

The cause of suicide of Jean was his friend Luc because he is mentally disturbed by the love affair of his friend Luc with the girl whom he loves much. I don’t think suicide was the only one way to release his love. He could have expressed his feeling to the girl or could have moved on in his life accepting Luc’s love instead of committing suicide.  
  
  

### Reference to the context

  

#### a. What is the central theme of the story?

The central theme of this story is the incompatibility between friendship and love. Jean and Luc are best friends under the most trying of circumstances. When both fall in love with the same girl, feeling of hatred, jealousy raised between them and hence that friendship cannot survive, and it ends in tragic circumstances.  
  

#### b. "What are you doing here? Are you watching the grass grow?"

  

#### i. Who is the speaker?

The dairy maid is the speaker.  
  

#### ii. What does the word “here” indicate?

"Here" means the place where the two soldiers visit every weekend.  
  

#### iii. Who does “you” refer to?

"You" refers to the two soldiers Jean and Luc.  
  

#### c. "He leaned–he–he was leaning–so far over–that his head carried him away–and–he–fell–he fell—-"

  

#### i. Who is the speaker?

The speaker is Luc.  
  

#### ii. Why is the speaker speaking with interruption?

The speaker is speaking with interruption because he is in pain and scared of the death of Jean.  
  

#### iii. What does he mean when he says “he–fell–he fell—”?

He means that Jean fell into the river.  
  

#### d. Two Little Soldiers can be viewed as a series of dramatic scenes. Describe the story as tragedy.

In a literary tragedy, a protagonist is typically brought to ruin because of some character flaw. This flaw, which is usually some common human flaw like greed or misplaced loyalty, causes the character to deteriorate and leads to his ultimate downfall.  
  
In “Two Little Soldiers”, two friends, Jean and Luc, begin the story in a pastoral and fairly idyllic setting despite being far from home. Together, they enjoy their time together, eating a simple meal and enjoying a bit of wine before having to return to their duties.  
  
This sense of camaraderie is ultimately destroyed by the introduction of a third party—a girl. Suddenly, the two friends are equally fascinated by this girl, and they begin to chat about her throughout their week. Neither soldier seems prepared to make a move for the girl’s affections at first, and then suddenly, Luc changes everything when he takes leave without telling Jean where he is going.  
  
When the three meet again, everything has changed. Jean and Luc are no longer the central pair of the group; instead, Luc and the girl have clearly become more intimate during his evenings of leave. They sneak off together, leaving Jean completely isolated.  
  
Luc was Jean’s central line of connection in a world of confinement and loneliness. Realizing that he has lost this connection, Jean is bewildered and desolate. He presumably commits suicide by intentionally falling into the river because of the fresh disconnect he feels from humanity.  
  
Luc is devastated by the loss of his friend, barely able to choke out the news when he returns to the barracks. Thus, the tragedy is dependent on Luc’s character flaw: he prioritizes romantic possibilities over a close friendship. This ultimately causes the tragic loss of the friendship which he had valued so much and which had sustained him through the loneliness of being so far from home.  
  

#### e. What is the setting and style of the story?

The story is set in the French countryside, near the town of Courbevoie.It is wartime, but the scene is peaceful and idyllic; Luc and Jean, who are soldiers stationed in barracks nearby, come here on Sundays for a quiet meal and to enjoy the area, which reminds them of home.The beauty of the setting is enhanced by the presence of a young village girl, who brings her cow out to pasture every Sunday when the boys are there.The boys begin to engage in “a bit of innocent ogling” of the comely girl, who after a while begins to talk to them.  
  

#### f. How would you describe the conflict between the friends?

In the story the conflict arises when the girl kisses Luc passionately, without paying attention to Jean. Seeing this Jean is upset and heartbroken. Jean got the reason behind why Luc seeks leave and borrows money from Jean but offers no explanation for his behavior. Luc and the girl go to attend to the cow and disappear into the woods for a long time. When they return, the lovers kiss again, and the girl doesn’t offer him any milk that day. Eventually, the conflict between friendship and love leads to death of Jean.  
  
  

### Reference beyond the text

  

#### a. Is it good to have conflict between friendship and love? Is it morally good that a person and his best friend can love the same person?

No, It is not good to have a conflict between friendship and love because these are the relationships which are extremely important for an individual. True friends and loved ones are always concerned about each other. They shed light on your qualities, both positive and bad, and improve self-esteem. Both friendship and love assists to relieve stress and suffering through laughing and good times. They will help you move forward.  
  
As long as love and intension are pure towards the person, it is not morally bad for two-person having the same feelings for the third one. This type of condition may arise in a person’s life either knowingly or unknowingly. Although it is not bad, but there is high possibilities for the problems to arise in the near future. So, the only solution is to talk with each other and solve the problem by controlling your emotions.  
  

#### b. How would you describe the triangular love?

The triangular love is a relationship in which three people are each in love with at least one other person in the relationship. While it can refer to a polyamorous relationship between three people, (i.e. Person A loves Person B loves Person C loves Person A). The short story “Two Little Soldiers” has presented a triangular love where the deception of a friend has created an unfortunate incident for the next friend.