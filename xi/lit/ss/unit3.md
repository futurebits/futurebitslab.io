## Unit 3
# God Sees the Truth but Waits
### Summary

  
Once there lived a young merchant named Ivan Dmitri Aksenov with his family in the land of Vladimir, who in his younger days lived life to the fullest by experiencing all the material things world has to offer until he got married.  
  
One summer, he planned to go to Nizhny fair but his wife warned him that she had a bad dream of her husband– she dreamt about Ivan that he returned from the town with hair of grey. Ivan laughed as if he doesn’t care and went on the fair.  
  
He travelled half way and met a merchant, whom he spent the night drinking tea with and shared an adjoining room in the inn. Since Aksenov is not used of sleeping for long hours he decided to wake up and continued his journey.  
  
Along the way of his journey, two soldiers in a troika stopped him, and began asking questions, for the merchant he met halfway on his travel was found dead. Since all evidences of the crime are pointing Aksenov guilty he was imprisoned.  
  
Learning the sad fate of Aksenov, his wife remembered her dream about Aksenov and was worried and even considered the thought of her husband being guilty. The thought made Aksenov even sadder.  
  
26 years in prison made Aksenov a well grounded and God-fearing man. In spite the fact that his family has completely forgotten him, he still serves as a “Grandpa” to the other prisoners. Then came a new prisoner named Makar Semyonich.  
  
After months of knowing each other, Aksenov discovered that Makar is the one who killed the merchant whom he was told he murdered. He was furious with what he found out but didn’t speak or uttered a word about it.  
  
Until one night, Aksenov heard some earth rolling under where the prisoners were sleeping. He went out and saw Makar. Makar told him not to tell a word about what he had witnessed or else he will kill him.  
  
When they were led out to work, a soldier noticed a prisoner took of some earth off his boots. The soldier searched for escaping plans and found the tunnel. Then, they asked each of them who knew about this but they denied for they knew they will be killed before the one who did it as Makar warned them. Finally, the governor asked Ivan for he knew he was a just man. But then Ivan said it wasn’t his right or his will but God’s to tell such name.  
  
Night fell and Makar went to Ivan. He thanked him and felt sorry for what he had done to him a long time ago that made Ivan suffer for all this years. He sobbed as well as Ivan and said that the Lord will forgive you. Makar said that he will confess to the governor so that Ivan would be sent free–back to his home.  
  
Ivan did not want to go out of prison for he has no family neither home to back to; rather, he waited for his last hour to come.  
  
In spite of what they’ve talked about, Makar Semyonich confessed his guilt. But when the order for Ivan Dmitri Aksenov’s release came, he was already dead.  
  

  

### Understanding the text

  

#### a. What bad habits did Aksionov have before his marriage?

Aksionov used to drink before his marriage and was riotous when he drank too much.  
  

#### b. What can be the meaning of his wife’s dream?

The meaning of his wife’s dream can be the bad fortune or unluck.  
  

#### c. Why did Aksionov think of killing himself?

When the Aksionov meet the real murderer of the businessman, he had lost everything. He was not getting support from the family members too, kept in prison for 26 years, grown old before time. So he thought of killing himself.  
  

#### d. Why did Makar disclose that he had killed the merchant?

Makar disclosed that he had killed the merchant because he realized his mistake and thought that he should not let other be punished for his crime. He did so because he felt pity over Aksionov.  
  

#### e. Why doesn’t Aksionov wish to return to his family at the end of the story?

Aksionov doesn’t wish to return his family his wife was dead, he didn’t know about his children and lived in the name of God at the end of the story.  
  
  

### Reference to the context

  

#### a. "Well, old man," repeated the Governor, "tell me the truth: who has been digging under the wall?"

  

#### i. Who is that old man?

That old man is Aksionov.  
  

#### ii. Which truth is the speaker asking about?

The speaker is asking about the truth of digging an escape hole.  
  

#### iii. Which wall does the speaker mean?

The speaker means the prison’s wall.  
  

#### b. Describe Aksionov’s character.

Aksionov is the protagonist of the story “god sees the truth but waits” written by Leo Tolstoy. He is a young merchant. He is a handsome, fair-haired, curly-headed fellow, full of fun and very fond of singing. He used to drink much before marriage but later on, he changed himself and became a good man. He had virtuous qualities such as faith, forgiveness, freedom, and acceptance. His comfortable life is disrupted when he is framed for a murder he didn’t commit and was sent to a Siberian prison camp. He earns a reputation as a good person among the prison officials and fellow prisoners. After finding himself imprisoned with the man who framed him, Aksionov is ready to kill himself. However, at the end of the story, he is able to forgive Semyonich. He dies shortly before the authorities order him to be released.  
  

#### c. What is the theme of the story?

In the story ‘God Sees the Truth, But Waits’ by Leo Tolstoy we have the theme of guilt, forgiveness, faith, conflict, freedom and acceptance. Narrated in the third person by an unnamed narrator the reader realizes after reading the story that Tolstoy may be exploring the theme of forgiveness.  
  
If the plea of his wife to the Czar is denied, Aksionov realises that he cannot rectify his wrong. He commits himself to God to offer him that righteousness which a man cannot give him.  
  
Aksionov becomes a modest and pious person in jail. Officials and inmates appreciate him in settling disputes for his neutrality. Aksionov’s trust in God is so great that he thinks that he must be wicked to deserve God’s tortured life.  
  
After Aksionov is reluctant to report the drilling of Semyonich’s tunnel, Semyonich is disturbed by the compassion of Aksionov. Aksionov eventually gives forgiveness to Semyonich.  
  

#### d. Which symbols are used in the story and what do they indicate?

The most important symbols in this short story are Aksionov’s house (and two shops) and the Siberian prison mine where he is sentenced to hard labor. His house and two shops represent his family, his material possessions, and his earthly affairs. The prison itself is a symbol of his suffering and his eventual spiritual transformation.  
  
  

### Reference beyond the text

  

#### a. What role does religion play in Aksionov’s life? How does he undergo a spiritual transformation in the story?

This story is all about Aksionov who lost hope and just trusted God. Though he didn’t commit any crime, he was imprisoned for 26 years. Makar revealed the truth that he was the real murderer. Aksionov forgave him for accepting the truth and attained self-realization. But when the order of his release came, he was already dead. Our weakness can only make the problem stronger. Being hopeful can make all things possible. The title means that every decision of God needs enough time. This story is for all those people who lose hope and don’t believe in God. We should wait for the truth to win with a faith in God. God gives us the answer to all our problems, as the title says ‘God Sees the Truth, But Waits’. At the end of the story there is a shift from materialism to spiritualism.  
  

#### b. What does the story tell us about the existence of an unfair system of justice?

The story tell us about that one who commits the crime must be punished and kept in prison but not the innocent one. As here in the story, Aksionov did not commit any crime but he was kept in prison whereas the real murder Makar was not punished. Thus this story depicts about the unfair system of justice.