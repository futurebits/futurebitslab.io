## Unit 5
# Civil Peace
### Summary

  
Jonathan Iwegbu has survived the Nigerian Civil War along with his wife and three of his four children, and thus considers himself “extraordinarily lucky”. He also treasures his still-working bicycle, which he buried during the war to ensure it would not be stolen. Another apparent miracle is his still-standing home, which he repairs and reoccupies after returning home to the capital city of Enugu. To explain both his good and bad fortune to himself and others, he often repeats a phrase: “Nothing puzzles God.”  
  
Jonathan works hard in the aftermath of the war, using his bicycle to start a taxi service and opening a bar for soldiers. His family mirrors his example, cooking food and picking fruit for sale. Since the coal mine where Jonathan worked before the war has not reopened, this resilience is crucial towards securing even their minor comfort.  
  
One day, after turning over rebel currency, Jonathan is given an award of 20 pounds. He takes care not to be robbed, remembering a theft he observed several days earlier, in which a man broke down in public over the indignity.  
  
That night, a group of thieves knocks on his door demanding money. Frightened, the family calls for the neighbors and police, but the heavy silence when they finish reminds them that nobody looks out for anyone but himself. The thieves then mock them, crying out even louder to indicate how helpless the family is.  
  
The thief leader demands 100 pounds, promising not to hurt Jonathan or his family if he cooperates. Eventually, Jonathan realizes their lack of options, and gives the thieves the 20 pounds of reward money so they will leave the family unharmed. Some thieves insist they should search the house for more, but the thief leader believes this is all Jonathan has, and accepts it.  
  
The next morning, Jonathan and the family are back at work as the neighbors arrive. Sensing their confusion over his ability to toss off the situation of the night before, Jonathan explains to his neighbors that the reward money cannot compare to what he lost in the war. He chooses to focus on his work in the present rather than regret what has happened, since “Nothing puzzles God.”  
  



### Understanding the text

  

#### Answer the following questions.

  

#### a. Why did Jonathan think of himself as ‘extraordinarily lucky’?

Jonathan thought of himself as ‘extraordinarily lucky’ because he had survived the Civil War along with his wife and children.  
  

#### b. What are the ‘five blessings’ for which Jonathan is grateful?

The ‘five blessings’ for which Jonathan is grateful are his head, his wife Maria’s head, and the heads of three out of their four children. That means he is grateful for the survival of five family members.  
  

#### c. Why did Jonathan mistrust the officer who wanted to take his bicycle? What does this tell you about the situation in Nigeria?

Jonathan mistrusted the officer who wanted to take his bicycle because the officer easily accepted bribe exchange of his bicycle. This tells us that the situation of Nigeria was so much worse as the government officers were corrupted and used to take bribes from the citizens.  
  

#### d. What visitors might be at the door? Are Jonathan and his wife completely surprised? Explain.

The visitors who knock at the door of Jonathan’s house might be the thieves. Jonathan and his wife are completely shocked due to fear as it was midnight and the thieves are knocking at the door violently.  
  

#### e. Why does no one in the neighborhood respond when the thieves pound on Jonathan’s door? Why do the thieves call for the police?

No one in the neighborhood responds when the thieves pound on Jonathan’s door because they are scared of the thieves. The thieves called for the police because they knew very well that no one will come to support Jonathan’s family.  
  
  

### Reference in the context

  

#### a. What does Jonathan mean by his expression “Nothing puzzles God”? What does this expression reveal about his character? Explain by citing details from the story.

In the story “Civil Peace”, Jonathan uses this expression “Nothing puzzles God” every time when something miraculous happens with him. He uses this line when he gets his bicycle back in fine condition out of the ground. Next, he uses it when he finds his house is still standing after the war. By “Nothing puzzles God” he means that anything can happen, but Almighty God, being omniscient, is not surprised and puzzled by anything which means that God knows each and every thing. The last time he uses it at the end of the story after his family is robbed of the exgratia. This expression reveals that he is quite optimistic person who has been believing in God. He feels too delighted to get miracles in his life. This expression also reveals that he is a good person who enjoys everything he has but never laments on his loses. Therefore, his character was optimistic, hopeful and religious.  
  

#### b. How does Jonathan change as he experiences the conflicts in his life? Explain.

Jonathan is a hardworking person who believes in his hard labour as well as god the most. He is quite optimistic person who never loses his hope. He has experienced many conflicts in his life. He has lost his child during the war but he believes himself lucky to get 5 inestimable blessings. Jonathan keeps on doing his hard labour for the sake and welfare of his family keeping faith or believes in God. He changes according to the demands of the time and his own family needs.  
  

#### c. Read the extract and answer the questions below.

“To God who made me; if you come inside and find one hundred pounds, take it and shoot me and shoot my wife and children. I swear to God. The only money I have in this life is this twenty pounds egg-rasher they gave me today …”  
  

#### i. Who is the speaker?

Jonathan is the speaker.  
  

#### ii. Who is the speaker talking to?

The speaker is talking to the thieves or robbers.  
  

#### iii. Who does “they” refer to?

"They" refers to the officials of the treasury i.e. the government.  
  

#### d. Nigerian English has words like soja ‘soldier’ and katakata ‘confusion’, ‘trouble’ derived apparently from English words but transformed by native languages’ phonologies. What does the author’s use of dialect here add to the story?

Nigerian English has words like Soja “soldier” and katakata “confusion”, “trouble” delivered apparently from English words but transformed by Native language phonologies. The author added dialects here in the story because of the following reasons:  
  
– To focus on the native language  
– To promote the dialects  
– To know the status of living people in Nigeria  
– To promote the nationality  
  

#### e. Why do you think the thieves who come to rob Jonathan speak English with a heavier African accent than Jonathan does?

The thieves who come to rob Jonathan speak English with a heavier African accent than Jonathan does to show that they were white Americans so that no one could doubt on them and also to prove their superiority.  
  

#### f. The title of the story "Civil Peace" itself is ironical as there is little to differentiate ‘civil peace’ from ‘civil war’. Do you think that the title of this story is appropriate, or would “Civil War” have been a better title? Explain.

The title of the story “Civil Peace” is especially interesting because it is used in an ironic sense. The story follows Jonathan Iwegbu as he collects the fragments of his life after the end of the Nigerian Civil War. The Nigerian Civil War, also known as the Biafran War, was a protracted conflict that resulted because a section of Nigeria attempted to secede and form its own country. The war leaves a tremendous amount of destruction in its wake, and Jonathan gathers up what he can to reestablish his life. The title of the story is ironic because even though the war has come to an end, the area is still chaotic and filled with strife. Indeed, a gang of robbers use the term “civil peace” when they rob Iwegbu and his family in the dead of night. Thus, Achebe uses the title ironically to highlight the violence still present in a post-war Nigeria.  
  
  

### Reference beyond the text

  

#### a. How would you describe the civil peace in Nigeria?

The condition of the Civil please in Nigeria wasn’t good. After the war, the time of the civil peace was the time of the resettlement.There was nothing new for the people.People had to face various problems during that time.The government’s law during that time was good.There were many risks in people’s life.Thieves used to do loots and beat people without having any fear of law.Though the Civil piece prevailed there, there was Anarchy everywhere.  
  

#### b. What kind of attitude towards life do you think you would have if your situation was similar to that of Jonathan’s?

If my situation was similar to that of Jonathan’s, I would have similar optimism about my life and my family. I would be ready to face any challenges so as to protect my family as like Jonathan did. I would try to do hard labour keeping faith in God for the welfare of my relatives. I would try to be quite away from violence. Hence I think that I would have positive, optimist and energetic attitude towards life if my situation was similar to that of Jonathan’s.  
  

#### c. Draw the character sketch of Jonathan Iwegbu.

Jonathan Iwegbu- Jonathan is a round character. His main trait is optimism, however some of his smaller traits are, peacefulness, persuasiveness, and calmness. He is a static character; he stays the same through the whole story. He does not waste his time to look back on the things that have troubled him in the past, he keeps calm and carries on with his life and deals with whatever happens next. His optimism is shown with every main event in the story.  
  
First off, the war has ended. He is happy to come out of it alive with his wife, his bike, and three out of four of his children; one child has died. The fact that his child has died does not seem to trouble him on the outside. It probably does on the inside, however, he does not let it get to him. He stops thinking about it and instead of being upset because of the death of his child, he is happy for everyone that has survived. Also, he refers to his bike as a taxi once, which shows more optimism.  
  
When he gets to his old house it is broken down. He is calm and gathers up some materials to fix it instead of getting mad over it.  
  
His old job as a minor isn’t available anymore, so he starts a new job of his own and opens up a bar.  
  
Thieves come to his house and try to rob him of money. He tries to find the most peaceful way out of it. After paying all the money he had he once again, he remains calm and carries on.