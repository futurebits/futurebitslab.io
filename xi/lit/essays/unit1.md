## Unit 1
# Sharing Tradition
### Understanding the text

  

#### Answer the following questions.

  

#### a. According to LaPena, what is the importance of the oral tradition? To what extent do you agree with his opinions and why?

Frank Lapena, the writer of this essay give more importance in the oral tradition. According to him, the tradition followed by our elders must be passed from generation to generation orally. It helps to maintain our cultural resource.  
Yes, I agree with his opinion because oral tradition helps the young generation to know about the lifestyle. Social, cultural environment of our elders in the past moreover, they can know about their family history. Our culture is our identity simply oral tradition helps people to preserve their identity.  
  

#### b. Who preserve and pass on the oral tradition?

Oral tradition is a form of human communication in which knowledge, culture, traditions, ideas etc. are transmitted orally from one generation to another generation. Its duty of every to preserve and promote to pass on their tradition because they are experienced one.  
  

#### c. What is the danger of not passing on information from generation to generation?

If the information from one generation to generation is not passed then there will not be perseverance of cultural environment i.e. following festivals, religion etc. our identity and values will be no more left.  
  

#### d. What is the difference between oral tradition and literary tradition?

The difference between oral tradition and literary tradition is that in an oral tradition information related to culture and values are transmitted from generation to generation through oral means whereas in literary tradition information is transmitted through written means.  
  

#### e. How does LaPena establish a relationship between art and the oral tradition?

Both art and the oral tradition are the form of communication in which the author or story teller tries to convey a message to the listener/reader. LaPena presents the oral tradition as the source of our inspiration for art. He says that the oral tradition has an impact on how one visualizes the stories, the characters, the designs and color for art.  
    

### Reference to the context

  

#### a. LaPena states that the oral tradition helps maintain the values of a culture. If you believe that the oral tradition is important, how would you maintain it?

It is true that oral tradition helps maintain the values of a culture. I would maintain oral tradition in the following ways:-  
i) I would save the original tape as well as the transcript of the interview that were given by old peoples.  
ii) I would pay much more attention to the things that the elders are willing to share with us.  
iii) I may start a project on preserverance of our oral tradition.  
iv) I would perform different street dramas about our oral tradition.  
v) In my old age I would try to share my views and ideas that I know about oral tradition. I would like to share it with the next generation.  
  

#### b. “Not everyone is capable of fulfilling the roles of the elders.” Explain this statement with reference to the essay.

Yes, obviously not everyone is capable of fulfilling the rules of elders. Elders are the old aged persons. On the one hand everyone who lives long enough automatically becomes an elder.  
  
According, to my view of point to be an elder one should live for long years. And to fulfill the roles of elders, the elders should have better views, knowledge about the past traditions. We know that all the elders are not of same type. Some may have followed their traditions from their young age and some may not have followed their traditions from their young age which means the one who has not followed traditions from their young age, they do not know much more about the tradition.  
  
So, to fulfill the responsibilities of the elders, the elders should be specialized, they should have to know much more things about past tradition. So, they could pass to us. If they don’t know anything about the tradition than they pass us the knowledge, views and ideas. Hence, “not everyone is capable of fulfilling the rules of elders.”  
  

#### c. What is the controlling idea or thesis of this essay?

The controlling ideas or thesis of this essay is to preserve and protect our tradition. Whatever our tradition are that we follow always should be preserve. The persons of next to next generation should also follow their tradition, they should be well known about the tradition that we are following and the same type of tradition they should also follow. Coming to present generation, many people does not know some traditions, they already forget and some traditions have already been disappear and stopped. So, to preserve and protect our tradition this essay is written.  
  

#### d. How do topic sentences guide the reader through the essay? What would be lost without them?

The topic sentence relates to the thesis, or main point of the essay guides the reader by signposting what the paragraph is about. All the sentences in the rest of the paragraph should relate to the topic sentence. An effective topic sentence combines a main idea with the writer’s personal attitude or opinion. It serves to orient the reader and provides an indication of what will follow in the rest of paragraph. If there is no topic sentences then the main point of a paragraph can’t be expressed and hence the reader can understand the main thesis or controlling idea of paragraph.  
  

#### e. What are the four major problems developed by LaPena with regard to maintaining the oral tradition. How are they used to structure the essay?

The four major problems developed by LaPena with regard to maintaining the oral tradition are as follows:  
– Some of the elders have played ideal and special roles in preserving the values. And it is hard to fill their places.  
– The youngsters do not show enough interest in listening to and asking the elders.  
– It is hard to maintain traditions in changing times when new technologies destroy old values.  
– There is no authentic sharing between the generations. Modern research may not collect true data about the oral contents of tradition. Once wrong information is published, correction is difficult.  
  

### Reference beyond the text

  

#### a. Write a paragraph or two explaining your attitude toward the oral tradition of passing along information.

Oral tradition is one of the effective practices for all the people of this world to maintain and preserve the culture and values of tradition. It is also an art form as it takes its subject matters from our tradition. There is an important role of both the elders and young people perform their vital roles in maintaining and preserving the culture and values of tradition through passing authentic information from generation to generation.  
  
I believe that it helps to share and transmit ancestors’ culture, skill and knowledge to next generation. Next generation gets a chance to know past culture and tradition. They will understand that our culture is our true identity. So my attitude towards the oral tradition is positive. I believe that if we lose our culture, we will lose our identity and existence. We will suffer from the problem of identity crisis. Our culture and tradition are a living experience. Without it, we will lose our true identity. So, we must preserve, follow and transmit it from one generation to next to save its existence.  
  

#### b. Our culture is our identity. Write a few paragraphs explaining how you intend to preserve your culture, values and norms.

Yes, it is true that our culture is our identity. Cultural traditions and perspectives have shaped who we are. The way we are determined to our cultural values, reveals who we are. If we lose our culture, we will lose our identity and existence. So its our duty to preserve our culture, values and norms.  
  
I intend to preserve my culture, values and norms doing the following activities:  
1\. Celebrating every ceremony related to my culture.  
2\. Making people aware of the importance of our culture, values and norms.  
3\. Promoting cultural and traditional events in my community.  
4\. Forming an organization related to cultural heritage.  
5\. Sharing information related to the culture and values of my tradition.  
  
In conclusion, cultural preservation is vital. It preserves a sense of unity and belonging among people of a specific community. So, the forefathers passed much cultural heritage to the new generation. Cultural preservation’s future is at risk. Because of today’s busy lifestyles and intense social responsibilities. Preservation of cultural heritage requires the protection of our cultural heritage. Above all, sharing your cultural heritage is the best way to preserve it. Future generations can live according to their ancestor’s values by preserving cultural heritage. Cultural heritage is a constitutional right that the constitution should protect.