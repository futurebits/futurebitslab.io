## Unit 5
# Scientific Research is a Token of Humankind’s Survival
### Introduction

Vladimir Keilis-Borok was a Russian mathematical geophysicist and seismologist. Here, he talks about how science has given greater contribution to the lives of the people. This essay has shown how scientists across the globe, rise above their national identities, to find solutions for common problems of nations. The speaker/writer advocates science saying that science is the indispensable guardian and caretaker of humankind.  
  

### Summary

Although scientists have low earnings in comparison to businessmen, doctors, lawyers etc., some people decide to be scientists because they cannot live without science. It’s an exciting adventure where the major reward comes from the discovery itself. In that way, scientists get honors and promotions and they enjoy freedom, camaraderie and independence.  
  
Next, the writer talks about his experience during the cold war. During 1960, he was invited to Geneva to attend a discussion about nuclear weapons. At that time, people were worried of destructive nuclear weapons of Russia, America and England. The three nuclear powers were willing to come to an agreement putting a ban on new nuclear test. However, they faced a problem that if anyone violated the agreement by secret underground testing of nuclear weapons, how they could detect that test. For that, the technical experts including the writer were called to solve that problem. As a seismologist, the writer had theoretical knowledge in seismic waves that could easily detect underground nuclear explosions as well as tremors produced by earthquakes. Here, his knowledge had a direct application in the survival of the humankind. That episode taught the writer that science is the hope of survival and well-being of all.  
  
Scientists are the most practical people in the world. This could be seen in new technologies, new brands of the industry from defense to entertainment. The scientists invent antibiotics, electronics, biotechnology, synthetic fibers, the green revolution, and genetic forensic diagnosis etc. The basic knowledge of science always supports people in their lives.  
  
The survival of our civilization is threatened by natural and man-made disasters. Among them, the natural disasters are earthquakes, a self-inflicted destruction of megacities, environmental catastrophes, economic and social crises. A massive release of radioactivity from nuclear waste disposal and an outburst of mass violence etc. are man-made disasters.  
  
Science is our indispensable guardian and caretaker because it is the only science that can ensure that we move with time safely. For the survival of humankind, countries had signed treaty of not testing the nuclear weapons secretly. But science and its scientists ensured about humankind’s survival. It is the only science that can differentiate between the tremors caused by nuclear explosions and natural earthquakes. It is science that can give us new sources of energy, new mineral deposits, and efficient defense from terrorism.  
  
The hope of science for the survival of mankind has larger significance. In the present scenario the safety of mankind is threatened by both natural and manmade disasters. Be it a tsunami, be it terrorism, money alone cannot tackle the problem. Scientific solutions are the only hope. Thus, the writer has numerous reasons to conclude that science is the hope of survival.  
  

 

### Understanding the text

  

#### Answer the following questions.

  

#### a. What does a scientist get instead of big money?

A scientist gets freedom, camaraderie and independence instead of big money.  
  

#### b. What was the problem that the nuclear powers had faced?

The nuclear powers had agreed to a moratorium to stop the test of nuclear weapons. But, they had faced the problem that if anyone violated the agreement by secret underground testing of nuclear weapons, they would not be able to detect it.  
  

#### c. In which area did Keilis Borok’s theoretical knowledge have a direct application?

As a seismologist, he had theoretical knowledge in seismic waves that could easily detect underground nuclear explosions as well as tremors produced by earthquakes. His knowledge could easily detect secret underground nuclear test. Here, his knowledge had a direct application in the survival of the humankind.  
  

#### d. What was the important decision that the politicians took before Geneva Summit?

The important decision that the politicians took before Geneva Summit was to ban nuclear weapons text for the shake of survival of humanity. According to them, after banning nuclear tests, there would be no competition for developing nuclear weapons.  
  

#### e. What are the natural and man-made disasters as mentioned by the author?

The natural disasters mentioned by the author are earthquakes, self-inflicted destruction of megacities, environmental catastrophes, economic and social crises, and so on. The man-made disasters mentioned include a massive release of radioactivity from nuclear waste disposal, an outburst of mass violence, war, and so on.  
  

### Reference to the context

  

#### a. The professional addressed as ‘you’ in the sentence “If you are clever, why are you so poor?” refers to a…  
_i. lawyer ii. doctor iii. scientist iv. businessman._  
Justify your choice.

Here the ‘you’ refers to a scientist. The author wants to know why some people with clever and brilliant mind want to be scientists though other professionals like lawyers, doctors and businessmen earn much more. The author intends to show that although scientists are very wise, their wisdom does not generally help them in generating wealth. Thus, scientists are generally wise but not wealthy.  
  

#### b. The writer says, “I found myself in Geneva.” What does it express?

It expresses the surprise of the writer. He says the above line because he was summoned all of a sudden to attend the meeting related to nuclear weapons in Geneva.  
  

#### c. Are the following statements true? Why or why not? Discuss with your partner.

  

#### i. Money is more powerful than intellectual resources.

This statement is not true. Money can give us the power to make a difference in the lives of others, but not the desire to do so. It can give us the time to develop and nurture our relationships, but not the love and caring necessary to do so. It can just as easily make us jaded, escapist, selfish, and lonely. How much do you need? What is it going to cost you to get it? It is keeping these two questions in mind that gives us a true sense of money’s relationship to happiness. If we have less than what we need, or if what we have is costing us too much, we can never be happy.  
  

#### ii. Intellectual resources help survival of the mankind.

This statement is true. It is because of the intellectual resources that we have combated many diseases. Though man is not the fittest among all the creations of God, he is able to ensure his safety against the other powerful creatures of nature because of his intellectual resources. Basic research can help our survival by giving us new sources of energy and new mineral deposits. Basic research can also protect us against terrorism.  
  

#### iii. Basic research is a way of stalling disasters.

This statement is true. People in the world are living their life securely believing in scientific researches. Scientists keep on studying the ecological changes and forewarn people about natural disasters like earthquakes, tsunamis, storms etc. People can be evacuated from the area posed with danger.  
  

#### d. How does the essayist justify that scientific research is the humankind’s survival?

The essayist justifies that scientific research is the humankind’s survival by presenting his opinions in favour of science and its research-based inventions. As a seismologist, he had theoretical knowledge in seismic waves that could easily detect underground nuclear explosions as well as tremors produced by earthquakes. His knowledge could easily detect secret underground nuclear test. Here, his knowledge had a direct application in the survival of the humankind. According to him, science is the indispensable guardian and caretaker of humankind’s survival. Scientific researchers have found the solutions to the problems of human beings. It has given us new sources of energy, new mineral deposits, medicines and efficient defense from terrorism. Scientific researches can easily predict the upcoming disasters ecological changes and forewarn people. People can be evacuated from the area posed with danger. Thus, the writer has numerous reasons to conclude that scientific research is the hope of humankind’s survival.  
  

#### e. What can be the purpose of the essayist of using quotations in the essay?

The purpose of the essayist’s use of quotations in the essay is to support the ideas and arguments presented in the essay. He wants to get all of his readers aware to the main points that he wishes to convey. People have many misconceptions about science and scientists so he compares public understanding and the reality of a scientist’s life. So in order to support his ideas, the writer uses quotations.  
  
f. Discuss and illustrate the writer’s stand that scientists are the most practical people in the world.  
The writer’s stand here in this essay is that scientists are the most practical people in the world. According to him, all new technologies and new brands of industry from defense to entertainment are the result of scientists’ research. The scientists invent antibiotics, electronics, biotechnology, synthetic fibers, modes of transport, the green revolution, and genetic forensic diagnosis etc. Only the scientific research can provide us new sources of energy and new minerals, efficient defense from terrorism, cure from cancer and new forms of transportation. By this we can say that scientists are the most practical people in the world.  
  

### Reference beyond the text

  

#### a. Everyone lives under the fear of annihilation by nuclear weapons. Explain this statement.

Nuclear weapons are the most terrifying weapon ever invented: no weapon is more destructive; no weapon causes such unspeakable human suffering; and there is no way to control how far the radioactive fallout will spread or how long the effects will last.  
  
Although the superpowers have already reached an agreement to stop nuclear test, the growing competition for power has increased fear and uncertainty about our future. It is a truth that Soviet Union, the United States and the United Kingdom have more than enough nuclear bombs to destroy the other nations in their first strike. A nuclear bomb detonated in a city would immediately kill tens of thousands of people, and tens of thousands more would suffer horrific injuries and later die from radiation exposure. Hiroshima and Nagasaki always remind people of the bitter truth. Anytime, the global politics can deteriorate and crazy rulers like that of North Korea can forget humanity and the world population can suffer. Therefore, every man, woman, and child on the earth is living under the threat of annihilation by nuclear weapons.  
  

#### b. The essayist says ‘While there is science, there is hope of survival and wellbeing for all of us.’ Explain it.

According to the essayist, science is our indispensable guardian and caretaker because it is the only science that can ensure that we are moving with time safely. For the survival of humankind, countries have signed the treaty of not testing nuclear weapons secretly. Specialists can invigilate the moves of different countries and make us alert on time. Science can differentiate between the tremors caused by nuclear explosions and natural earthquakes. It gives us new sources of energy, new mineral deposits, efficient defense from terrorism, antibiotics and cure for human diseases. Besides that, science teaches us how to preserve our ecosystem and biosphere correcting our past mistakes. At present, the safety of mankind is threatened by both natural and man-made disasters. Be it a tsunami, be it terrorism, money alone cannot tackle the problem. Scientific solutions are the only hope. Therefore, while there is science, there is hope of survival and wellbeing for all of us.  
  

#### c. Is science a blessing or a curse? Write an essay on it

Science – A Blessing or A Curse

Science has benefited man in several ways. It has made man’s life more comfortable, more secure and more powerful. But it is not an unmixed blessing. There are several disadvantages of science. However, the advantages far outweigh the disadvantages.  
  
Science has revolutionized the human existence. Take, for instance, electricity which is one of the many gifts of science. All one has to do is to press a button. The room is flooded with light, the fan begins to whirr and give refreshing air, or still more, a desert cooler or an air conditioner turns the room into a hill resort. Thousand types of machines are run by electricity. Room heaters, electric ovens and cooking ranges and several other household appliances are operated by electricity. It runs factories which produce innumerable things of our daily use.  
  
Science has also relieved to a great extent human suffering. Advance in the field of medicine and surgery have reduced the rate of infant mortality. The average life h4 of man has increased. Today, many the dreaded diseases like cholera, small-pox and even tuberculosis are easily curable with the help of medicines. Surgery can remove malignant also proved to be harmful. The machine age made many people unemployed. Now there is no need for so many hands to do a task. Only one person is required to manipulate the machine. Thus, it has led to unemployment.  
  
In the light of all these grave dangers, it is imperative that man tries to make judicious use of machines. After all, man had applied his mind to invent new things not to make his life miserable, but to make it more comfortable and better. Machines should serve mankind. Man should ensure that he does not allow them to destroy his environment and health and put the life of all living things in grave danger beyond a point of no return.