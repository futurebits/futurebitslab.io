## Unit 2
# How to Live Before You Die
### Understanding the text

  

#### Answer the following questions.

  

#### a. What is the story about Steve Jobs’ birth?

Steve Jobs was given birth by an unwed college graduate student. She decided to put her baby for adoption during her pregnancy. She wanted her baby to be adopted by an educated family. Initially, a lawyer’s family was ready to adopted Steve Jobs. But when he was born the lawyer’s family insisted to adopt him as they had changed their minds to adopt a girl. Later a mechanic named Paul Jobs, and his wife, Clara Jobs adopted him with a promise that one day they would sent him in college to study.  
  

#### b. What does he mean when he says, “you can’t connect the dots looking forward; you can only connect them looking backwards”?

By saying “you can’t connect the dots looking forward; you can only connect them looking backwards”, he wants to make it clear to all of us that our success and failures rely on how we learn lessons from our past experience. Life is full of suspenses and anything can happen at any time. Everyone has been failure once in a life. Learning from the failures and building up ourselves helps to get success in future. Taking his example, he had also faced a lot of failures in his life. He was dumped by his girlfriend. He also failed to get job at McKinsey. He wasted his four precious years without getting support from anyone. But he became successful at last. It proves that in our life, we shouldn’t be worried about our failure instead we should learn from it because one day all the dots (our experience) will be connected to bring something great.  
  

#### c. What happened when Steve Jobs turned 30?

When Steve Jobs turned 30, the board of directors fired him from his own company.  
  

#### d. Jobs contends that you need to love to do what you do in order to be great at it. Do you agree or disagree? Why?

Yes, I agree with his views that we must love what we do to become great because continuous effort and hard work is required to carve our path to be success.  
  

#### e. Is death really life’s greatest invention?

Yes, death is life’s greatest invention, death is inevitable and nobody lives forever. Nobody wants to die, but everyone shares this intellectual concept. Instead of being worried about death, one should look how he/she could do great until they survive.  
  
  

### Reference to the context

  

#### a. Read the extract given below and answer the questions that follow: “We have an unexpected baby boy; do you want him?” They said: “Of course.”

  

#### i. Who was the baby boy?

Steve Jobs was the baby boy.  
  

#### ii. What does ‘do you want him?’ mean?

‘Do you want him?’ It is a question related to the adoption of Steve Jobs to his parents Paul and Clara Jobs, who were on the waiting list to adopt the baby.  
  

#### iii. Who does ‘they’ refer to?

‘They’ refer to Mr. Paul Jobs and Mrs. Clara Jobs.  
  

#### b. Explain the following lines:

  

#### i. “You have to trust in something — your gut, destiny, life, karma, whatever.”

This line was said by Steve Jobs in his speech at Stanford University. Here he advises everyone to believe in ourselves. We have to build up self-motivation as no one is going to do it for us. If we believe in ourselves and have faith nothing can stop us from being successful.  
  

#### ii. “Your time is limited, so don’t waste it living someone else’s life.”

Through this line the speaker is trying to convey that our life is limited and we shouldn’t be trapped by other’s principle. This can misguide us. We must listen to our own inner voice and utilize the time properly so that we could be the person what we want to be.  
  

#### c. What does he mean by “don’t settle”?

By “don’t settle” he means that we should not accept something which isn’t as good as it is meant to be. We should keep on putting our efforts until and unless we get the result we are searching for. Instead of running behind the success, we should go for the excellence. If we are excellent, success automatically comes to us.  
  

#### d. Which style of speech is used by the speaker to persuade the audience?

The speaker uses a personal and informal style of expression that appears to be of narrative type to persuade the audience. The speech is short and simple, and heavily based on encouragement and he tries to motivate the audience with his real life stories.  
  

#### e. It is not easy to motivate others. How do you think Steve Jobs’ speech is so inspiring?

Steve Jobs’ speech is emotional, passionate, inspirational and simply structured. It is so inspiring because of his act of delivering real life stories in his speech to motivate his audience. Each of the three stories from Jobs’ life involves struggle or sacrifice. His speech encourages people to continue on the path of progress regardless of the family background they belong to. His life story is relatable to the many middle class peoples. He told us about how he struggled in his life to achieve success in his speech because of which I found it to be so inspiring.  
  

#### f. Why do you think Steve Jobs used the personal narrative story telling technique in his speech? What influence does it have on the audiences?

I think Steve Jobs used the personal narrative storytelling technique in his speech to motivate his audiences. His speech contains several facts related to hardships, difficulties and struggles of life. It has a very positive influence on audiences. His speech gives a chance to audiences to know about what it takes to achieve a successful life in real sense.  
  

### Reference beyond the text

  

#### a. One of Steve Jobs mottos was: ‘Think differently’. Can this make a person succeed in life? What challenges are there in thinking differently?

Yes, thinking differently can make a person successful in life. We have to think differently and act differently in order to achieve success. Doing the thing that is already done by other or following the path that is already followed by other can’t help us to gain success. Thinking differently is not an easy task. For this one should get into the real world and learn from the past activities. If we have different way of thinking then other people may find us to be crazy and they may demotivate us saying that it’s not going to work. We should believe in ourselves, build up the self-confidence and do not run on other principles to become successful in our life.  
  

#### b. What does the slogan “Stay hungry; stay foolish” mean to you?

The slogan “Stay hungry; stay foolish” means being prepared for accepting anything that comes to our life and working regularly to fulfill our dreams, wishes and desires. People stop after achieving a certain point in their life. We should not do that. Rather than that we should always dream higher and run behind it.  
  

#### c. What does it mean to be a visionary? What makes Steve Jobs different from a fortune teller?

Being a visionary means to have a good vision or prospect for the future. Steve Jobs is quite different from a fortune teller in the sense that a fortune teller only predicts about people’s life but people like Steve Jobs lead the society with new visions. They set the direction of future and work hard to get there. Their dream is to change the present situation of the world and make it better. Visionaries like Jobs not only think about self but also about the society.