## Unit 5
# The Gift in Wartime
### Summary

  
The first stanza of this poem begins with the speaker offering someone—an unnamed “you”—roses and a wedding gown. In the second stanza, this “you” replies by giving the speaker medals, silver stars, and a badge. These items appear to be less meaningful and personal than the items that the speaker offers.  
  
This pattern—where the speaker gives away much more than the “you”—continues throughout the poem. In the third stanza, the speaker offers their youth. In return, the “you” gives them the “smell of blood.” Indeed, as the poem unfolds, it appears as if the speaker’s offerings become more abstract, while the offerings of the “you” become more violent and indifferent.  
  
In the fifth stanza, the speaker gives the “you” clouds and a sacrifice. They sacrifice their pleasant “springtime” for the harshness of “cold winters.” Yet the “you” remains unmoved by these offerings. The “you” gives the speaker “lips with no smile” and “arms without tenderness.” dead.  
  
In the final stanza, the violence of the “you” is further clarified when the speaker mentions the “shrapnel”—that is, bomb fragments—that the “you” has given them.  
  
Taking this summary into consideration, it seems that Tran Mong Tu’s poem is mainly telling about all of the things that a victim of war is forced to give up. Perhaps the “you” in the poem is war itself. That might be why the “you” reacts to the speaker’s offerings with a mix of violence and indifference.  
  



### Understanding the text

  

#### Answer the following questions.

  

#### a. Who is the speaker addressing and why can that person not hear or understand what she is saying?

The speaker is addressing her husband. He cannot hear or understand what she is saying because he is dead.  
  

#### b. What can you infer about the speaker’s feelings for the person addressed as “you”?

Her feelings for that person she addresses as “you” is full of love, affection and devotion. She has gifted him all the pleasures of life. She is saddened by his demise. Although he is no more in the world, she is still hopeful to meet him in their next life.  
  

#### c. What is the speaker’s attitude toward war?

The speaker has bitter attitude towards war as she has lost her husband.  
  

#### d. In what ways do you think this person’s fate has affected the speaker?

This person’s fate was to die in war because he was a soldier. The speaker’s happiness and youth ended with his death. Thus, the ill-fate of the person has made her helpless, loveless and miserable.  
  

#### e. What does the speaker promise at the end of the poem? Why do you think the speaker does this?

At the end of the poem, the speaker promises to meet her lovable person in their next life. She wants to take shrapnel as a proof to show him the reason behind his death and their separation. I think the speaker does this because of her love for the absent person is so deep and she wants to be in love with him again and again.  
  

### Reference to the context

  

#### a. What is the theme of the poem?

The theme of the poem is the cruelty and inhumanity of the war and its negative impacts over humans. The poem talks about the tragedy caused by war. One life ends but many others are affected. After the untimely death of the soldier, his family and dear ones will suffer. Life is priceless, so there is no compensation for this great loss.  
  

#### b. What imagery from the poem made the greatest impression on you? Why?

The poet has used several powerful images in this poem. Among them all, I liked the shrapnel image the most. It made it very clear what gift a war can give to human beings. The shrapnel does not only mean that it shattered the body of the soldier, but also shattered the life of the beloved. That is the deadly gift of the war.  
  

#### c. Which figurative language is used in the poem? Explain with examples.

Figurative language is phrasing that the goes beyond the literal meaning of words to get a message. We find the use of irony, apostrophe, anaphora, and metaphor as the figurative language here in this poem.  
  
Irony takes place when the poet talks about the gift which is not a real gift but of grief and loss. A grave and shrapnel as tokens of remembrance are the examples of it.  
  
The poet uses imagery when roses are offered in her beloved’s grave, and her husband is described as a corpse with lips with no smile and eyes with no sight. The red roses traditionally symbolize love.  
  
The next figure of speech is anaphora, which is the repetition of the same words at the beginning of a line. In the first, third and fifth stanzas, the poet repeats “I offer you” and coming to the sixth stanza where the speaker repeats “you give me” three times in a row. These are the examples of anaphora used in this poem.  
  
The poem also utilizes the apostrophe literary technique that is addressed directly to a non-present person or an inanimate object. Like in the poem, the speaker confronts the dead corpse of her spouse.  
  

#### d. What does the speaker “offer” in this poem? What does the person addressed as “you” give in return?

The speaker offers various things like roses, her wedding gown, her youth, clouds, cold winters etc. to her lovable person. The person addressed as “you” gives her the medals with shining stars, badge with yellow pips, the smell of blood from wardress, lips without a smile, arms without tenderness, eyes without sight, and body without motion.  
  

#### e. An apostrophe is a literary device in which a writer or speaker addresses an absent person or an abstract idea in such a way as if it were present and can understand. Discuss the poem in relation to apostrophe.

An apostrophe is a literary device in which a writer or speaker addresses an absent person or an abstract idea in such a way as if it were present and can understand. Here in the poem, we can see the use of an apostrophe when the speaker addresses the dead body as if he can understand her. She offers him red roses and her wedding gown at his grave. Her youth has ended with his death. His badge, bravery medals and the blood from his dress make her sad. Her eyes are full like summer clouds and her life has changed from spring to winter. She wants to prove her deep love and respect for her husband by showing her sacrifice. Thus, this poem has shown the bitterness of war on behalf of the speaker using the apostrophe.  
  

### Reference beyond the text

  

#### a. One way to get relief from grief is to write or talk about it. In your opinion, how might the speaker in this poem have benefitted from saying what she did? Explain.

One way to get relief from grief is to write or talk about it. Grief is caused by loss. In emotional situation a person can get relief by talking with dear one. We can share our pain to other by writing poem, stories, etc. or sharing our experiences with other. The speaker in this poem has also chosen the same path. She has become able to reduce her pain and suffering by talking with her dead husband.  
  
Happiness and pain are an inevitable part of human life. In our life, we must experience both sides of life. We must find a way to cope with the grief and move on with our life. The speaker is talking to her husband who was killed in war. She offers him red roses and her wedding gown at his grave. Her youth has ended with his death. His badge, bravery medals and the blood from his dress make her sad. Her eyes are full like summer clouds. Her life is changed from spring to winter. She wants to prove her deep love and respect for her husband by showing her sacrifice. In this way, the speaker becomes able to cope with her deep pain by talking with her dead husband.  
  

#### b. Write an essay on the effects of war.

Effects of War

The effects of war are widely spread and can be long term or short term. Soldiers experience war differently than civilians, although either suffer in times of war, and women and children suffer unspeakable atrocities in particular. In the past decade, up to two million of those killed in armed conflicts were children. The widespread trauma caused by these atrocities and suffering of the civilian population is another legacy of these conflicts, the following creates extensive emotional and psychological stress. Present-day internal wars generally take a larger toll on civilians than state wars. This is due to the increasing trend where combatants have made targeting civilians a strategic objective. A state conflict is an armed conflict that occurs with the use of armed force between two parties, of which one is the government of a state. “The three problems posed by intra‐state conflict are the willingness of UN members, particularly the strongest member, to intervene; the structural ability of the UN to respond; and whether the traditional principles of peacekeeping should be applied to intra‐state conflict”. Effects of war also include mass destruction of cities and have long lasting effects on a country’s economy. Armed conflict has important indirect negative consequences on infrastructure, public health provision, and social order. These indirect consequences are often overlooked and unappreciated.