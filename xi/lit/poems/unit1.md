## Unit 1
# Corona Says
### Summary

  
The poem “Corona Says” is written by a Nepali poet Vishnu S. Rai who was educated in India, Nepal and UK. He is a retired professor of TU who taught English Language Education. In this poem the poet has taught about a pandemic (Covid-19) which has its devastating impact on human life.  
  
This poem is written about the current issue of the Corona world crisis and its devastating impact on human life. This is a subtle satire about human behavior and attitudes. This poem considers Covid-19 as a by-product of human treatment of nature.  
  
The speaker asks man not to cry and curses him for the damage. He claimed that the irrational behavior of humans had invited him (corona) and he had no choice but to let people suffer and die. For speakers, the number of deaths caused by war is higher than that of a pandemic. The so-called superiority of man over other creatures is criticized in the poem.  
  
The pandemic questions us about our indifference to the living organisms that fly in the sky, live in the oceans and crawl on land, and the trees that provide oxygen. These organisms are considered as slaves. As per our wish, we will either kill them or sell them.  
  
The poem mentions the positive effects of the disease too. During the pandemic, the sky is clear without dust and smoke because there is no human activity. People feel like animals locked up in a zoo. The pandemic has allowed the earth to rest.  
  
The speaker wants humans to know themselves at first even if they claim to have control over everything. He says people should realize that the earth is a common home for all creatures. The speaker raised us by saying that the disease can be eradicated, and at the same time reminded us that there are still many other diseases that we can suffer from. It warns us that if we continue our immoral activities, we may incur irreparable harm and our civilization may be in danger.  
  

  

### Understanding the text

  

#### Answer the following questions:

  

#### a. Who is the speaker in the poem?

The speaker in the poem is Corona.  
  

#### b. Who claims that they are superior to all?

The humans of this modern world claim that they are superior to all.  
  

#### c. Why has the speaker come to the Earth?

The speaker has come to Earth as a result of the abuse of nature by people.  
  

#### d. What positive changes have occurred on Earth after the speaker’s visit?

After the speaker’s visit, the sky has been clean without dust and smoke. People have felt like caged animals do in a zoo. The disease has allowed the earth to have a little rest.  
  
  

### Reference to the context

  

#### a. What does the speaker mean when he says:

_But have you ever counted  
How many have died so far  
Because of you and your wars?_  
  
In the given lines, the speaker blames that the human beings themselves are responsible for wars and loss of their lives. The consequences and effects caused by wars is more terrible than the pandemic has done. Wars are the result of disputes over resources and land, or of a government’s will to increase its influence, power and authority. The parties or government involving in wars never think of the consequences people have to face. Millions of people have lost their lives and properties because of wars. Aftereffects of wars is also causing them to suffer physically and psychologically.  
  

#### b. Explain the following:

_I will depart one day.  
But remember  
There’re many others like me.  
They’ll come too.  
If you don’t get rid of your inflated ego,  
You’ll be back to your cave time  
That you endured  
Long, long, long ago …_  
  
In the given extract, the speaker warns us to terminate our egoistic behaviour. No matter who we are, we have to preserve the nature and shouldn’t go against the law of nature. By saying the above line the poet awares us that the pandemic can be controlled but they may suffer from other fatal diseases due to their own behaviours, and finally they can be the cause of the extinction of human civilization.  
  

#### c. What does the speaker mean in the following lines? Explain.

_The earth is not your property alone –  
It’s as much ours as yours._  
  
In the given lines, the speaker denotes that the earth is the common habitat of all the living creatures. All of them have an equal right to use the resources available on earth, to sustain their lives. But we human beings are ruling over others thinking ourselves as superior and depriving them of using resources. The speaker is worried about growing human pressure on natural world. They have controlled all the natural resources for their own benefits though the earth is the common home for all the living organisms. In the name of development and progress, humans are destroying the sources of food and habitats of other creatures. In this way, slowly and slowly entire ecosystem is being destroyed.  
  
  

### Reference beyond the text

  

#### a. What human behaviours are responsible for suffering in people’s lives?

The human behaviours are the main cause behind all these sufferings of the people. Due to human egos, their greediness and bad deeds, the present world is facing a lot of crisis. Many people have lost their lives during this critical period. Due to their selfish nature, the earth have faced numerous problems of diseases. Their so-called egos and wars have snatched the lives of many people. Hence, human beings themselves are responsible for suffering in people’s lives.  
  

#### b. How does an epidemic differ from a pandemic? Briefly explain the impact of Corona Virus on human life and environment.

An epidemic is a widespread disease that affects many people in a population whereas pandemic is a disease that affects a wide geographical area and a large proportion of the population. Epidemics occurs when an agent and susceptible hosts are present in adequate number and the agent can be effectively conveyed from a source to the susceptible hosts.  
  
Coronavirus is one of the greatest threat of the twenty-first century. This disease has taken away the lives of numerous number of people. People in the world are panic-stricken and living their life in mental fear. Businesses are down and economic crisis is seen all over the world. Many peoples are jobless, homeless and due to the scarcity of food also many peoples are dying. But due to the impact of Corona Virus, environment is clean and pollution free than before. As peoples are caged inside their houses, many of the industries are closed and the numbers of vehicles on the road is also decreased. Due to this the environment is cleaner and fresher than before.