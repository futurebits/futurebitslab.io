## Unit 3
# All the World’s a Stage
### Summary

  
Shakespeare seems to have the impression, in the poem, that human life is not real. What we see and hear isn’t a reality. Human life is a play of make-believe. Here Shakespeare traces human life through the famous seven ages – the infant in arms, the schoolboy, the lover, the soldier, the justice, the retired man, and the worn-out senior, sinking back into dissolution. The whole world’s a stage. We‘re only actors. We enter the stage and we go off it again. One man in his lifetime plays a lot of roles. At first, he plays the part of the infant, crying and throwing milk in the arms of the nurse.  
  
Then he plays the part of a schoolboy who is not willing to go to school. With his shining face of the morning, he trudges at the pace of the snail. Then there comes the lover. He sighs like a furnace, and writes pitiful verses, addressing his mistress. He plays the role of a soldier. It’s stocked with all the violent oaths. He’s wearing a wonderful beard. In a quarrel, he is too sensitive and fast and hasty. He is willing to sacrifice his life for the sake of unsubstantial glory. Then he will play the role of judge. He’s a bulging belly man, with severe eyes. He’s a very wise man.  
  
Then Shakespeare describes his old age. It’s pretty funny. The old man is in slippers, wearing glasses. His mannish voice once more turns into a child’s shrill tone. The last role is the second child. It’s so full of forgetfulness. It’s without teeth, without eyes, without taste, without everything.  
  

### Understanding the text

  

#### Answer the following questions.

  

#### a. Why does the poet compare the world with a stage?

The poet compares the world to a stage because he considers all men and women like the actors performing their different roles here in this stage. By stage he means the world.  
  

#### b. What is the first stage in a human’s life? In what sense can it be a troubling stage?

The first stage in a human’s life is childhood. It can be a troubling stage in the sense that this stage is a fully dependent stage and the infant can cry and even vomit anytime in mother’s arms.  
  

#### c. Describe the second stage of life based on the poem.

The second stage of life is the stage of boyhood. In this stage, the boy is a school going student. He complains all the time. His face is like shinning morning. He carries his bag and reluctantly goes to school as slowly as a snail.  
  

#### d. Why is the last stage called second childhood?

The last stage is called second childhood because in this stage the man loses his senses of sight, hearing, smell and taste and behaves as like a child.  
  

#### e. In what sense are we the players in the world stage?

We are the players in the world stage as we appear on the world stage when we get birth and leave it when we die like the actors do on the stage in a theater.  
  
  

### Reference to the context

  

#### a. Explain the following lines:

_All the world’s a stage,  
And all the men and women merely players_  
In the given lines, the poet has compared the whole world with a stage where men and women are the actors. After birth, every person perform their roles here in this worldly stage and finally, leave this stage moving towards their final destination called death.  
  

#### b. Explain the following lines briefly with Reference to the context.

_They have their exits and their entrances;  
And one man in his time plays many parts,_  
The given lines are taken from the poem ‘All the world’s a stage’, composed by William Shakespeare. These lines express similarity between the roles the actors play on the stage and humans in their lives.  
  
Here, the poet has said that the people arrive here in this worldly stage through birth and leave this stage through death. Like the actors in a drama, we are assigned various roles to be performed. When we get our roles completed we quit the stage of our life. The poet wants us to realize the fact that human life is like the stage of a theatre. A man is fated to act several roles in his life.  
  

#### c. Read the given lines and answer the questions that follow.

_Then the whining schoolboy, with his satchel  
And shining morning face, creeping like snail  
Unwillingly to school._  
  
**i. Which stage of life is being referred to here by the poet?**  
The childhood stage of life is being referred to here by the poet.  
  
**ii. Which figure of speech has been employed in the second line?**  
In the second line, simile, a figure of speech has been employed where the boy has been compared with snail using like.  
  
**iii. Who is compared to the snail?**  
The school-going boy is compared to the snail.  
  
**iv. Does the boy go to the school willingly?**  
No, the boy doesn’t go to the school willingly. His unwillingness can easily be the motion of snail towards his school.  
  

#### d. Simile and metaphor are the two major poetic devices used in this poem. Explain citing examples of each.

Here in this poem, we find major poetic devices as simile and metaphor. The poet has used these poetic devices a lot. The examples of simile and metaphor of this poem are as follows:  
  
a) "All the world’s a stage" – Metaphor  
b) "And all the men and women merely players" – Metaphor  
c) "And shining morning face, creeping like a snail" – Simile  
d) "Full of strange oaths, and bearded like the pard," – Simile  
e) "Seeking the bubble reputation" – Metaphor  
f) "His youthful hose, well sav’d, a world too wide" – Metaphor.  
g) "and his big manly voice, Turning again toward childish treble" – Metaphor.  
  

#### e. Which style does the poet use to express his emotions about how he thinks that the world is a stage and all the people living in it are mere players?

The poem is written in blank verse with regular metrical but unrhymed lines. The style of the poem is narrative. In this poem he has expressed his innermost emotions about how he thinks that the world is a stage and all the people living in it are mere players or characters. These characters go through seven different phases in their lives. He has explained the real aspects of human life for all readers to understand the reality of life.  
  

#### f. What is the theme of this poem?

In ‘All the world’s a stage’ Shakespeare discusses the futility of humanity’s place in the world. He explores themes of time, aging, memory, and the purpose of life. Through the monologue’s central conceit, that everyone is simply a player in a larger game that they have no control over, he brings the themes together. Shakespeare takes the reader through the stages of life, starting with infancy and childhood and ending up with an old man who’s been a lover, a soldier, and a judge. The “man” dies after reverting back to a state that’s close to childhood and infancy.  
  

### Reference beyond the text

  

#### a. Describe the various stages of human life picturised in the poem "All the world’s a stage."

According to Shakespeare, the world is a stage and everyone is a player. He says that every man has seven stages during his lifetime. The first stage of a man is childhood. He plays in the arms of his mother. He often vomits and cries in this stage. In his second stage, the man is an unwilling school going student. He becomes a lover in his third stage. He is very busy composing ballads for his beloved and yearns for her attention. In the fourth stage, he is aggressive and ambitious. He seeks reputation in all that he does. He is ready to guard his country and becomes a soldier. In his fifth stage, he becomes a fair judge with maturity and wisdom. In the sixth stage, he is seen with loose pantaloons and spectacles. His manly voice changes into a childish treble. The last stage of all is his second childhood. Slowly, he loses his faculties of sight, hearing, smell and taste and exits from the roles of his life. Thus, Shakespeare has presented the pictures of the seven stages of a man’s life in the poem ‘All the World’s a Stage.’  
  

#### b. Is Shakespeare’s comparison of human’s life with a drama stage apt? How?

Shakespeare has compared human life to a play or drama played by every man and woman. He has described seven stages of life, which are like the seven acts of a play.  
  
The comparison of the world to a stage and people to actors goes before Shakespeare. We find such comparisons made in many philosophical books too.  
  
But, even if nobody had written about it, it is by a simple observation of life around us we find the same thing happening. Everybody takes birth, grows, and with every growth, man’s life changes. He works, fulfils duties and responsibilities according to age, and finally leaves the world.  
  
This simple observation tells us Shakespeare’s comparison of human life with a drama is very apt.