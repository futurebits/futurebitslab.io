## Unit 3
# A Devoted Son
### Understanding the text

  

#### Answer the following questions.

#### a. How did the morning papers bring an ambience of celebration to the Varma family?

The morning papers brought an ambience of celebration in the Varma family as it showed that the Rakesh had topped the Medical Examination and it was the matter of pride for the family.  
  

#### b. How did the community celebrate Rakesh’s success?

The community people celebrated Rakesh’s success by visiting his little yellow house at the end of the road. There they congratulate Rakesh’s parents on their son’s remarkable success and congratulate Rakesh himself. The whole house and garden was quickly filled with the sights and sounds of a festival, with beautiful garlands, party clothes, and gifts.  
  

#### c. Why was Rakesh’s success a special matter of discussion in the neighbourhood?

Rakesh’s success was a special matter of discussion in the neighbourhood because he was the first son in the family to receive an education. His parents had sacrificed so much in order to send him to school and then medical college. It was the matter of pride for the family as well as neighbours. Rakesh’s exemplary behavior that he touched the feet of his father as soon as he saw his results made Rakesh success a special matter of discussion in the neighbourhood.  
  

#### d. How does the author make fun with the words ‘America’ and ‘the USA’?

Rakesh had won a scholarship in USA. His father learnt it to be called as ‘USA’ and taught the whole family not to say it as ‘America’ as it was the term to be called by his ignorant neighbours. In this way the author makes fun of the words ‘America’ and ‘the USA’.  
  

#### e. How does the author characterize Rakesh’s wife?

The author characterizes Rakesh’s wife as an uneducated, old fashioned, plump girl. She was so placid, complaisant and lazy but too good-natured and pretty fat one.  
  

#### f. Describe how Rakesh rises in his career.

After completing his medical education, he started his career as a doctor in the city hospital and quickly reached the top of administrative: organization and was made a director. Then, he opened his own private clinic and became known not only as of the best but also the richest doctor in city.  
  

#### g. How does the author describe Rakesh’s family background?

Rakesh was from a poor family who used to live in a village in India. His father worked for a kerosene vendor and his mother spent her life in the kitchen. His grandparents also used to work as vegetable vendors. Despite of the poor economic condition, his parents worked hard and sacrificed everything they have for the higher education and medical college of his son Rakesh.  
  

#### h. What is the impact of Rakesh’s mother’s death on his father?

Rakesh’s father was broken into pieces by the death of his wife. He fell ill so frequently and suffered with such mysterious disease named a peevish whim (sudden irritation in mind) that even his son could no longer make it out.  
  

#### i. What did Rakesh do to make his father’s old age more comfortable?

Rakesh used to bring his father his morning tea in the old man’s favorite brass tumbler, and sat at the edge of his bed, comfortable and relaxed his father’s night-shirt, and read out the morning news for him. He used to help his father down the steps and onto the bed, soothing him and settling him down for a night under the stars.  
  

#### j. Why did the old man try to bribe his grandchildren?

The old man tried to bribe his grandchildren in order to buy him sweets and Jalebis from the shop at the crossroads.  
  

#### k. Are Mr. Varma’s complaints about his diets reasonable? How?

I don’t think that the Mr. Varma’s complaints about his diets are reasonable. His son had made a diet plan for him so that he could live a healthy life. Rakesh being a responsible son and a doctor too, can’t allow his father to risk his life eating unnecessary foods and sweets. Rakesh did so because he didn’t what to lose his father as he had already losed his mother. Thus the Mr. Varma’s complaints about his diet aren’t reasonable.  
  

 

### Reference to the Context

  

#### a. How did the Varma couple make sacrifices for their son’s higher education?

Mr. Varma being a worker in a Kerosene Dealer Deport and Mrs. Varma being a housewife doing household things have done and made a lot of sacrifices for their son’s higher education. Mr. Varma had never seen how the school looks like from the inside as he had never gone to study. So, he wanted to fulfill his dream from his son and he sent Rakesh to the school for quality education. Rakesh was the first son in the family to receive an education. Thus the Varma couple worked hard and sacrificed their life, time, money and everything they have for the higher education and medical college of son for the betterment of his future.  
  

#### b. Mr. Varma suffers from diseases one after another after his wife’s death. Would he have enjoyed better health if she had not died before him? Give reasons.

I think yes he would have enjoyed better health if she had not died before him. He had fell ill as soon as his wife died. Before the death of his wife he was enjoying his life with no mental harassment but after the death of his wife he started being in depression and many other mental conditions. He was broken mentally as well as physically. It is usual too because when a person with whom you have spended decades, they become part of your life and when they leave permanently, it is obvious to get a shock. Thus, if she had not died before him, he would have enjoyed better health.  
  

#### c. Dr Rakesh is divided between a doctor and a son. As a son, he loves his father and worries about his weakening health but as a doctor, he is strict on his father’s diet and medicine. In your view, what else could Rakesh have done to make his father’s final years more comfortable?

With no doubt Rakesh is a good son and a good inspiring doctor. Though he did remain devoted to his father. Actually he is divide between a doctor and a son. In my view, Rakesh could have been more polite and respectful in his behaviour with his father in the final years. He could have let his old father take the lead and give him the same independence and freedom because it is the age where parents feel like they are left behind. Rakesh could have also given his father some personal space and let him dictate how and when his son help him. His loud-speaking made his father very troublesome and miserable. So, if he had spoke to his father politely and in respectful way, he could have made his father’s final years more comfortable.  
  

#### d. What does the story say about the relationship between grandfather and grandchildren?

Desai’s story “A Devoted Son” shows loving companionship between grandfather and grandchildren. There is an unbreakable bond between them. Having a close relation, once the grandfather tried to bribe his grandchildren to bring Jalebis for him. He used the innocence and mean nature of a small kid for his own benefit. Though he loved his grandchildren very much but due to his meanness he tried to show his grandson the wrong path. For that reason, the relationship between grandfather and grandchildren was found to be innocent, tricky, trusty and bonding in the story.  
  

#### e. Do you call Rakesh a devoted son? Give reasons.

Yes, I call Rakesh a devoted son. He always did whatever his parents have advised or wanted him to do like; good academic performance, crucial part of a boy’s life which is marriage, and also choosing to get back to his own nation and live with his family though he could earn a lot of more money living in the USA. Though Rakesh stucked at some part of giving his father a happy life but he had tried his best. He always used to separate time for his old father despite of being very busy in his professional life. He never wanted his father to suffer. Hence, I think that Rakesh is a devoted son in the sense that he is able to put his own wishes aside for their betterment.  
  

### Reference beyond the text

  

#### a. Write an essay on The Parents’ Ambition for their Children in Nepali Society. You must give at least five examples.

The Parents’ Ambition for their Children in Nepali Society

One of the primary roles of parents is to be a leader of their children. Dedicated, motivated parents are usually one of two kinds of leaders; ambitious leaders or led leaders. While ambition seems like a powerful quality in a leader, it’s flawed because ambition is self-serving. On the other hand, a leader who is a good follower of truth, principles, and a higher moral authority, even if it is inconvenient to themselves or lacks showiness is a powerful influence for good in the lives of their children.  
  
Parents have great ambition for their children in Nepali Society. Most of the parents want their children to be the best person in the society such as the best doctor, the best engineer, the best teacher, and many more. Due to parents’ ambition for their children, some children work hard to achieve their destination and get succeed at the end. But due to this the children are forced to do the thing other than their interest and follow the paths shown by the parents blindly. Due to which many children are lacking behind in Nepalese society. Thus, parents should listen to the voice and aim of their children as well for their better in future.  
  

#### b. Medicines replace our diets in old age. What can be done to make old ageless less dependent on medicine?

As we get older, the body becomes less efficient and weaker. It cannot fully function the way it used to on young age and hence many viruses or diseases enter into our body. At old age our immunity system also becomes weak and our body cannot fight or prevent from the severe diseases. So to prevent form this medication is done. Hence medicines replace our diets in old age.  
  
To make old ageless less dependent on medicine, they should do the following things:  
  
– Exercise daily by running, jogging, playing sports, dancing or brisk walking.  
  
– Eat three nutritional meals a day with all the food groups (carbohydrates, proteins, fats, vitamins, and minerals).  
  
– Make sure to get enough sleep, i.e., 7-8 hours per night.  
  
– Drink about 10-15 glasses of water a day to prevent dehydration especially in summer.  
  
– Don’t sleep too much or too less, because either can lead to fatigue.  
  
– Avoid harmful habits like smoking, drinking, drugs, etc.  
  

#### c. Write an essay on “Care of Elderly Citizens” in about 300 words.

Care of Elderly Citizens

Elderly refers to old people who have crossed middle age. The old age is the final period of human life. During this time a person needs love and affection and proper elderly care. It is said that caring for the elderly is the moral duty of every man. Generally, an old person faces different health issues and thus he or she needs proper care. The length of an old person’ life depends on how much care he/she gets. Taking care of the elderly is not a naïve task. The care needs for the elderly is very limited.An old man doesn’t have much requirement. He /she only need a little affection, care, and a homely environment to spend his/her final stage of life.  
  
We all should know how to take care of old people. But in today’s busy schedule some people consider the elderly a burden. They even don’t want to spare time for their parents. And thus they prefer to put their old parents in old age homes rather than taking care of them. This is nothing but a shameful act. Being a human we all should know the importance of elderly care. In every country, there are different laws to protect the elderly. But the elderly care law can’t do anything if we don’t change our mindset.