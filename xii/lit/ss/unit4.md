## Unit 4
# The Treasure in the Forest
### Summary

  
The story begins with two men, Evans and Hooker, “stranded British wastrels”, heading in a canoe towards a coral island in the heat of the noon sun, after having paddled all night from the mainland. Hooker is studying a map, which the narrator reveals they have stolen from a Chinaman, Chang-hi, whom they murdered during the theft. Chang-hi had by chance discovered the treasure left behind by a shipwrecked h4ish galleon, and had decided to rebury it elsewhere, at a location revealed by his map. One aspect of the map puzzles Evans and Hooker though; part of it is covered by little dashes pointing in every direction.  
  
Evans and Hooker identify the spot indicated on the map, and after beaching their canoe they strike into the interior of the island, through the forest. They soon discover the identifying pile of stones just as the map says, but alongside it lies the purple and swollen body of a Chinaman who had evidently himself been looking for the treasure, as they can see some half-exposed yellow bars of gold in the hole he had been digging. The men assume the Chinaman to have been one of Chang-hi’s associates, who had decided to try and claim the treasure for himself.  
  
Evans starts to pick up the gold ingots to take them back to the canoe, but as he does so feels a thorn prick. The two men load as much of the gold as they can drag back to the boat in Evans’s jacket and set off, but after about a hundred yards Evans’s arms start to ache, he becomes sweaty and he begins to convulse. Hooker, in rearranging the ingots on the jacket after Evans’s collapse, himself feels a thorn prick, and at last realizes the meaning of the little dashes on the map; Chang-hi had protected his treasure with thorns “similar to those the Dyaks poison and use in their blowing-tubes”.  
  
The story ends as Hooker lies dying alongside the “still quivering” body of his companion.  
  



### Understanding the text

  

#### Answer the following questions.

#### a. Describe the expository scene of the story.

The story opens with two treasure hunters, Evans and Hooker in the canoe approaching the land, the thicker and deeper green forest, sloppy hill with a little river flowing to the sea.  
  

#### b. What does the map look like and how do Evan and Hooker interpret it?

The map looks like very old rough map. By much folding it was creased, unclear and worn to the pitch of separation. Evan interprets twisting lines in the map as the river and the star as the place whereas Hooker interprets the dotted line and straight line and the way to the lagoon in the map.  
  

#### c. How did Evan and Hooker know about the treasure?

Evan and Hooker knew about the treasure by hearing the Chinese man conversation and the map he had.  
  

#### d. Describe Evan’s dream.

During their journey, Evan began to doze and saw a dream. Evan had a dream about the treasure and Chang-hi. In the dream, they were in the forest and saw a little fire where three Chinese men sat around it and talked in quiet voices in English. Evans went closer and he knew that Chang-hi took the gold from a h4ish galleon after shipwrecked and hide it carefully on the island. He worked alone and it was his secret, but now he wanted help to get the gold back. There was a battle and Chang-hi was brutally killed by them. He thought that someone was calling him, so he suddenly woke up.  
  

#### e. What do the two treasure hunters see when they walk towards the island?

The two treasure hunters saw three palm trees, thick bushes and dim white flowers at the mouth of the stream when they walk towards the island. They also saw the dead body of Chinese man named Chang-hi which makes both of them surprised.  
  

#### f. In what condition did the treasure hunters find the dead man?

When Evan and Hooker reached the place where the treasure was buried, they saw a dead man lying in a clear space among the trees with a puffed and purple neck and swollen hands and ankles.  
  

#### g. How did the treasure hunters try to carry gold ingots to the canoe?

The treasure hunters tried to carry gold ingots to the canoe with the help of the coat of which one end of the collar catching by the hand of Hooker and the other collar by Evan.  
  

#### h. How were Evan and Hooker poisoned?

Evan and Hooker were poisoned as a slender (thin) thorn nearly of two inches length pricked in Hooker’s thumb and Evan rolled over him and both of them crumpled together on the ground which made them suffered a lot.  
  

### Reference to the Context

  

#### a. How do you know the story is set on a tropical island?

The story “The Treasures in the Forest” has been set on a tropical island. It begins with two men, Evans and Hooker, heading in a canoe towards a coral island in the heat of the sun. It’s atmosphere, naturally variable ecosystems, thicker and green forest, freshwater lakes and streams, salt marshes and mudflats (wetland), mangrove and coastal forests, fringing and offshore coral reefs, and deep sea represents that the story is set on a tropical island.  
  

#### b. Why do you think Evan and Hooker took such a risk of finding the buried treasure on a desert island?

I think Evan and Hooker took such a risk of finding the buried treasure on a desert island because of their greed for wealth. They took it as a challenge and the risks as rewards. They were also attracted by the fact that it was the treasure left behind by a shipwrecked h4ish galleon which may cost millions of dollars. So they became crazy about the treasure and took such a risk of finding the buried treasure on a desert island.  
  

#### c. Do you think the narrator of the story is racist? If yes, what made him feel superior to other races?

Yes, I find some sort of racist feelings in the narrator of the story when he presents Evan and Hooker as superior to that of the Chinese man in the story. In fact, a racist is a person who is prejudiced against or antagonistic towards people based on their membership of a particular racial or ethnic group, typically one that is a minority or marginalized. In the story, we find the Chinese man was brutally killed by Evans and Hooker. When Hooker said to Evans, “Have you lost your wit?”, it also reflects dominating nature of Hooker over Ivan. Thus, many instances in the story state that the narrator of the story looks like a racist.  
  

#### d. What do you think is the moral of the story?

The moral of the story is that peoples can do anything for money. They can take any kind of risks for it. As in the story Evan and Hooker killed a Chinese man brutally to get their way to treasure buried in the desert, people can even take lives of other. It’s greed that is the disordered desire for more than is decent, not for the greater good but one’s own selfish interest, and at the detriment of others and society at large. Greed can be for anything but is most commonly for money or treasures and power which is much more. At the end of the story, Evan and Hooker both were poisoned. This shows that greediness leads to the destruction. So we should not run behind the money and treasure, rather than focus on our dreams and be happy with the things as much as we have.  
  

### Reference beyond the text

  

#### a. Interpret the story as a mystery story.

The story “The Treasures in the Forest” is full of mystery. The story has fine descriptions of its mysterious setting and characters. It has a fine plot with various twists in characters’ situations.It’s hard for a reader to predict what is going to happen next. The story is about two treasure hunters, Evans and Hooker who seek to find the hidden treasures in the forest left behind by a shipwrecked h4ish galleon. Some mysterious events are : Hooker murder Chang-hi, a Chinese man to steal the treasure map. Chang-hi grins at them when he gets murdered. They travel by canoe sailing towards the coral island. However, neither of them understands the intention behind it. The fearful surroundings of the forest and the final painful death of the characters create a kind of suspense among the readers. The poisonous thorns appear out to be the main cause of the death of the characters. All these things prove this story as a mystery story.  
  

#### b. Treasure hunting is a favorable subject of children’s story. Remember a treasure hunting story you read in your childhood and compare and contrast it with ‘The Treasure in the Forest.’

I have read a treasure hunting story named “Treasure Island” by Robert Louis Stevenson.  
  
Both “The Treasures in the Forest” and “Treasure Island” are almost similar.  
  
In the story, treasure hunts began when Stevenson sketched an imaginary island complete with swamps, graves, and an X to mark the spot where the “Bulk of Treasure” was buried. From his sketch, Stevenson conceived the tale of young Jim Hawkins, who finds the map in a dead man’s chest and takes up the role of cabin boy in a search for pirate gold. At the end of the story, a significant part of the treasure is left on the island, and the way is left open for a number of sequels, though none by Stevenson himself. Treasure Island is packed with vivid characters, but it’s Long John Silver who steals the show as a murderous mutineer who nevertheless spares Jim’s life. In a metafictional companion piece, The Persons of the Tale, Stevenson has Silver and his arch-enemy Captain Smollett step out of the story between chapters to smoke a pipe and discuss the intentions of the Author, of whom Silver says: “I’m his favourite chara’ter … he likes doing me.”