## Unit 7
# A Very Old Man with Enormous Wings
### Understanding the text

  

#### Answer the following questions.

#### a. How does the narrator describe the weather and its effects in the exposition of the story?

The narrator set up the picture of a rainy day to describe the weather and its effects in the exposition of the story. The rain falls continuously for three days and makes the land and houses filled with rainwater. During the rainy season, the sky and sea were changed into ash grey colour and the sand of the shore glimmered like powdered light, which had become a hash of mud and rotten shellfish. The light sparked was so weak and hard to see.  
  

#### b. Describe the strange old man as Pelayo and his wife first encounter within their courtyard.

When Pelayo and his wife first encountered the strange old man, they noticed him dressed in the manner of a rag picker. He had a bald head and looked almost completely shaved. His mouth was almost empty with no sans teeth remaining. His large and dirty buzzard wings were half-plucked and entangled in mud.  
  

#### c. Why did Pelayo and Elisenda imprison the old man in the chicken coop?

Pelayo and Elisenda imprisoned the old man in the chicken coop thinking that he is an angel and he was there to take their child.  
  

#### d. Why was Father Gonzaga not sure about the old man being a celestial messenger?

Father Gonzaga Was not sure about the old man being a celestial messenger because the old man didn’t know how to greet ministers and also didn’t understand the language of god. Father noticed this when he said good morning in Latin, he just murmured something in his dialect which father didn’t understand.  
  

#### e. Many people gathered at Pelayo’s house to see the strange old man. Why do you think the crowd assembled to see him?

Many people gathered at Pelayo’s house to see the strange old man because he was unique and strange with his buzzard wings. People thought of him as an angel or messenger of god and thought of making some fun with him.  
  

#### f. Some miracles happened while the crowd gathers to see the strange man. What are these miracles?

In the story, it is mentioned that some miracles happened while the crowd gathers to see the strange man. Those miracles were the blind man who couldn’t recover his eyes but grew three new teeth, the paralytic person who couldn’t walk but almost won the lottery and the leper whose shore flourished sunflowers.  
  

#### g. State the irritating things that the people did with the strange old man.

The people gathered at Pelayo’s house also did some inhuman and teasing tasks with the strange old man. Such irritating things include throwing stones at the old man and burning his side with an iron rod. Which made him injured and he was not capable of moving. He laid there like a dead man for half an hour and woke up full of tears. People pulled his feathers too.  
  

#### h. How and why was the woman changed into a spider?

The reason behind the changing of the woman into a spider is lightning. Without any permission, she went out to dance and when she was returning from the forest a massive lightning strike shattered the sky and a bolt of brimstone hit and converted her into a spider.  
  

#### i. Describe how Elisenda saw the strange man flying over the houses?

Elisenda saw the strange man flying over the house from her windows while she was chopping vegetables in the kitchen. While doing so, she felt a breeze and moved to the window to check whether it is. She then saw the elderly guy was trying to fly and after a while, she saw that he succeeded to fly and vanished as quickly.  
  

  

### Reference to text

  

#### a. The arrival of a strange old man at Pelayo’s courtyard arouses many suspicions and explanations. Explain how the neighbour woman, Father Gonzaga and the doctor speak of the strange man. Why do you think these three people give three different kinds of interpretations?

The appearance of a strange old man at Pelayo’s courtyard produces many suspicions and explanations. According to the neighbour woman, he was an “angel” who came for their child, but the poor fellow is so old that the rain knocked him down. When Father said Good Morning in Latin, he muttered something in his dialect which father couldn’t understand.  
  
Father Gonzaga was not sure about the old man being a celestial messenger because he (old man) did not understand the language of God or know how to greet His ministers. He looked much too human, he had an intolerable smell of the outdoors, and the backside of his wings was sprinkled with parasites. There was nothing about him measured up to the proud dignity of angels. So Father told to the viewer that an old man who has wings wasn’t an angel. But the crowd didn’t pay attention to Father Gonzaga.  
  
According to the doctor, the strange man couldn’t be alive for a long time. He found so much whistling in the heart and so many sounds in his kidneys. It seemed the body of an angel was complete as a human organism but he couldn’t understand why other men didn’t have them too.  
  
In my opinion, these three people give three different kinds of variations to a strange old man because they are all involved in different types of activities and they all see the strange old man with their different views. An old woman believes in things like an angel, ghosts, etc. Father Gonzaga was a person who believed in god and interpreted everything with proof. And a doctor was a person who see the strange old man as his patient.  
  

#### b. This story belongs to the genre of ‘magical realism, a genre perfected by Gabriel Garcia Marquez in his novels and short stories. Magical realism is a narrative technique in which the storyteller narrates the commonplace things with magical colour and the events look both magical and real at the same time. Collect five magic realist happenings from the story and argue why they seem magical to you.

The story ‘a very old man with enormous wings’ belongs to the genre of ‘magical realism’, a genre perfected by Gabriel Garcia Marquez in his novels. Following are the five magic realist happenings from the story:  
  
At the beginning of the story Pelayo saw a very old man, lying face down in the mud in his garden who couldn’t get up in spite of his tremendous efforts which also had wings. The writer here uses Magical realism techniques through the word ‘enormous wings’ of an old man. This show that an old man belongs to another universe or world.  
  
When Pelayo and Elisenda tried to talk with the strange old man, he answered in an unbelievable dialect with a strong voice. It shows the example of a magical link in the sense that he was from another mysterious world.  
  
After seeing the old man, the old woman called him the “angel” who came for their child, but the old man is so old that the rain knocked him down. The word ‘angel’ itself justifies the magical link in the story.  
  
The modification of the woman in spider for having disobeyed her parents by going out to dance without any permission to a spider. In his childhood, she had crept out of her parent’s house to go to a dance, and while she was coming back through the dark forest after having danced all night without permission, a massive thunderclap hit and through the crack came the lightning bolt of brimstone that changed her into a spider. This also links the story to magical realism techniques.  
  
As shown by the story, the doctor failed to treat the old man. According to the doctor, the strange man couldn’t be alive for a long time. He found so much whistling in the heart and so many sounds in his kidneys. An old man became weak and sick. His feathers had fallen down. But quickly his feathers regrow again and fly in the sky. This also links the story to magical realism.  
  

#### c. The author introduces the episode of a woman who became a spider for having disobeyed her parents. This episode at once shifts people’s concentration from the strange old man to the spider woman. What do you think is the purpose of the author to bring this shift in the story?

The author introduces the episode of a woman who became a spider for having disregarded her parents by going outside to dance without any permission. The scene of shifting a woman into a spider is somewhat breaking the attention of an old man, I think, the author brought this shift in the story is to show another example of magical realism. The intention of the author was to show us the greed of humans. In the beginning, Pelayo, and Elisenda take care of a strange old man when they benefit from him by charging five cents admission to see. But when a spider woman comes and people pay less attention to an angel, they do not care about him. Old man drags here and there. The house of an old man collapse due to rain and sun but they didn’t repair it.  
  

#### d. The story deals with the common people’s gullibility. How do Pelayo and his wife take advantage of common people’s whim?

The story ‘A Very Old man with Enormous Wings’ deals with the common people’s gullibility. Pelayo and his wife saw an old man with wings in their garden. When they wanted to talk with him, he answered in an impenetrable dialect with a strong voice. They were unable to understand his language. So they called an old woman who was a witch. She called an old man an “angel” who come to take their child but he is so old that the rain knocked him down. Pelayo pulled him out of the mud and locked him up with the hens in the wire chicken coop. A short time afterwards the child woke up without a fever and with a desire to eat. They now felt like magic just happened. Then they felt generous and decided to put the old man on a raft with fresh water and provisions for three days and leave him to his fate on the high seas.  
  
But when they get early in the morning and saw that, their courtyard was full of crowds and they were amazed by seeing that. The news of the angel spread like a fire. People didn’t even listen to Father Gonzaga’s voice that he was not an angel. Pelayo’s wife got an idea to charge five cents admission to see an angel. They gathered a lot of wealth from it and built a luxurious house for themselves.