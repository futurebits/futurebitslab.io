## Unit 1
# Neighbours
### Understanding the text

  

#### Answer the following questions.

#### a. Describe how the young couple’s house looked like.

The young couple’s house was small, but it’s high ceiling and paned windows gave it the feel of an elegant cottage. From his study window the young man could see out over the rooftops and used car yards the Moreton Bay figs in the park where they walked their dog.  
  

#### b. How did the young couple identify their neighbours in the beginning of their arrival?

The young couple identify their neighbours in the begining of their arrival by the sound of spitting, washing and day break watering.  
  

#### c. How did the neighbours help the young couple in the kitchen garden?

The neighbours helped the young couple in the kitchen garden by advicing them about spacing, hilling, mulching and providing the bagful of garlic cloves for planting.  
  

#### d. Why were the people in the neighborhood surprised at the role of the young man and his wife in their family?

The people in the neighborhood were surprised at the role of the young man and his wife because his wife used to do work in hospital while he used to live in the house working on his thesis and cook for his wife when she return back home.  
  

#### e. How did the neighbours respond to the woman’s pregnancy?

The neighbours responded to the woman’s pregnancy by smiling tirelessly. The man in the deli gave her small presents of chocolates and him packets of cigarettes. In the summer, Italian women began to offer names. Greek women stopped the young woman in the street, pulled her skirt up and felt her belly, telling her it was bound to be a boy. By late summer the woman next door had knitted the baby a suit, complete with booties and beanie and the Polish widower next door had almost finished his two-car garage.  
  

#### f. Why did the young man begin to weep at the end of the story?

The young man began to weep at the end of the story because he was greatly touched by the help of neighbours which wasn’t expected by him due to the human feelings of neighbours towards them.  
  

#### g. Why do you think the author did not characterize the persons in the story with proper names?

I think the author didn’t characterize the persons in the stroy with proper names because he wants to generalize the case not to specific person but also for the every person who are culturally and linguistically from different society. So the writer make the couple for the universal character and he tries to share his idea that in neighbourhood, humanity remains even after having different languages and cultural norms.  
  


### Reference to the context

  

#### a. The story shows that linguistic and cultural barriers do not create any obstacle in human relationship. Cite some examples from the story where the neighbours have transcended such barriers.

The story shows that linguistic and cultural barriers do not create any obstacle in human relationship. Humanity and love is one of the such feelings that links the persons. It doesn’t look race, caste, language, nationality, culture or language. The love, respect and kindness of a person towards othe beautify the person or shows a real person. In this story also, due to different languages there was some misunderstandings on newly couples towards their neighbours. But when they were in problems the neighbours helped them alot. Some examples where the neighbours have transcended such barriers are as follows:  
  
1\. In spite of the different languages and cultures, the neighbours gave advice to the young couple about spacing, hilling and mulching the vegetables in the kitchen garden.  
  
2\. The polish widower rebuilt the falling henhouse of the young couple although they didn’t understand his words.  
  
3\. The young couple offered heads of cabbage and took gifts of grapes and firewood from neighbours.  
  
4\. The neighbours gifted chocolates, knitted the baby a suit when the couples were going to be parents.  
  

#### b. The last sentence of the story reads “The twentieth-century novel had not prepared him for this.” In your view, what differences did the young man find between twentieth-century novels and human relations?

In my view, the young man as the writer used to think that the people in twentieth were selfish and helpless and his thought become true for sometimes when he had just shifted to the new community. The people of the community are noisy and annoying. They used to make noises and shout each others. Even the small kids also have poor sanitation. But when he and his wife need small help too, all the neighbours helped them without saying a word. Yeh way of counselling each othes in nedd and sharing happiness with each othe touches his heart and proved him wrong. He found the huge difference between his thinking and reality. His thinking changed and he said that the novel for which he was researching had not prepared for him.  
  

#### c. A Nepali proverb says “Neighbors are companions for wedding procession as well as for funeral procession.” Does this proverb apply in the story? Justify.

Yes, this proverb applies in the story. Neighbours are the real companions of life who keep on passing their lives with us in various situations. Neighbours are need in every step of life no matter joy or sorrow. They are the companions for wedding procession as well as a funeral procession. Here in the story, we find the nieghbours involvement in various events. The story has mainly focused on three close neighbours who are seen living their lives sharing and caring for eachother. We find the concept of share, care and help among the neighbours in the story. During the pregnancy, the young woman was cared, assured, presented gifts by people of her neighbourhood. These neighbours are seen passing time teaching each other, enjoying and shouting, living in a neighborhood. Thus neighbours in the story are seen as the best companions as mentioned in the proverb.  
  

#### d. The author has dealt with an issue of multiculturalism in the story. Why do you think multiculturalism has become a major issue in the present world?

Multiculturalism is the way in which a society deals with cultural diversity, both at the national and at the community level. Multiculturalism can take place on a nationwide scale or within a nation’s communities. It may occur either naturally through immigration, or artificially when jurisdictions of different cultures are combined.  
  
I think multiculturalism has become a major issue in the present world because along with making people of different ethnic and cultural backgrounds together, it invites various social problems such as failure to assimilate, ethnic segregation, and adaptation issues such as school dropout, unemployment, and high crime rates etc. Also, migration is another reason for multiculturalism. People travel across different places of different countries and they settle in any place where they get proper facilities and job due to which multicultural peoples are found in same city or region. Due to this many peoples in neighbourhood don’t know each other and the relation, love, compassion between the neighbours found in the past days are not found in the present days. In this way, multiculturalism has become a major issue in the present world.  
  

### Reference beyond the text

  

#### a. Write an essay on Celebration of Childbirth in my Community.

Celebration of Childbirth in my Community

Childbirth celebration is the ceremony or ritual to celebrate the birth of a child with joy and happiness. My community is a hindu majority community with the majority of castes like Brahmin, Chhetris, Newar, Magar etc. Due to the majority of Hindu my community people worship child as the image of god. Specially girls are considered as goddess, laxmi and sarswati in my community. Every good works are started with worshipping the girls.  
  
When a baby get birth in my community, people congratulate the parents and celebrate it as a ceremony. When the child become of 6 days, there is a ritual known as chhaiti. In this day it is believed the god comes to write the future of that baby. So, the lamp is burn all over the night and a paper and pen is kept below the childs pillow so that God will come and write the child’s future. After that at the 11th day of child birth there is naming ceremony called ‘Nwaran’. At this the day the child is give a name. When the child is of 5-6 months, there is weaning ceremony in which the child is feded with different kinds of foods especially rice. This day is also known as rice fedding day.  
  
This is how the people celebrate in my community at the birth of a child.  
  

#### b. Do the people in your community respond with similar reactions upon the pregnancy and childbirth as depicted in the story? Give a couple of examples.

Yes, the people in my community also respond with similar reactions upon the pregnancy and childbirth as depicted in the story. As in the story our culture also includes child birth and pregnancy. In our community, a pregnant woman is blessed by all her relatives. She is given alot of advice and cares. Pregnant womes are helped in each and every aspects regarding food and nutrition. After knowing about a woman’s pregnancy in the community, community members (especially women) will visit her at her home and spend time discussing the mother’s condition. They celebrate child birth by providing shower gifts, chocolates, and clothing on the families of pregnant women. They also come up with a lot of names for the unborn child, both male and female. In this way the peoples express their love and affection upon the pregnancy and celebrate the child birth in my community.