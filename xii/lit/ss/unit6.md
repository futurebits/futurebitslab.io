## Unit 6
# The Half-closed Eyes of the Buddha and the Slowly Sinking Sun
### Understanding the Text

  

#### Answer the following questions.

#### a. How does the tourist describe his initial impression of the Kathmandu valley?

The tourist describes his initial impression of the Kathmandu valley through the words of appreciation of natural sceneries, landscape, the fragrance of soil, and the clay-made homes painted in red, yellow, and white. He feels the air filled with the serenity of mountains and is fascinated by views of the valley.  
  

#### b. According to the tourist, why is the West indebted to the East?

Tourists insist that the west is indebted to the east because the west has provided different civilizations through Puranas, brass- figures and ivory decorations. In addition, the manuscripts of palm leaves, copperplate inscriptions and ornaments of the west have a different value and this fascinates the west. The atmosphere, cultural and religious harmony is what the west is indebted to the east.  
  

#### c. How does the tourist interpret the gaze of the monks and nuns?

The tourist interprets the gaze of the monks and nuns as ‘the samyak gaze’. They believe that the gaze of monks has a holy sight. They take it as an uncontaminated, free from all impurities and discriminations.  
  

#### d. Why do the tourists think Nepali people are wonderful and exceptional?

The tourists think Nepal is wonderful and exceptional because of their co-existent lives and peaceful nature.  
  

#### e. What are the different kinds of communities in the Kathmandu valley and how do they coexist with each other?

Aryans, non-Aryans, Hindus, and Buddhists are the various kinds of communities in the Kathmandu Valley. They co-exist with each other due to the effect of Nepali soil that allows them to grow together and live together in harmony and peace.  
  

#### f. What does the tourist feel about the temple of Adinath?

When she saw the Adinath temple from Chobhar hill, she feels the living example of Nepalese tolerance. Nepal is stand out with the example of harmony because of coexistence and variety of gods, religions, and philosophies  
  

#### g. Why does the guide take the tourist to the remote village?

The guide takes the tourist to the remote village to find the harsh reality of people living beyond the scenario. He wants to see her the poverty and sympathetic elements of the beautiful country which was never talking in any novels or books nor seen by other tourists and neither captured by their cameras.  
  

#### h. What does the innocent village couple think of the doctor?

The innocent village couple accepts her as the eldest son who has brought a life-restoring treatment across the seven seas for his brother.  
  

#### i. What are the differences between the paralyzed child and his sister?

The difference between the paralyzed child and his sister as mentioned in the story are:  
  
Paralyzed child (boy) is very sick as his whole body is useless; he cannot speak, he can’t move his hands, he can’t chew his food, or even spit. His body is not in the situation of heeding commands from his brain. The thing that indicates himself alive is his eyes.  
  
Meanwhile, his sister is quite fine as her whose body functions properly work. She crawls around, picking up everything she comes across and putting it into her mouth, knocking over the beer, overturning the cooking stone.  
  

#### j. Why does the guide show the instances of poverty to the tourist?

The guide shows the instances of poverty to the tourist to make her feel how actually so-called bright faces are in reality. In the beginning, tourist only sees the bright aspect of the country i.e. various cultural and religious diversity, natural beauty and Himalayas, the gaze of monks and nuns. So, the guide shows him the pulse of reality to guide him through the instances of poverty and sympathetic way i.e. poverty and sympathetic aspect of the beautiful country in the farmer’s house.  
  

 

### Reference to the Context

  

#### a. Which narrative technique is used by the author to tell the story? How is this story different from other stories you have read?

The author uses the technique of ‘stream of consciousness technique’ in this story. This story is different from other stories I read ever now because of the appearance of double views on the story which shows the good and bad aspects. Unlike other many stories where the author writes only about the event of the story from one side but if you read this story, it deals with the monologues of two characters a tourist guide of Kathmandu valley and a foreign tourist. in other many conventional stories, I read ever follow the stream of awareness techniques where a narrator shows the action and events through fictional characters or other ways.  
  

#### b. How is the author able to integrate two fragments of the narration into a unified whole?

The author is able to integrate two fragments of the narration into a unified whole by providing the insights and examples of different eyes, views, experiences and linking them to two different aspects. The author shows the reality of Nepalese(eastern) poverty and sympathetic aspect which the western not see from their eyes and doesn’t capture from their camera nor write in their books. They only see the upper beauty of the Himalayas and green forests. But they don’t understand and feel the inner reality of the eastern and their pain suffocated in the poverty and the life spent in lacking. The author shows the hardship, diseases that the people face in the community. The tourist only pictures the things of beauty But a guide makes her see the things of reality and sympathetic pain.  
  

#### c. The author brings some historical and legendary references to the story. Collect these references and show their significance in the story.

In the story ‘The Half-closed Eyes of the Buddha and the Slowly Sinking Sun’, the author brings some historical and legendary references in the story. They are:  
  
**Manjushri and his deed:** It is believed that Kathmandu was once a lake and Manjushri cut a gorge at a place called Chobhar Gorge, near Chobhar Ancient Hill Village, and drained away the waters to establish a habitable land and that’s where the Kathmandu valley was formed.  
  
**Cultural, tradition, and art crafts:** it is mentioned in the story that easterner had given many things to Westerners; Puranas, images of brass and ornaments of ivory, manuscripts of palm leaves and inscriptions on copperplate and also civilization and many musical instruments. If we destroy all the history books then these are things which would again create a new tradition, religion, and culture and made us rich in culture, tradition, religion, and air crafts.  
  
**The gaze of monks and nuns:** The story shows the gaze of monks and nuns who received alms and spread the law in the nooks and fissures of the Kasthamandap with ‘the samyak gaze’. This is a sight that perceives everything in its purest form.  
  
**Historical Relationship:** The close relationship of Nepal and Tibet at the time of the licchavi dynasty is mentioned in the story.  
  
**Half closed eyes of buddha and setting sun reflected in the eyes of the Buddha:** The Half closed eyes of buddha and the setting sun reflected in the eyes of the Buddha represents peacefulness and harmony in the country by which people feel the love and peace.  
  

#### d. The author talks about the eyes in many places: the eyes of the shaven monks and nuns, eyes in the window and door panels, the eyes of the Himalayas, the eyes of the paralyzed boy, the eyes of the welcoming villagers and above all the half-closed eyes of the Buddha. Explain how all the instances of eyes contribute to the overall unity of the story.

  
Different eyes are mentioned in the story: the eyes of the shaven monks and nuns which is compared with eyes ‘samyak gaze’: a sight that senses everything in the purest form. The author mentions other many eyes in the story; eyes in the window and door panels, the eyes of the Himalayas, the eyes of the paralyzed boy, the eyes of the welcoming villagers, and above all the half-closed eyes of the Buddha. These all eyes represent Nepal as a country that is rich in culture, religion, tradition and diversity.  
  
The half-closed eyes of Buddha represent Nepal as a peaceful country where people feel peace and warmth. The author connects all these eyes for the overall unity of the story through the connection of cultural, traditional, religious aspects which made Nepal rich. He insists that if all the history books were even burnt then also these eyes will create the old tradition of love and peace.