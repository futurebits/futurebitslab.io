## Unit 2
# A Respectable Woman
### Understanding the text

  

#### Answer the following questions.

#### a. Why was Mrs. Baroda unhappy with the information about Gouvernail’s visit to their farm?

Mrs. Baroda was unhappy with the information about Gouvernail’s visit to their farm because she was looking forward to a period of unbroken rest, now, and undisturbed tete-a-tete with her husband.  
  

#### b. How was Gouvernail different from Mrs. Baroda’s expectation?

Mrs. Baroda had formed an image of Gouvernail in her mind as tall, slim, cynical; with eyeglasses, and his hands in his pockets but he was different from her expectation. He was slim enough, but he wasn’t very tall nor very cynical, neither did he wear glasses nor carry his hands in his pockets.  
  

#### c. How does Mrs. Baroda compare Gouvernail with her husband?

Mrs. Baroda compares Gouvernail with her husband by their frankness. Mr. Gaston Baroda, her husband was frank and chatty but Gouvernail was not frank and he was quite silent person.  
  

#### d. Why and how did Mrs. Baroda try to change Gouvernail’s solitary habits?

Mrs. Baroda was a frank woman and wanted. Gouvernail to be frank chatty and demanding to make him feel at home. She tried to change Gouvernail solitary habits by being frank and offering wordy hospitality.  
  

#### e. How does Gaston disagree with his wife on Gouvernail’s character?

Mrs. Baroda was taking Gouvernail seriously and Gaston disagrees with his wife on Gouvernail character by saying that Gouvernail gave no trouble to her and he didn’t like commotion.  
  

#### f. Why is Gaston surprised with his wife’s expression towards the end of the story?

Gatson is surprised with his wifeâ€™s expression towards the end of the story because she never like the arrival of Gouvernail but at the end she proposed, wholly from herself, to have Gouvernail visit them again.  
  



### Reference to the context

  

#### a. What is the cause of conflict in Mrs. Baroda’s mind? What role does Mrs. Baroda ‘being a respectable woman’ play in the story?

The cause of conflict in Mrs. Baroda’s mind is her attraction towards Gouvernail and the fear of society. Being a respectable woman Mrs. Baroda controls her feeling and let the social norms win. She was so attracted to her husbandâ€™s friend Gouvernail, she wanted to touch his face, lips with her sensitive fingers but she doesn’t do that and controls herself being a respectable woman.  
  

#### b. Sketch the character of Gouvernail and contrast it with Gaston.

Gouvernail is the Mr. Barodaâ€™s college friend who came to spend a week or two in his friendsâ€™ sugar plantation. Gouvernail is a slim and attractive man. He doesn’t wear eyeglasses and he is so lovable and inoffensive. He is a silent guy and loves peace. He neither frank nor too chatty. He loves to sit on portico and enjoy the cool air with cigar. He accepts what Baroda couple gives and doesn’t demand anything. Some characters of Mrs. Baroda and his friend Gouvernail are just opposite. Mr. Baroda is so frank and chatty but Gouvernail is too silent. Gouvernail smokes but Mr. Baroda does not. It can be said that Mr. Baroda is extrovert and more sociable man whether in contrast Gouvernail is introvert and less sociable man.  
  

#### c. Why does Mrs. Baroda not disclose her feelings towards Gouvernail to her husband?

There are some battles in life which a human being must fight alone. Mrs. Baroda was a respectable woman and if she discloses her feelings towards Gouvernail to her husband, it may cause a serious problem. A husband never likes his wife being attracted to another man. Many husbands have given divorce to their wives after knowing that they are attracted towards other man. Moreover, if she reveals her feelings the friendship of Mr. Baroda and Gouvernail may weaken. That’s why Mrs. Baroda does not disclose her feelings towards Gouvernail to her husband.  
  

#### d. The last three sentences of the story bring a kind of twist. After reading these three sentences, how do you analyze Mrs. Baroda’s attitude towards Gouvernail?

The last three sentences of the story bring a kind of twist. Mrs. Baroda liked Gouvernail but due to the fear of society and fear of loss of her respect she didn’t want Gouvernail to come in their house. But last she proposed, wholly from herself, to have Gouvernail visit them again. She said to her husband, “I have overcome everything! You will see. This time I shall be very nice to him”. After reading this I think Mrs. Baroda has overcome the fear of society. And now, she will not control herself and will flow with her emotion. I think she will do everything what her heart wants to do with Gouvernail as she has said that she will be nice to him. In another point of view, we can say that she has overcome her feelings and emotions towards Gouvernail and will be normal to him.  
  

### Reference beyond the text

  

#### a. The entry of an outsider into a family has been a recurring subject in both literature and films. Narrate a story real or imaginative where an outsider’s arrival destroys the intimate relationship between the husband and the wife and causes break up in marital relationship without direct fault of anyone. Anton’s Chekhov’s story ‘About Love’ is a story on this subject.

The story of “About Love” has been written by a Russian writer Anton Chekhov. “About Love” is a story about love affairs. Here in this story, the third love story somewhat reflects the same ideas as seen in the story of Kate Chopin’s “A Respectable Woman”.  
  
The third story has presented the love story of the major character Alyohin and Anna, the wife of Dimitry Luganovich.  
  
When Alyohin is elected for honorary justice of the peace, he has to go to town for work. There he meets another judge whose name is Dimitry Luganovich. Once he goes to the house of Luganovich to have dinner. There he meets Anna Luganovich, the wife of Dimitry Luganovich. The beauty, youth and intelligence of Anna attract him. Alyohin’s mind is disturbed by Anna’s memory. He starts meeting with her. They pass time together for hours. They share gifts. They go to the theatre and also share their joys and sorrows. Alyohin falls in love with her, but his morals and thoughts stop him to express his love openly. As a result, Anna suffers from mental tiredness. Both of them suffer a lot due to middle-class moralities. But later on, Alyohin sees the illusion that Anna enjoys her life and doesn’t pay attention to him. Due to their relationship, Anna suffers from mental tiredness. Finally, the Luganovich family has to move. Alyohin goes to say goodbye to Anna inside the train. When their eyes meet for the last time, both cry and hug each other. Alyohin kisses Anna and expresses his deep love. Finally, both separate from each other because Anna is leaving for Creamea for her treatment.  
  

#### b. Mrs. Baroda makes an expectation about Gouvernail even before meeting him. Suppose you are a mature girl/boy and your family members are giving you pressure for getting married. Write in about 200 words describing what qualities you would like to get in your future husband/wife.

Marriage is a sacred relationship between two people. Simply, it is the union of two people that creates a family tie and carries legal, social or religious rights and responsibilities. It is a lifetime decision and should be taken carefully as our whole life will depend on the decision we take today. Before marriage, we need to think as well as know about the person with whom we are going to get married. It is a lifelong relationship that’s why we have to choose a perfect life partner.  
  
If I get pressure from my family members for getting married, I would like to have the following qualities in my future wife.  
  
1\. Lovable and caring: My future wife must be lovable and caring. She must be lovable and caring for all my family members.  
  
2\. Well mannered: She must be well mannered. She should be polite and well-mannered to all.  
  
3\. Beautiful: She must be beautiful not only from her physical appearance but also from her heart. She should be outstanding in performing the role in the family.  
  
4\. Responsible and respectable: She must be a responsible and respectable wife. She must know her responsibilities towards family and spend her life being a respectable wife.  
  
5\. Educated: She must be highly qualified and know the way of educating others. She must have the quality of making others realize the importance of education in life.