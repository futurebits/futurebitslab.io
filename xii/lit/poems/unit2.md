## Unit 2
# Every Morning I Wake
### Understanding the text

  

#### Answer the following questions.

#### a. When does the speaker pray to the Lord?

The speaker prays to the Lord in the morning time and evening time.  
  

#### b. What does the speaker pray for?

The speaker prays to the Lord for having his loving eye on all poor creatures who are mortal.  
  

#### c. Who are the ‘poor creatures’? Why does the speaker call them ‘poor creatures’?

The ‘poor creatures’ mean all the innocent creatures that are on planet earth. The speaker calls them ‘poor creatures’ because they are born to die and it is not sure whether they can see another morning or not.  
  

#### d. What does Milk Wood sound like? A type of wood or a place? Why?

Milk Wood sounds like a place because there is no use of article before this noun. If it is a type of wood then an article either ‘a’, ‘an’ or ‘the’ must have been used according to grammatical rules.  
  

#### e. Why do the inhabitants of Milk Wood bow to the setting sun ‘but just for now’?

The inhabitants of Milk Wood bow to the setting sun ‘but just for now’ because they know that they are not permanent and they are ending the prayer of that day and wishing to see another day.  
  
  

### Reference to the context

  

#### a. Discuss “Every Morning When I Wake” as a prayer to the God.

“Every Morning When I Wake” is a prayer poem by Dylan Thoma’s. In this poem, the speaker prays to the magnificent God to have mercy on ordinary inhabitants living under the Milk Wood. The speaker prays to the Lord to keep his loving eye on all mortal poor creatures. He says the remote of all creatures is in the hand of the Lord and prays to have mercy on them. He prays to the Lord to let him/ them see the next morning as no one knows what is going to happen tomorrow.  
  

#### b. Why does the speaker make a prayer to the God, but not to a king, a billionaire or a scientist?

The God is the creator of all beings. The God is immortal and all creatures are created and destroyed by him. The remote of all creatures life is in the hand of the God. A king, a billionaire and a scientist are nothing in front of the God. They are just humans created by the God. They are born to die but the Lord is immortal. Hence the speaker makes a prayer to the God, but not to a king, a billionaire or a scientist.  
  

#### c. How does the poet highlight the magnificence of the God?

‘Every Morning I Wake’ is a prayer poem by Dylan Thomas. In this poem, the speaker has highlighted the magnificence of the God. He says that the God is the creator of all creatures who can destroy it too. The God is immortal. We can see next morning because of the blessings of the God. He makes us and is guiding us. He see the best side of us as nobody is perfect to do so. In this way the poet describes the magnificence of the God by highlighting his powers.  
  

#### d. How does the rhyme scheme of the poem reinforce its message?<

A rhyme scheme is the ordered pattern of rhyming words at the end of each line of a poem. The rhyming structure of this poem is AABB. The rhyme scheme of the poem reinforces its message beautifully. It makes the poem lovely to read. AABB rhyme scheme has made the poem much more attractive. It is a prayer poem in which the speaker prays to the God to have mercy on all creatures. For this poem, the rhyme scheme that has been used is the best.  
  

Reference beyond the text
-------------------------

  

#### a. Does the God exist? Give your opinion.

“Does God exist?” is a controversial topic which has been discussed since the beginning of human civilization. It depends upon our belief. Some people believe in the existence if the God and some do not. In my opinion, the God exists. The God live in our mind. The existence of God is like the existence of radiation. We just see the effects of it. We don’t see God but we find effects in our lives. Unexpected and miraculous things are the example of the existence of God. Although there is no physical presence of God but the God exists in our sub conscious mind. Hence god exists upon us and he is immortal and intangible.