## Unit 5
# Soft Storm
### Understanding the text

  

#### Answer the following questions.

#### a. When does the speaker grow soft? Enlist the occasions when he grows soft?

The poem ‘soft storm’ composed by Abhi Subedi is filled with a touch of compassion and investigates the insanities of tumultuous times. The speaker has said he grows soft when:  
• He hears the tumult of earth.  
• The sky grew like crocuses  
• The moon skids down  
• The moon sang of lampposts and gutters in this seamless city.  
  

#### b. What do you understand by ‘this seamless city’?

I understand the term “this seamless city” as a place that is free from all forms of disturbance, free from awkward transitions, and indications. The city, from boundaries and full of freedom for human choices.  
  

#### c. Describe the poor children portrayed in the poem.

In the poem, the poor and homeless children seen in Thamel are portrayed. They were hungry and crying with hunger under the bat-bearing tree at kesharmahal.  
  

#### d. What do you understand by ‘the unwedded gardens of history’?

The term ‘unwedded gardens of history’ signifies the culture and history of the Kathmandu valley that has been ignored for a long time.  
  

#### e. Why was the forlorn child wailing?

The forlorn child portrayed in the poem was wailing to find his mother in the corridors of violent history.  
  

#### f. What do you understand about ‘soft storm’?

The term ‘ soft storm’ signifies its meaning itself by presenting the concept of a storm that is soft or smooth. In this poem, the poet’s disturbed feelings that are not disastrous are generalized by the term ‘soft storm’.  
  

#### g. Why does the speaker call our time ‘mad time’?

The speaker points out time as mad time because the speaker finds out the disturbing mixture of destruction. The narrator finds unusual and selfish behaviour of people. People are mannerless and their activities are unlawful. Their thoughts and actions are reasonable with stupidity. Modern people are trying to dominate and keep themselves up with materialism.  
  
The speaker observes the activities like stones growing on flowers, rhododendrons blooming in winter, the earth is full of tumults in the song of the birds, moon hums melodies, history rashes under the lampposts, and birds share the bizarre journey over the warming earth. Thus the speaker claims mad time.  
  

#### h. What does the speaker want to do in “hard times”?

The poet wants to melt like a rainbow in “hard times”.  
  
 

### Reference to the context

  

#### a. The poet uses the word ‘soft’ with the words like ‘storm’ and ‘gale’, which generally refer to disorder and violence. What effect does the poet achieve through the use of such anomalous expressions?

The poem ‘soft storm’ composed by Abhi Subedi expresses the touch of compassion and the inner disturbance of the poet. The title itself glorifies the emotions of the poet as they were disturbed but yet not disastrous.  
  
In the poem, the speaker uses the word ‘soft’ with the words like ‘storm’ and ‘gale’ to express his inner disturbance. Generally, the anomalous expression refers to the syntactically well-formed but semantically meaningless. In the poem, the expressions ‘soft storm’ and ‘softness rose like a gale’ are unusual and paradoxical. They are used to define the psychological effect of a speaker. By connecting such ideas, the poet is trying to achieve the ability to express his inner state through psychology.  
  

#### b. What is the speaker’s attitude towards the time he describes in the poem?

The speaker expresses his thoughts toward the time by using the term ‘ mad time’. He considers the time to be mad as he experiences different unusual things and unlawful activities. The things happening around him were unusual and mannerless. Society is in complete disorder and mannerless. Also, he observes that modern society is out of control and it gives him inner disturbance so his attitude toward the time seems not to be positive.  
  

#### c. What is the speaker like? Is he a rebel? Why? Why not?

The poem ‘soft storm’ describes the inner emotion of the speaker when he observes modern society. With the touch of compassion, the speaker reveals the tumultuous nature and describes what he feels. The poet finds our society in complete disorder. Society and people are suffering from corruption. People with hunger are seen everywhere in the streets. He witnesses modern people ignoring glorious history.  
  
On the other hand, most things are unusual and unlawful. People are forgetting about humanity and are behaving inhumanely with each other. They have forgotten the mutual respect and sympathy. In the poem, the poet seems to challenge these things. So we can call him a rebel though his rebellious nature is not directly presented.  
  

#### d. Explain the stanza below in your own words:

_I became soft  
when I saw  
a blood-stained shirt  
speaking in the earth’s ears  
with bruised human lips  
in the far corner  
under the moon  
of history and dreams  
playing hide and seek  
in open museums  
of human times._  
  
The above lines are extracted from the fourth stanza of the poem ‘soft storm’ composed by Abhi Subedi. As the title glorifies, the poem describes his inner emotion using anomalous expression. He seems to be rebellious indirectly to the unlawful activities of modern society.  
  
Through the above lines, the poet expresses his suffering from uneasy feelings when he observes a person in miserable condition during the night. When he sees a person with a blood-stained shirt during the night, he experiences the inner soft storm inside him. In the poem, the person is lying unconsciously on the ground in the moonlit night. Here, human dreams and achievements are presented by the moon of history. When the moon appears in the sky, the moonlight falls on a place of cultural and historical importance. Through this natural interplay during the night, the poet describes people who are indifferent to the injured person who might be the victim of violence.  
  
Through this natural interplay during the night, it seems people are indifferent to the injured person who might be the victim of violence.