## Unit 3
# I Was My Own Route
### Summary

  
‘I Was My Own Route’ is a poem written by Julia de Burgos, a writer of Carolina. Puerto Rico, who collected the experiences of a literary writer, journalist and the freedom fighter. Her poems are full of racial and gender sentiments where she provokes the equality between male and female and the black and the white race. She has been also regarded as a contemporary Latino writer who depicts how the women are burdened with the patriarchal ideologies from the past. Therefore, de Burgos urges the women to detach themselves from the past so as to redefine their own identity.  
  
The poem ‘I was My Own Route’ has altogether 6 stanzas. Each stanza expresses the powerful feelings of the poet who is always remaining in between masculinity and her own femininity. She begins her poem reflecting her past feelings when she wished to convert herself as the wishes of the men. In such acts, she has found a hide and seek game between her own instinct and the instinct of the patriarchal society. The same game inspired herself to move forward to investigate a new path that is totally new for all. Though it is challenging, she joyfully accepted it.  
  
In discovering a new path, she moves alone mainly to get her internal happiness and a feeling of intimate liberation. She has described her journey to a new path beautifully where she faced a serious problem in balancing herself and the truth of the time. However, she expresses her joy in discovering a new route of her life that has no history, even she doubts for its future. Anyways she is happy enough living in the present and waiting for the response of the time. To denote this, she has repeated a line in the poem. “a game of hide and seek with my being but l was made of nows”. This refrain has captured the main essence of the poem. The poem is written in free verse and it is the perfect example of a poem of the marginalized community.  
  

 

### Understanding the text

  

#### Answer the following questions.

#### a. Why did the speaker try to be the way men wanted her to be?

The Speaker tried to be the way men wanted her to be because she was a modern woman and actually she didn’t like to be a puppet of male ideologies. Infact, she was determined to fight against the prevailing male domination.  
  

#### b. What do you understand by her feet ‘would not accept walking backwards’?

The term her feet ‘would not accept walking backwards’ means that she didn’t like to live a life being inferior to men. She was digging out her own route that could lead the entire female race in a similar position to men that is the destination of freedom.  
  

#### c. Who are the old guards? Why did they grow desperate?

The old guards are the rigid members of male-dominated society. They behave women as inferior creatures and dominate them in each and every aspect of their lives. They grow desperate because Julia was advancing her steps to the liberation of the women race.  
  

#### d. How did the speaker have ‘a feeling of intimate liberation’?

The Speaker had a feeling of intimate liberation by advancing steps forwards to a new route despite difficulties. Ultimately she made a separate route separating more and more in spite of pains from the old route made by males.  
  

#### e. Why did the speaker’s desire to follow men warp in her?

The speaker’s desire to follow men wrapped in her because she sensed the rigid rules and regulation imposed by men on woman. She wanted to become independent, enjoy freedom and happiness, and utilize her own potentials which were suppressed by men ideologies that is why she twisted them.  
  

### Reference to the context

  

#### a. What does the speaker mean when she says she was playing a game of hide and seek with her being’?

The line “she was playing a game of hide and seek with her being” appears in the third line of the first stanza and is repeated in the last stanza too. Hide and seek is a game played by the children in which one player is blind folded, other players hide in different place and the blindfold is unfolded and he or she finds the other places from the hiding places. “A game of hide and seek is also used as an idiom which means a situation in which one is constantly evading or avoiding partly the other. In the poem, the line a game of hide and seek with my being means that the speaker is trying to avoid the norms and limitations set by males to females. Men wanted her to be a woman defined as by them but as a modern and rebellious being, she was evading males.  
  

#### b. Why, in your view, was her back ripped by the old guards as she was advancing forward?

The Speaker says “At each advancing step on my route forward, my back was ripped by the desperate flapping wings of the old guard” in the second stanza. She was walking ahead on the path of women’s liberation challenging the chains of male ideologists. She was advancing forward the desperate flapping wings of the old guard was pushing her back. The limitations and chains of patriarchal ideologies can be found as the old guard was obstructing her on the way forward intimate liberation of women race. In her advancing steps forward, the old guard imposed several threats to her.  
  

#### c. What, according to the speaker, did it feel like to be free?

According to the speaker, she felt being free is like getting cherished liberation. It is like she won independent identity being free from all kinds of social norms and limitations imposed on women by the men.  
  

#### d. Why does the speaker prefer the present to the past?

The speaker prefers the present to the past because the situation of her past was miserable. She was one of the victims of male domination. Her family background was not so good. Even one of her siblings’ died of malnutrition. She was living under the shadow of a male-dominated society. But in the present, she has become an iconic person, a pathfinder, and a savior of the female race. She has set a route for all the women who can walk freely pursuing their own identity. She feels proud of herself and deserves the homage.  
  

#### e. John Donne, in his poem “No Man is an Island”, says, “No man is an island entire of itself.” Would Burgos agree with Donne? Do you agree with Donne or Burgos?

John Donne (1572-1631) is an English poet and is considered the preeminent representative of the metaphysical poets. He is also famous for his quotes. The quotation, “Noman is an island entire to itself” is taken from his poem “Noman is an island” which means that no one is truly self-sufficient. Everyone must rely on the company and comfort of others in order to thrive. Donne believes in co-existence. Every man is a part of the whole entire population. All human are an equally integral part of the collective group of humanity. We must value and respect all lives. It is said that “a man and a woman are two wheels of a cart”. The cart can’t move ahead if one wheel is broken. Burgos wanted to avoid male’s existence in terms of getting liberation to women. Males and females are equal and the co-existence of males and females shapes a balance in the society. Her concept is impractical in reality; it is just a way to express her race against males.  
  

### Reference beyond the text

  

#### a. Write an essay on My Idea of Freedom.

My Idea of Freedom

A freedom, what a wonderful word! How much energy there is in it! How much opportunity, dream, even believe that only we ourselves determine our capacity to do something, to achieve new horizons, to acquire new skills.  
  
I am sure that each and every one of us has his or her own definition for this word. Someone believes it to be somewhat ephemeral, unachievable. But somebody has it like a deliberate way of life and for others it is a goal they crave it with all their heart.  
  
So, what is the enigma of this seems to be a simple word? What is freedom? And what is importance of freedom in our life.  
  
To me, freedom means to be able to learn from my mistakes. If I didn’t have freedom, I would have to do what the top authorities always tell me to do. I don’t have any room for mistakes so it would be harder to learn about life.  
  
Freedom also means having the time to do things right. No freedom, in this case, would mean that I wouldn’t have any time for fantastic, clever thoughts. I would have to do things extremely precise and quick. When I don’t have freedom, I am under pressure constantly. While I am under so much pressure, it makes it a more stressful world to live in.  
  
One last example of my idea of freedom is being able to do many things without being forced into doing anything. No freedom means that I might have to enlist in the army reluctantly. I might even be forced to quarter troops and watch them take over my home! If I didn’t have freedom, I might have to get married at a young age and start a family which I have no intention of doing at my age. My life minus freedom would equal being controlled with everything. When I don’t have freedom I can’t do anything except for what the strict laws tell me to do.  
  
In summary, we are lucky to be in a society of freedom. Just remember, we are the land of the free and the home of the brave!