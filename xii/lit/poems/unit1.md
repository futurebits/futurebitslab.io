## Unit 1
# A Day
### Understanding the text

  

#### Answer the following questions.

#### a. How does the poet describe the morning sun in the first stanza?

In the first stanza, the poet describes the morning sun as the beautiful beginning for a new day which brings hope, glow and beauty along with joy and happiness.  
  

#### b. What does the line ‘The news like squirrels ran’ mean?

The line ‘The news like squirrels ran’ means that with the beginning of a new day, everything changes so fast and everyone starts accomplishing their task in a hurry after the sunshine.  
  

#### c. What do you understand by the line ‘The hills untied their bonnets’?

The line ‘The hills untied their bonnets’ implies that hills are no longer covered with darkness and have begun to glow because of sunshine.  
  

#### d. Is the speaker watching the morning sun? Why? Why not?

In reality, the speaker is not watching the morning sun because all the visions that she had created are not possible to occur in same time. The rising and setting of sun is not possible to occur simultaneously.  
  

#### e. How does the sun set?

The sun sets by taking all the happiness, glow and beauty of a day and leaving the darker side of dusk.  
  
 

### Reference to the context

  

#### a. What, according to the speaker, is a day?

According to the speaker, a day is a minor form of journey throughout the life which usually begins with many new hopes and lots of happiness. After learning many new things and gaining lots of experience throughout the day, it comes to an end leaving a dusk.  
  

#### b. What purpose does the hyphen in the first line serve in the poem?

A hyphen (-) is a punctual mark used as pauses, longer ones than commas or semi-colons. It enhances the methodical, unhurried nature of the scene. Poet has filled the gap by letting readers imagine after the end of the phrase.  
  

#### c. What makes this poem lyrical and sonorous? Discuss.

The poem “A Day” by Emily Dickenson is both a lyrical and sonorous poem. Emily has used her magic wand to make this poem lyrical and sonorous. She has depicted her own experience of dusk and dawn in her writing. The pronoun “I”, as used in lyrical poems, indicates the poet’s persona. On that note, ‘A Day‘—like most of Dickinson’s poems—is a lyric; it expresses a powerful thought from the perspective of a single persona. This poem’s musicality is enhanced by the use of sound techniques such as alliteration, assonance, rhyme, and rhythm.  
  

#### d. Who are the target audience of the speaker? Why?

Basically, children are the target audience of the speaker because the poem has tried to reveal the truth of human life by comparing it with a day. The speaker is imparting the message about the beginning and ending of a day where a kid accomplishes his journey from innocence to experience. Thus, the speaker is delivering her beautiful message about life to everyone mostly the children.  
  

#### e. The poem seems to describe a day for children. How would the adult people respond to this poem? Discuss this poem with your parents/guardians and write the answer based on their responses.

As this poem speaks about the reality of human life, it is somehow obvious that the adult people will respond normally and will find it meaningful because they also have gained the same experience throughout the life-time.  
  

Reference beyond the text
-------------------------

  

#### a. Observe your surroundings of one fine morning and write a poem based on your own experience.

My Surroundings

I look at my surroundings,  
The paintings and pictures,  
Some are good some are bad,  
Some are happy and some are sad,  
They are doing sports or dancing,  
Others are kissing or singing,  
I wish I could be one of those happy people again,  
Singing and dancing every day,  
But I find myself depressed and deceived,  
I long to have a perfect life.  
  
  

#### b. Write a personal essay on A Day in the School.

A Day in the School

Everyone loves school life and mostly the beautiful events which one experiences in a school. People develop a sense of emotional and social connection with the school. The school teaches us so many life lessons and values.  
  
But, there comes a day when we all need to part away from our school and our friends. It is difficult for anyone to leave the school one day and forever. When I recall my last day at school, I become very emotional. It was a moment of painful separation.  
  
That day it felt like we had a really short journey together that it passed so fast. Some of my friends became too emotional that tears started rolling down their cheeks. There was a small gathering arranged in our school hall.  
  
Our juniors welcomed us to the hall which was decorated for us. Everyone gathered to bid us goodbye and to wish us for a better future. Our juniors gave speeches. Some of them sang and danced for us. They also asked us to give speeches. Some of my friends and I went to the stage and performed a group song. I also gave a speech on how we spent our school days there.  
  
Our teachers came to us and started consoling us. They tried to inspire us with motivational words. Our headmaster praised us for being disciplined and honest in school. She also advised us to maintain the same wherever we go. All other teachers encouraged us and told us to do hard work in life because it would help us to be successful in life.  
  
The day was a mixture of sadness and happiness. I felt happy I was going to a new school. I was sad because I was leaving my teachers and was parting from our dearest friends. It was a hard day for every one of us. This is how I experienced my last day at school.