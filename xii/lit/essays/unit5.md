## Unit 5
# Human Rights and the Age of Inequality
### Understanding the text

  

#### Answer the following questions.

#### a. What is the first human rights declaration adopted by the United Nations?

The first human rights declaration adopted by the United Nations is mobilization for economic and social rights.  
  

#### b. When is Human Rights Day observed?

Human rights day is observed on 10th December every year.  
  

#### c. What is the goal of the Universal Declaration of Human Rights?

The goal of the Universal Declaration of human rights is to assure the most basic entitlements and key values of human welfare and rights like equality, dignity, freedom, fairness and respect. Also, its goal is to assert justice and peace in the world alongside the foundation of freedom.  
  

#### d. What are two big stages that involve writing the history of human rights about that of political economy?

The two big stages that involve writing the history of human rights about that of the political economy include the heroic age of national welfare after the condition of World War II and the bitter enemies of the new cold war era in 1948.  
  

#### e. What are the facts that have been missed in Roosevelt’s call for a “second Bill of Rights”?

The facts that have been missed in Roosevelt’s call for a “second Bill of rights” are:  
– It marked a provincial America’s late and ginger entry into a North Atlantic consensus which is already foreordained.  
– His highest promise was “special privileges for the few” instead of protection of masses which creates a ceiling of inequality.  
– He hoped that it would h4 the globe but it was organized nationally instead of organizing internationally.  
  

#### f. Write the truth expressed in Herodotus’s Histories.

The truth expressed in Herodotus’ histories is to assure global socio-economic justice, local socio-economic justice. it would require redistribution from rich to poor by novel forms of legal activism.  
  

#### g. Why is the Universal Declaration of Human Rights important to you?

The universal declaration of human rights is important to me as it assures the basic rights of humans and provides justice, equality, equity for all human beings equally. it aimed to remove injustice, partiality, discrimination, inequality and so many inhuman behaviors from society.  
  


### Reference to the context

  

#### a. Does the essay give ways on how to stigmatize inequality? Explain.

The essay “human rights and age of inequality” written by Samuel Moyn is the essay briefing about how the concept of human rights arises and what it brought to human welfare. The essay focuses on the stigmatization of inequality rather than giving some specific ways how to stigmatize inequality. History shows us that there are wrong kinds of agents who are not fearful enough to provoke redistribution. Surely, opponents will arise someday if inequality grows like this.  
  
Thus the need for justice and equality is in demand. so the new human rights movement sorted of different views for the common people can bring and justify social equality and liberation. it is very necessary to remove partiality and discrimination by proper supervision, monitoring and enacting strict laws. For the welfare of common people and all human beings, inequality must be removed as it is believed to be a stigma for society.  
  

#### b. Is another human rights movement necessary? Why?

In my opinion, another human rights movement is necessary. Also as the essayist finds human rights is functioning under different political and economical suppression, I also agree on that point. He wishes to see other human rights movements in the coming days due to many reasons. The people with political and economical power and in higher positions are violating the law. In human affairs, inequality has been contained. The practice of nepotism and favoritism is still in use and it has suppressed different new ideas and talents. More than that political and social hegemony is still prevalent. Laws and rights are only documented rather than taken into practice. And the major thing is the classification of society based on their economic conditions like rich or poor has brought injustice and inequality. Thus the need for another human rights movement is true.