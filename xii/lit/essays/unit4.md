## Unit 4
# Humility
### Understanding the text

  

#### Answer the following questions.

#### a. Describe the claim of the Chinese nationalists about human history.

Chinese nationalists claim that history began with the Yellow Emperor and the Xia and Shang dynasties. They believe that anything that was accomplished by other rulers like westerners, Muslims or Indians is a bit imitation of Chinese achievement.  
  

#### b. What do pious Muslims believe about human history?

Pious Muslims believe that all history follows the Quran. All the history before Prophet Muhammad was mostly meaningless and all history after that following Quran’s revelation revolves around the Muslim ummah.  
  

#### c. What did the Aztecs firmly believe about the universe?

The Aztecs firmly believed about the universe that annual sacrifice is the reason behind the existence of the universe. Without annual sacrifice, the sun wouldn’t rise and the whole cosmos would collapse.  
  

#### d. What, according to the essay, are the universal human abilities?

According to the essay, the universal human abilities are art, creativity, spirituality and mortality.  
  

#### e. How are the basic yoga postures derived from the shape of the letters of the Hebrew alphabet?

The basic yoga postures derived from the shape of letters of the Hebrew alphabet as the yoga posture Tuladandasana imitates the letter ‘daled’, Trikonasana imitated the form of the Hebrew letter ‘aleph’ and so on.  
  
  

### Reference to the context

  

#### a. How do Hindu nationalists refute the Chinese claim that human history began with the Yellow Emperor and the Xia and Shang dynasties? Who do you agree with, and why?

Hindu nationalists refute the Chinese claim that humans began with the Yellow Emperor and the Xia and Shang dynasties by claiming that Indian sages developed the theory of planes and nuclear weapons long before Plato, Confucius or Einstein and the Wright brothers.  
  
In the Hindu holy books of Hindus, different evidence of discoveries and inventions are found long before any scientists proved them. Hinduism is found to be the oldest religion in the world according to various scientific as well as archaeological studies. Thus I agree with the Hindu nationalists over Chinese claims through different evidence that was found in Hinduism.  
  

#### b. The author has dealt with a controversial debate on human history. Why do you think history has been a major contested issue in the present world?

The essay ‘Humility’ written by Yuval Noah Harari is about the different controversies about human history. The essay presents the thoughts and people’s beliefs in human history according to their religion.  
  
In my opinion, history has been a major contested issue in the present world due to egoism. I would like to call egoism the supremacy of their belief in their religion only rather than understanding others. Most individuals believe that their religion is the oldest and they are the center of the universe. They believe that their culture is superior to all others. According to them, everything originated from their culture. So because of egoism human history has been a major contested issue. The author wishes to remove a sense of superiority and domination from humanity.