## Unit 2
# Marriage as a Social Institution
### Understanding the text

  

#### Answer the following questions.

#### a. According to the author, what is marriage?

According to the author, marriage is much more than the sum of spouses which is defined by loyal, moral and conventional assumptions and have a variety of close personal relationships and associations.  
  

#### b. How is marriage an institution?

Marriage is an institution in the sense that it is a means of meeting social, economic, physical and family requirements by two individuals, and it is linked to other institutions such as education, the economy and politics.  
  

#### c. What are the rules that a marriage has?

Marriage has a complex set of rules that help in the planning and maintenance of the rights of the spouses to each other within a society..  
  

#### d. Why does marriage matter to men?

Marriage matters to men because it provides structure to their lives and organizes their goals and ambitions.  
  

#### e. What is one of the central problems in modern society?

One of the central problems in modern society is putting various legitimate boundaries around modern individuals seemingly limiting the desires for wellbeing, comfort, luxury and prestige.  
  

#### f. What does social capital consist of?

Social capital consists of a vast network of people who are all connected by a bond of trustworthiness and reliability.  
  

#### g. What is normative marriage? Explain.

Normative marriage is a form of social control, a way by which behaviors and aspirations are channeled appropriately. It is a force greater than the individuals involved because it represents the collective sentiments of others. Marriage in this view is qualitatively different from other intimate relationships.  
  



### Reference to the context

  

#### a. Discuss six dimensions that define normative marriage in America.

The six dimensions that define normative marriage in America are: marriages are entered voluntarily by mature, heterosexual adults with the expectation that husbands will be the principal earners, that both partners will be sexually faithful, and that married partners will become parents. Although many marriages depart from these ideals, the ideals still constitute the core of normative marriage as it is expressed in law, religion, and custom. Normative marriage is a form of social control, a way by which behaviors and aspirations are channeled appropriately. It is a force greater than the individuals involved because it represents the collective sentiments of others. Marriage in this view is qualitatively different from other intimate relationships. Much of the meaning of marriage in men’s lives will be found in these rules. Together, these six premises provide a definition of marriage that will inform the rest of this work.  
  

#### b. Do marriages differ according to culture? How is your marriage practice different from marriage in America?

Marriage practices vary across cultures. Every culture has its own way of conducting marriage according to their traditions and customs. Most cultures share common customs and practices, while some cultures have unique practices. Even within our country, the marriage practices of one geographical place differ from those of others. However, the Hindu religion is practiced by the majority of Nepalese people.  
  
Marriage practices in Nepal is totally different than in America. Marriage practices are not so important in the American culture, and couples are free to choose; to follow common or to choose a combination of practices. The American culture is not strong on the institution of marriage as it is for Nepal. The current American society does not consider marriage institution; its importance comes after career and financial matters. Nepal considers the marriage institution as very important and should be treated with all respect by all in the society. The importance of the marriage institution is evident from the marriage practices that have remained consistent in the Nepalese culture. In Nepal, most of the marriage are arranged by the parents but in America, couples choose their partner themselves. Marriage not only connects the two individuals (boy and girl), but also unites two different families. There is equal involvement of the parents of the couple for the marriage ritual in Nepal. In this way, the marriage practices differs in our culture than in America.  
  

### Reference beyond the text

  

#### a. Write an essay on the marriage practice in your own culture.

Marriage Practice in My Culture

Marriage is the social, spiritual, cultural and legal union between a man and a woman as husband and wife. Nepal has a wide variety of ethnicity and tribal population, giving diversity to the rituals and practices involved in weddings. However, the majority of marriages in Nepal are either according to Hindu tradition or Buddhist traditions. I am from Hindu culture.  
  
In my culture the groom and his family along with neighbors, relatives and music band leave for bride’s house on the wedding day. The group of people along with the groom and music band is called ‘Janti’. The janti is leaded by the group of women carrying trays with foods and gift (called ‘Saipata’) for the bride’s family. There is traditional music band that play traditional musical instruments called ‘Panche Baja’. Behind them are groom and the group of people performing dance. The wedding ceremony is performed at bride’s house. On the arrival of janti at bride’s house they are welcomed by the parents and relatives of Bride. The saipata is handed to the bride’s family and Janti is welcomed with the feast called ‘Janti Bhoj’ (marriage party). Groom is taken to ‘Jaggey’ or ‘Mandap’ (the place to perform the wedding ritual).  
  
The most important person of the wedding ceremony is ‘Pundit’ (priest). The Pundit performs the marriage ceremony by chanting the ‘mantras’ through the holy book. The marriage ceremony is interesting due to different rituals performed in front of the sacred fire. There are numerous instructions for the couples while performing the ceremony guided by the Pundit. The ritual is performed by revolving around the sacred fire for seven time with the knot tied between the couples indicating the seven lives of togetherness. The marriage ritual is completed when the groom puts ‘Sindur’ (red vermillion powder) on bride’s head and ‘Potey’ or ‘Tilari’ (a holy Necklace) around the neck. Sindur and Potey have great significance for married women in my culture. After this the bride’s father washes the feet of the wedded couple and all the relatives and family members will bless them with ‘Tika’ (on forehead) and gifts. Finally the married couple along with the janti departs from the bride’s house dancing and celebrating with Panche baja.  
  
The wedded couples are welcomed in the groom’s house by groom’s family. Lot of entertainment activities and celebration is carried out at Groom’s house called “Ratyauli”. Ratyauli is celebration at groom’s house which includes singing, dancing and starts a day before the wedding. The marriage ritual is thus the most important as well as entertaining ritual since whole family members and relatives are involved. The wedded couple starts their new life by sharing their feeling, body, culture, and love for eternity. In this way, the couple begins their newly wed life in my culture.  
  

#### b. Is marriage a social institution? Discuss.

Marriage is a social institution. It defines a personal forms of relationship between man and women who make a long-lasting commitment to each other. Marriage is a unique social institution that could be interpreted and defined in a variety of forms and characterizations.  
  
In an emotional aspect, marriage can be generalized as being in a romantic love relationship in which it is legally monogamous. Although sharing an emotional bond with a significant other is important, there are several factors that must be taken into consideration when making a decision into getting married. A couple must understand the depths of marriage in terms of survival. This emphasizes the realistic aspect of marriage which involves money. Affording a house, a family, and the necessary items to accommodate the institution can accumulate. With the right education and career, middle and upper class couples’ divorce rates have decreased, their marital happiness is consistent along with family structure.  
  
Marriage is a socially approved courtship between two individuals in which the relationship involves sexual and economic benefits that is assumed to be permanent and includes mutual rights and obligations. It is a stable relationship in which a man and a woman are socially permitted to live together without losing their status in the community. Marriage is not merely concerned with the couple; rather it affects the whole society and future generations. The responsibilities it entrusts a couple with are thus both heavy and delicate.  
  
In Hindu view, marriage is not a concession to human weakness, but a means for spiritual growth. Man and woman are soul mates who, through the institution of marriage, can direct the energy associated with their individual instincts and passion into the progress of their souls. So, I think that marriage is a social institution.