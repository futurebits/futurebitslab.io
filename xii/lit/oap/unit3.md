## Unit 3
# The Bull
### Understanding the Text

  

#### Answer the following questions.

#### a. Why have Gore and Jitman come to see Laxminarayan?

Gore and Jitman have come to see Laxmi Narayan because they want to tell him about the death of King Ranabahadur’s bull.  
  

#### b. What, according to cowherds, is the reason behind the death of Male?

According to cowherds, the reason behind the death of the male was that he couldn’t eat more grass and couldn’t digest fine rice and a split gram soup.  
  

#### c. Why does Ranabahadur want to see the bull himself?

Rana Bahadur wants to see the bull himself because he wants to see and examine the condition of the bull. He wants the bull to be cured if possible at Gauchar, Kathmandu so it doesn’t need to be transported to a hill.  
  

#### d. Why does Laxminarayan run ahead of the convoy at Thulo Gauchar?

Laxminarayan runs ahead of the convoy at Thulo Gauchar because he wants to convey the message to cowherds to tell them to take care of the bull. He urges to massage the bull’s back feet and wave the fan at the bull. In actuality, he wants to show king Rana bahadur shah that they are taking care of the bull. If not, the king would become angry and he would punish them.  
  

#### e. Why do Gore and Jitman cry when the king declares that Male is dead?

Gore and Jitman cry when the king declares that male is dead to show their supposed sympathy and affection for the bull. They cry in a pompous manner, seeking the king’s forgiveness. They want an excuse from the king’s punishment. Otherwise, the king may become enraged and may punish them, making them responsible for the bull’s death.  
  

#### f. How do we learn that the bull is dead?

There are various indications of the bull’s death. The conversation between cowherds, Laxminarayan and the king clearly indicates the death of the bull. Alongside the physical appearance of the bull as his eyes are motionless, tail has loosened, it was breathless and ears were dropped down and he was unable to eat anything.  
  

#### g. How does the play make a satire on the feudal system?

The play ‘the bull’ written by Bhimnidhi Tiwari is one of the mirrors of the feudal system and its terrific act. The horrible acts towards ordinary people by the feudal system have been tried to show as it was then in the play. The domination and dehumanization towards ordinary people have been tried to portray in the play. The feudal system at its best is cruel and doesn’t value the people. In the play, the survived cowherds are presented in such a panic even though they have survived by the mercy of their lord. The play shows that the king’s animal receives more respect, comfort and care than the people there. The people there are living in terror and they are hiding their reality in order to save their lives in front of the king. Thus the play satires on the feudal system showing that the oppression and dehumanization of king’s worker.  
  

#### h. Write down the plot of the play in a paragraph.

The play ‘The Bull’ was written by well-known Nepali poet and dramatist, Bhimnidhi Tiwari. The plot of this play shows the emotional connection between bull and King Rana Bahadur Shah.  
   

### Reference to the Context

  

#### a. Discuss the late eighteenth-century Nepali society as portrayed in terms of the relation between the king and his subjects as portrayed in the play.

As we know, during the eighteenth century the monarchy system was prevalent in Nepal. Nepal was ruled by the shah dynasty. They were used to being strict and so is the Nepalese society. People’s choices were under the king’s or ruler’s dominance. They weren’t free to conduct their lives.  
  
The same similarity is portrayed in the play ‘the bull’. It shows a horrible society in which people are compelled to lavender the terror of kings or lords. Their masters treated them horrifically. Also if they or their opinion went against the masters, they were punished. This play shows the miserable life condition of Nepalese society.  
  
People were not given even fundamental rights. They were deprived of political and financial knowledge. They were bound by patriarchal rules and ideals. The society was purely male dominated. Men’s were allowed to marry multiple wives. In this play, Laxminarayan also has seven wives. And the king had complete control over the lives of common people.  
  
  

#### b. What does the relation between Laxminarayan and his wives tell us about the society of that time? To what extent has the Nepali society changed since then?

Laxminarayan is the main character of the play and a Forty years old legal officer and king’s bull’s doctor. Taking the scenario of his relationship with his wives, he has seven wives. Yet he is not satisfied with all of them and he is planning to marry his eighth wife. He calls his wives with different nicknames. This practice of having more wives shows about the male dominant society. Men’s are allowed to marry multiple wives whereas women’s are compelled to live their life under the full dominance and violence of their husband. Women’s were illiterate and unconscious about their rights. People and especially women’s are deprived of political and financial knowledge. Child marriage was prevalent at that time. The society was reliant under male supremacy.  
  
But the Nepalese society has altered this situation dramatically since then. The current situation of Nepali society and especially Nepali women is significantly better. Human rights and women rights are there to protect them with their basic rights. The literacy rate of women has also increased substantially. They were in a higher power of economic and political authority. The concept of equality and equity is operational in Nepalese society. Many of the patriarchal traditions like child marriage and prathas are at the end. Many organizations are working for the betterment of women. Overall the society is significantly changed.  
  
  

#### c. Shed light on the practice of chakari as portrayed in the play. Have you noticed this practice in your society?

The practice of chakari was prevalent and very popular during the king’s rule. During the royal system, there used to be a craze or we can say majority of people were involved in the chakari of their leaders, monarchs and lords. Chakari was famous for gaining wealth or gifts from the king. They aimed to please the king by doing chakari and receiving the wealth. They do chakari in the hope of their advancement of life and living but meanwhile there are serious consequences if they do not perform correctly.  
  
The practice of chakari was shown very perfectly in the play too. The play’s main character Laxminarayan and cowherds Gore and Jitman are frequently seen as doing the chakari of their king .Laxminarayan was once also punished due to his bad deed of speaking in loud voice in front of the king. They used to be very anxious from the deed of kings. When the bull dies, Laxminarayan is so aware of the king’s anger and punishment so he picks his moves carefully to save his and cowherds life from severe punishment. In the play it is shown that they are calling the bull by “the bull sir“. Therefore the action of Laxminarayan and cowherds demonstrates the chakari they are doing.  
  
  

#### d. How does Laxminarayan outsmart Rana Bahadur?

Laxminarayan outsmarts Rana Bahadur with his great trickery. Laxminarayan is 40 years old, one of the legal officers and doctor of the king’s bull. As Gore and Jitman inform him about the death of the king’s bull, he rushes to the king’s palace. He has the amazing talent of flattering the king away from his emotion. He knew king will be angry and enraged by the death of bull, so Laxminarayan didn’t inform immediately. Rather than telling the truth immediately, he tells the bull about the bull’s sickness to the king. He even tells cowherds to massage and wave fans at the bull. By this action the king believes the bull died due to sickness despite excellent care and treatment. It helps to protect the lives of cowherds. This way Laxminarayan flattered the king and protected the life of his and cowherds from the king’s severe punishment.  
  
  

#### e. Sketch the character of Laxminarayan.

Laxmi Narayan Dahal is one of the main characters of the play ‘the bull’. he is forty years old, one of the legal officers and king’s bull’s doctor. He is married to seven wives. It is shown that he is still unsatisfied with all of them despite having seven different women at home. He is planning to marry eighth wife. When he learns about the king’s bull’s death, he flatters the king using his intellectual moves. Instead of telling the news immediately, he tells the king about the bull’s sickness first. When king arrives to the cowshed at thulo Gauchar to see his bull, Laxminarayan tells the cowherds to massage and wave fan to the bull in order to please king and save his life .due to this act of Laxminarayan and cowherd, they were saved from king’s punishment. Therefore, we can say Laxminarayan was a cunning man and he unmastered the king with his trickery.