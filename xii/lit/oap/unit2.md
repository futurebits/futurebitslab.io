## Unit 2
# Facing Death
### Understanding the Text

  

#### Answer the following questions.

#### a. Where does the play take place?

The play takes place in Monsieur Durand’s dining room. He lives there with his three daughters as the owner of a boarding house, widower and former railroad worker.  
  

#### b. Why do the grocery, the baker and the butcher send their bills to the Durand household?

As Durand hasn’t paid their bills for a long time, the grocery, the baker and the butcher send their bills to Durand’s household. As a consequence, they couldn’t provide any further items until the bills were paid.  
  

#### c. Why does Monsieur Duran spend money on candles when he doesn’t have money to buy even bread?

As a strange scenario, Monsieur Durand spends money on candles when he doesn’t have money to buy even bread. This is because he wishes to burn the candles on the death anniversary of his late loving son, Rene, who died in his childhood. From the death of his son, Durand was heartbroken.  
  

#### d. Why did Monsieur Duran sell his life insurance?

Monsieur sold his life insurance to pay off the loan to his debtor. He was in the worst condition and the debtor was furious with him for not paying his money.  
  

#### e. Why has Monsieur Duran paid fire insurance?

He wants to compensate for his daughter’s financial difficulties. Thus, to file compensation he has paid fire insurance. He wants to burn down his own house and claim compensation, with the proceeds going to his daughters.  
  

#### f. How did Monsieur Duran and Mrs. Duran run out of their inheritances from both the sides?

Monsieur Durand and Mrs. Duran ran out of their inheritances from both sides due to recklessness and stupid speculation of Mrs. Duran. Both of them lost their parental and maternal assets.  
  

#### g. Why does Monsieur Duran tell a lie about his birthplace?

Monsieur Durand tells a lie about his birthplace because he used to love a woman before his marriageable age. To marry that lady, he left his hometown and migrated to Switzerland. He tells a lie to protect his and his wife’s reputation. Also, he had fought against his homeland from the side of Switzerland. So he wanted to mask his humiliation. Therefore, he tells a lie.  
  

#### h. What business is Monsieur Duran running to make a living?

Monsieur is running a lodge to make a living. He turned his house into a lodge and restaurant. His visitors may stay with him there. He made it like such a hotel with a good facility to earn money.  
  

#### i. What plan does Monsieur Duran have to help his daughters with money?

Monsieur Durand plans to burn his house and commit suicide to help his daughters with money, which will come as compensation for burning. He paid for fire insurance to claim compensation after he will burn his own home. He wishes to give his life for the sake of his daughters.  
  

#### j. How does Monsieur Duran die?

Monsieur Durand dies by committing suicide. He drinks poison and sets his own house on fire at the end of the play. The motto behind this was to help his daughters with money which will come as compensation for fire insurance policy which he had paid earlier.  
  

 

### Reference to the Context

  

#### a. Sketch the character of Monsieur Duran.

Monsieur Durand is the leading character of the one-act play ‘Facing Death’ written by August Strindberg. In this play, he is portrayed as a widower, the owner of the lodge and a former railway worker. He is financially weak and he has three daughters. They all live in the lodge. He has turned his living house into a lodge to make a living. In this play, Durand is a loving, caring and protective father who has a fragile relationship with his daughters. Due to bankruptcy, his living is being hard. He isn’t even receiving love from his daughters as they always despise him and blame him. But it is shown in the play that despite the hatred and dislike of his daughters, he gave his life for the sake of his daughters. Also, he is a devoted husband and keeps worrying about his dead son. He is a sad hero who deals with financial hardships before tragically giving his life for the sake of his daughters.  
  

#### b. How do we know that the Duran family has reached a dead end?

By watching the sequence of his downfall and his miserable condition, we know that the Durand family has reached a dead end. Mr. Durand’s family is completely bankrupt and is spending their life in the lodge, which he turned into his own house to make a living. They already have lost ancestral property.  
  
Mr. Durand’s household has been borrowing money from others for years so he is full of debt. There are bills in his name and he is unable to pay those bills. His condition is going worse and he has nothing to do. His daughters hat him and blame him all the town for that miserable condition though it was because of their mother’s recklessness and stupid speculation.  
  
His family is dying of hunger and starvation. His relationship with his daughters is not good. His debtors are forcing him to pay their bills whereas Durand is surviving by eating rat food. And in his mind, he has a plan to kill himself and burn his house. By this sequence of action, we know that the Durand family has reached a dead end.  
  

#### c. ‘The mother, though already dead, seems to have had a great influence on the daughters, especiallyTheresa.’ Do you agree?

Yes, I must agree with this sentence as the mother, though already dead, had a great influence on the daughters, especially Theresa. When she was alive she used to teach the hate towards their father. She forcefully made them obey her. Most of the time she blamed her husband and taught the same to the daughters.  
  
After the death, Mr. Durand remained silent all his life because he didn’t want his daughters to question their mother’s goodness. But due to their mother’s teaching, his daughters keep hating him. They thought and blamed their father that he was the root of their financial hardships.  
  
And especially Theresa, the most influenced by her mother is the one who always showed rude behaviour to the father. She threw the matches away when he tried to burn tobacco. Similarly, she snatches the glass of milk from him. She was cruel towards the father and never showed respect and love.  
  

#### d. Discuss the relationship between Monsieur Duran and his wife.

Monsieur Durand and his wife don’t have a sound relationship. Mrs. Durand always used to blame her husband even though she had wrecked the familial property herself. Due to her rash gambling and stupid speculation, she ruined the inherited property. She used to misuse money on lottery tickets from the family budget. She threatened her husband with becoming a prostitute for money after being beaten. She taught all her daughters to hate their father and load them with negativity. She played the main role in diverting her daughter’s mindset against their father.  
  

#### e. ‘Money determines the relationship between characters in this play.’ Elaborate this statementwithexamples from the play.

‘Money determines the relationship between characters in this play.’ This sentence is so truthful in this play. In the play ‘facing death’, money is highlighted as the main asset and set up was made accordingly. It is shown that money is very important in the life of Monsieur and his daughters. Money is the cause behind all difficulties and issues among his family. Mr. Durand’s family’s financial difficulties have led the entire family to live a miserable existence. There was a lack of many vital requirements in his family. The relationship between husband and wife and between father and daughters have become worse due to lack of money. The family faced starvation. Due to their poor economic condition, they were even unable to purchase bread. In the play, it is portrayed as Mr. Durand is eating rat food. Because of no money, he is unable to provide necessities for his daughters.  
  
Even more, the lack of money brought hatred to the girls against the father. Due to their economic downturn, they are compelled to turn their house into a lodge. The characters’ connection is being determined by money. The baker, the grocery and the butcher are refusing to provide him with items. Most of the time, his daughter blamed him and showed impolite behaviour toward him. Thus the protagonist had been through a lot due to lack of money.  
  

#### f. Monsieur Duran kills himself so that his daughters would get 5000 francs as the compensation from theinsurancecompany. What does his plan tell us about him?

Monsieur Duran killed himself so that his daughters would get 5000 francs as compensation from the insurance company. This plan of his tells us about their love, care and concern toward his daughter.  
  
Mr Durand is a sad hero of the play who lives his life in tragedy, hatred and starvation. in the play ‘facing death, it is shown that he commits suicide by consuming poison and setting his house on fire to obtain compensation from the fire insurance company which will help his daughter’s financial condition. he spends his whole life in economic hardship and downturns. As a result of the economic crisis, he has always been blamed by his wife and his daughters. His plan for his daughters’ well-being illustrates that he is a loving, caring and kind father who is serious and concerned about his daughter’s future.  
  

#### g. Discuss Facing Death as a modern tragedy.

Modern tragedy generally refers to the oppression of the hero. It deals with the tragic events of society rather than fortune or fame. It came with a sad ending and shows the downfall of the hero. The play ‘facing death’ is a modern tragedy as it ends with the tragic death of dad hero Monsieur Durand. Modern tragedy, since used from the twentieth century, deals with various real problems rather than being obsessed with ideology. It deals with socio-economic conditions, mental health or destruction, common problems of people. The two main factors of modern tragedy are realism and naturalism.  
  
The common man of modern society is the protagonist of modern tragedy. Anyway, it deals with the individual problems of a common man like family problems, financial problems, and socio-cultural problems to mental problems like loneliness and depression. The character becomes the victim of these problems and deals with hatred all around them.  
  
The socio-cultural problems, fate, economic class, gender and loneliness around them embrace the character. Thus, we can say facing death is a modern tragedy as these all characters match with the protagonist of the play.  
  
Mr. Durand is a common man and his life is full of crises. He has suffered through financial hardships and starvation. His relationship with his wife and daughters is not smooth even though he loves them very much. All of his daughters hate him. He doesn’t have money to buy even bread and he had to turn his house into a lodge to make a living. And at the end, he kills him with poison and even burns his own house for the welfare of his daughters. This sequence of the downfall of Mr. Durand demonstrates facing death as a modern tragedy.  

### Reference Beyond the Text

  

#### a. Write a few paragraphs describing the role of the father in the family.

While both parents are equally important, a father is an instrumental and key figure in the modern family. This is because he is the head of the family. Other tasks and roles performed by a father are unique and defined basing on the cultural background of the community that he comes from. However, from culture to culture, the fundamental tasks that are required of manhood are basically the same.  
  
A father should always be there for his wife and children to provide the much needed love and support. Showering them with lots of cash does not necessarily indicate love and support. He should be ready to give them an ear, comfort them and spend quality time as well as quantity time with them. This is crucial in creating an emotional tie, a prerequisite for strong and healthy family relationship.  
  
A father figure can significantly influence the life and wellbeing of their child. In families where the father figure is present, the father serves as one of the first male role models and male relationships the child will encounter. Children are extremely sensitive and observant beings and internalize relational experiences. These early interactions with their father serve as a blueprint for what a relationship with a man looks like and impacts both the father-son relationship, and the father-daughter relationship. This means that unhealthy relationships with a father figure can significantly impact not only the child’s psychological wellbeing, but their unconscious relational choices as they become adults.  
  
The father therefore plays the role of loving and supporting his spouse and offspring.