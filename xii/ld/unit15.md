## Unit 15
# Leisure and Entertainment
### Working with words

  

#### A. The words in the crossword puzzle are from the text. Find them from the text to solve the puzzle based on the clues given below.

  
**Across**  
1\. outdated, of or relating to the Middle Ages : **Medieval**  
3\. the character and atmosphere of a place : **Ambience**  
6\. highly decorated : **Ornate**  
7\. beat with a strong, regular rhythm; survive : **Throb**  
10\. with the agreement of all people involved : **Unanimously**  
11\. the destructive effects of something : **Ravages**  
  
**Down**  
1\. a large city : **Metropolis**  
2\. wandering from place to place without any purpose : **Loitering**  
4\. an unpleasant place; a place of extreme misery : **Hellhole**  
5\. an active revolt or uprising : **Insurgency**  
8\. paved with cobbles : **Cobbled**  
9\. prosperous and growing; flourishing : **Thriving**  
  

#### B. Look up the meanings of the following words in your dictionary.

  
**Amenities**  
a desirable or useful feature or facility of a building or place.  
  
**Exquisite**  
extremely beautiful and delicate.  
  
**Etiquette**  
the customary code of polite behaviour in society or among members of a particular profession or group  
  
**Accommodation**  
a room, group of rooms, or building in which someone may live or stay.  
  
**Mainstay**  
a person or thing on which something else is based or depends.  
  
**Antique**  
having a high value because of age and quality.  
  

#### C. Underline the content words in the following sentences.

a. The cat sat lazily on the dirty mat.  
b. She gives performances on the stage beautifully.  
c. Don’t count your chickens before they are hatched.  
d. I came I saw I conquered.  
e. It is my life, my very identity.  
  

  

### Comprehension

  

#### Answer the following questions.

  

#### a. Why did the author and his classmates decide to visit Bandipur?

The author and his classmates decided to visit Bandipur because all of them were fed up with the hectic pace of life in the Kathmandu metropolis.  
  

#### b. What is special about Bandipur?

Bandipur is special because it is a place where old Nepal still lives on in that cool, cobbled Newar settlement.  
  

#### c. How has the business changed in Bandipur today?

Today, the traditional variety of business has changed into tourism-related business in Bandipur.  
  

#### d. Why do the local people who have migrated to Kathmandu visit Bandipur time and again?

The local people who have migrated to Kathmandu visit Bandipur time and again because they want to be free from the hectic life of Kathmandu.  
  

#### e. How do the local people learn to speak English in Bandipur?

The local people learn to speak English in Bandipur from the local school children.  
  

#### f. What does the author mean when he says that ‘time stops in the town of Bandipur’?

When the author says that “time stops in the town of Bandipur’, he means to appreciate the natural beauty of Bandipur which catches attention in such a way that everyone gets lost in its beauty and originality.  
  

### Critical thinking

  

#### a. The writer seems to believe that rural life is better than urban life. Do you agree with him?

Yes, I agree with the writer that rural life is better than urban life. First and foremost, the tranquil surroundings and environment attracts me a lot. The village is always described as a quiet and peaceful paradise, which is a perfect place to unwind and escape from the rat-race and Hustle and bustle of the city life. Besides, the rural areas are less crowded because there are not many people and cars. The scenery in a village is beautiful as the beauty of nature is still well-conserved. Thus, people who live in the village can enjoy nature and have a peaceful mind.  
  
This condition is completely different with the city dwellers, who lead a hectic life and often feel annoyed due to great stress in their daily life. The environment in the rural area is less polluted. The air in the village is fresh and clean as there are plenty of undeveloped areas and greenery. In the rural area, there is less air pollution since there are not many factories and cars around. Living in the city may have its benefits such as better facilities and education. However, I am still in the opinion that living in the rural area brings more advantages than living in the city.  
  

#### b. Is the writer’s way of looking at Bandipur just a gaze of an urban tourist? Why? Why not?

No, the writer’s way of looking at Bandipur is not just a gaze of an urban tourist. He doesn’t visit Bandipur just as an urban tourist. Being fed with his hectic life in Kathmandu, he visit there along with his friends to feel the peaceful environment and amazing beauty of the place by his heart.  
  
Bandipur is a hilltop settlement and a rural Municipality which lies in Tanahu district of Nepal. This place is the centre of attraction for various reasons. The true and original beauty of the place attracts most of the tourists to this place. The amazing beauty and scenario of Bandipur also attracted the writer. At Bandipur, he and his friends are amazed by the local cultures, behaviours and organic and delicious food items. He enjoys various places in Bandipur along with his friends. He feels himself heartily connected to this place where he gets all sorts of refreshments which he has been looking for. He learns about places, people and their tourism businesses. The writer spends his best time in Bandipur. Thus, his gaze towards this beautiful place is more than an urban tourist.  
  

### Grammar

  

#### A. Tick the correct one.

  
a. He has obtained full mark/**marks**.  
b. More than two boys **were**/was absent.  
c. One of the boy/**boys** was not found there.  
d. His wonder knew no **bounds**/bound.  
e. I give you my words/**word**.  
f. He gave me **much**/many good **advice**/advices.  
g. He gave wrong **information**/informations.  
h. Give me two dozens/**dozen** eggs.  
i. Six miles **is**/are a long distance.  
j. The clock has struck four **hours**/hour.  
k. Would you lend me a ten rupees/**rupee** note?  
l. Nepal government makes five **year**/years development plan.  
m. They went to Singapore on a four days/**day** trip.  
  

#### B. Which of the sentence is correct in each pair? Rewrite the correct one.

  

#### a. My all books are lost. All my books are lost.

All my books are lost.  
  

#### b. His both brothers are ill. Both his brothers are ill.

Both his brothers are ill.  
  

#### c. Give me a hundred rupee. Give a hundred rupees.

Give me a hundred rupee.  
  

#### d. Everybody except me was absent. Everybody except I was absent.

Everbody except me was absent.  
  

#### e. He was died of fever. He died of fever.

He died of fever.  
  

#### f. It is raining for a week. It has been raining for a week.

It has been raining for a week.  
  

#### g. I have seen my friend long ago. I saw my friend long ago.

I saw my friend long ago.  
  

#### h. My friend has gone out before I arrived. My friend had gone out before I arrived.

My friend had gone out before I arrived.  
  

#### i. He said he has never seen him before. He said he had never seen him before.

He said he had never seen him before.  
  

#### j. He assured he will come. He assured he would come.

He assured he would come.