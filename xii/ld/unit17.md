## Unit 17
# War and Peace
### Working with words

  

#### A. What do the underlined words in the following sentences mean? Tick the best answer.

a. clamour : a loud and confused noise  
b. precariously : not securely  
c. scrutiny : examination  
d. absconded : hid somewhere secretly  
e. anglicized : characteristics of English  
f. platitudes : doubtful remarks  
  

#### B. Many words have been borrowed in English from different languages.  
Find the meaning of the following words which are borrowed from Hindi language.

  
**Veranda :** a roofed platform along the outside of a house, level with the ground floor.  
  
**Khaki :** A strong cloth of wool or cotton, often used for military or other uniforms.  
  
**Pashmina :** a shawl made from fine-quality goat’s wool.  
  
**Pajamas :** loose-fitting jacket and trousers for sleeping in.  
  
**Pukka :** genuine, excellent  
  
**Pundit :** an expert in a particular subject or field who is frequently called upon to give their opinions to the public.  
  
**Avatar :** the incarnation of a deity, particularly Vishnu.  
  
**Bangle :** A rigid bracelet or anklet, especially one with no clasp.  
  
**Cheetah :** a large slender spotted cat found in Africa and parts of Asia.  
  
**Guru :** A Hindu or Sikh spiritual teacher.  
  
**Jungle :** an area of land overgrown with dense forest and tangled vegetation, typically in the tropics.  
  
**Karma :** good or bad luck, viewed as resulting from one’s actions.  
  
**Nirvana :** Complete cessation of suffering; a blissful state attained through realization of sunyata; enlightened experience.  
  
**Shampoo :** an ideal or idyllic state or place.  
  
**Raita :** an Indian side dish of yogurt containing chopped cucumber or other vegetables, and spices.  
  

#### C. Add the given suffixes to the following words to make new words. Notice whether ‘e’ is retained or dropped.

  
**a. change + -able / -ing / -ed / – less**  
changeable, changing, changed, changeless  
  
**b. time + -ly / -ing / -ed / -less**  
timely, timing, timed, timeless  
  
**c. notice + -able / -ing / -ed**  
noticable, noticing, noticed  
  
**d. praise + -worthy / -ing / -ed**  
praiseworthy, praising, praised  
  
**e. home + -less / -ing / -ly**  
homeless, homing, homely  
  

  

### Comprehension

  

#### Answer the following questions.

  

#### a. Why did Iqbal want to sleep in the afternoon?

Iqbal wanted to sleep in the afternoon because he had spent the night sitting on his bedroll in a crowded third-class compartment of the train.  
  

#### b. How did people react with each other in the train?

People reacted in a very loud way with each other in the train.  
  

#### c. Why did the book Iqbal was reading bring commotion in the compartment?

The book Iqbal was reading brought commotion in the compartment because the book was an English book and the people thought that reading the book in the English language as wise, praiseworthy and prestigious.  
  

#### d. Why did Iqbal have to give clarification with his personal details?

Iqbal had to give clarification with his personal details about his background because the travelers in the train thought him as an intellectual and wise person and therefore, they insisted him to reveal his personal details.  
  

#### e. Who was Meet Singh and what did he report to Iqbal?

Meet Singh was a priest of Gurudwara and he reported to Iqbal about the murder of a fellow villager perhaps by a village’s dacoit named Jugga.  
  

#### f. How according to Meet Singh, was Jugga Sing different from his forefathers?

According to Meet Singh, Jugga Singh was different from his forefathers in the sense that his forefathers were also dacoits like him in the past. They never looted or harm their own village or its people. They used to keep the villagers safe from dacoits. But Jugga robbed the people of his own village and even killed the people of his villager. He killed one of the people of his village.  
  

#### g. How does the author show contradiction in Meet Singh’s character?

The author shows contradiction in Meet Singh’s character by saying that Meet Singh was not bothered by Jugga’s act of murdering fellow villager and as a priest of Gurudwara, he even wanted to swear on the holy Granth to defend Jugga from the crime of murder.  
  

#### h. Who was Hukum Chand and how did he succeed in his career?

Hukum Chand was the Deputy Shahib, also known as Nar Adami. He succeeded in his career by pleasing his Shaibs and getting promotions one after.  
  

### Critical Thinking

  

#### a. Iqbal is addressed as Babu Sahib by general folk simply because he knew English. Are Nepali people who can speak English taken with respect? Discuss the importance of learning English in the Nepali context.

Yes, Nepali people who can speak English are taken with respect. In Nepal, English is treated as a language of elite class. Most of the Nepalese think that English is closely associated with class and social status of a person. If you are fluent in English you’ll be perceived as a very respectable person who has achieved great things in life. It’s almost same as visiting a store wearing classy clothes and getting all the attention from the salespeople there. If you visit the same store with normal clothes, you are bound to get less attention. People often judge a book by its cover. For Nepalese, if the cover is in English they’ll directly buy the book, not just judge it.  
  

#### b. Do you agree with Iqbal’s comments on crime and punishment? In your view, what should the state, society and individuals do for peace and order in social lives?

Yes, I agree with Iqbal comments on crime and punishment. To cultivate and maintain a culture of peace, state, society and individuals ought to work together.  
  
Peace begins with the individual. We must realize that, as individuals, we are not powerless and that the power of one can make a difference. As individuals we must accept the responsibility to end the scourge of war and culminate a culture of peace. We must realize that peace is more than the absence of war. War is a drain on both human and financial resources and as history proves, is not an effective means of resolving conflict. Peace involves a process of individual and communal participation. It requires justice, equal rights and equal opportunities.  
  
Next, society plays a very important role in maintaining peace and order in social lives. In society, every family has to be united to make their society a fine and secure place to live in. There should be a formation of a special team in society who takes care of peace and order in society. This team has to perform its duty for peace and order management and determine the punishment to the criminals.  
  
The state also plays a very important and responsible role in maintaining peace and order in social lives. The state has to implement various rules and regulations for the sake of peace and order in social lives. It should focus attention on the security of social lives by minimizing the crime and violence from the state. The state must have strict rules against crime and criminals. It should have the proper management of punishment to the criminals too.  
  
These are the things the state, society and individuals should do for peace and order in social lives.  
  

### Writing

  

#### A. Meet Singh says Jugga a badmash. There can be such people in your locality, too. Write a paragraph describing him / her.

I met a guy during my school days, initially he was so good to me. Later he turned out to be the most dishonest guy I have ever met. I used to share all tiny and big secrets of mine with him as we had become good friends in no time. As time passed, I found out that he was not worth that. He used to reveal all those things of mine with his stupid friends and misinterpret me. When I got to know that, I was completely broken and frustrated. But being strong, I forgave him, who didn’t even sincerely apologize. I believe in — forget, forgive and move on and that is how we should deal with such people. But don’t forget the lesson you learn from them.  
  

#### B. You may have travelled by bus or train. During your travel, you might have got different experiences. Write a letter to your friend describing about your unforgettable journey.

Date: 2078-10-13  
Pokhara-7, Kaski

Dear Pratik,  
Hope this letter of mine finds you in the best of health and spirit. My heart fills with joy to share with you about one of my experience of travelling in bus to my hometown Sankhuwasabha. I traveled in Manakamana Yatayat numbered Nine Hundred and Ten boarded at 05:30 AM on 10/10/2078 and reached the destination at 06:30 PM on the same day.  
  
The journey throughout was amazing and relaxing. The view from the windowpane while it feels like trees running far from us looks so relaxing and calming. Waterfalls going by our side, fresh air coming through the window also adds excitement to the journey. The ticket price was not much expensive and was reasonable. Meals expenses were included in the ticket pricing. I got the corner seat. People traveling along with me were so friendly that we played cards/ ludo/ chess while traveling and the time passed very easily.  
  
I believe this is some experience everyone should have at least once in the whole lifetime. The time spent on the bus that day was very comforting and enjoyable. I will surely be making another trip plan in the coming days and will be asking you to join me. Till then, take care.  

Much love,  
Aayusha Sharma

  
  

### Grammar

  

#### C. Fill in the gaps with the suitable form of the verb in the brackets. Use past simple/past continuous/past perfect tenses. You may need to use negative too.

  
It was Sunday afternoon. I **was watching** (watch) a cookery programme on TV when I **realised** (realise) how hungry I was. But of course, I was hungry; I **hadn’t eaten** (eat) anything since lunch, and I **had run** (run) a race in the morning. “Biscuits!” I thought (think). My mother **had given** (give) me a jar of delicious home-made biscuits.  
  
I **went** (go) into the kitchen, **opened** (open) the fridge and **poured** (pour) some milk in a big glass. Then I **looked** (look) for the kitchen chair but it wasn’t (be) there: somebody **had taken** (take) it away. And there were no biscuits in the biscuit jar: somebody **had eaten** (eat) them all! I was sure I **had put** (put) the jar there the previous day and I **had eaten** (eat) only one cookie. It was very strange.  
  
A few minutes later, I **was drinking** (drink) my glass of milk when I **heard** (hear) a loud noise coming from the dining room. I **went** (go) there quickly and I **opened** (open) the door. I couldn’t believe my eyes. An enormous monkey **was eating** (eat) the biscuits excitedly on the kitchen chair.  
  

#### D. Rewrite the following sentences correcting the mistakes.

  

#### a. She played the flute and then she had sung in their choir.

She had played the flute and then she had sung in their choir.  
  

#### b. I borrowed Rima’s car. Had you known about it?

I had borrowed Rima’s car. Had you known about it?  
  

#### c. After the lesson had finished, we run out of school.

After the lesson had finished, we ran out of school.  
  

#### d. Had you be there? – Yes, the previous year.

Had you been there? -Yes, the previous year.  
  

#### e. Did you liked my article published in the Himalayan Times yesterday?

Did you like my article published in the Himalayan Times yesterday?  
  

#### f. I recognized him because I saw him before.

I recognized him because I had seen him before.  
  

#### g. I hadn’t gone out because I hadn’t finished my homework.

I didn’t go out because I hadn’t finished my homework.  
  

#### h. We had done nothing like this at that time.

We did nothing like this at that time.  
  

#### i. It was quite difficult. I had had no idea what to do.

It had been quite difficult. I had no idea what to do.  
  

#### j. As far as I’d known, she had always had some pets.

As far as I knew, she had always had some pets.  
  

#### k. When I met Jim, he was already a soldier for three years.

When I met Jim, he had already been a soldier for three years.  
  

#### l. He had gone to the coffee because somebody had told him.

He went to the the coffee because somebody had told him.