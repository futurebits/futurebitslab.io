## Unit 7
# Humour
### Working with words

  

#### A. The following words are synonyms and antonyms of the verb ‘laugh’. Group them into synonyms and antonyms.

  
**Synonyms**  
chuckle, giggle, chortle, crackle, snicker, groan, smirk, whoop  
  
**Antonyms**  
grin, sob, grimace, frown, pout, scowl, cry, moan  
  

#### B. Match the following emotions with their definitons.

  
**a. fear**  
an unpleasant emotion caused by the threat of danger, pain or harm  
  
**b. anger**  
a strong feeling of displeasure  
  
**c. surprise**  
an unexpected event, fact, etc.  
  
**d. disgust**  
a feeling of strong disapproval aroused by something unpleasant  
  
**e. sadness**  
affected with grief or unhappiness  
  
**f. happiness**  
a pleasurable or satisfying experience  
  
**g. relief**  
removal or lightening of something oppressive, painful, or distressing  
  
**h. triumph**  
a great victory or achievement  
  
**i. contentment**  
the quality or state of being satisfied   
  

  

### Comprehension

  

#### A. Complete the following sentences with words/ phrases from the text.

  
a. According to Sophie Scott, laughter is one of the important and misunderstood **behaviours.**  
b. After her study in Namibia, Scott came to the realization that **laughter is one of our richest** vocal tics.  
c. Studies have shown that there are **six universal emotions across cultures** based on facial expressions.  
d. Scott found out that most of the laughs have nothing **to do with humour.**  
e. The couples who laugh at each other are likely to **stay together for longer.**  
f. The primary way of communication of grown up people is **laughing.**  
g. Comedians usually find it easier to work in larger places due to **the contagious nature of laughter.**  
  

#### B. Answer the following questions.

  

#### a. Why do people giggle at someone’s pain or suffering?

People giggle at someone’s pain or suffering because giggling through someone’s pain or suffering is a way of convincing yourself (and therefore others) that you’re alright, or at least on our way to being alright.  
  

#### b. What did Scott’s study in Namibia come up with?

Scott’s study in Namibia came up with the realization that laughter is one of our richest vocal ties.  
  

#### c. How is laughter a social bonding?

Laughter is a social bonding because it is an emotion that brings us together and helps us to bond, whether or not something is actually funny.  
  

#### d. According to the author, what role does laughter play in husband-wife relationship?

According to the author, laughter makes the husband-wife relationship stronger and long-lasting by making it easier to dissipate tension after a stressful event.  
  

#### e. How does laughter work as a painkiller?

Laughter correlates with increased pain threshold, and by encouraging the release of endorphins, it works as a painkiller.  
  

#### f. What did the study find about the relation between laughter and brain?

The study about the relation between laughter and brain found that the brain responds to each kind of laughter and both seem to tickle the brain’s mirror regions – the areas that tend to mimic other’s actions.  
  

#### g. What are two emotions that the author associates with infants and adults?

Crying and laughter are two emotions that the author associates with infants and adults.  
  

#### h. How does a stand-up comedian take the audience’s laughter?

A stand-up comedian takes the audience’s laughter by interacting with them, taking the advantage of contagious nature of laughter and reducing the audience to fits of hysterics.  
  

#### i. Show the relation between laughter and crowd.

The relation between laughter and crowd is that laughter is contagious in crowd means that it can easily catch a wave when there are more people in crowd.  
  

#### j. What does the author mean when he says “there is always a meaning to it?”

When author says “there is always a meaning to it” he means that laughter is never neutral and always have a meaning although it seems to be trivial, ephemeral and pointless.  
  

### Critical thinking

  

#### a. Do you agree that ‘the couples, who laugh together, stay together?’ Is it important for married couples to have the same sense of humour? Why?

Yes, I agree with the statement that “the couples, who laugh together, stay together.” A sense of humour is an attractive trait. There is abundant cross cultural evidence that shows that being funny makes you more desirable as a mate. Simply having a funny bond doesn’t mean you two are the perfect match – apparently being able to laugh at the same things makes your partnership even stronger. A good sense of humour makes a person more attractive. Laughter is called the best medicine not without a reason, and this holds true for couples who might be going through stressful times. Studies have proved time and again how laughter helps release feel-good hormones that help to relieve stress. So, it’s important for married couples to share good laughter and have the same sense of humour whenever they find themselves bogged down by trouble.  
  

#### b. Some people believe that sometimes crying is good for health. Do you believe it? Give your reasons.

Crying is a natural response humans have to a range of emotions, including sadness, grief, joy, and frustration. Yes I believe that crying is good for health. Medical benefits of crying have been known as far back as the Classical era. Thinkers and physicians of ancient Greece and Rome posited that tears work like a purgative, draining off and purifying us. Today’s psychological thought largely concurs, emphasizing the role of crying as a mechanism that allows us to release stress and emotional pain. When we cry, we let out tons of emotions that allow our body and mind to reboot after you release all those emotions. Crying is particularly important during periods of grieving. It may even help to process and accept the loss of a loved one. People often report feeling better after they cry. That could be because crying forces us to pay attention to what triggered us and work through our emotions and thoughts. Crying might also help in understanding what’s important to us, especially if we cry over something that upset us unexpectedly. By this we can say that crying is an essential form of relief and hence it is good for health too.  
  

### Writing

  

#### B. Write a description of your favourite comedian explaining his/her personality, acting, performance and uniqueness.

Charlie Chaplin

Charlie Chaplin was an English comic actor, filmmaker, and composer who rose to fame in the silent era. He is mostly famous for his screen persona “the tramp”. Born on April 16, 1889 in London, Chaplin is considered one of the most important figures in the history of the film industry. He had been a productive and creative film maker for about 75 years before he died in 1977.  
  
Chaplin suffered from poverty and hardship in his childhood. He was sent to a workhouse twice before the age of nine. His mother struggled financially when his father was absent. When he was 14, his mother was sent to a mental asylum.  
  
Chaplin’s first performances were at music halls as a stage actor and comedian at the age of 19. He went to the USA where he was scouted for the film industry, and began appearing in 1914 for Keystone Studios. He soon developed the Tramp persona and formed a large fan base. Chaplin directed his own films from an early stage, and continued to hone his craft. By 1918, he was one of the best known figures in the film industry.  
  
Chaplin wrote, directed, produced, edited, starred in, and composed the music for most of his films. He was a perfectionist, and his financial independence enabled him to spend years on the development and production of a picture.   
  

### Grammar

  

#### B. Put the frequency adverbs in appropriate place and rewrite the following sentences.

  

#### a. I forget to do my homework. (sometimes)

Sometimes I forget to do my homework.  
  

#### b. My father has touched an alcoholic drink in his life. (never)

My father has never touched an alcoholic drink in his life.  
  

#### c. My father goes for a walk on Saturdays. (often)

My father often goes for a walk on Saturdays.  
  

#### d. We go to the movie theatre. (occasionally)

We occasionally go to the movie theatre.  
  

#### e. My brother is in America. He telephones us. (from time to time)

My brother is in America. He telephones us from time to time.  
  

#### f. My mother gets up at five o’clock. (always)

My mother always gets up at five o’clock.  
  

#### g. He does not like alcoholic drinks but takes some wine. (now and then)

He does not like alcoholic drinks but now and then takes some wine.  
  

#### h. I drink my tea with milk. (generally)

Generally, I drink my tea with milk.  
  

#### i. Have you been to Agra? (ever)

Have you ever been to Agra?  
  

#### j. The restaurant hours vary as it is booked for special events. (frequently)

The restaurant hours vary frequently as it is booked for special events.