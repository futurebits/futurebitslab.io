## Unit 1
# Critical Thinking
### Working with words

  

#### A. Find the words from the text that match with the following meanings.

  
**a. Shuffle** – walking by dragging one’s feet along or without lifting them fully from the ground  
**b. Doodle** – drawing pictures or patterns while thinking about something else  
**c. Plaque** – a piece of flat metal with writing on it  
**d. Precepts** – a general rule intended to regulate behavior or thought  
**e. Sneaky** – move or go in a furtive or stealthy way  
  

#### B. Consult your teacher and define the following thinking skills.

  
**a. Convergent thinking** Convergent thinking is the type of thinking that focuses on coming up with the single, well-established answer to a problem. It is oriented toward deriving the single best, or most often correct answer to a question.  
  
**b. Divergent thinking** Divergent thinking is a thought process or method used to generate creative ideas by exploring many possible solutions. Many possible solutions are explored in a short amount of time, and unexpected connections are drawn.  
  
**c. Critical thinking** Critical thinking is the analysis of facts to form a judgment. The subject is complex, and several different definitions exist, which generally include the rational, skeptical, unbiased analysis, or evaluation of factual evidence.  
  
**d. Creative thinking** Creativity is the ability to generate a thought or an idea that is completely new, appealing, and useful. Creative thinking is a skill that enables you to come up with original and unconventional ideas.  
  
 

### Comprehension

  

#### Answer the following questions.

  

#### a. Who was Jack? How did he make children laugh?

Jack was a kind of kid other kids like and had a lot of friends. He made children laugh by saying jokes.  
  

#### b. Why are “Sharks” important to Reid?

Sharks are important to Reid because they keep ocean clean by eating dead organisms.  
  

#### c. What does Mr. Browne think about the most important thing?

Mr. Browne thinks that the most important thing is to know oneself, or own character’s and evaluate them to bring the best version of someone.  
  

#### d. What is that has not been noticed by the student?

The thing that has not been noticed by the student is the message in the notice board of the school entrance gate that writes – “Know Thyself”.  
  

#### e. How did Jack make fun of the English class?

Jack made fun of the English class by saying that they were there to attend English class to the teacher’s question “Why are you here?”.  
  

#### f. What were the students going to do at the end of the month?

The students were going to write an essay on the basis of the Brownie’s percept at the end of the month.  
  

#### g. What particular act of students surprised a girl student?

Students used to come up with their very own personal precept, write it on a postcard, and mail it to Mr. Brownie after their graduation, this thing surprised the girl student.  
  

### Critical Thinking

  

#### a. Have you made your own precept after you read this lesson? What is it? Share it with your friends.

Yes, I have made my own precept after reading this lessons. They are as follows:  
"Success is temporary but failure is permanent"  
"Always stay Positive"  
"Believe in Karma rather than fate&quot  
"Anything that happens, happens for good"  
  

#### b. According to Josh Lanyon, “If there was one life skill everyone on the planet needed, it was the ability to think with critical objectivity.” Justify this statement with your logic.

Josh Lanyon’s statement regarding critical objectivity somewhat reflects the core idea related to the development of human skills on this planet. He has presented the fact behind the skills of the people of this planet. Here, Josh has related life skills of people of this planet with critical objectivity. According to him, the need for life skills itself was the ability within people to think with critical objectivity.  
  
People on this planet moved along with different life skills in their lives. From ancient times up to now, people have been doing development in the sector of skill development and invention. The development of people on this planet was possible due to their ability of critical objectivity which runs along with their life skills knowingly and unknowingly.  
  
Critical objectivity always moves along with life skills. People have applied various life skills in their lives which means they can think with critical objectives. Behind all these life skills, the ability of thinking with critical objectivity always remain with them.  
  

### Writing

  

#### Write an email to your friend explaining an interesting class you had.

From: binodkumar12@gmail.com  
To: shristi2011@gmail.com  
Subject: Interesting class that I had  
  
Dear Shristi,  
I hope this letter finds you in good health and high spirits. I am doing pretty fine here. I am writing to you in order to share my experience of my English classes.  
  
Compared to any other classes I have taken during my school history, I feel that no other class could give me so many joys, knowledge and pleasures than the English class could do. I learned it quite good and, in every test, I got the best mark in the class. I learned not only a lot of Basic English knowledge in my middle school but also the personal characteristics which lead my interests along through my later education life.  
  
When I was in my middle school, it used to be a great thing when I know that there would be an English class tomorrow morning because I could enjoy another interesting 45 minutes during the class. Rather than other classes, like Science, Nepali or Mathematics, I established my English-relative interests’ group in social media, which give me a great benefit in my study in the university. I believe no other class can inspire me so far to pursue a scientific result.  
  
Another important thing I achieved through the English class is to collaborate with other students. I think it certainly formed my teamwork sprite when I entered the career position as an engineer. I can still clearly remember my English teachers of primary school and high school. They are all the people of great patient and instructive characteristic. They told me not to quit when I failed again and again. They give me a lot of confidence and disciplines of the mind toward success. Saying that English class is the only class that I used to enjoy most, I would like to end this mail.  
  
Regards,  
Binod Kumar  
  

### Grammar

  

#### Rewrite the following sentences adding appropriate question tag.

  
a. Gill does not know Ann, **does he**?  
b. I’m very patient, **aren’t I**?  
c. They’d never met me before, **had they**?  
d. Listen carefully, **will you**?  
e. Let’s have a break, **shall we**?  
f. Let us invite them, **will you**?  
g. Hari used to live in France as a boy, **didn’t he**?  
h. You’d better not take a hard drink, **had you**?  
i. Sheep eat grass, **don’t they**?  
j. Mr. Pande can speak nine languages, **can’t he**?  
k. She’s finished her classes, **hasn’t she**?  
l. She barely managed to reach the goal, **did she**?  
m. Don’t let him swim in that pond, **will you**?  
n. There are lots of people here, **aren’t there**?  
  

#### Read the following situations. What do you say in these situations? Use question tags.

  

#### a. The sky is full of cloud. You can see lightning and hear thunder.

It’s going to rain soon, isn’t it?  
  

#### b. You want to pay the taxi fare but you are short by 100 rupees.

Shyam, you have to lend me Rs. 100, haven’t you?  
  

#### c. You have met a stranger at a party and you want to have a chat with him/ her.

We will talk later, won’t we?  
  

#### d. You came out of the film hall with your friend. You enjoyed the film.

The film was very enjoyable, wasn’t it?  
  

#### e. You and your friend listened to a comedian on the stage and felt spellbound by his/her performance.

He gave an excellent presentation, didn’t he?  
  

#### f. You think your friend’s father has arrived from the US but you are not sure.

Your father has not arrived from US, has he?  
  

#### g. You think Susan will join the new job tomorrow but you are not sure.

Susan will go to her new job tomorrow, won’t she?  
  

#### h. Your friend’s hair looks too short.

You have got too short hair, haven’t you?  
  

#### i. You want to go for a picnic with your friends in class.

Let’s go to picnic, shall we?  
  

#### j. You want permission from your father to go for a walk.

Let me go for a evening walk, will you?