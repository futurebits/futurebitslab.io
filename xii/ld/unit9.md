## Unit 9
# Ecology and Environment
### Working with words

  

#### A. The words/phrases in the box are from the text. Check their meanings in a dictionary and use these words to complete the given sentences.

  
a. **Logging** is one of the main reasons behind the rapid deforestation in the world.  
b. That’s one example of how the pandemic should be a **wake-up call**.  
c. **Propane** is a gas used as a fuel for cooking and heating.  
d. Emergency teams are still clearing the **debris** from the plane crash.  
e. What a **transformation!** You look great.  
f. My father made a New Year **resolution** to give up smoking.  
g. He has worked in the Army for two years. He hates that two-year **detrimental**.  
h. Emissions from the factory are widely suspected of having a/an **stint** effect on health.  
i. My father is an **occasional** smoker. He doesn’t smoke often.  
j. **Redwood** is a very tall type of tree that grows especially in California and Oregan.  
  

#### B. Choose one word from each box to make sensible compound words.

Rattle : Rattlesnake  
Sun : Sunflower  
Touch : Touchdown  
Moon : Moonlight  
Day : Daydream  
Fire : Fireworks  
Water : Watermelon  
Basket : Basketball  
Pass : Passport  
Wash : Washcloth  
Weather : Weatherman  
Grand : Grandmother  
Cross : Crosswalk  
  

#### C. Match the following words/phrases related to the ecology with their meanings.

  
**Sustainability**  
to keep in existence; maintain. To supply with necessities or nourishment  
  
**Tree line**  
the height on a mountain above which the climate is too cold for trees to grow  
  
**Precipitation**  
water that returns to the earth as rain, hail, sleet, or snow  
  
**Tropical Zone**  
the region between latitudes 23.5 degrees S and 23.5 degrees N  
  
**Kyoto Protocol**  
an agreement between countries to reduce their greenhouse gas emissions. It was established in Japan in 1997 but didn’t become international law until 2004  
  
**Pollutants**  
substances that destroy the purity of air, water, or land  
  
**Geosphere**  
the soils, sediments, and rock layers of the Earth’s crust, both continental and beneath the ocean floors  
  
**Deciduous**  
a plant that sheds all or nearly all its leaves each year  
  
**Ephemeral**  
an organism that has a short life cycle  
  
**Trash**  
items that are discarded  
  

 

### Comprehension

  

#### A. Choose the best answer.

  
a. The author of the text above has the opinion that Julia Hill **made her pastime in a tree for two years more.**  
  
b. The sentence ‘Julia had occasional visitors’ indicates she had **a few visitors now and then.**  
  
c. The logging company managed 24 hour security service around the tree to **discourage her from her campaign.**  
  
d. Ms. Hill began to respond the loggers with songs and conventional conversations because she **had unconditional love for all nature’s creations.**  
  
e. Julia Hill climbed down the tree after 738 days when her demands were **about to be fulfilled.**  
  

#### B. Answer the following questions.

  

#### a. Who was Julia Butterfly Hill? How did Hill’s campaign gain popularity?

Julia Butter Jill was a person who lived in a 200-ft-tall redwood tree for more than two years to draw attention to the continued clearcutting of California’s remaining redwood forests. Her campaign gained popularity when she started to have occasional visitors.  
  

#### b. What made Hill start her mega campaign to save redwood trees?

When she arrived in the redwood forest, she was gripped by the spirit of the forest. She dropped to her knees and began to sob seeing the clearcutting of redwood trees. She sat and cried for a long time and finally decided to start her mega campaign to save redwood trees.  
  

#### c. What kinds of amenities were there to support Hill’s life in the tree?

Hill had a sleeping bag, a solar-powered cell phone for media interviews, and a single-burner propane stove to cook and heat water to support her life in the tree. She had few necessities, and no luxuries.  
  

#### d. Did Hill’s value of life change after her car accident? How?

Yes, Hill’s value of life changed after her car accident. She said the experience was a wake-up call. Before accident her main focus had been work but later on she became clear that value as people is not in their stock portfolios and bank accounts, but it is in the legacies they leave behind.  
  

#### e. Deforestation causes natural calamities. What evidence do you have in the text to prove this?

Yes it’s true that deforestation causes natural calamities. In the text we can see that the cutting of redwoods destabilized the hillside and caused the mudslide which carried trees, stumps and debris from that hillside down into the town, leaving seven families without homes.  
  

#### f. How did the logging company try to discourage Hill in the early days of her sit-in?

The logging company tried to discourage Hill in the early days of her sit-in by hiring 24-hour security guards to harass her and to ensure her support team couldn’t deliver her supplies. She was menaced with a helicopter at a dangerously close range. A neighboring tree was felled, hitting Luna’s outer branches and nearly causing Hill to fall. She was verbally abused, threatened with violence, rape and death, kept awake with floodlights, and bugles and air horns were blown through the night.  
  

#### g. How were the vagaries of nature unwelcoming to Hill?

The logging company wasn’t her only problem. One of her scariest times was a 16-hour, 70-mph windstorm that shredded the tarps that surrounded her, and even ripped huge branches off the tree. When lightning struck nearby during an electrical storm, her hair stood straight up. In this way the vagaries of nature were unwelcoming to Hill.  
  

#### h. What is the purpose of the author to write a review on Hill’s book? Do you think the author stands for ecological sustainability? Give reasons.

The purpose of the author to write a review on Hill’s book is to bring a spotlight to the issue of deforestation. He wants people to learn about the deforestation and its ill effects through his review and make people know about what a person should do to prevent from this should. Yes I think the author stands for ecological sustainability.  
  

### Writing

  

#### A. Write a review of a book/film which you have read/watched recently.

The Time Machine

**1\. Title of the Book:** The Time Machine  
**2\. Author of the Book:** H.G. Wells  
**3\. Country:** United Kingdom  
**4\. Language:** English  
**5\. First originally published by:** William Heinemann, London in 1895.  
**6\. Genre:** Science Fiction Novel  
**7\. Cost of the Book:** Rs. 300  
**8\. Name of the Publisher:** Dover Publications  
**9\. Edition and year of Copyright:** April 3, 1995  
**10\. No. of pages:** 80  
**11\. Writing style:** Narrative  
**12\. Characters:** The Narrator-Hillyer, Eloi, Morlocks, Weena  
  
**13\. Plot:** The story follows a Victorian scientist, who claims that he has invented a device that enables him to travel through time, and has visited the future, arriving in the year 802,701 in what had once been London. The narrator recounts the Traveler’s lecture to his weekly dinner guests that time is simply the fourth dimension and demonstrates a tabletop model machine for travelling through the fourth dimension. He reveals that he has built a machine capable of carrying a person through time and returns at dinner the following week to recount a remarkable tale, becoming the new narrator.  
  
**14\. Summary:** A group of men, including the narrator, is listening to the Time Traveler discussing his theory that time is the fourth dimension. The Time Traveler produces a miniature time machine and makes it disappear into thin air. The next week, the guests return, to find their host stumble in, looking disheveled and tired. They sit down after dinner, and the Time Traveler begins his story.  
  
**15\. My Impressions:** The time traveler’s machine is described in such sketchy terms that it can scarcely be believed as an instrument of science, and the time traveler’s account is similarly sketchy and bizarre. The very nature of time travel means that he’s away for only a short period of time, and the only “proof” of his travels is a crunched up flower. And given that the narrative is told in a twice-removed manner, the reader can’t help but wonder whether any of the novels is true at all. Did the time traveler truly engage in such chronological shenanigans, and did he experience what he claims? Or is he simply using an imagined future to provide a warning about the current state of society? But the reality is that neither the truth nor the journey matters: it’s only the outcome.  
  

#### B. Your school is going to organize a speech competition on coming Friday. The subject of the speech is “Let’s save the trees and protect our environment.” Draft a speech using the following prompts.

  

Let’s Save the Trees and Protect Our Environment

Good morning everyone, thank you for giving me the opportunity to speak on such an important topic Let’s Save the Trees and Protect Our Environment. When one thinks of the most important element to survive the answer that always comes to our mind is air. Air is important for all living beings be it animals or humans. The trees which occupy 32% of the earth’s surface provide us with this air.  
  
The trees of the planet act as the lungs in our body and purify the air we breathe. It is due to the existence of trees we inhale fresh air. As trees inhale carbon dioxide and exhale oxygen which serves as the important life source of all living beings. The more trees the planet has the fresher and purer the air we breathe.  
  
Man’s attempt to outlive nature is costing us an unfortunate price. This drive for civilization and modernization is harming the environment and therefore the planet to an irreversible extent. When man violates nature the planet suffers and we have to bear the harmful consequences. Let’s understand how.  
  
The road to civilization is more often through the way by destroying natural resources.  
  
Man views trees as roadblocks in modernization, that is they see it as it comes in the way of building more buildings and roads. So they tend to deforestation, which is an act of cutting down trees. When trees are cut they build more homes and offices in the land.  
  
Deforestation leads to many unnatural things that disrupt the balance of nature. Due to deforestation, air pollution increases, and global warming rises. Animals lose their home and shelter, the quality of the soil where deforestation is practiced deteriorates due to the untimely removal of roots of trees leading to soil erosion.  
  
As their population is only increasing, it is imperative to have more trees so that the next generation lives breathing fresh air. If deforestation continues the planet will not be able to inhabit future generations because of the imbalance it experiences.  
  
The best way to solve this problem is by avoiding deforestation and moving towards afforestation that is planting trees. We should all strive to save our home which is the planet. Even planting one tree can save so many lives.  
  
It is high time that we take action and protect the environment which nourishes all living and breathing creatures. When saving trees they save lives and inevitably save the planet. Saving trees and protecting environment is the motto we should all live by. Let’s leave behind a green planet for the next generation. Spread the word and take action to save trees and in that way, we can protect the environment.  
  
Thanks for the patient hearing!  
  

### Grammar

  

#### B. Someone says something to you which contradicts to what they told you earlier. Match the beginnings of the conversations with the correct endings.

  

#### a. I’m going to Pokhara on holiday.

You said you were going on business.  
  

#### b. He’s a lawyer.

You told me he was a teacher.  
  

#### c. She’s had a baby girl.

You said she’d had a boy.  
  

#### d. I haven’t seen Binesh for ages.

You told me you’d seen him previous week.  
  

#### e. I love these new boots.

You said you hated them.  
  

#### f. I only cheated in one exam.

You admitted you cheated in all your exams.  
  

#### g. She doesn’t speak Hindi or Chinese.

You told me she was fluent in both.  
  

#### h. He works in Kathmandu.

You told me his office was in Biratnagar.  
  

#### C. Rewrite the following sentences with the sentence beginnings given below.

  

#### a. The principal said, “You can phone from my office, Rita.”

The principal said that she could phone from his office.  
  

#### b. “You must not neglect your duty,” said the teacher to the student.

The teacher told the student that he/she must not neglect his/her duty.  
  

#### c. The student said, “Sir, please, grant me a leave for two days.”

The student requested to grant him leave for two days.  
  

#### d. I said to her, “Go to school or you will be fined.”

She was told to go to school or she would be fined.  
  

#### e. The headmaster said, “Don’t make any noise, boys.”

The headmaster asked boys not to make any noise.  
  

#### f. “Work hard if you want to rise in life,” said the old man.

The old man suggested working hard if they wanted to rise in life.  
  

#### g. He said, “Goodbye, my friends!”

He said goodbye to his friends.  
  

#### h. She said to me, “Have a pleasant journey ahead.”

She told me to have a pleasant journey ahead.  
  

#### i. “Don’t give me the book, please,” Sharmila said.

Sharmila pleaded not to give her the book.  
  

#### j. “Where have you been these days?” she spoke on the telephone.

He asked on the telephone where I had been those days.  
  

#### k. The teacher said, “Have you submitted your assignments, students?”

The teacher asked students if they had submitted their assignments.  
  

#### D. These are the exact words Dinesh said to you yesterday.

_“I’ve just got engaged! We’re getting married next month. We’re going to Pokhara for our honeymoon. It’s all going to be very expensive. Luckily, my friend is a photographer so he’ll take the photos for us. We’ll be having the reception in my parents’ back garden. My mum is baking the cake for us and my sister’s band is playing free for us. I hope you’ll come to the wedding.”_  
  

#### Now, you’re telling your friend what Dinesh told you. Complete the text.

  
He said he (1) **had** just got engaged. He told me that he (2) **they were getting married** next month. He told me (3) they were going to Pokhara for their honeymoon. He mentioned that it (4) **was all going to be** very expensive. He said that his friend (5) **was a photographer** and he (6) **would take** the photos for them. He mentioned that they (7) **would be having** the reception in his parents’ garden. He admitted that (8) **his mum was baking the cake for them.** He said his sister’s band (9) **was playing free for them.** He said he (10) **hoped** I’d come to the wedding.