## Unit 18
# Music and Creation
### Working with words

  

#### A. Find the single words for the following definitions. The words are given in the jumbled letters at the end.

  
a. A person who hates or distrusts mankind **misanthrope**  
  
b. A sensation of noise, such as a ringing or roaring **tinnitus**  
  
c. An examination of a body after death to determine the cause of death **autopsy**  
  
d. A musical composition or movement for five instruments or voices **quintet**  
  
e. A severe life-threatening illness caused by a bacterium **meningitis**  
  
f. An object that directs one’s attention away from something else **distraction**  
  
g. The action or process of becoming impaired or inferior in quality, functioning, or condition **deterioration**  
  
h. Failing to perceive something **impercipience**  
  
i. A hearing disorder that makes it hard to deal with everyday sounds **hyperacusis**  
  
j. A bacterial infection usually spread by sexual contact **syphilis**  
  

#### B. Put the musical instruments into different categories as below.

  
**Wind Instruments :** trumpet, harmonica, clarinet, conch, trombone  
**Stringed Instruments :** violin, viola, cello, double bass  
**Percussion Instruments :** drum, tabls, cymbal, bell, tambourine  
  

#### C. Use a dictionary and find the definition of these genres of music.

  
**rock music :** a form of popular music that evolved from rock and roll and pop music during the mid and late 1960s.  
  
**pop music :** commercial popular music, in particular accessible, tuneful music of a kind popular since the 1950s and sometimes contrasted with rock, soul, or other forms of popular music.  
  
**hip hop :** a style of popular music of US black and Hih4ic origin, featuring rap with an electronic backing.  
  
**jazz :** a type of music of black American origin which emerged at the beginning of the 20th century, characterized by improvisation, syncopation, and usually a regular or forceful rhythm.  
  
**folk music :** music that originates in traditional popular culture or that is written in such a style.  
  
**classic music :** music written in the European tradition, when forms such as the symphony, concerto, and sonata were standardized.  
  
**blues :** melancholic music of black American folk origin, typically in a twelve-bar sequence.  
  
**heavy metal :** a type of highly amplified harsh-sounding rock music with a strong beat, characteristically using violent or fantastic imagery.  
  
**Gospel music :** a fervent style of black American evangelical religious singing, developed from spirituals sung in Southern Baptist and Pentecostal Churches.  
  
**country music :** a form of popular music originating in the rural southern US. It is a mixture of ballads and dance tunes played characteristically on fiddle, banjo, guitar, and pedal steel guitar.  
  
**grunge :** a style of rock music characterized by a raucous guitar sound and lazy vocal delivery.  
  
**breakbeat :** a sample of a syncopated drumbeat, usually repeated to form a rhythm used as a basis for dance music, hip-hop, etc.  
  
**reggae :** a style of popular music with a strongly accented subsidiary beat, originating in Jamaica.  
  
**disco :** a club or party at which people dance to recorded pop music.  
  
**dubstep :** a form of dance music, typically instrumental, characterized by a sparse, syncopated rhythm and a strong bassline.  
  

 

### Comprehension

  

#### A. Put the following events in the life of Beethoven in chronological order.

c. Beethoven was born into a musical family in Bonn, Germany.  
e. He learned to play the organ, piano, violin, and viola.  
a. He migrated to Vienna.  
h. At the age of twenty-seven, he felt the deafness to high-pitched sound.  
d. His deafness got gradually worse: He could hear but not understand.  
g. He lived a life in seclusion.  
b. He even made suicidal attempts.  
f. He died of hepatitis at the age of fifty-six.  
  

#### B. State whether the following statements are True or False.

a. Beethoven became blind in a gradual process over two decades. **False**  
b. He became a celebrity musician in his teenage. **True**  
c. He did not disclose his hearing problem for a long time. **True**  
d. He knew that he misunderstood the speakers and gave up his public performances. **False**  
e. He tried to get his deafness treated until his death. **False**  
f. Beethoven died in his early fifties. **False**  
g. Doctors found the cause of his deafness after his death. **False**  
  

#### C. Answer the following questions.

  

#### a. What does Hellen Keller think about deafness and blindness?

Hellen Keller thinks that blindness separates people from things whereas deafness separates people from people.  
  

#### b. Why does the author compare Beethoven with Milton, Van Gogh and Toulouse-Lautrec?

The author compares Beethoven with Milton, Van Gogh and Toulouse-Lautrec because those people have similar types of disabilities like that of Beethoven.  
  

#### c. When and how did Beethoven notice him being deaf?

Beethoven noticed himself being deaf at the age of 27. At that time, he lost his ability to hear high pitch sounds, an indication of nerve deafness. He noticed that he had some sort of disabilities like deafness.  
  

#### d. What psychological effects did he have when he noticed that he was being deaf?

When he noticed that he was being deaf, he suffered from fear, lack of self-esteem, emotional disarray, increasing isolation, self-neglect and lack of confidence at work.  
  

#### e. How did he triumph over his suicidal thoughts?

He triumphed over his suicidal thoughts with the help of his life’s ethics and interest in music. Also, he had read somewhere that a man should not give up his life as he can do many other good deeds in life. These things inspired him to conquer suicidal thoughts.  
  

#### f. How did he accept his deafness?

He accepted his deafness by saying that no one can save him from deafness and to go further in his life, he must eventually accept it.  
  

#### g. How was his deafness ironically good for the world?

His deafness was ironically good for the world in the sense that his deafness resulted in extra creativity within him. It also allowed him to listen to his inner sounds and feelings without any distraction.  
  

#### h. When did Beethoven give up his musical performances forever?

Beethoven gave up his musical performances forever during his mid-forties. During that time, it was some sort of embarrassing faux pas for him.  
  

#### i. What did Stephen von Breeuning comment on Beethoven’s reactions?

In 1804, when Beethoven had problems in hearing the wind instruments during a rehearsal for the Eroica, his friend, Stephen von Breuning commented that Beethoven had become withdrawn and mistrusted by his best friends.  
  

#### j. How did Beethoven express the conflict in his mind?

Beethoven expressed the conflict in his mind by isolating himself from social functions, mistrusting his best friends and thinking to have suicidal attempts repeatedly in his mind.  
  

### Critical thinking

  

#### a. Suicidal thoughts came in Beethoven’s mind several times but he did not commit suicide and kept on composing music. Write a monologue in about 150 words from Beethoven’s perspectives describing his suicidal thoughts and his will to live.

I was the life of the party. Even when there was no party. I made one happen. I made people laugh. I made myself laugh. I worked good and hard to make myself laugh. Lord, you could hear my laugh from a block away.  
  
“Isn’t he something, that Beethoven?,” people would say. “Always happy. Look at him. Always with the smile.” In this situation, I’m feeling quite lonely and helpless. My condition has isolated me from my coworkers and relatives. They think negatively about me. They consider me a deaf guy. The entire efforts of my life to make music alive and people of the world proud would go in vain. I can’t stand this situation in my life. There is no hope within me to get cured. It wore me out sometimes, working like that, keeping the motor running, the horn honking, the smoke pouring out. Sometimes I just wanted to come to rest. So today, I made a mistake. I stayed alone. And then I decided, that’s what I’ll do. I’ll just stop. I’ll rest.  
  
Later the day, I think it’s bad to kill my own life. I must try to conquer these suicidal thoughts. I should think about the ethics of life and my interest in music. People say that a man should not give up his life because he can do many other good things in life. Why do I have those thoughts? I can make everyone think that I am normal, that I am coping, and that I am fine. I think to myself, suicide is not a permanent solution to a temporary problem’. Human life is precious and important. I’m ready to leave this miserable thoughts! I’m ready for a real life. It doesn’t matter if I can’t listen to others. I’m going to listen to my inner sounds and feelings!  
  

#### b. Was it divine inspiration or rigorous practice that made Beethoven one of the world’s greatest musicians? Give the reasons.

No, it wasn’t divine inspiration but rigorous practice and his belief in music that made Beethoven one of the great composers of the world. Ludwig van Beethoven was a German pianist and composer widely considered to be one of the greatest musical geniuses of all time. His innovative compositions combined vocals and instruments, widening the scope of sonata, symphony, concerto and quartet.  
  
Beethoven’s personal life was marked by a struggle against deafness, and some of his most important works were composed during the last 10 years of his life, when he was quite unable to hear. He went on singlehandedly to change the musical world forever. Due to his deafness, he became quite sad and serious. In that suffering too, he didn’t give up his passion for his music and continued his creative works further. He kept on composing his best music even after his deafness. Due to his rigorous practice and musical devotion, his music career reached its height and made him quite popular not only within his country but across the globe. It has been said that he alone dragged music out of the Classical age and into the Romantic. But that doesn’t go far enough. I believe passionately that Beethoven both defined and deserves his own musical era.  
  

### Writing

  

#### Who is your favourite Nepali musician? Write his/her biography in about 300 words.

Swar Samrat Narayan Gopal Guruacharya

Narayan Gopal was born to a Newar family in Kathmandu in 1996 Bikram Sambat (BS). His father was a classical musician and wanted his son to follow his footsteps. Narayan Gopal was interested in music from an early age but not exactly into classical field. Regarded one of the most significant cultural icons in Nepal, he is referred as “Swar Samrat” in Nepal. He is also known as “King of Tragedy” owing to his numerous tragedy songs. He also sang in Nepal Bhasa.  
  
Narayan Gopal is by far the most prominent and popular singer in Nepali music. When people think or talk about Nepali music, the name most associated is that of Narayan Gopal. For most people, he is the singer. He was also an accomplished music composer in his own right. Not only was he gifted with a great voice, he was very versatile. His voice range allowed him to sing songs of every genre. His songs are often richly orchestrated with the sitar, harmonium and the flute. Some of his most popular songs include: Euta Manche Ko Maya le Kati, Jhareko Paat Jhai, Yo Samjhine Man Cha, Saawan ko jhari bani, Manche ko Maya and many more.  
  
He has sung over 500 songs, including in films, ballets and drama. Most of his songs are melodies. It is said that he was very selective about which songs he sang. Although 500 songs is not a lot for some of his caliber, they are outstanding in some ways–be it the lyrical depth or superb rendition. In recognition of his contribution in the field of Nepali music, he has received several national honors and awards, Trishakti Patta, Chinnalata Puraskar to name a few.  
  
Continued sales of his albums attest to his immense popularity. Even more than a decade after his death, his songs are equally liked, loved and sung. His legacy lives on.  
  

### Grammar

  

#### B. Fill in the blanks with appropriate prepositions.

  
a. I was accompanied to the hospital **by** my friend.  
b. I have great respect **for** my teachers.  
c. The culprit was sentenced **to** death.  
d. His arguments are not based **on** truth.  
e. He has not contributed **to** the development of our nation.  
f. He died **because** of Corona at the age of thirty-five.  
g. The young generation of Nepali people don’t take interest **in** politics.  
h. Our village was not infected **by** Corona.  
i. Why do you sneer **at** me?  
  

#### C. Fill in the blanks with for, since, until, by or in.

  
a. The classes will be over **by** 5 PM.  
b. Karma Sherpa reached the top of Mt. Everest **in** 8 hours.  
c. We had met after five years. So we kept on talking **until** three in the morning.  
d. She has been living in America **since** she got married.  
e. He has been playing video game **for** ten hours.  
f. She practices the Sitar every day **for** five hours.  
g. He lived in Jumla **until** he passed high school examinations.  
h. There is no chance of dry weather even today. It has rained **since** last Saturday.  
i. I can type 120 words **in** a minute.  
j. We take an early breakfast. It’s generally ready **by** six in the morning.