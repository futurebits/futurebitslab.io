## Unit 11
# Hobbies
### Working with words

  

#### A. Find the words from the text which have the following meanings.

  
a. a wave-like motion  
**undulation**  
  
b. to make or do something again exactly in the same way  
**replicate**  
  
c. decayed, deteriorated, or fallen into partial ruin especially through neglect or misuse  
**dilapidated**  
  
d. a bright, smooth surface  
**sheen**  
  
e. the feeling of having no energy and enthusiasm  
**listlessness**  
  
f. a state of noise, commotion and confusion  
**cacophonous**  
  
g. being alone, often by choice  
**solitude**  
  
h. to regard with respect, often tinged with awe  
**revere**  
  
i. said in a way that is not direct, so that the real meaning is not immediately clear  
**obliquely**  
  

#### B. Find the meaning of the following words from a dictionary.

  
**Sophisticated :** deprived of native or original simplicity, highly complicated  
**Conscious :** likely to notice, consider or appraise  
**Blistering :** extremely intense or severe  
**Proximity :** immediately preceding or following  
**Invisible :** inaccessible to view, recognize or identify  
  

 

### Comprehension

  

#### Answer the following questions.

  

#### a. What is the author’s favourite hobby? Why does she like it so much?

The author’s favourite hobby is walking. She likes it so much because the movement, the rhythm, the undulation of the senses and of the body it initiates is enjoyable.  
  

#### b. What sorts of roads did the writer prefer to walk on when she was very young?

The writer preferred to walk on the roads that were unpaved and uneven, like the paths around her mamaghar when she was very young.  
  

#### c. How did walking give the author and her classmates a sense of freedom?

Walking gave the author and her classmates a sense of freedom because they could be free-willed masters of their time and could find their own way.  
  

#### d. In what ways were the roads in Kathmandu different from the ones in Sydney?

The roads in Sydney were far quieter and organized when compared to the cacophonous streets of Kathmandu.  
  

#### e. How did walking help the author in the new country?

Walking was an escape of all kinds of pressures, a search for pace and a rhythm to the author. It helped her to navigate the new country and it renewed the circumstances of her life.  
  

#### f. What were the treasures of Petersham, where the writer lived with her family?

The treasures of Petersham, where the writer lived with her family were a tiny park which was a minute down the Parramatta road, another park with a huge rock at one end, a buffer zone suburb etc.  
  

#### g. What things became her permanent friends with whom she could share her feelings?

The components of nature such as the earth, the sky, the trees, water, air became her permanent friends with whom she could share her feelings.  
  

#### h. Why did she feel that she had travelled to ‘a desert, to emptiness’ as she went to the United States?

She felt that she had travelled to ‘a desert, to emptiness’ as she went to the United States because people living there were running after material pursuit and comfort because of which she felt lonely and a barrier of communication with them.  
  

#### i. Why did the author eventually feel that the strange city was known to her?

Eventually the author felt that the strange city was known to her because she found the place similar to every other place on the earth. The essence of the nature she felt in the strange city was same to the one she found in her hometown.  
  

#### j. How did walking make her feel at home with different places she visited?

Walking made her feel thankfulness to all the streets she walked on because they gave shape to her feet, her body and her being.  
  

### Critical thinking

  

#### a. Do you believe that walking helps us understand ourselves? Give reasons in support of your opinion.

Yes, I believe that walking helps us to understand ourselves. Walking provides the best of both worlds. It offers the physical benefits of exercise while also boosting your emotional well-being. It can help reduce anxiety, depression, and a negative mood. It can also boost self-esteem and reduce symptoms of social withdrawal. Walking also helps to clear our head and think creatively. It opens up a free flow of ideas and is a simple way to increase creativity and get physical activity at the same time. While walking we interact with our inner self, we evaluate ourselves, our works, our behavior. This makes us realize actually who we are. Understanding ourselves means self-realization which can happen when we are alone and submerged with natural world. Thus it’s true that walking helps us to understand ourselves.  
  

#### b. Think of one of your hobbies. How does this hobby relate to your psyche and self?

Hobbies are seen as markers of a balanced person who is adept at juggling personal and professional well-being. I have only one hobby. Yet, it consumes all my free time. I enjoy reading and have always been a voracious reader.  
  
Although I prefer reading fiction, I make a deliberate effort to read at least one non-fiction book in a couple of months.  
  
My first book was “Famous Five Go to Kirrin Island” at the age of ten. What I truly treasure about my hobby is its power to transport me to different locations whilst I’m still on my couch.  
  
It is a low budget travel option that I often take! Recently, I started attending book club meetings.  
  
It’s a group of 15 individuals and we meet on the third Sunday, every month to discuss books that we have read in that month.  
  
It’s wonderful how a few lines can have so many varied interpretations, and can give rise to a multitude of emotions and opinions.  
  
My hobby has made me develop analytical thinking and open-mindedness.  
  
Reading has also broadened my imagination across horizons. I wouldn’t possibly trade it for anything else, ever!  
  

#### c. Many people turn their hobbies into careers. Is it good to turn one’s hobby into a career?

Yes, it’s true that many people turn their hobbies into careers. But what I think that is; a big decision of whether to turn a hobby into a career is a godsend for some lucky folks, but for other poor souls, it’s not all it’s cracked up to be. Whether you’re looking to go freelance, set up a limited company or become a sole trader, making hobby turn into livelihood can be a messy business. Hobbies are really important for personal growth and enjoyment, and the last thing you want to do is turn a form of escapism from the hustle, bustle, and grind of everyday life into something stressful and pressurized. To sum up, if a person is truly passionate about his/her carry, then he/she can easily turn their hobby into a career else it’s going to be a great suffering.  
  

### Writing

  

#### a. Highlighting the advantages of walking, Henry David Thoreau says, “An early morning walk is a blessing for the whole day.” Write an essay on the advantages of morning walk.

Advantages of morning walk

The modern-day world is full of psychological disorders, poor health, mental tension, and many more problems. Likewise, the life of some people is like a mad rush from one work to another without any break. Besides, there are very few people in the world that care about their health more than their work or daily tasks. But, there are ways by which we can restore our healthfully and morning walk is one of them. Additionally, it is so effective that it can reduce the amount of health disorder from the world.  
  
From childhood, we have heard that “early to bed and early to rise makes a man healthy, wealthy and wise.” This is not just a saying because morning walks make a man healthy and wise.  
  
Moreover, it improves the physical shape and state of the body which protects us from many diseases. Besides, all this morning walks create a sense of equality among the people.  
  
Above all, morning walk gives you energy, motivates you to avoid laziness, creates a positive mindset, it is good for your organs especially heart, and it gives you time to plan your schedule. According to research, the best time for a morning walk is in the latter part of the afternoon between 3 pm to 7 pm.  
  
To sum it up, we can say that, Morning walk is very important for the body. Also, it helps to keep the body and mind healthy. Besides, everyone whether kids or elders should try to make a morning walk a part of their daily routine. As it is seen that the life h4 of people who walk daily is more in comparison to those who do not do morning walk.  
  

### Grammar

  

#### B. Change the following sentences into passive voice.

  

#### a. I want someone to love me.

I want to be loved.  
  

#### b. Someone broke into our house while we were on holiday.

Our house was broken into while we were on holiday.  
  

#### c. I don’t like people staring at me.

I don’t like being stared at.  
  

#### d. Is it true that someone stole your car?

Is true that your car was stolen?  
  

#### e. The cat enjoys someone tickling him.

The cat enjoys being tickled.  
  

#### f. Would Swostika open the window?

Would the window be opened by Swostika?  
  

#### g. Did they confess the crime?

Was the crime confessed by them?  
  

#### h. He thinks that someone is teaching Jennie.

He thinks that Jennie is being taught.  
  

#### i. Sabina hates people laughing at her.

Sabina hates being laughed at.  
  

#### C. Complete the following sentences as in the example.

  

#### a. English people think that the number thirteen is unlucky.

The number thirteen **_is thought to be_** unlucky by English people.  
  

#### b. What are you wearing for the wedding?

Actually, I am having a **_suit made_** (a suit make). They will give it tomorrow.  
  

#### c. The carpet in our drawing room is very dirty.

It needs **_to be cleaned_** (clean).  
  

#### d. There are rumours that the factory at the corner is manufacturing bombs.

The factory at the corner is rumoured **_to be manufacturing_** bombs.  
  

#### e. Some people believe that Silajit from Jumla cures all indigestion problems.

Silajit from Jumla **_is believed to cure_** all digestion problems.  
  

#### f. People claim that Changu Narayan temple is the oldest temple in Nepal.

Changu Narayan temple **_is claimed to be_** the oldest temple in Nepal.  
  

#### g.

A: Your car is making a terrible noise.  
B: Thank you. I am not a mechanic and I will **_make it be repaired_** soon. (repair).  
  

#### h. The police suspect that the criminal left the country.

The criminal **_is suspected to leave_** the country.  
  

#### i.

A: Where are you going?  
B: I am going to the stationery to get **_my document photocopied_**. (my document /photocopy)  
  

#### j. People allege that the corrupt leader has embezzled millions of rupees.

The leader **_is alleged to embezzle_** millions of rupees.