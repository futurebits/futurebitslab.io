## Unit 13
# History
### Working with words

  

#### A. Match the words with their definitions/meanings.

  

#### a. passion

any powerful or compelling emotion or feeling, as love or hate  
  

#### b. surveillance

continuous observation of a place, person, group, or ongoing activity in order to gather information  
  

#### c. integrity

the state of being whole, entire, or undiminished  
  

#### d. avuncular

like an uncle  
  

#### e. livelihood

a means of supporting one’s existence  
  

#### f. fascination

powerful attraction  
  

#### g. innovation

an idea, practice, or object that is perceived as new  
  

#### h. panorama

an unobstructed and wide view of an extensive area in all directions  
  

#### i. resilience

the power or ability of a material to return to its original form, position, etc., after being bent, compressed, or stretched  
  

#### j. evacuation

the removal of people or things from an endangered area  
  

#### B. Define the following professionals. One has been done for you.

  

#### architect

An architect is an engineer who designs buildings and advises in their construction.  
  

#### civil engineer

A civil engineer is an engineer who designs and oversees the construction of public works.  
  

#### mechanical engineer

A mechanical engineer is an engineer who designs, produces, and operates machines.  
  

#### aerospace engineer

An aerospace engineer is an engineer who designs, tests and manages the manufacturing of aircrafts.  
  

#### automobile engineer

An automobile engineer is an engineer who designs, manufactures and repairs vehicles.  
  

#### electronic engineer

An electronic engineer is an engineer who designs, develops and manufactures electrical equipments.  
  

#### electrical engineer

An electrical engineer is an engineer who creates, designs and manages electrical equipments.  
  

#### computer engineer

A computer engineer is an engineer who evaluates, designs and maintains computer hardware and software systems.  
  

#### food engineer

A food engineer is an engineer who ensures the safety and efficiency of processing, packaging and delivering of food items.  
  

#### chemical engineer

A chemical engineer is an engineer who develops and designs chemical manufacturing processes.  
  

#### biomedical engineer

A biomedical engineer is an engineer who develops new equipment for improving human health.  
  
 

### Comprehension

  

#### Answer the following questions.

  

#### a. Where were Frank and Nicole employed?

Frank and Nicole were employed as architects in the twin towers of the World Trade Centre situated in the USA.  
  

#### b. How does the author describe Frank’s attachment to the twin towers?

The author describes Frank’s attachment to the twin towers as a source of livelihood and passion.  
  

#### c. How did the two families become intimate with each other?

The two families became intimate with each other because of their children.  
  

#### d. What, according to Nicole, did Frank think of the towers?

According to Nicole, Frank thought of the towers as an incredible human fest. The scale, magnitude, innovative design, efficient use of materials of the towers were awesome to Frank. Sometime he said that they were built to take the impact of a light airplane.  
  

#### e. Why did Frank want to help the people in the tower after the attack?

Frank wanted to help the people in the tower after the attack because only few people knew about the strength of the building.  
  

#### f. Why did Frank not follow Nicole’s request even after knowing that the building was on fire?

Frank did not follow Nicole’s request even after knowing that the building was on fire because he had a confidence that he could help the injured people on their way to safe place without hurting himself.  
  

#### g. Who did Frank call from the building after the attack?

Frank called his sister Nina, who lived on West 93rd street in Mahnattan, from the building after the attack,  
  

#### h. How did Nicole feel after the collapse of the first tower?

After the collapse of the first tower, Nicole felt that was like the beginning of a nuclear war. Everything went absolutely quiet and she felt in the middle of a fog that was as blindingly bright as a snowstorm on a sunny day.  
  

#### i. Were Frank’s children serious as soon as they heard the news of their missing father? Why?

Frank’s children were not serious as soon as they heard the news of their missing father because they were busy playing games.   
  

### Critical Thinking

  

#### a. Many innocent people lose their lives in ruthless attacks every year. What do you think the governments should do to protect their people from such attacks and make the world a safer place to live? Discuss with your friends.

Yes, it’s true that many innocent people lose their lives in ruthless attacks every year. The government must be responsible to handle and control such situations. It should think seriously for the security of their citizens from such ruthless attacks. The things the government should do to protect their people from such attacks and make the world a safer place to live are as follows:  
  
– Every government should maintain stability in them, the leaders and the political parties.  
– Borders restrictions should be done.  
– Armed forces should be kept at several places where there is the high risk of attack outbreaks.  
– Though the peace treaty are being signed but not followed accordingly. So every nation should work to maintain peace.  
– Peoples should be moved to safer place if there is any danger.  
  

#### b. Revenge and violence are the integral parts of the history and civilization. They can’t be ignored, only managed. Do you agree or not with this statement? Present your logic.

Yes, I agree with the statement that revenge and violence are the integral parts of the history and civilization. Revenge is associated with hurt and damage, and violence is the use of physical force so as to injure, abuse, damage, or destroy. The cause of World War II is also due to the Revenge from German, in 1919 June 28. When the Treaty of Versailles was signed and the war between German and Allied Power was ended, it humiliated and blamed Germany for World War I and imposed heavy debt payments on Germany. Germany revenged for it in 1939 and from that World War II had stared. We have seen from our past that revenge and violence and lead to a miserable situation. Thus, it can’t be ignored, but only managed.  
  

### Writing

  

#### A. Write a description of an event that you have recently witnessed.

Recently I witnessed a very special celestial event known as the full lunar eclipse. It was special because astronomers declared it to be longest lunar eclipse of this century which took place on July 27. It was difficult for me because I had to wake up till late night for watching this lunar eclipse. Finally the eclipse started at 11:30 pm and ended at 1 a.m. I was accompanied by a few friends who chose to witness this lunar eclipse. We kept looking up in the sky for one and a half hours which was a bit difficult but rewarding in the end.  
  

#### B. Human life does not pass as one expects. You might have also gone through different but memorable incidents. Write an essay on “An Unforgettable Event in my Life’ in about 500 words.

It is true that if we have joys in our life we have sorrows too. Like all others, I cannot be an exception. I still remember the day when I felt that there is no joy in the world but sorrow.  
  
I was enjoying the vacations after my tenth class board ex-examinations. My best friend in the neighborhood, Alisha, was staying with me. Her parents had gone to Kathmandu where her grandfather was to undergo some major operation. I and my parents did our best to keep her happy and away from missing her parents. For us it was play time all through the day.  
  
One evening as we were playing our little brain game, her parents called and said her grandfather’s operation was successful and her parents were to return the next day by plane. On reading it Alisha was overjoyed and I joined her in her moment of happiness.  
  
That night Alisha did not sleep. She counted the hours till the early morning. We dressed hurriedly and my father took us to the airport. On reaching there we sensed something wrong. The Yeti Airlines flight from Kathmandu had crashed while landing. Fire tenders and ambulances were on the run. Our hearts sank. Alisha fainted from shock. My father hired a taxi and sent us back home.  
  
We waited with throbbing hearts for my father to return with her safe and sound parents. My mother sent for a doctor as Alisha’s condition became worse. I sat by her side silently wishing for a happy turn. My father returned only to say that there was no sign of her parents though their names were mentioned in the airline’s passenger list. Alisha was hysterical and it became difficult to control her.  
  
She said that she wanted to go to her home. I decided to go with her while my mother would soon follow. Once out on the street she ran towards her home. I saw a speeding car and shouted after her to stop. She didn’t. The car didn’t stop and sped away leaving her laying on the road badly hurt.  
  
We immediately took her to a nearby hospital. The doctors who examined her said she may recover gradually but did not confirm her recovery. She was placed in the intensive care unit.  
  
I returned home with my parents. The moment we entered the room the telephone rang. It was a call from the hospital to inform that Alisha was no more. I felt numb and tears welled up in my eyes. I still refused to believe what I heard was true.  
  
Again, the telephone rang and it was from Alisha’s parents to inform that they had missed their flight due to some car trouble on their way to the airport. The news of Alisha’s parents as alive did not touch my heart.  
  
Alisha’s parents were shocked to hear about their daughter’s death when they came home next day. But nothing could be done. Even today when I remember the incident every moment of that day flashes through my eyes and I begin to cry, “Alisha, why did you have to die? I have lost a good friend in you.”  
  

### Grammar

  

#### Prepositions

#### B. Complete these sentences with the correct prepositions.

  
a. The relationship **between** the two boys has changed significantly over the past few years.  
b. In Nepal, many girls get married **at** an early age.  
c. I’m not **in** the mood for such silly games.  
d. There were no security personnel **on** duty at that time.  
e. The new smartphone is similar **to** the one I bought a few years ago.  
f. My dad insisted **on** taking the later train.  
g. People **with** ambition always try to achieve their goals, no matter what happens.  
h. Mr. Jenkins has been disabled all his life as a result **of** a childhood illness.  
i. We bought the TV because it was **on** sale.  
j. She has no understanding **of** how computers really work.  
k. I gave him my new T-shirt in exchange **for** a few cigarettes.  
l. The company is run **by** two people who hardly ever meet.  
m. All the celebrations and parties were called **off** because of the tragic accident.  
n. There was a great need **of** volunteers at the site of the crash.  
  

#### C. Complete the following text with correct prepositions.

What are we seeing here? One very real possibility is that these are the educational consequences of the differences **in** parenting styles that we talked about **in** the Chris Langan chapter. Think back to Alex Williams, the nine-year-old whom Annette Lareau studied. His parents believe **in** connected cultivation. He gets taken **to** museums and gets enrolled **in** special programs and goes **to** summer camp, where he takes classes. When he’s bored **at** home, there are plenty of books to read, and his parents see it as their responsibility to keep him actively engaged **in** the world **for** him. It’s hard to see how Alex would get better **at** reading and math **in** the summer.  
  

#### Simple future, future continuous, future perfect and future perfect continuous

  

#### B. make sentences from the given clues as stated in brackets.

  

#### a. Who/pass the exam? (future simple)

Who will pass the exam?  
  

#### b. How/you/get home? (future continuous)

How will you be getting home?  
  

#### c. I/come later. (future simple)

I’ll come later.  
  

#### d. She/catch the train by 3 pm. (future perfect)

She will have caught the train by 3 PM.  
  

#### e. It/rain tomorrow. (future simple)

It will rain tomorrow.  
  

#### f. John/sleep at 4 am. (future continuous)

John will be sleeping at 4 am.  
  

#### g. How long/you/see your boyfriend when you get married? (future perfect continuous)

How long will you have been seeing your boyfriend when you get married?  
  

#### h. It/rain in Kathmandu next week. (future continuous)

It will be raining in Kathmandu next week.  
  

#### i. How long/you/work here when you retire? (future perfect continuous)

How long have you been working here when you retire?  
  

#### j. He/not/finish the cleaning by the time she gets home. (future perfect)

He won’t have finished the cleaning by the time she gets home.  
  

#### C. Complete the following sentences with the correct future tense form of the verbs in the brackets.

  
a. It **will snow** (snow) in Brighton tomorrow evening.  
  
b. On Friday at 8 o’clock, I**’m going to meet** (to meet) my friend.  
  
c. Wait! I **will drive** (to drive) you to the station.  
  
d. When they get married in March, they **will have been** (to be) together for six years.  
  
e. You’re carrying too much. I **will open** (to open) the door for you.  
  
f. Do you think the teacher **will have marked** (to mark) our homework by Monday morning?  
  
g. When I see you tomorrow, I **will show** (show) you my new book.  
  
h. After you take a nap, you **will feel** (to feel) a lot better.  
  
i. I **will let** (to let) you know the second the builders finish decorating.  
  
j. We **will wait** (to wait) in the shelter until the bus comes.  
  
k. I’m very sorry, Dr. Jones **won’t be** (not be) back in the clinic unit 2 pm.  
  
l. This summer, I **will have been living** (to live) in Birgunj for four years.  
  
m. I don’t think you **will have** (to have) any problems when you land in Pokhara.  
  
n. The baby should be due soon, next week she **will have been** (to be) pregnant for nine months.  
  
o. By the time we get home, they **will have been playing** (to play) football for 30 minutes.  
  
p. When you get off the train, I **will be waiting** (to wait) for you by the ticket machine.  
  
q. This time next week, I **will be skiing** (ski) in Switzerland!  
  
r. Now, I **am going to check** (to check) my answers.