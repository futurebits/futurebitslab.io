## Unit 14
# Human Rights
### Working with words

  

#### A. Pair the following words as opposites.

Despair : hope  
Kind : cruel  
Fresh : stale  
Strange : familiar  
Normal : eccentric  
Fierce : gentle  
Corrupt : honest  
Selfish : generous  
  

#### B. By adding a suitable suffix to each word in the table, form another word as in the examples below.

**into noun**  
open-minded : open-mindedness  
accommodate : accommodation  
rehearse : rehearsal  
transgress : transgression  
angry : anger  
mix : mixture  
  
**into adjective**  
pain : painful  
differ : different  
behave : behavior  
remark : remarkable  
indifferent : indifference  
thought : thoughtful  
  
**into verb**  
less : lessen  
sure : ensure  
real : realize  
glory : glorify  
power : powering/powers  
prison : prisoning/prisoned  
  

#### C. Pronounce the following words with the help of a dictionary.

| Words | Pronouncation |
| --- | --- |
| Viewer | /ˈvjuːə/ |
| sure | /ʃɔː,ʃʊə/ |
| cure | /kjʊə,kjɔː/ |
| fluent | /ˈfluːənt/ |
| poor | /pɔː,pʊə/ |
| affluence | /ˈaflʊəns/ |
| flower | /ˈflaʊə/ |
| curious | /ˈkjʊərɪəs/ |
| tourist | /ˈtʊərɪst/ |
| allowance | /əˈlaʊəns/ |
| usual | /ˈjuːʒʊəl/ |
| intellectual | /ˌɪntəˈlɛktʃʊəl/ |
| visual | /ˈvɪʒ(j)ʊəl,ˈvɪzjʊəl/ |
| mature | /məˈtʃʊə/ |
| endure | /ɪnˈdjʊə,ɛnˈdjʊə,ɪnˈdʒɔː,ɛnˈdʒɔː/ |
| join | /dʒɔɪn/ |
| coin | /kɔɪn/ |
| boy | /bɔɪ/ |
| voice | /vɔɪs/ |
| noise | /nɔɪz/ |
| soil | /spɔɪl/ |
| hoist | /hɔɪst/ |
| moist | /mɔɪst/ |
| avoid | /əˈvɔɪd/ |
| toy | /tɔɪ/ |
| toilet | /ˈtɔɪlɪt/ |
| annoy | /əˈnɔɪ/ |
| enjoy | /ɪnˈdʒɔɪ,ɛnˈdʒɔɪ/ |
| poison | /ˈpɔɪz(ə)n/ |

  

 

### Comprehension

  

#### A. Write True or False after each statement. Give reason for your answer.

  
a. The author says his father was an ideal person in the family. **False**  
_He used to abuse his mother verbally and physically when he was drunk._  
  
b. The author wanted to forgive his father but he did not get an opportunity. **False**  
_Because he had many pressure and stresses._  
  
c. It’s worth forgiving a person if he/she realizes his/her mistakes. **True**  
  
d. South Africa had dual type of education system in the 1960s. **True**  
  
e. The author lived in a joint family. **False**  
_Because the author had only his wife and two children in his family._  
  
f. The author regretted for not getting a chance to talk to his father. **True**  
  
g. According to the author, all our glories and splendors are short lived. **True**  
  

#### B. Answer the following questions.

  

#### a. How does the author remember his family environment when he was a small boy?

The author remembers his family environment when he was a small boy as his father was verbally and physically abused to his mother.  
  

#### b. Why does the author blame system more than his father?

The author blames system more than his father because the system was the reason behind his father’s stresses, pressures and traumas.  
  

#### c. How does the principle of forgiveness work?

The principle of forgiveness does not depend on the actions of others. . It is the way of self-healing and obtaining peace and harmony. It releases our pain, brings the state of freedom in our heart and mind. It shows that people do not forgive for helping another person but they forgive for themselves as it is a matter of selfishness.  
  

#### d. How does the author interpret the noise, squalls and tantrums of his children?

The author interpret the noise, squalls and tantrums of his children as the whole catalog of failures, irritations, fatigue thought etc. for the parents.  
  

#### e. Why did the author decide to educate his children in Swaziland?

He decided to educate his children in Swaziland because he was dissatisfied with the inferior education as a result of Bantu Education Act of South Africa to black children.  
  

#### f. How does the author define human life?

According to the author, human life is a great mixture of goodness, beauty, cruelty, heartbreak, interference, love and so much more.  
  

#### g. According to the author, is it heredity or environment that shapes a man’s character? Explain.

According to the author, it is heredity or environment that shapes a man’s character. The birth of every child is same. By birth, a child is neither a liar, nor a rapist. He/she is not born full of hatred or violence. No one is born in any less glory or goodness that us. Hence heredity doesn’t shape a man’s character. It is our surrounding environment that shapes out character.  
  

#### h. Why is forgiveness important in our life?

Forgiveness is important in our life because it is the way of freeing ourselves from our past errors and moving forward into our future unaffected by the mistakes we made in the past.  
  

### Critical Thinking

  

#### a. Desmond Tutu once said, “Forgiving is not forgetting; it is actually remembering-remembering and not using your right to hit back. It’s a second chance for a new beginning.” Does this quotation apply to this text? Analyze.

“Forgiving is not forgetting; it is actually remembering-remembering and not using your right to hit back. It’s a second chance for a new beginning.” is one of the famous quotes of Desmond Tutu. Forgiveness is the way to release feelings of vengeance toward a person or group who harmed us, not matter they deserve forgiveness or not. It is not to deny seriousness of an offense against us or it doesn’t mean forgetting. Here in the text, even if the author is willing to apologize, he is still a victim of his father’s domestic violence. Desmond Tutu remembers the pain his father had given to his mother. He wanted to hurt back his father in his childhood. But later he forgives his father justifying that his father’s rude behavior was because of the mistreat of white people to black people in South Africa. Therefore forgiving is not forgetting rather it is remembering-remembering and not using it to hit back.  
  

#### b. The author interprets “I am sorry” as three hardest words to say. How does it apply to your life?

Committing mistakes are the part of human life. But accepting it and apologizing for it is the hardest task. In the text, the author interprets “I am sorry” as three hardest words to say. Empathy is the ability to put ourselves in another person’s shoes and feel what they feel. This is something we need to develop. It takes humility. Too often, we are preoccupied with our own feelings. Empathy is the recognition that it’s not all about us. Other people matter. They have feelings, too, and those feelings are important. By saying we are sorry—sincerely and with authentic humility—we validate them as human beings.  
For me the three words “I am sorry” are hardest words to say when I hurt others. Instead to say sorry, I remain aloof to them who have been hurt by me and don’t go close to them to talk again. I don’t talk and be close to them unless they come to speak to me. I am so much proudly and sensitive person. I easily get hurt by other and feel sad and I wish them to come close to me and say “sorry”. But I never realize how much they are affected by my misbehavior. I feel very uneasy and difficult to say, “I am sorry.” Thus, as a matter of pride, fear of being shameful, lack of awareness, sense of superiority, gap in understanding, overwhelming emotion of guilt etc. also makes me hard to say the three words “I am sorry”.  
  

### Writing

  

#### The author talks about dual education system based on race in South Africa in the second half of the twentieth century. We also have private schools and public schools in Nepal. What should be done to make education equal to all citizens of Nepal? Write a couple of paragraphs expressing your views.

Nepal has come a long way in improving equity in education. In most countries, factors like income, geography, gender, language and disability contribute to inequitable access and high drop-out rates. Nepal’s education system has faced many problems since the mid-1800s. The first education system in Nepal was only available to elite families, and Nepali people did not have access to education until 100 years later in the 1950s. Current day education in Nepal is still in the developing stage and did not really start integrating the use of technology in the classroom until 2007.  
  
To illustrate the issues that Nepal’s public school systems face, the children need access to clean drinking water while they attend school as well as at home. The lack of water and high temperatures result in the children having difficulty concentrating and comprehending the material at hand. Thus, this combined with child malnutrition in Nepal, children in public schools do not have an advantage to performing well and tend to fall behind or drop out of school. Given these facts, Nepal’s school system is indeed fairly new and continuing to develop, but there is still limited access to public schools. This limited access is a result of isolation of women from continuing education which leads families into poverty. In Nepal, there is an additional factor of caste and ethnicity which further aggravates this situation. While the caste system was legally abolished in the 1960’s, its legacy continues to impact the population for many years thereafter, and people considered low-caste are often economically and socially disadvantaged. In the past, children from such castes did not have access to schools. This has now changed with government initiatives such as school enrollment campaigns and scholarships for marginalized children.  
  
  

### Grammar

  

#### A. Join the following pairs of sentences using when and while

  

#### a. Bibha Kumari was doing her homework. The doorbell rang.

i. Bibha Kumari was doing her homework when the doorbell rang.  
ii. While Bibha Kumari was doing her homework, the doorbell rang.  
  

#### b. I heard the telephone ring. I picked it up.

When I heard the telephone ring, I picked it up.  
  

#### c. Dil Maya found a thousand rupee note. She was washing her pants.

i. While Dil Maya was washing her pants, she found a thousand rupee note.  
ii. Dil Maya was washing her pants when she found a thousand rupee note.  
  

#### d. Tenjing gave his measurements to the dressmaker. He was visiting the market yesterday.

i. When Tenjing gave his measurements to the dressmaker, he was visiting the market yesterday.  
ii. While Tenjing was visiting the market, he gave his measurements to the dressmaker.  
  

#### e. I was at the butcher’s shop. I met Harikala.

When I was at the butcher’s shop, I met Harikala.  
  

#### f. The sales agent was dealing with the customer. A thief stole the jewels.

i. While the sales agent was dealing with the customer, a thief stole the jewels.  
ii. The sales agent was dealing with the customer when a thief stole the jewels.  
  

#### g. My small brother was sleeping. I played chess with my father.

i. While my small brother was sleeping, I played chess with my father.  
ii. My small brother was sleeping when I played chess with my father.  
  

#### h. The old lady fell down. She was climbing up the stairs.

i. When the old lady fell down, she was climbing up the stairs.  
ii. The old lady fell down while she was climbing up the stairs.  
  

#### i. The leader was giving a speech loudly. He lost his voice.

i. While the leader was giving a speech loudly, he lost his voice.  
ii. The leader was giving a speech loudly when he lost his voice.  
  

#### j. Kanchan broke her backbone. She was lifting up the load.

i. Kanchan was lifting up the load when she broke her backbone.  
ii. While Kanchan was lifting up the load, she broke her backbone.  
  

#### B. Fill in the blanks with one of the connectives from the box.

  
a. We didn’t go for a morning walk today **since** it was raining.  
b. I wanted to go home early **as** I was not feeling well.  
c. My brother stayed at home **because of/ owing to** his illness.  
d. I was late in the class **owing to** traffic jams.  
  
e. He didn’t like dogs **so** he was not happy when his wife brought a puppy at home.  
f. He was not included in the team **owing to** his knee injury.  
g. **As** I was tired, I went to bed early.  
h. He was very unhappy **since** he lost one million rupees in share market.  
i. We cancelled our trip to Rara Lake **owing to** the bad weather.  
j. These two lines intersect with each **because** they are are not parallel lines.