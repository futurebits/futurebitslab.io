## Unit 16
# Fantasy
### Working with words

  

#### A. Match the following words with their definitions.

  
**a. snappy**  
irritable and inclined to speak sharply  
  
**b. discretion**  
the ability to behave without causing embarrassment or attracting too much attention  
  
**c. radiant**  
showing great happiness, love or health  
  
**d. irresolute**  
not able to decide what to do  
  
**e. brusque**  
using very few words and sounding rude  
  
**f. fitful**  
a quiet period between times of activity  
  
**g. harlequin**  
a humorous character in some traditional plays  
  
**h. lull**  
happening only for short periods  
  
**i. janitress**  
a lady whose job is to take care of a building such as a school or a block of flats  
  
**j. speck**  
a very small spot  
  

 

#### B. Consult a dictionary or search over the internet and write definitions of the following terminologies used in the stock market.

  
**a. Liquidity :** the availability of liquid assets (cash) to a market or company.  
  
**b. IPO :** the process of offering shares of a private corporation to the public in a new stock issuance.  
  
**c. NEPSE :** the only stock exchange of Nepal which stands for Nepal Stock Exchange  
  
**d. index :** measurement of the value of a section of the stock market  
  
**e. portfolio :** collection of financial investments like stocks, bonds, commodities, cash, and cash equivalents, including mutual funds and ETFs  
  
**f. dividend :** a payment to shareholders that is made in shares rather than as cash  
  
**g. turn over :** a measure of stock liquidity  
  
**h. margin :** the amount of equity an investor has in their brokerage account  
  

### Comprehension

  

#### A. Write ‘T’ for true statements, ‘F’ for false ones or ‘NG’ if the information is not given in the text.

  
a. Harvey Maxwell was a stock broker. **T**  
b. He was not happy with his clerk, Pitcher. **NG**  
c. Miss Leslie had been married to Maxell for a year. **F**  
d. Maxwell had hired a machine to work in his office. **NG**  
e. He had instructed Pitcher to get a new stenographer. **T**  
f. Miss Leslie was amazed by Maxwell’s proposal. **T**  
g. She thought that he had probably gone mad. **F**  
h. She realized that he had been absent-minded due to his business. **T**  
  

#### B. Answer the following questions.

  

#### a. How did Maxwell enter his office?

Maxwell entered his office briskly with the young lady stenographer and greeting Pitcher by saying “Good Morning”.  
  

#### b. Describe the physical appearance of the young lady.

She was beautiful in a way that was decidedly unstenographic. She forewent the pomp of the alluring pompadour. She wore no chains, bracelets or lockets. She had not the air of being about to accept an invitation to luncheon. Her dress was grey and plain, but it fitted her figure with fidelity and discretion. In her neat black turban hat was the gold-green wing of a macaw. On this morning she was softly and shyly radiant. Her eyes were dreamily bright, her cheeks genuine peachblow, her expression a happy one, tinged with reminiscence.  
  

#### c. What changes did Pitcher notice in the young lady?

Pitcher noticed a difference in her ways this morning. Instead of going straight into the adjoining room, where her desk was, she lingered, slightly irresolute, in the outer office. Once she moved over by Maxwell’s desk, near enough for him to be aware of her presence.  
  

#### d. What was Pitcher’s reply to the young lady concerning a new stenographer?

Pitcher’s reply to the young lady concerning a new stenographer was that Maxwell had already told him to get another stenographer and then he had notified agency a day before to send over a few samples that morning and none of the candidates arrived till the time except her.  
  

#### e. What proposal did Maxwell make with Miss Leslie?

Maxwell made a marriage proposal with Miss Leslie.  
  

#### f. How did she react to his proposal?

She acted very queerly when he proposed her. At first she seemed overcome with amazement; then tears flowed from her wondering eyes; and then she smiled sunnily through them, and one of her arms slid tenderly about the broker’s neck.  
  

### Grammar

  

#### A. Study the following sentences and underline the relative clauses.

  
a. I snatched a minute **when** things had slackened a minute.  
b. Instead of going straight into the adjoining room, **where** her desk was, she lingered slightly irresolute in the outer office.  
c. She was beautiful in a way **that** was decidedly stenographic.  
  

#### B. Fill in the blanks with suitable relative pronouns: who, which, that, whom, whose, where, why, how, what or when.

  
a. There is a lady **whose** wallet has been stolen.  
b. Do you know the man **who** sold these glasses?  
c. The knife **which** you cut the bread with is very sharp.  
d. Why do you blame him for everything **that** goes wrong?  
e. A cemetery is a place **where** dead bodies are buried.  
f. This school is for those children **whose** mother tongue is not Nepali.  
g. I don’t know the name of the person **whom** I spoke over the telephone.  
h. I came to Kathmandu on the day **when** the devastating earthquake took place.  
i. In the application form she wrote **when** she needed a good payment.  
j. The pilot explained **why** he made force landing on the road.  
  

#### C. Join the following pairs of sentences using who, which, that, whose, whom, where, when or what.

  

#### a. The building was destroyed in the fire. It has now been rebuilt.

The building which was destroyed in the fire, has now been rebuilt.  
  

#### b. A new cricket stadium is being made in Chitwan. It can accommodate one hundred thousand people.

A new cricket stadium which is being made in Chitwan, can accommodate one hundred thousand people.  
  

#### c. Shanghai is the most populated city in the world. I stayed there for five years.

Shanghai, where I stayed there for five years, is the most populated city in the world.  
  

#### d. This is the man. I gave some money to him this morning.

This is the man whom I gave some money this morning.  
  

#### e. Do know the man? His son was awarded in the school yesterday.

Do you know the man whose son was awarded in the school yesterday?  
  

#### f. His step-mother was not very kind to him. He had been staying with her.

His step-mother, with whom he had been staying, was not very kind to him.  
  

#### g. He can never forget Nakkhu Jail. He had spent 14 years there.

He can never forget Nakkhu Jail where he had spent 14 years.  
  

#### h. He showed me his new tab. He had paid a hundred thousand rupees for it.

He showed me his new tab for which he had paid a hundred thousand rupees.  
  

#### i. Nelson Mandela joined ANC in 1930s. He was a student then.

Nelson Mandela joined ANC in 1930s when he was a student.  
  

#### j. She forgot to buy the things. Her mother had told to buy them.

She forgot to buy the things which her mother had told her to buy.