## Unit 2
# Family
### Working with words

  

#### A. Find the words from the text and solve the puzzle. Clues are given below.

  
**ACROSS**  
1\. the custom of having more than one wife at the same time – **Polygamy**  
5\. to find an acceptable way of dealing with opposing ideas, needs etc. – **Conflict**  
6\. the process in which towns, streets, etc. are built where there was once countryside – **Urbanization**  
7\. the process by which somebody learns to behave in an acceptable way in their society – **Socialization**  
  
**DOWN**  
1\. ruled or controlled by men – **Patriarchy**  
2\. the act of taking over a position – **takeover**  
3\. done in a way without caring if people are shocked – **surprise**  
4\. single–parent – **lone-parent**  
  

#### B. Find the meanings of the following family–related words and use them in your own sentences.

  

#### Nuclear family

**Meaning:** A family of a couple and their dependent children  
**Sentence:** I live in a nuclear family and my parents are modern enough.  
  

#### Monogamy

**Meaning:** The practice of marrying or state of being married to one person at a time  
**Sentence:** When my husband and I got married, we took a vow of monogamy.  
  

#### Sibling–in–law

**Meaning:** Brother–in–law or sister–in–law  
**Sentence:** I had a sibling–in–law, who came last Monday to see me.  
  

#### Milk kinship

**Meaning:** A form of fostering allegiance with fellow community members  
**Sentence:** Milk kinship, formed during nursing by a non–biological mother  
  

#### Matrilineal

**Meaning:** Based on kinship with the mother or the female line  
**Sentence:** The fact that all livestock are inherited along the matrilineal line is exceptional.  
  

#### Nepotism

**Meaning:** The practice among those with power or influence of favoring relatives or friends, especially by giving them jobs.  
**Sentence:** The nepotism in which the pope indulged is especially inexcusable.  
  

#### Maternity:

**Meaning:** The period during pregnancy and shortly after childbirth, motherhood  
**Sentence:** Many expectant mothers were crowded out of the maternity hospitals.  
  

#### C. The following words are from the above text. Each word has two parts.

_poly, un, non, dis and in are prefixes. They make new words when they are added to the beginning of other root words._  

#### Make at least five words using the prefixes given. Consult a dictionary to learn how they change the meaning of root words.

  
**Pre** – preposition, precaution, previews, prefer, prepare  
**Semi** – semi abandoned, semiabstract, semiarid, semi casual, semi-conductor  
**Sub** – subconscious, subjugate, submissive, subordinate, subsidiary  
**Mis** – misunderstand, mistake, misbehave, mistrust, misspell  
**Mono** – monochrome, monocot, monocular, monoplane, monopoly  
**Un** – unhappy, unhealthy, untouchable, undo, unemployment  
**In** – internal, inflated, inject, informal, infant  
**Inter** – Intermission, interactive, intercept, interchange, interlock  
  
 

### Comprehension

  

#### A. The headings of the first five paragraphs of the above text are given below. Write paragraph number next to them.

a. Patriarchal family – 3  
b. Functions of the family – 2  
c. Modern model of family – 5  
d. Effects of industrialization on family structure – 4  
e. Defining family – 1  
  

#### B. Answer the following questions.

  

#### a. What type of family is thought to be the oldest form of the family?

Nuclear family is thought to be the oldest form of the family.  
  

#### b. How does a family provide security to its members?

Family provides emotional and psychological security through the warmth, love, and companionship. Families also provide physical security for their members, who are too young or too old.  
  

#### c. What were the features of medieval European family?

The main feature of the Medieval European family is that it was a patriarchal or male-dominated and extended family.  
  

#### d. What caused the dissolution of extended families in the West?

Many people, particularly unmarried youths, left farms and went to urban centers to become industrial workers. Thus industrialization and urbanization led to the dissolution of extended families in the West.  
  

#### e. What change occurred in gender role in the modern family that emerged after the Industrial Revolution?

Following change occurred in gender role in the modern family that emerged after the Industrial Revolution:  
(i) Patriarchal rule began to give way to greater equality between the sexes.  
(ii) Family roles once considered exclusively male or female broke down.  
(iii) Some couples choose not to marry legally.  
(iv) The rising levels of divorce led to increase in the number of one-parent households.  
  

#### f. What is family law?

Family law is a practice area concerned with legal issues involving family relationships, such as adoption, divorce, and child custody. Attorneys practicing family law typically handle divorce, child custody, child support, and other related legal matters.  
  

#### g. How is modern marriage defined?

Modern marriage is defined as a voluntary union, usually between a man and a woman.  
  

#### h. What do special family courts try to do?

Special family court tries to deal more fairly with sensitive issues such as custody of children.  
  

#### i. What does the legislation on child labor and child abuse declare?

The legislation on child labor and child abuse declares that there is the responsibility of society for a child’s best future and to provide compulsory education to the children.  
  

#### j. What is common among most legal systems regarding property?

In most legal systems a departed family member deals with property left by the deceased family member.  
  

### Critical Thinking

  

#### a. What changes have started to occur in Nepali families in recent days? What impacts will they bring on the society? Discuss.

Since the world is now in 21st century, everything is gradually developing through traditional culture to modern culture. Nowadays, people want to satisfy their feeling. They change their behaviour to family lifestyle in modern societies. They want to live in nuclear family with the full of happiness with child, husband and wife but other side in there is rapid increase of divorce in Nepal due to misunderstand in nuclear family. In Nepali families both male and female can rule household, country. Everybody male and female is getting equal right in family. Every family member understanding each other felling, happy and they can share their idea, concept in family member. In Nepalese families young age at marriage in Nepal is closely linked to the widespread practice of arranged marriages, where relationships and agreements between families prevail over individual choices. Thus, a decline in very early marriage, accompanied by a decrease in the interval between marriage and cohabitation, may indicate a change in the marital decision-making process and an increase in the level of involvement on the part of spouses in the formation of their own marital unions. Thus, they will bring both positive and negative changes on the society.  
  

#### b. We see many elderly people in the elderly homes these days in Nepal. Some of them are abandoned while others live there willingly. Do you think Nepali people are deviating from their traditional culture? Give reasons.

With the rapidly increasing number of aged, the care of elderly has emerged as an important issue in Nepal. Providing care for the aged has never been a problem in Nepal where a value based joint family system was dominant. This family structure has been the socio-economic backbone of the average Nepalese. The families were sharing the responsibility to look after their elderly by giving them all kind of support including emotional, psychological, behavioural or economic. They were getting full respect and value. They were living in the family till the end of their life. With the increasing influence of modernization and new life styles resulting in transitional changes in value system in recent times, the joint family is breaking down into several scattered nuclear families. Change in family structure and contemporary changes in the psycho-social matrix and values often compel the elderly to live alone or to shift from their own homes to some institutions or old age homes.  
  
Misbehaviour of children, financial crisis often lead to feeling of ignorance and lack of emotional support in elderly which often compel them to opt other places for living a problem free life. And, in present scenario along with other reasons elderly homes are being considered as a better alternative to reside. There is a need to generate emotional support facilities in these homesandthegovernmentandvoluntaryagenciesinNepalmustmakearrangementsfor institutional support and care for the elderly.  
  

### Writing

  

#### a. Write an essay on The Importance of Family. In your essay, you can use these guided questions.

_• Why family is important to you.  
• Why family is or is not important for society  
• How you think families will change in the future._  

The Importance of Family

In today’s world when everything is losing its meaning, we need to realize the importance of family more than ever. While the world is becoming more modern and advanced, the meaning of family and what stands for remains the same. It does not matter what kind of family one belongs to. It is all equal as long as there are caring and acceptance. You may be from a joint family, same-sex partner family, nuclear family, it is all the same. The relationships we have with our members make our family strong. We all have unique relations with each family member. In addition to other things, a family is the strongest unit in one’s life.  
  
One cannot emphasize enough on the importance of family. They play a great role in our lives and make us better human beings. The one lucky enough to have a family often do not realize the value of a family. However, those who do not have families know their worth. A family is our source of strength. It teaches us what relationships mean. They help us create meaningful relationships in the outside world. The love we inherit from our families, we pass on to our independent relationships. Moreover, families teach us better communication. When we spend time with our families and love each other and communicate openly, we create a better future for us. When we stay connected with our families, we learn to connect better with the world. Similarly, families teach us patience. It gets tough sometimes to be patient with our family members. Yet we remain so out of love and respect. Thus, it teaches us patience to deal better with the world. Families boost our confidence and make us feel loved. They are the pillars of our strength who never fall instead keep us strong so we become better people.  
  
As we know that society is important for family. Society is the combination of one or more family member so, without family society cannot create. A family makes us responsible for our duty toward society. Family provides social security in the society. Family introduces you in the society. In the social world without a family an individual’s identity is never complete. And I think that family will change in future due to upgrading of modern technology, 5-10 year there is big family where very family member were residing but now day most of the family is living in nuclear family format.  
  

#### b. Some people think it is better to live in a nuclear family. Other people think that living in extended family is more advantageous. What do you think? Write an essay discussing the advantages and disadvantages of both.

In my views both the Extended family and Nuclear family are better at their own place as both of them have some advantages and some disadvantages.  
  
Extended family is a combination of all generation of member. In these families, all member of the family including the children also grow with the belief that they also have certain duties and function to perform. They should learn to control their demand and expectation. In Nepal, more people prefer to stay in extended family and I’m also one of them in comparison to nuclear family. The advantage of extended family are, in extended family there is a good social and emotional relationship among the family members. There is a proper economic and social security. In extended family there is a proper care and supervision for the children also proper education and control over the problem of adolescence. The important advantage of extended family is a sense of protection of culture, tradition and values. There is not only advantage of Extended family but also some disadvantages occur in extended family. In extended it can push back in personality development. There is less privacy. There may be probability of low living standards. There may be difficult to meet needs and demand of each member in the family. Less of self confidence and dependency may increase in the extended family  
  
Now talking about the nuclear family, Nuclear family is composed of husband, wife and their children, otherwise Extend family composed of grandfather, grandmother, parents, brother and their wives, daughter in-law, grandson etc., who share same kitchen and shelter up to four generation. Some of the advantages of nuclear family is that there is high understand and cooperation among each other, less possibility of quarrel among family member due the fewer members. It is easy to meet need and demand of each family.There is freedom of making decisions of their own. Also there are some disadvantages of having nuclear family as there is lack of security and confidence, there is lack of proper care of children and old age people.There is generation gap in respecting the norms and value of people. There is lack of love and care for the children if the couple engaged in professions.  
  
Thus in short we can say that both the nuclear and extended family are better at their own place.  
  

### Grammar

  

#### B. Choose the best answer to complete the sentences.

  
a. ‘How much was your parking ticket?’ ‘Fifty rupees.’ ‘Oh well, it **could have** been worse.’  
b. It **must have**. got lost in the post. These things happen sometimes.  
c. ‘Sorry I’m late. I got delayed at work.’ ‘You **must have** called. I was really worried about you.’  
d. ‘I don’t think he meant to be rude.’ ‘He **must have** said sorry.’  
e. ‘Whose signature is this?’ ‘I don’t know. It **could** be Manoj’s. That looks a bit like an M.’  
f. I had it when I left the office so I **mustn’t have**. lost it on the way to home.  
g. You **must have** think it’s funny, but I think it’s pathetic.  
  

#### C. Complete the following sentences with appropriate endings. Use correct modal verbs.

  
a. At the end of the course, **we must be prepared for the individual presentation.**  
b. If you want to earn a lot of money, **I would suggest some tips.**  
c. You were not in your house yesterday. **You could have your belongings stolen.**  
d. I’m quite busy tomorrow. I **must visit my dentist.**  
e. When you were a small kid, **you could ask for money from your relatives.**  
f. My car is broken. I **should take it to the service center.**  
g. I’ve got a fast speed internet at home. I **can watch movies in high quality.**  
h. Even though she didn’t study well, she **could solve all the problems.**  
i. There are plenty of newspapers in the library. You **can read any of them if you want.**  
j. What do you think you were doing, playing in the road? You **could have been hit by the car.**  
k. I have no time. I **can’t meet you.**  
l. You don’t look well. You **should take a rest.**