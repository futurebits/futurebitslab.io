## Unit 12
# Animal World
### Working with words

  

#### A. Find the words from the text which have the following meanings.

  
a. Persons who are related to you and who live after you, such as your child or grandchild are called **descendants**.  
  
b. **‘Twirl’** means to turn or spin around and around quickly  
  
c. **Glycoproteins** are proteins which contain oligosaccharide chains (glycans) attached to amino acid side-chains  
  
d. **Lectins** are carbohydrate-binding proteins that are highly specific for sugar groups that are part of other molecules and so cause agglutination of particular cells.  
  
e. **Polyps** are abnormal tissue growths that most often look like small, flat bumps or tiny mushroom like stalks found in the phylum Cnidaria and the medusa  
  
f. **Predation** is a biological interaction where one organism, the predator, kills and eats another organism, its prey.  
  
g. **Anemone** is a genus of flowering plants in the buttercup family.  
  
h. A/An **finicky** person is extremely or excessively particular, exacting, or meticulous in taste or standards.  
  
i. **Nudibranch** is a group of soft-bodied, marine gastropod molluscs which shed their shells after their larval stage.  
  
j. A **medusa** is a free-swimming sexual form of a coelenterate such as a jellyfish, typically having an umbrella-shaped body with stinging tentacles around the edge.  
  

#### B. Look up a dictionary and write the meanings of the following words then use them in your own sentences.

  
**Arthropod :** _an invertebrate animal of the large phylum Arthropoda_  
Many arthropod animals undergo physiological changes that smooth their integration.  
  
**Gastropod :** _a mollusc of the large class Gastropoda_  
The problem of the causes of the torsion of the Gastropod body has been much discussed.  
  
**Biomass :** _the total quantity or weight of organisms in a given area or volume_  
Energy from biomass is a growing source of renewable energy.  
  
**Calcification :** _the hardening of tissue or other material by the deposition of insoluble calcium compounds_  
The extremities of the cartilages frequently undergo calcification.  
  
**Metamorphosis :** _the process of transformation from an immature form to an adult form in two or more distinct stages_  
A butterfly is produced by metamorphosis from a caterpillar.  
  
**Sturgeon :** _a very large primitive fish with bony plates on the body_  
Caviar is sturgeon roe prepared by a special process.   
  

  

### Comprehension

  

#### Answer the following questions.

  

#### a. What are the indicators of the fact that we are very self-conscious about ourselves these days?

The indicators of the fact that we are very self-conscious about ourselves these days are self-realization, self-help, self-development, self-awareness, self-respect and self-enlightenment.  
  

#### b. How have we celebrated the fact that we have our individual identity?

We have celebrated the fact that we have our individual identity by providing ourselves with a real name.  
  

#### c. Are we, human beings, really unique? Why/Why not?

No, we human beings, aren’t really unique. It’s because uniqueness is so commonplace a property of living things that there is really nothing at all unique about it. The phenomenon can’t be unique and universal at the same time.  
  

#### d. How do fish recognize each other?

Fish recognize each other by the smell of each other.  
  

#### e. What is the function of individuality?

The function of individuality is self-preservation and maintaining individuality.  
  

#### f. What does the mix-up of two selves tell us about our identity?

The mix-up of two selves tells us unity and cooperation to each other is required for survival and existence.  
  

#### g. What does the author illustrate with the tale of the nudibranch and the medusa?

The author illustrates the value of co-existence, cooperation, helpfulness, collaboration and togetherness with the tale of the nudibranch and the medusa. It also projects the significance of friendship and dependence on each other for survival and existence.  
  

#### h. Why is the author disturbed by the thought of the creatures like the nudibranch and medusa?

The author is disturbed by the thought of the creatures like the nudibranch and medusa because it provides him with an odd feeling and confuses in with vague and bizarre concept like a dream.  
  

#### i. What does the writer mean by “they remind me of the whole earth at once?”

By saying “they remind me of the whole earth at once'”, the writer means to say that it is very complicated and bizarre to justify the relationship between the creatures and non-living things of the world and as more as one tries to understand, the more he/she gets confused.  
  

### Critical thinking

  

#### a. How does the author make satire on the modern idea of the ‘self’ based on individuality, independence and uniqueness?

The author in this essay satires on the modern idea of ‘self’ based on individuality, independence and uniqueness. This essay is about the self and what nature can teach us. In the beginning the writer brings an attention to the trend in humans on uplifting the feeling of self-based on individuality, independence and uniqueness.  
  
Here the author is presenting the close relationship between the nudibranch and medusa living in the Bay of Naples. A mature jellyfish engulfs a tiny newly-hatched slug only to be devoured bit by bit until the snail dominates and the jellyfish is reduced to a round “successfully edited parasite” affixed to the skin near the snail’s mouth. Finally the tentacles, until the jellyfish becomes reduced in substance by being eaten while the snail grows correspondingly in size.  
  
This essay projects both creatures cannot live in any other way, they depend for their survival on each other. The author illustrates the value of co-existence, cooperation, helpfulness, collaboration and togetherness with the tale of the nudibranch and the medusa. We all are important and one’s life is determined by another like the relation between anemones and crabs as well as medusa and snail. It also shows the significance of friendship and dependence on each other for survival and existence. Through this, the author justifies that the modern people’s pride over the concept of individuality, independence and uniqueness is not applicable in the real and practical life.  
  

### Writing

  

#### A. Write an essay on “Independence vs. Interdependence” in about 250 words.

Independence vs. Interdependence

Human beings usually categorize themselves into individual or group entities. These self-conceptions usually emanate in people’s minds and determine their motivation as well as cognitions. There are usually two perceptions that people have about themselves regarding their relationship with others. The first is the independent self-concept where an individual’s conception about his or her existence is that he or she exists separate from other people. Independent self-concept encompasses behaviour traits, preferences and attitudes of an individual. The second perception is the interdependent self-concept where a person is affiliated or feels connected to others. This paper explores the advantages of interdependence as opposed to independence. Gish Jen argues that “the joy of a functioning interdependent relationship can be tremendous.”  
  
Unlike independent-self which focuses on individual responsibility separate from the entire society, interdependent self-concept emphasizes shared responsibilities. The main tenet of interdependence self-concept is the reliance that exists among individuals in the society for survival and mutual development. Interdependence self-instils the idea that people are not alone in their attempts at mental growth and development. People also provided support for one another to grow socially. Interdependent self-concept also promotes responsibility sharing by encouraging individuals to take various roles, which contribute to the development and growth of the society.  
  
Interdependent self-concept is also crucial for overcoming fear. Unlike independence, which puts an individual against the rest of other members of the society, interdependence promotes cohesion among society members, thus helping individuals to experience lower levels of fear and insecurity. The recognition of the fact that one has support of others remains crucial for alleviating negative feelings, which could yield fear in individuals. Support for each other is only possible in a society where people feel close to each other and the society is characterized by high level of interdependence.  
  
Interdependence emphasizes on how people relate with each other and get on well with existing norms of the society, which dictate how people relate. Intrinsic attributes of individuals play no major role in the way individuals think, feel or act. However, Jen also argues that “we need both the interdependent and the independent self,” appealing that people from either side of self-concepts accommodates and understands each other.  
  

### B. Write a newspaper article highlighting the increasing individualism in the modern Nepali society.

Increasing Individualism in the Modern  
Nepali Society

**By Ramesh Thapa  
March 12, 2019**  
  
Individualism is the moral stance, political philosophy, ideology and social outlook that emphasizes the intrinsic worth of the individual. Individualists promote the exercise of one’s goals and desires and to value independence and self-reliance and advocate that interests of the individual should achieve precedence over the state or a social group while opposing external interference upon one’s own interests by society or institutions such as the government. Individualism is often defined in contrast to totalitarianism, collectivism and more corporate social forms.  
  
In the modern Nepalese society, individualism is increasing rapidly. This increase appears to be due mostly to increasing socio-economic development, including higher incomes, more education, urbanization, and a shift toward white-collar jobs. Increases in these factors in a given region are reliably followed by gains in individualist beliefs and practices in the ensuing decade Nepal comes under those few countries that bucked the global trend toward individualism.  
  
Rising prosperity and education are largely welcome changes that reflect not merely gains in wealth for the already-wealthy, but for the desperately poor as well. Wealth, and the individualism that follows, are often conflated with selfishness. This is, in part, because individualism’s inverse — collectivism — emphasizes close social ties and an interconnected rather than independent view of the self.  
  

### Grammar

  

#### A. Make passive sentences from the following information as in the example.

  

#### a. volleyball/every/country/play

Volleyball is played in every country.  
  

#### b. spaghetti/boiling water/cook

Spaghetti is cooked in boiling water.  
  

#### c. each lesson/an exercise/follow

Each lesson is followed by an exercise.  
  

#### d. taxes/the price/include

Taxes are included in the price.  
  

#### e. extensive information/the internet/find

Extensive information is found on the internet.  
  

#### f. our order/the waiter/took

Our order was taken by the waiter.  
  

#### g. the schedule/the participants/will distribute

The schedule will be distributed by the participants.  
  

#### h. the police/footprint/found

Footprint was found by the police.  
  

#### i. the children/the sandcastles/built

The sandcastles were built by the children.  
  

#### j. the father/the window/not going to open

The window is not going to be opened by the father.  
  

#### B. Rewrite the following sentences in the passive voice using the correct form of verbs in the brackets.

  
a. Call the ambulance! Two boys **have been injured** (injure) in a motorbike accident.  
  
b. The clock **has been used** (use) since the 17th century.  
  
c. I had to wait outside the classroom while the classroom **was being cleaned** (clean).  
  
d. The problem **is being discussed** (discuss) by the subject specialists at the moment.  
  
e. By the time I came back, the task **has been finished** (finish).  
  
f. Women **are said** (say) to be happier than men.  
  
g. Look! The house **had been destroyed** (destroy) by the fire.  
  
h. The other three reports **will be submitted** (submit) by next month.  
  
i. Many people **have been resuced** (rescue) from the floods by the security persons this year.  
  
j. The state of Florida **was hit** (hit) by a hurricane that did serious damage.