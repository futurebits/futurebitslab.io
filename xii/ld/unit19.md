## Unit 19
# Migration and Diaspora
### Working with words

  

#### A. Find the words from the text that have the following meanings. The first letter of the word has been given.

  
a. **sojourner** a person who resides temporarily in a place  
b. **transnational** existing in or involving different countries  
c. **remittance** a sum of money sent in payment or as a gift  
d. **assimilation** the process of allowing somebody to become a part of a country or community  
e. **misnomer** a name or a word that is not appropriate or accurate  
f. **confer** to give somebody an award or a particular honour or right  
g. **dormant** not active or growing now but able to become active  
h. **persecution** the act of treating somebody in a cruel and unfair way  
  

#### B. Pronounce the following words. What sounds do the underlined letter(s) represent?

  
ribbon : /ˈɹɪbən/  
filled : /fɪld/  
phone : /fəʊ̯n/  
often : /ˈɒf(t)ən/  
ghost : /ɡəʊst/  
who : /huː/  
back : /bæk/  
edge : /ɛdʒ/  
jellyfish : /ˈdʒɛliˌfɪʃ/  
Christmas : /ˈkɹɪsməs/  
acclaim : /ə.ˈkleɪm/  
spell : /spɛl/  
summer : /ˈsʌmə(ɹ)/  
sink : /skɪn/  
tongue : /tɒŋɡ/  
happy : /ˈhæpiː/  
wrong : /ɹɒŋ/  
sun : /sʌn/  
batter : /ˈbætə(ɹ)/  
five : /faɪv/  
why : /waɪ/  
yellow : /ˈjɛl.əʊ/  
treasure : /ˈtɹɛʒɚ/  
cheese : /t͡ʃiːz/  
shark : /ʃɑɹk/  
thief : /θiːf/  
feather : /ˈfɛð.ə(ɹ)/  
  

#### C. Write the number of syllables and mark the stressed syllable of the following words.

  

| Words | Stessed Syllables | Syllables |
| --- | --- | --- |
| certificate | /səˈtɪfɪkeɪt/ | 4 syllables |
| holiday | /ˈhɒlɪdeɪ/ | 3 syllables |
| zoology | /zuːˈɒlədʒi,zəʊˈɒlədʒi/ | 4 syllables |
| photographic | /ˌfəʊtəˈɡrafɪk/ | 4 syllables |
| geography | /dʒɪˈɒɡrəfi,ˈdʒɒɡrəfi/ | 4 syllables |
| curiosity | /kjʊərɪˈɒsɪti/ | 5 syllables |
| mechanically | /mɪˈkanɪkli/ | 5 syllables |
| characteristics | /karəktəˈrɪstɪk/ | 5 syllables |
| examination | /ɪɡˌzamɪˈneɪʃ(ə)n,ɛɡˌzamɪˈneɪʃ(ə)n/ | 5 syllables |
| negotiation | /nɪɡəʊʃɪˈeɪʃ(ə)n/ | 5 syllables |
| paraphrase | /ˈparəfreɪz/ | 3 syllables |
| paradoxically | /ˌparəˈdɒksɪkli/ | 6 syllables |
| territoriality | /ˌtɛrɪˈtɔːrɪəlɪti/ | 7 syllables |

 

### Comprehension

  

#### A. Match the first halves of the sentences (a-g) with their second halves (i-vii).

  

|     |     |
| --- | --- |
| a. The term assimilation has been used | iii |
| b. It is essential to study the process of diasporization | v   |
| c. The definition of dediasporization | vii |
| d. The dediasporization process for migrants who have not given up their native citizenship | vi  |
| e. Some countries grant full citizenship to the returnees | iv  |
| f. It is surprising that | i   |
| g. The role of a state in dediasporization | ii  |

|     |
| --- |
| i. in some countries the returnees are referred as diaspora. |
| ii. reveals its identity. |
| iii. as an analytical tool in the study of integration of the migrants. |
| iv. while some countries bar them from certain rights. |
| v. in order to understand the description of immigration. |
| vi. requires them only to return to their homeland. |
| vii. focuses only on the aspect of relocating migrants to their homelands. |

  

#### B. Answer the following questions.

#### a. According to the author, what are the three aspects of migration?

According to the author, the three aspects of migration are the forward motion, the migrants themselves and the backward motion.  
  

#### b. Which aspect of migration is neglected by the researchers?

Pluridimensionality of the dedasporization phenomenon aspects of migration is neglected by the researchers.  
  

#### c. What is ‘dediasporization’?

Dediasporization is the process by the help of which a diasporic subject either reacquires homeland citizenship by returning to the sending country, effects generational assimilation in the host state, or reinscribes himself or herself in the transnational circuit of the transnation-state.  
  

#### d. Why is the role of the state important in dediasporization?

The role of the state is important in dediasporization because it intervenes in the process to assure itself of the eligibility of such a person to reacquire state citizenship, with all of its privileges and obligations.  
  

#### e. How is Chinese diaspora in the Caribbean different from others?

The Chinese diaspora in the Caribbean is different from others as they are considered foreigners by the local Caribbean citizens though they have been living there for more than a century.  
  

#### f. Why is it difficult to regain citizenship after returning to the homeland?

It is difficult to regain citizenship after returning to the homeland because the integration of citizens in society depends on the state’s bestowal of legal legitimacy.  
  

#### g. What do the Germans feel towards the returnees from Russia, Kazakhstan and Uzbekistan?

The Germans have distinct feelings and are quite alienated towards those returning from Russia, Kazakhstan and Uzbekistan.  
  

#### h. How are the returnees’ activities in Israel different from that of other countries?

The returnees’ activities in Israel are different from that of other countries as these individuals assert full citizenship, form their own political parties and grant freedom and advocate rights to other citizens of the country.  
  

#### i. What is the role of the individual in dediasporization?

The role of the individual in dediasporization is to assimilate with the people and culture and maintain citizenship and help in the nation-building process.  
  

### Writing

  

#### A. The following words and phrases are used in interpreting data of different types of charts and diagrams. Study the words/phrases and put them in the right column.  

| Upward Trend | Downward Trend | Stable Trend |
| --- | --- | --- |
| grow, go up to, boom, peak, level up, rise, climb, increase | decline, collapse, fall, drop, dip, go down, reduce, decrease, crash, plunge, plummet | stay constant, remain stable, no change, remain steady, stay, maintain the same level |

  

### Grammar

  

#### A. Make sentences from the table below using used to / didn’t use to as shown in the example.

  
a. She used to travel a lot but now she rarely leaves her town.  
b. She used to be lazy but now she works hard.  
c. She didn’t use to like junk food but she eats momo and noodles these days.  
d. She used to have a pet but it died last year.  
e. She used to be a school teacher but now she is a professor.  
f. She used to have many friends but now she has limited friends.  
g. She did not use to read many books but she reads a lot books.  
h. She didn’t use to take coffee but now she drinks coffee.  
i. She used to go to parties a lot but now she doesn’t attend parties.  
  

#### B. Complete the following sentences with used to or would.

  
a. My sister **used to** have short hair when she was young.  
b. We **used to** have lunch in the same school café when I was in middle school.  
c. My father **used to** play badminton before he had backbone problem.  
d. When I was very young, I **didn’t use to** (not) like milk.  
e. She **would** call me after class for a chat.  
f. My mother **didn’t use to** (not) wear glasses when she was at the university.  
g. When I was a child, we **used to** live in a village.  
h. On Sundays, My mother **would** wake up and go to the temple.  
i. How many friends **would** have in class ten?  
j. My father **would** always read me bedtime stories before bed.