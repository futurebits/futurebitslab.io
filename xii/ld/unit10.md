## Unit 10
# Career Opportunities
### Working with words

  

#### A. Complete the sentences with the correct words from the box below.

  
a. You must learn about 2000 Kanji to develop **competence** in the spoken Japanese language.  
  
b. I can type both English and Nepali but not as fast as **professional** typists.  
  
c. When she was in class eight, she got the **opportunity** to participate in a national painting competition.  
  
d. The Prime Minister gave a long list of his **achievement**.  
  
e. He spent many years in jail before reaching the **position** of a minister.  
  
f. About fifty workers were made **redundant** because of the financial crisis in the factory.  
  
g. He is affiliated to the World Bank as a senior **consultant**.  
  
h. They registered my application after making **verification** of my documents.  
  
i. I requested my teacher to be my **referee** in my CV.  
  
j. To make our country self-sustained in food production is an **imperative** need at present.  
  

#### B. Define the following employment-related terms and use them in the sentences of your own.

  
**Volunteering :** _work for an organization without being paid._  
Volunteering is an easy way to get involved in practical conservation.  
  
**On the job training :** _a practical approach to acquiring new skills._  
I learned the skills necessary to be a top music producer on the job training.  
  
**Career opportunity :** _a particular job that may be a steppingstone to loftier ambitions._  
Networking opens a lot more career opportunities and at a faster rate.  
  
**Skill development :** _the method of detecting skill gaps and improving these skills._  
The social skill development needs to be going on all the time.  
  
**Apprenticeship :** _the position of an apprentice._  
He served his apprenticeship as a fitter.  
  
**Career counselling :** _a type of support provided by career counselors to their clients._  
They oversee career counseling programs and testing which measures students.  
  
**Credentials :** _something that gives a title to credit or confidence_  
His campaign has refused to credential any reporter from that outlet.  
  
**Human capital :** _the economic value of a worker’s experience and skills._  
Investment in education is seen as the key to improving human capital.  
  
**Internship :** _a period of work experience offered by an organization for a limited period of time._  
He served his medical internship at Norvic Hospital.  
  
**Soft skills :** _character traits skills that characterize a person’s relationships with other people._  
Veterans should be valued for their soft skills more than their hard technical skills.  
  
**Minimum wage :** _the lowest wage per hour that a worker may be paid, as mandated by federal law._  
The country requires a minimum wage because workers paid below the poverty line have an added cost on society.  
  
**Recruitment :** _the action of enlisting new people in the armed forces._  
An excellent career opportunity has now arisen with a leading recruitment agency.  
  
**Role model :** _a person looked to by others as an example to be imitated._  
She was a great role model and indeed a great mentor to me.  
  
**Aptitude :** _a natural ability to do something._  
He has demonstrated a great aptitude for carpentry skills.  
  
**Assessment :** _the action of making a judgment about someone or something_  
It’s a difficult problem that requires careful assessment.  
  

#### C. Based on their pronunciation, divide the following words into two groups so that the vowel sounds rhyme with here and hare.

  
**Here /hɪə/**  
Fear, beer, ear, leer, cheer, sheer, shear, tear (n.), mere, deer, dear, clear, sneer, gear  
  
**Hare /hɛː/**  
Fare, fair, bear, bare, care, heir, air, share, lair, chair, share, tear (v.), mare, dare, snare  
  


### Comprehension

  

#### Answer the following questions.

  

#### a. What does a CV mean and why is it important in one’s career?

Curriculum vitae is a Latin term that literally means ‘course of life’. In practice, it is a document that sets out a whole host of your personal details, experience and achievements as they relate to your working life.  
It is important because it provides with a crucially important opportunity to reflect on and plan further developments in one’s career. It is need when applying for jobs, seeking promotion and trying to get research funding.  
  

#### b. Does the same CV work for all job opportunities? Why or why not?

Yes, the same CV works for all job opportunities but it’s better to make slight tweaks to ensure that the CV highlights your most relevant skills and qualifications on the basis of job description and uses terminology found in the job listing.  
  

#### c. What are the different areas where CV can help you?

A good CV is required when applying for jobs, seeking promotion and trying to get research funding. It helps for things such as when you’re asked to be an external examiner for a course or a research thesis; for audits of teaching and research activity; if your faculty or department has to be validated by an external professional body – in short, any circumstances in which somebody needs to judge your individual professional competence or that of you and your colleagues collectively.  
  

#### d. What do you mean by ‘staying CV’ and ‘leaving CV’? Which one would you develop for yourself as a freshman?

A ‘staying CV’ is that of the good university citizen, including plenty of committee work and administration, pastoral care of students, a heavy teaching load as well as a credible research record. A ‘leaving CV’ is that which reflect the interests of a prospective new employer and will probably highlight research achievements, while still showing that you are generally competent and willing across the range of duties undertaken by academics.  
  
I would develop a leaving CV for myself as a freshman.  
  

#### e. How can you draft a good CV?

One can draft a good CV with the help of your friends, family, mentors and more experienced colleagues. A basic framework can be used to structure your recollections and thinking. You should show your draft CV to people who know you and/or who know what an academic CV should look like and ask for feedback. In this way, you can draft a good CV.  
  

#### f. What is the difference between academic CV and non-academic CV?

There are two key differences between academic and non-academic CVs. One is that academic CVs tend to be quite a bit longer than those of non-academics, and they get longer as a person’s career develops. A second, and perhaps more fundamental, difference is that non-academics, especially when they are seeking middle management positions, are frequently encouraged to make largely unverifiable assertions about their qualities and skills rather than to list verifiable achievements.  
  

### Critical thinking

  

#### a. CV may not represent a person’s skills and abilities accurately because one’s confidence cannot be rendered in a paper. What do you think the employers should do to find the best people for the job?

It’s true that CV may not represent a person’s skills and abilities accurately because one’s confidence cannot be rendered in a paper. Employers prefer candidates who can communicate effectively. Oral and written communication is a key skill needed in almost every job. In my view, instead of judging the ability of a person by a piece of paper, employers can divide the selection process in three steps. First step is to get acquainted with the willing participants. Employers can introduce their company terms and conditions to the participants and if they think they can work under the conditions, then they can be invited for further process. Next step is cover letter explanation. In this step the employers should judge the person’s behaviors, skills, their character and ability whether they could perform well in the company or not. Last but not the least step is calling the selected candidate to participate intelligently to join colleagues at weekly Friday lunch where everyone “talked shop”. Employees are meant to work in team. So only a person should not take their interview. Every person who is already in the company should get a chance to talk to them and decide whether they can work with him/her or not. These are the things the employers should do to find the best people for the job rather than focusing on CV only.  
  

#### b. If the employers provide job opportunity by assessing one’s CV, how can fresh graduates compete with the experienced competitors?

In today’s competitive job market, having a degree and doing good work unfortunately may not be quite enough to land you the job you. In order to get yourself one step closer than other graduates to your dream job, you have to stand out from the crowd and make a good impression on your potential employers.  
  
For some careers you will be required to have achieved a good degree in a specified area, however a lot of the time employers are looking for more than just a great set of exam results in their prospective employees. They want candidates with personality and the right combination of soft skills, in areas such as interpersonal and leadership. Many fresh graduates have no idea on how to write a good CV. Most use the same resume template and end up having CVs with similar format. To make yours stand out, you should break the mould and be creative. If you’re applying for a creative industry, you may wish to opt for something a bit out-of-the-box or visually impressive that will really make it stand out from others. By doing this a fresh graduate can compete with the experienced competitors.   
  

### Writing

  

#### A. Study the following advertisement. Write an application for one of the positions. Prepare your CV too that suits for the job.

  
_**Application:**_  
Bharatpur-7, Chitwan.  
6th June 2021  
  
**To:**  
The Assistant Director,  
MM Construction Pvt. Ltd.,  
Satdobato Marg,  
Lalitpur.  

Subject : Application for The Post of Civil Engineer

Dear Sir,  
With reference to your advertisement in THE PIONEER on 2nd July inviting application for the post of a civil engineer under your authority, I beg to offer myself as a candidate for the same. My qualifications and experience are given below.  
  
I passed my civil engineering examination from the IOE, Pulchowk Campus in first division and also got degree in B.Sc. Physics (Hons.) I have an excellent academic career and have won many scholarship and awards during my school and college life.  
  
I worked in ArEiCon Engineers Pvt. Ltd. – Lalitpur for five years and during the period tried advanced techniques which proved to be highly beneficial to the firm. At present I have been working with Global Impex International Pvt. Ltd. for the last four years. The job involves administrative and supervisory duties. In recognition of my services, the company has offered me further incentives in the form of special bonus. But this job does not have much future. So I seek a job under you which will open new avenues for my career. I am ready to work at any of the branches.  
  
I have attached my CV to the email for your reference. Please have a look at it.  
  
I hope to meet you and discuss this opportunity further. Thank you for considering my application for the role.  
  
I may assure you if I am given the job, I shall work diligently.  
  
Yours Faithfully,  
Manoj Poudel  
  
_**CV:**_  
**Manoj Poudel**  
Pashupati Vision Complex, Ring Road, Kathmandu 44600  
**T:** 01-4113713  
**M:** 981-234-5678  
**E:** manojpoudel@gmail,com  
  
**PERSONAL SUMMARY**  
An ambitious and dedicated civil engineer with strong practical and technical skills and a range of experience within construction engineering and project management. Having a sound knowledge of designing, testing and evaluating a designs overall effectiveness, cost, reliability, and safety. Currently seeking a challenging professional civil or structural engineering position either in the UK or abroad and willing to consider permanent or short term contracts.  
  
**CAREER HISTORY**  
  
**SENIOR CIVIL ENGINEER** – Global Impex International Pvt. Ltd.  
_January 2018 – present_  
• Directing tasks to a multi-disciplined team of staff.  
• Arranging the planning and scheduling of work.  
• Creating Monthly Quality Audits.  
• Carrying out spot inspections on welding, pipe cleanliness, compression fitting installation etc.  
• Liaison with engineers, designers, local authorities, contractors and suppliers.  
  
**CIVIL ENGINEER** – ArEiCon Engineers Pvt. Ltd.  
_April 2013 – January 2018_  
• Involved in a project that included the construction, maintenance and repair work on motorways.  
• Designing and supervising of motorway maintenance works.  
• Responsible for the design and development of bridge maintenance schemes.  
• Maintaining non-conformance / non-compliance records and logs.  
• Responsible for quality control of engineering and construction documents.  
  
**PROFESSIONAL EXPERIENCE**  
  
**Civil Engineering**  
• Able to understand a client’s quality compliance requirements and then make sure they are met.  
• Have worked for both public and private sector clients.  
• Excellent communication skills and able to work closely with both clients and other specialists such as architects and building contractors  
• Ability to carry our detailed feasibility studies for projects to ensure the most effective and efficient utilization of materials, equipment and labour.  
• Experience of using the latest computer software for modeling and design purposes.  
  
**Project Management**  
• Arranging and chairing meetings with clients.  
• Ability to liaise with key project stake holders like the clients, water companies, the Environment Agency, local authorities and also utility companies.  
• Experience of designing, project managing & liaising with clients.  
• Directing outside consultants in construction activities.  
• Assisting in the pricing of tender enquiries and valuations.  
  
**KEY COMPETENCIES AND SKILLS**  
Structural Design  
Inspections  
Quality assurance  
Certification  
Surveying  
Project management  
Strategic planning  
Feasibility studies  
AutoCad  
Highway design  
Producing ACAD plans  
Site supervision  
  
**TECHNICAL SKILLS AND EXPERTISE**  
AutoCAD  
Autotrack  
Autosign  
Signplot  
Microstation 2D & 3D  
Inroads  
  
**ACADEMIC QUALIFICATIONS**  
BSC Civil Engineering 2009  
B.Sc. Physics (Hons.), 2011  
Jan 2012 Introduction to AutoCad 2012  
Sept 2012 CIWEM Sewerage and Waste Water Treatment Seminar  
Sept 2012 CDM Regulations 2007 and compliance  
  
**REFERENCES –** available on request.  
  
**Driving license:** Yes  
  
**DOB:** 1985  
  
**Languages:** English, Nepali, Hindi  
  

### Grammar

  

#### B. Rewrite the following sentences using the correct form of the verbs.

  
a. If you sell your stocks now, you **won’t get** much money for them. (not/get)  
  
b. A lot of people would lose job if the factory **closed down**.(close down)  
  
c. Our country won’t have to export wheat if it **rains** in November and February. (rain)  
  
d. If we **had found** him earlier, we could have saved his life. (find)  
  
e. If he had not been wearing helmet, he **would have been** seriously injured. (be)  
  
f. Unless you follow the instructions, you **won’t** pass the exams. (not/ pass)  
  
g. I don’t mind walking home as long as the weather **is** fine. (be)  
  
h. The bank will sanction you the loan provided you **deposit** a collateral. (deposit)  
  
i. What **would you have done** if you had not got this job? (you/do)  
  
j. If you had the choice, where **would you have lived?** (you/live)  
  

#### C. Change the following sentences into ‘if sentences’ as in the example.

  

#### a. The driver was talking on the phone so the accident happened.

If the driver wasn’t talking on the phone, the accident wouldn’t have happened.  
  

#### b. There is no anyone at home because all the lights are off.

If there was someone at home, all the lights wouldn’t be off.  
  

#### c. He must be an educated person because he has subscribed ‘The Kathmandu Post’

If he wasn’t an educated person, he wouldn’t subscribe ‘The Kathmandu Post’.  
  

#### d. His head was not injured in the accident because he had put the helmet on.

His head would have been injured in the accident if he hadn’t put the helmet on.  
  

#### e. I am sure he passed the exam because he gave a heavy treat to his friends.

If he hadn’t passed the exam he wouldn’t have given a heavy treat to his friends.  
  

#### f. You didn’t take any breakfast so you are hungry now.

If you had taken some breakfast, you wouldn’t have got hungry now.  
  

#### g. I am sure he is a doctor because he is wearing the white gown.

If he wasn’t a doctor, he wouldn’t be wearing the white gown.  
  

#### h. She is very rich so she drives a Mercedes.

If she wasn’t very rich, she wouldn’t drive a Mercedes.  
  

#### i. I didn’t know it was only half a kilometer from my house, so I booked a ride.

If I had known it was only half a kilometer from my house, I wouldn’t have booked a ride.  
  

#### j. He has hidden something in his mind, so he does not look fresh.

If he hadn’t hidden anything in his mind, he would have looked fresh.