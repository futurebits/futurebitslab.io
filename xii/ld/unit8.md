## Unit 8
# Human Culture
### Working with words

  

#### A. Complete the given sentences with the suitable words from the box.

  
a. The annual report has caused acute **embarrassment** to the government.  
b. Ellen has worn high-heels. She is **teetering**.  
c. Look! The poor horse is **dragging** a heavy load.  
d. As they approached the **outskirts** of the city, Ella’s mood visibly lightened.  
e. The parcel was **wrapped** in plain brown paper. It still looks attractive.  
f. Fruits and vegetables grow in **abundance** in the Terai region.  
g. They can abstract precious medicines from **ordinary** substances.  
  

#### C. Add suitable suffix to the root words given and write the parts of speech of each newly formed word.

| Root Word | Suffix | New Words |
| --- | --- | --- |
| Neighbour | hood | Neighbourhood (n.) |
| Comfort | able | Comfortable (adj.) |
| Shop | ing | Shopping (n.) |
| Husk | er  | Husker (n.) |
| Squeamish | ly  | Squeamishly (adv.) |
| Change | ing | Changing (adj.) |
| Shine | ing | Shining (adv.) |
| Colloquial | ly  | Colloquially (adv.) |
| Collect | ion | Collection (n.) |
| Embarrass | ment | Embarrassment (n.) |
| Persuade | ive | Persuasive (adj.) |
| Type | ing | Typing(adj.) |
| Differ | ence | Difference (n.) |
| Ship | ing | Shipping(adj.) |
| Tropic | al  | Tropical (adj.) |
| Drive | ing | Driving (adj.) |
| Reside | ence | Residence (n.) |
| Brother | hood | Brotherhood (n.) |
| Fresh | ness | Freshness (n.) |

  

#### D. Look for the following words related to waste management in an English dictionary. Make sentences of your own using them.

  
**Sewage**  
Sewage often pollutes the ocean.  
  
**Sludge**  
A lot of sludge was scooped out of the base of the river.  
  
**Organic waste**  
Oxygen is required for efficient decomposition of the organic wastes.  
  
**Inorganic waste**  
The inorganic waste is being processed in a new government-owned plant.  
  
**Methane**  
The ignition of methane gas killed eight men.  
  
**Waste reduction**  
The government should focus on waste reduction for pollution free environment.  
  
**Monofil**  
It was caught in a monofilament gill in about 60 feet of water.  
  
**Market waste**  
Proper recycling and management of market waste should be everyone’s first priority.  
  
**Incineration**  
The government is proposing to incinerate cattle carcasses at 22 sites.  
  
**Hazardous waste**  
Regular CFLs are categorized as household hazardous waste.  


### Comprehension

  

#### Answer the following questions.

  

#### a. How does the author describe the Japanese waste management system?

The author describes the Japanese waste management system saying that three nights a week, the residents of his neighborhood deposit their household trash at specified areas on the street corners, wrapped in neat bundles, like gifts, and it disappears at dawn.  
  

#### b. What are the two reasons behind the existence of sodaigomi in Japanese culture?

The two reasons behind the existence of sodaigomi in Japanese culture are: one is the small size of the typical Japanese house and another is the Japanese desire for freshness and purity.  
  

#### c What, according to the author, do the Japanese feel at the thought of buying second hand items?

According to the author, the Japanese feel embarrassment at the thought of buying second hand items.  
  

#### d. How is Malaysian culture different from Japanese culture concerning the used items?

In Malaysian culture no one throws anything away whereas in Japanese culture heaps of clean, new-looking merchandise are thrown on the street. Several customer bids enthusiastically for second handed small to small things in Malaysia whereas shoppers seem to feel embarrassment at the thought of buying second hand items in Japan.  
  

#### e. Why did the author feel awkward at the sodaigomi pile?

The author felt awkward at the sodaigomi pile because he never believed that everything looing perfectly clean, whole, and serviceable is thrown out in Japanese culture.  
  

#### f. How many articles did the author bring to his house one after another?

The author laid in as many provisions as he decently could. At first he grabbed an ordinary low Japanese tea table. He then brought a shiny new bell for one son’s bicycle, a small but attractive wooden cupboard, a complete set of wrenches and screwdrivers in a metal toolbox, a Naugahyde-covered barstool and a lacquer serving tray.  
  

#### g. Why do most people try to find things in trash pile ‘in the dead of the night’?

Most people try to find things in trash pile ‘in the dead of the night’ because everyone can pretend not to notice and they can bring no shame upon their kind.  
  

#### h. How did the author’s family assimilate Japanese culture in using consumer goods?

The author’s family assimilated Japanese culture in using consumer goods by operating in the dead of night to collect the essential materials or goods thrown out in the sodaigomi pile.  
  

### Critical thinking

  

#### a. If you happen to be in Japan someday, will you collect articles from sodaigomi? Why or why not?

Yes, I would have collected articles from sodaigomi if I happen to be in Japan someday. I live in the country where people only buy the new things after it gets old, damaged and unable to function properly. So, living in Japanese culture, following the sodaigomi tradition is not of my kind. It is painful to go from the world to one in which we didn’t have any household goods, couldn’t bring ourselves to buy the overpriced new ones in the store – and then saw heaps of clean, new-looking merchandise just sitting on the street. By picking up items form sodaigomi, I would save a lot of money and would live a good life. So, definitely I will collect articles from sodaigomi instead of going for the new one.  
  

#### b. There are some second hand shops in Nepal, too. But, people are not much interested in them. What practice would be suitable in managing second hand items in Nepal?

Some consumers love buying and selling second-hand items due to cost-effectiveness, as well as ethical and environmental benefits. But many of the peoples if Nepal are not much interested in them. So to encourage second hand shopping in Nepal following things should be considered.  
  
1\. Second hand shops should partner with the brands that sell well, and provide assistance for consignors on what products and what brands are best to consign with.  
  
2\. A realistic price should be kept by starting with the original value of the newly manufactured item, and then reducing it in accordance with wear and tear and extent of usage.  
  
3\. Providing additional support, such as offering consignment consultations with advice on pricing, photographing, and shipping items can encourage more people to buy and sell on these shops.  
  
4\. Second hand stores have vibrant social media followings and it should work toward getting out to as many people as possible.   
  

### Writing

  

#### A. Write a paragraph elaborating the idea of 3Rs (reduce, reuse and recycle) in garbage management.

3Rs – Reduce, Reuse & Recycle

The principle of reducing waste, reusing and recycling resources and products is often called the “3Rs.” Basically, it is a sequence of steps on how to manage waste properly. Reducing means choosing to use things with care to reduce the amount of waste generated. Reusing involves the repeated use of items or parts of items which still have usable aspects. Recycling means the use of waste itself as resources. The three R’s all help to cut down on the amount of garbage we throw away. They conserve natural resources, landfill space and energy. Plus, the three R’s save land and money communities must use to dispose of waste in landfills. Siting a new landfill has become difficult and more expensive due to environmental regulations and public opposition.  
  

#### B. Garbage management is a big problem in most of the cities in Nepal. Write a letter to the editor to be published in the daily newspaper suggesting the ways of ‘Solving Garbage Problems.’

12/24 Amritnagar Tole,  
Kalanki, Kathmandu  
26th Jan, 2021  
  
To the Editor,  
The Kathmandu Post  

Sub : Regarding the news coverage about ‘Solving Garbage Problems’.

Dear Sir,  
I would be grateful if you allow a little space in your widely circulated and popular newspaper. In order to draw the attention of the general public as well as the concerning authority of the government to the problem of garbage management in most of the cities in Nepal, I am writing you this. We know that we have a serious garbage problem. It is clear that there will be no value from waste, as energy or material, if it is not segregated. But this is where our waste management system stops short.  
  
The problem of waste is something that needs to be handled on a smaller scale and aggregated to solve the bigger problem. Waste-pickers already contribute greatly to solving this problem. We need to tap into their capacity. An IT platform like I Got Garbage can build business models for wastepickers. Instead of picking and dumping garbage from each house, the government can provide subsidy and give every home a composter. Landfill sites can never be a solution to solid waste in cities as it is an outdated idea. Reduce, Reuse and Recycle are the most common methods to reduce landfill waste. 80 percent of organic waste littering the streets can be used if the government cooperates with pig farmers in the respective cities. I wonder why the authorities are not adopting this method. As long as governments try cleaning the city by doing the same things they always did and say they will do it better this time, the city won’t become cleaner. We need something dramatically different.  
  
I hope the concerning authorities will make appropriate steps immediately.  
  
Looking forward to seeing an article in this regard in your upcoming publication.  
  
Faithfully yours,  
Subarna P.  
  

### Grammar

  

#### B. Use the words from the brackets to complete the sentences.

  
a. This house is very small. I want to buy a **much bigger** one. (much / big)  
  
b. I liked the magic show. It was **far more exciting** than I’d expected. (far / exciting)  
  
c. It was very cold yesterday. It’s **a bit warmer** today. (a bit / warm)  
  
d. The warmer the weather the **better** I feel. (good)  
  
e. An average American earns **considerably higher** than an average Nepali. (considerably / high)  
  
f. Health care in Nepal is not as **expensive** as it is in the US. (expensive)  
  
g. I think the problem is **far more complicated** than it seems. (far / complicated)  
  
h. You are driving very fast. Would you please drive **a bit slowly**? (a bit/ slowly)  
  
i. Your handwriting is not legible. Can you write **a bit neat**? (a bit/ neat)  
  

#### C. Rewrite the following sentences with the sentence beginnings given below.

  
a. Kabir is less intelligent than he pretends. He is not as **intelligent as he pretends.**  
  
b. I am busy today but I was busier yesterday. I’m not **as busier as I was yesterday.**  
  
c. Hari has lived in Kathmandu for 10 years but Bikram for 20 years. Bikram has **lived in Kathmandu 10 years more than Hari.**  
  
d. I used to study 12 hours a day but nowadays I study only 5 hours a day. I don’t **study as much as I used to do.**  
  
e. It’s a very good room in our hotel. In fact, it’s the **best compared to other.**  
  
f. He earns 30 thousand rupees a month but spends 40 thousand. He spends **more than he earns.**  
  
g. There is no other mountain higher than Mt. Everest in the world. Mt. Everest is the **highest mountain in the world.**  
  
h. The place was nearer than I thought. It was not as **far as I thought.**  
  
i. Bharat can play better than Mohan. Mohan can’t **play as good as Bharat.**