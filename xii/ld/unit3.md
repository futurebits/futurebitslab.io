## Unit 3
# Sports
### Working with words

  

#### A. Many English words are from other languages, such as redemption in the news above comes from the Latin word redimere, a combination of re(d)-, meaning “back,” and emere, meaning “buy.”  
  
Now, find out the origin and the meaning of the following words from the text.

  
**Penalty** : From Middle French pénalité  
a punishment imposed for breaking a law, rule, or contract  
  
**Major** : From Latin word “great” and French word “majeur”  
important, serious, or significant.  
  
**Stadium** : From Latin word meaning “study”, “zeal”, “dedication”  
A center for the study of the Liberal Arts  
  
**Trophy** : From the French trophée in 1513, Old French trophee, from Latin trophaeum  
A cup or other decorative object awarded as a prize for a victory or success.  
  
**Defender** : From the Latin word in this sense was “defensor” and Old French “defendeor”  
One who protects from injury a champion  
  
**Anthem** : From Greek antiphōnos  
A rousing or uplifting song identified with a particular group, body, or cause.  
  
**Dejection** : From the Latin deicere meaning “throw down.”  
A sad and depressed state; low spirits.  
  
**Jubilant** : From Latin word “jubilare”  
Making a joyful noise  
  
**Record** : Form Latin word “cor” and “re-”  
A thing constituting a piece of evidence about the past, especially an account kept in writing or some other permanent form.  
  
**Suave** : Form Latin word ” suavis” means “agreeable.”  
Charming, confident, and elegant  
  
**Podium** : Form Greek word “pous and pod” meaning “foot”  
finish first, second, or third, so as to appear on a podium to receive an award.  
  
**Tournament** : Form old French torneiement  
A series of contests between a number of competitors, competing for an overall prize.  
  
**Reminiscent** : Form Latin word “reminisce” meaning remembering  
Tending to remind one of something.  
  
**Incredible** : Form Latin word “In-” meaning “not” and “credibills” meaning is “believe.”  
  
**Savor** : Form Latin word “sapere” meaning “to taste”  
  

#### B. Consult a dictionary and define the following terms related to cricket.

  
**Stump:**  
Each of the three upright pieces of wood which form a wicket.  
  
**Crease:**  
Any of a number of lines marked on the pitch at specified places.  
  
**Boundary:**  
A hit crossing the limits of the field, scoring four or six runs.  
  
**Sixer:**  
A hit for 6 runs.  
  
**Googly:**  
Type of deceptive delivery bowled by a right-arm leg-spin bowler.  
  
**Leg-by:**  
A run scored from a ball that has touched part of the batsman’s body (apart from the hand) without touching the bat.  
  
**Wicket:**  
Each of the sets of three stumps with two bails across the top at either end of the pitch  
  
**Maiden:**  
An over in which no runs are scored.  
  
**Pitch:**  
The strip of ground between the two sets of stumps.  
  
**Power-play:**  
The set of overs with special fielding rules during a limited-overs cricket match  
  
**No-ball:**  
An unlawfully delivered ball, counting one as an extra to the batting side if not otherwise scored from.  
  
**Yorker:**  
A ball bowled that hits the cricket pitch around the batsman’s feet.  
  

### C. Pronunciation

#### Divide the following words into two groups in accordance with their pronunciation /aɪ/ and

  
**/aɪ/**  
Fine, shine, why, height, bright, might, five, sight, white, by  
  
**/eɪ/**  
Rein, rail, sleigh, snail, break, fake, eight, game, claim, friend, gait, hail, frame  
  

  

### Comprehension

  

#### A. State whether the following sentences are True or False or Not Given.

  
a. England was the champion of 1996 World Cup Football. – **True**  
b. Italy bagged its first Euro Cup trophy in the Euro Cup 2020. – **True**  
c. The English team has won more trophies in International Football than Italy. – **False**  
d. According to the captain of the English team, the pain of losing a match has lasting effects. – **Not Given**  
e. The Italian team was playing in their home ground. – **False**  
f. The goalkeeper of the Italian team had performed the best than any other players in the match. – **True**  
  

#### B. Answer the following questions.

  

#### a. Why does the reporter say that England is waiting to heal its half-century long pain?

The reporter said that England is waiting to heal its half-century pain because England had not won any title after 1966 A.D.  
  

#### b. How did the Italian players react as soon as they became the champions?

The Italian players reacted by shouting into a TV camera amid the celebrations, mocking the famous lyric “it’s coming home” from the England team’s anthem as soon as they became the champions.  
  

#### c. Why did Saka and Sancho cry?

Saka and Sancho cried because of their third straight penalty failure.  
  

#### d. Penalty shoot-out has long been a bitter experience for the English team. Why?

Penalty shoot-out has long been a bitter experience for the English team because they missed three penalty shoot-outs consequently.  
  

#### e. State the contribution of Roberto Mancini to the Italian football.

Less than four years ago, Italy fell to the lowest point in its footballing history with failing to qualify for the World Cup for the first time in six decades. They are now the best team in Europe and with a national record of 34 games unbeatenunder Roberto Mancini. Mancini brought great confidence to the Italian football team and went all out to win the Euro 2020.  
  

#### f. How does the Euro Cup final 2020 remind the audiences of the 2018 World Cup semifinals? How?

The Euro Cup Final 2020 remind the audiences of the 2018 World Cup semifinals because England also scored early against Croatia then spent most of the game chasing its opponent’s midfield before losing in extra time.  
  

### Critical thinking

  

#### a. “Every match is a new opportunity. Put its failure behind and start over again.” Does this apply to the Italian football team when we analyse their performance from their failure to qualify for the World Cup Football to their victory in the European Championship in these four years?

Yes, the statement “Every match is a new opportunity. Put its failure behind and start over again” applies to the Italian football team. Italy is constantly improving and investing more and more in the improvement of its football team. Despite having faced so many challenges, digesting so many defeats, we can say that they have continued to improve and now the football culture in Italy is second to none.  
  
There is no place in the world where a typical technician is more qualified than another. Every casual Italian staff member is excellent in rehabilitation, strategy and organization. Wherever they go, they have a manager who can lead a top club to success. They have an upscale nightclub. Juventus has a rich history, well-organized organization and a management team that is able to bring out the best in every player they sign. Few clubs reach this level of competence, which should be the standard. Think of the three biggest clubs in Europe: Bayern Munich, Real Madrid and Manchester City.  
  
In the last four years Italy has undergoneradical changes under Mancini. The Azzurri won 4-2 on penalties after taking the lead with Federico Chiesa, but came back from a draw by Alvaro Morata, who scored 1-1 goals and needed added time before the Italians won by 4-2 on penalties. From this we can say that Italian football team constantly learned from their past games and hence qualified for the World Cup Football to their victory in the European Championship in these four years.  
  

#### b. When a team plays in a home ground, it gets a huge support from the audience in the stadium. Does this support them to win the match or the players may feel pressure to win and thus lose? What do you think?

I think a team playing in the home ground gets huge support and confidence from their home fans but the teams from other places get huge disadvantages; according to their zone, climate, and fan follower. Many teams have been won Match on home ground. More than 100 teams won the match on the home ground.  
  
In team sports, the term home ground – also called home field describes the benefit that the home team is said to gain over the visiting team. This benefit has been attributed to psychological effects by the supporting fans on the competitors or referees. Also, the players are used to, playing on their home ground due to which they can easily play there, despite of the weather, pitch or ground condition. But the visiting teams suffer from changing time zones or climates, or from the rigors of travel; and in some sports, to specific rules that favor the home team directly or indirectly.  
  
According to my views, in Euro Cup 2020 England lost the final match against Italy due to their fear and lack of faith. They had not won any match after 1966 A.D so that they had less hope and got more pressure from the home ground fans. Therefore, getting a huge support from the audience in the stadium does both i.e., supports the team to win the match as well as the players may feel pressure to win and thus lose.  
  

### Writing

  

#### A. The following passage does not have any punctuation marks. Punctuate it with appropriate punctuation marks.

A lot of people try to get away from home for a few days each year for a holiday. There are lots of things to choose from, and where you go depends on how much money you’ve got to spend. Ideas for holidays include relaxing on a beach, exploring cities, and skiing. One man wanted to go to Australia but thought he’d never have enough money to get there. He’d go to his brotherâ€™s caravan at the seaside instead. A young woman wanted to go clubbing in Ibiza as she’d heard it was a lot of fun.  
  

### Grammar

  

#### B. Complete the following sentences with much, many, few or little.

  
a. He is an introvert. He has got very **few** friends.  
b. I am busy in preparing my examinations. I have very **little** time to give to you.  
c. The entire winter season was dry this year. We had **little** rain.  
d. Our town has almost been modernized. There are **few** old buildings left.  
e. You can come today. I haven’t got **much** to do.  
f. The party was crowded. There were too **many** people.  
g. How **many** photographs did you take while you were in Switzerland?  
h. There was **little** traffic so I came in time.  
i. Can I borrow **few** books from you?  
j. Mohan can’t be a good teacher. He has **little** patience.  
  

#### C. Put each or every in the following sentences.

  
a. The party split into three factions, **each** faction headed by a former prime minister.  
b. Leap years occur **every** four years.  
c. **Every** parent worries about their children.  
d. We had a great time in Singapore. We enjoyed **every** minute of our time.  
e. I could catch the main idea of his speech but I didn’t understand **each** of his words.  
f. In Nepal, **every** motorcycle rider should wear a helmet.  
g. You must read **each** of these books for the exam.  
  

#### D. Rewrite the following sentences using all of, most of, none of, both of or some of.

  
a. Your garden is superb. **Most of** the flowers are beautiful.  
b. Do you know Bharat and Kamal? Of course, I do. **Both of** them are my friends.  
c. I bought a box of apples thinking it would be cheaper but I was mistaken. **All of** them were rotten.  
d. When I was in the town, I asked some people for direction but **none of** them were able to help me. I had to call my friend.  
e. We all were soaked in the rain because **none of** us had carried an umbrella.  
f. My father is healthy in his seventies but he feels isolated because **all of** his friends are dead now.  
g. All of the tourists are not Chinese. **Some of** them are Korean too.  
  

#### E. Choose the best word from the brackets to complete the sentences.

  
a. Give me **the** money I owe you. (which/the/a/an)  
b. I want **the** boat which would take me to the island. (the/an/those/a)  
c. Could you pour me **some** water, please? (many/few/these/some)  
d. Only **a few** employees know how important the project was. (a few/a little/little/few)  
e. He was looking for **an** umbrella. (an/a/those/these)  
f. **A large amount of** water was evaporated due to excessive heat. (A large number of/ A lot of/ A large amount of/Many)