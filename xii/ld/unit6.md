## Unit 6
# Money and Economy
### Working with words

  

#### A. Match the words in column ‘A’ with their meanings in column ‘B’.

  
**a. prominent**  
vi. standing out so as to be seen easily, conspicuous  
  
**b. vulnerable**  
iv. exposed to the possibility of being attacked / harmed  
  
**c. potentially**  
viii. with the capacity to develop or happen in the future  
  
**d. transaction**  
i. an instance of buying or selling of something  
  
**e. initiatives**  
ii. the power or opportunity to do something before others do  
  
**f. launched**  
iii. to introduce a new plan or product  
  
**g. enduring**  
ix. lasting over a period of time; durable  
  
**h. robust**  
v. strong and unlikely to break or fall  
  

#### B. There are different abbreviations used in the text. With the help of the internet, find their full forms.

  
**QR** : Quick Response  
**ISO** : International Organization for Standardization  
**IEC** : International Electrotechnical Commission  
**URLs** : Uniform Resource Locators  
**EMVCo** : Europay, Mastercard, and Visa Company  
**PIN** : Personal Identification Number  
  

#### C. Pronounce the following words and identify the vowel sounds /ʊ/ and /u:/. You can take help from a dictionary.

  
**/ʊ/**  
put, push, hood, full, bull, book, foot, look, hook, cook, should, soot, stood  
  
**/u:/**  
boom, fool, food, loose, boost, groom, moon, soon, room, soothe,  
  



### Comprehension

  

#### A. Decide whether these statements are True or False. Write NOT GIVEN if you do not find the information.

  
a. The system of QR codes was first launched in South Asia. **True**  
b. The standards of payments via the QR codes were approved by EMVCo. **True**  
c. All sorts of businesses are aided by the QR code payments. **False**  
d. There are several models to the QR code payment service. **True**  
e. In countries like Nepal, the QR code paying system is a complete fiasco. **False**  
f. The QR codes payment service is not applicable to small business. **False**  
  

#### B. Answer the following questions.

  

#### a. How can one pay with QR codes paying system?

One can pay with QR codes paying system either by presenting a QR code for scanning to the merchant, or by scanning at the merchant’s QR code.  
  

#### b. How did Tencent and Alibaba companies utilise the QR code services at the beginning?

At the beginning, Tencent utilized the QR code services in its social media WeChat for friending and Alibaba utilized to facilate Alipay payment services.  
  

#### c. How do the QR code based payment services launched by Visa and Mastercard mitigate payment problems?

The QR code based payment services launched by Visa and Mastercard mitigate payment problems by linking QR code payments to debit and credit card accounts.  
  

#### d. Why do we need a scheme operator to run the QR code based payment system?

We need a scheme operator for scheme branding at acceptance points, defining the scheme rules and providing a mechanism for handling disputes and exceptions.  
  

#### e. What basic requirements are needed to use the QR codes services?

A suitable, secured scheme operator is required to use the QR code services.  
  

#### f. How can security concerns related to payment via QR codes be addressed?

Security concerns related to payment via QR codes can be addressed by the use of merchant IDs, registration and real-time notification of payment.  
  

#### g. Who should be more careful: customer or the merchant in terms of payment issues? Why?

A customer should be more careful in terms of payment issues because it would be better to stop the fraud happening in the first place rather than trying to correct it later by the merchant.  
  

#### h. Do you think the QR code based payment can be a panacea for all sorts of payment problems? Why?

Yes, I think the QR code based payment can be a panacea for all sorts of payment problems because it is fast, secure and easy to create.  
  

### Critical thinking

  

#### a. Some business houses, shops and department stores in city areas have started to adopt QR code payment systems in Nepal, too. What should be done to make it more accessible? Discuss.

The Covid-19 pandemic has immensely boosted the Scan and Pay service through the QR code in Nepal too. People being concerned with their hygiene are supporting the contactless payment system. Evolving digital payment service providers like Fonepay, IMEPay, Khalti, QPay, and SmartQR also encourage QR code payment by offering their users Scan and Pay services.  
  
QR code payment systems can play an impactful role in changing informal sectors into the formal economy. However, Nepal needs to tackle challenges in many infrastructural and behavioural traits before reaching the full potential of e-payment. The lack of interoperability is another major issue for a stable digital payment service. Rapid growth in several PSOs and PSPs may result in an imbalance in the ecosystem of the digital economy. To avoid this hurdle, a unified payment switch is necessary.  
  
QR code payment can be a vital factor in uplifting Nepal’s digital economy. The government giving more priority to digital payment systems and the central bank to promote this system can be a head start for QR code payment.  
  
Mass awareness campaigns and promotional events can also influence this transition. Infrastructural development like high-speed internet connection and wider user accessibility must be considered. Payment service providers, payment service operators, bank and financial institutes, and traders should work together to bring interoperability to their digital payment services.  
  

#### b. The payments landscape is shifting gears from cash to digital mode. Digital payment brings ease and convenience to the consumer. Is it possible to apply such cashless methods in payments in Nepal? Discuss.

It’s hard but not impossible to apply Digital payment system in Nepal. Nepal needs to tackle challenges in many infrastructural and behavioural traits before reaching the full potential of e-payment. The number of internet users is impressively growing but still not enough to process an established digital economy.  
  
QR code payment systems can play an impactful role in changing informal sectors into the formal economy. The government giving more priority to digital payment systems and the central bank to promote this system can be a head start for QR code payment.  
  
Mass awareness campaigns and promotional events can also influence this transition. Infrastructural development like high-speed internet connection and wider user accessibility must be considered. Payment service providers, payment service operators, bank and financial institutes, and traders should work together to bring interoperability to their digital payment services.  
  
The ultimate goal should be offering a one-stop solution for the consumers to access all sorts of e-payment services, including QR code payment.  
  

### Writing

  

#### A. Punctuate this paragraph with appropriate punctuation marks.

It was a cold freezing day, it had been snowing all day in New York. Mr. Tim cooked and I went outside to play in the snow. We had not seen much snow since we went skiing in Sweden last year. Mrs. Smith was right because she had said that we’d see snow. The next day we had really a wonderful time there, didn’t we?  
  

#### B. Write a news article about digital payment systems in Nepal.

Digital Payment Systems in Nepal

Digital payment is gaining momentum due to social distancing during the Covid-19 pandemic. The evolution of digital payment platforms and increasing consumers’ preference towards e-payment secures a bright future for online payment in Nepal.  
  
**What is Digital Payment?**  
Digital Payment is definitely growing in popularity and is becoming a trend these days. The reason is simple – convenience. In the era when businesses are shifting online, why not transactions!  
  
**Major Use of Digital Payment System in Nepal**  
You can find QR codes in various good and service sectors like departmental stores and small shops, and many banks and financial institutes (BFIs). You can make payments in supermarkets via a QR code scanner using a digital wallet. Local vendors and shops are also using QR codes for quick payment. Fruits and vegetable markets also feature digital payments for grocery shopping.  
According to a report by the Nepal Rastra Bank, the number of digital transactions from mid-December 2020 to mid-January 2021 was 372,176. Similarly, the transactional amount was Rs 1.245 billion. The next month, the number of transactions increased to 535,790 and the amount to Rs 1.712 billion, suggesting a clear rise in the use of digital payment.  
  
**Future and Challenges**  
Switching to any e-payment services like QR code will be a long-running process. PSOs like eSewa have been offering QR payment services without charging any additional fee during transactions to encourage such platforms. This generation has access to different media that can raise awareness of digital payment significance, so the numbers of users is expected to grow in the future rapidly but still not enough to process an established digital economy. The lack of interoperability is another major issue for a stable digital payment service.  
  
**Conclusion**  
QR code payment can be a vital factor in uplifting Nepal’s digital economy. The government giving more priority to digital payment systems and the central bank to promote this system can be a head start for QR code payment. The ultimate goal should be offering a one-stop solution for the consumers to access all sorts of e-payment services, including QR code payment.  
  

### Grammar

  

#### A. Look at the following questions and say what type of questions they are.

  

#### a. Do you like this country?

Yes/No Question  
  

#### b. Where is she from?

Wh Question  
  

#### c. How many eggs do we need for this cake?

Wh Question  
  

#### d. Whose children are playing in the yard?

Wh Question  
  

#### e. Does she like ice cream or sweets?

Choice Question  
  

#### f. She sent him an invitation, didn’t she?

Tag Question  
  

#### g. Could you tell me if the doctor is available?

Indirect Question  
  

#### h. Do you know how tall they are?

Yes/No Question  
  

#### B. Choose the correct words from the box to complete the sentences.

  
a. **Is** your friend a scientist? – No, he’s an artist.  
b. **Does** Naresh live in Kathmandu? – No, he lives in Pokhara.  
c. When **did** you get home? – I got home yesterday.  
d. What time **do** you get up? – I get up at 6:00.  
e. **Did** the children go to the part? – Yes, they went there after school.  
f. Are **you** going to school? – No, I’m going home.  
g. **Can** you speak Chinese? – Just a little.  
h. **Where** did you grow up? – I grew up in Okhaldhunga.  
  

#### C. Make wh-questions so that the words in bold become the answer.

  

#### a. Romeo loves Juliet.

Whom does Romeo love?  
  

#### b. My mother made a delicious bread yesterday.

What did your mother make yesterday?  
  

#### c. The music was composed by Narayan Gopal.

Who composed the music?  
  

#### d. I’m looking for a new book.

What are you looking for?  
  

#### e. They were talking about the new movie.

What were they talking about?  
  

#### f. She got the idea from a story.

Where did she get the idea?  
  

#### g. She always goes to school on foot.

How does she go to school?  
  

#### h. She sometimes goes to the cinema.

How often does she go to the cinema?  
  

#### i. They have been waiting for three years.

How long have they been waiting for?  
  

#### D. Change these indirect questions into direct ones.

  

#### a. Could you tell me where the Market Street is?

Where is Market Street?  
  

#### b. I’m longing to know what time the bank opens.

What time does the bank open?  
  

#### c. Do you have any idea how he’s managed to get in shape so quickly?

How has he managed to get in shape so quickly?  
  

#### d. I’d like to know how much this motorcycle costs these days.

How much does this motorcycle cost these days?  
  

#### e. Did you notice if he had left the car in the park?

Had he left the car in the park?  
  

#### f. Have you found out if the train has left?

Has the train left?  
  

#### g. I was wondering if they speak English well.

Could they speak English well?  
  

#### h. Would you tell us how we can get to the post office from here?

How can we get to the post office from here?  
  

#### i. Do you remember if I locked the front door?

Did I lock the front door?