## Unit 4
# Technology
### Working with words

  

#### A. Choose the correct words from the box to complete the following sentences.

  
a. I think that covering up the facts is **equivalent** to lying really.  
  
b. If there is not a substantial move to public transport, we will have **deceleration** and the whole regeneration will not work.  
  
c. Each new leader would blame his **predecessor** for all the evils of the past.  
  
d. We have, in fact, **envisioned** a better world and have made it happen.  
  
e. The main linear actuators of the **pneumatic** systems are cylinders.  
  
f. The barrel was short and the bullet emerged at **subsonic** speed.  
  
g. The city is said to receive two-fifths of the total **freight** delivered in the country.  
  
h. It would still take four hours to get down, in a spiral of **gridlock**.  
  
i. Apparently, the magician will be doing some **levitation** on the stage tomorrow.  
  
j. We might be experiencing some **turbulence** on this flight due to an approaching electrical storm.  
  

#### B. Add three more words that are formed with the following prefixes.

  

#### a. hyper

hyperloop, hypersensitive, hyperlipidaemia, hyperinflation.  
  

#### b. ultra

ultrahigh, ultralight, ultrashort, ultrabasic, ultrafiche.  
  

#### c. up

uphill, Upwork, upside, uplift  
  

#### d. over

overcoming, overwhelm, overdrunk, oversized  
  

#### e. multi

multibillion, multinational, multicultural, multiply  
  



### Comprehension

  

#### Answer the following questions.

  

#### a. What is a hyperloop? How does it work?

Hyperloop is a planned train that would be capable of travelling at incredible speeds. It’s superfast because it’s enclosed in a tube with no air in it. As a result of the lack of air, there will be no friction to slow the train down.  
  

#### b. How is hyperloop more beneficial than the traditional trains?

Hyperloop is much beneficial and superior than any traditional or a high-speed rail. It’s less expensive and uses less energy since the track doesn’t have to provide electricity to the pods constantly and because the pods may depart every 30 seconds, it’s more like a demand-based service.  
  

#### c. Does hyperloop have a successful history? How?

No, it does not have a successful history since the original concept for hyperloop was to use waggons and pneumatic tubes to transport mail and parcels between buildings.  
  

#### d. Write the contributions of Robert Goddard and Elon Musk for the development of hyperloop.

In order to provide ultra-fast intercity transport, Space X and Tesla founder Elon Musk created the hyperloop idea. In addition to automobiles, aircraft, boats, and trains, Elon envisions a “fifth mode” of transportation that utilises travel pods housed within steel tubes. He revived interest in the idea of hyperloop in August 2013 with his ‘Hyperloop Alpha’ paper.  
  
The ‘vactrain’ idea, created by Robert Goddard in the early 20th century, is clearly a prototype of the hyperloop. Since then, a slew of similar concepts have been proposed.  
  

#### e. What relation does speed have with air resistance? Explain.

More Air resistance decreases the speed of a moving body and vice versa. Since, Hyperloop will travel through the vacuum tubes, the speed will drastically increase as the result of no-air resistance.  
  

#### f. What are hyperloop capsules compared with? How are they similar?

Because both Hyperloop and Boeing 747 accelerate at the same measure of acceleration, Hyperloop capsules are likened to Boeing 747.  
  

#### g. How do the passengers feel while travelling via hyperloop? What will be done to make it luxurious?

On a hyperloop, the passengers feel approximately the same as riding a lift or a passenger aircraft. The cabin shows beautiful scenery and each passenger has access to his or her own private amusement systems, to let them have a easeful travel.  
  

#### h. Why does the writer doubt about the success of hyperloop? What does the success depend on?

Hyperloop’s success will be determined on the destinations, the local economy, and the geographical location. The author has concerns regarding hyperloop’s viability because of the enormous multibillion-dollar expenditure and the fact that the projects are still in the pilot and experimental phases. In the past, Nikola Tesla and Elon Musk had already closed the same concept program, due to no response from government authorities.  
  

### Critical thinking

  

#### a. Is the hyperloop the future of transportation or just a dream? What do you think? Justify your opinion with suitable reasons.

Basically, Hyperloop is a conceptual transport system in which passengers are loaded into pods and fired through vacuum tubes all at a supersonic speed. Hyperloop isn’t just about speed; it also aims to be environmentally friendly.  
  
The concept just like of Hyperloop is not new though. Pneumatic tubes for the parcel, mail, and money transfers found widespread acceptance in the 19th century and are still used at supermarkets, postal sorting stations, and banks today. Hyperloop’s concept resembles the pneumatic mail delivery systems. Even flying on the air seemed to be impossible on past days but now travelling via air is common for us. So no one can say that it’s not possible because everything which was a dream once, is reality now.  
  
In conclusion, we can see that Hyperloop may become the future of transportation systems worldwide. If this becomes a reality, even though it is not expected for decades, there will be an immense impact on ordinary people’s lives. Apart from the convenience and the green impact, we can also hope for economic success.  
  
However, despite the immense promise, new technologies take time, and will be a while until we can travel in a pod inside a sealed tube with a sound only as loud as a gentle ‘SWOOSH.’  
  

#### b. The number of private vehicles is increasing day by day in Nepal beyond the capacity of our infrastructure. What do you think should be done to curb the ever-growing number of private vehicles? Discuss.

The number of private vehicles is increasing day by day in Nepal beyond the capacity of our infrastructure and is outpacing the country’s infrastructure. The number of Nepalese who own personal cars is steadily increasing. Despite the fact that it eases their day to day life, its causing great environmental loses. Private vehicles should be prohibited since their widespread usage degrades the environment and increases emissions of hazardous gases that will create major problems in the future. To control this critical situation some safety measures had to be taken.  
  
The best approach is to discourage people from opting to buy cars. This can be effectively done by increasing toll fare for cars and by devising a system of progressive taxation on cars. The system can have an upper limit on the number of cars owned per family and the rate of tax on each car can be increased as the number of cars bought keep rising.  
  
But these efforts would be meaningless unless they are accompanied by measures to encourage people to use public transport by revamping the dismal public transit system in Nepal. More buses need to ply during rush hour and the buses must be made more comfortable (by using better cushions, air conditioning, better suspension, etc…). Punctuality and reliability of public transport systems must be upheld. Cleanliness and hygiene must be kept in mind while maintaining bus and train stations.  
  
The above approaches must be complemented with efforts to change the false notion the using public transport is contemptible. The best weapon to deal with this is to make public transport as dignified and pleasant as possible.  
  

### Grammar

  

#### B. Rewrite the following sentences with the correct form of the verbs in the brackets.

  

#### a. The invitation is for one person. I don’t mind whether you or she (come) to the party.

The invitation is for one person. I don’t mind whether you or she comes to the party.  
  

#### b. Neither the MPs nor the Prime Minister (have) felt regret for the party split.

Neither the MPs nor the Prime Minister has felt regret for the party split.  
  

#### c. I don’t care whether he or she (win) the lottery.

I don’t care whether he or she wins the lottery.  
  

#### d. Either the Kantipur or the Republica (be) used for the advertisement.

Either the Kantipur or the Republica is used for the advertisement.  
  

#### e. She speaks in a strange accent. Neither I nor my sister (understand) her.

She speaks in a strange accent. Neither I nor my sister understands her.  
  

#### f. I forgot whether the singers or the actress (be) given the Film Fair Award last year.

I forgot whether the singers or the actress was given the Film Fair Award last year.  
  

#### g. Neither the tracksuit nor the pajamas (fit) me perfectly.

Neither the tracksuit nor the pajamas fit me perfectly.  
  

#### h. Neither the gas fire nor the electric heaters (be) suitable for room heating.

Neither the gas fire nor the electric heaters are suitable for room heating.  
  

#### C. This passage contains the agreement errors. Correct the subjects or verbs that don’t agree with each other. Remember to use present tense in your corrections.

  
**Corrected Passage:**  
Within the state of Arizona, Rob, along with his family, moves frequently from city to city. After his arrival, one of his first tasks was to find an apartment close to work, as he did not have a car. Usually, there are many different places to choose from, and he considers cost, location, and luxury. If one apartment has a washing machine and dryer and costs four hundred dollars a month, he prefers to rent it over another apartment which has significantly less rent and is located two blocks from a Laundromat. Rob’s family never wanted to live in an apartment on the thirteenth floor, since all of them fear heights. He also tried to choose an apartment with a landlord recommended by former tenants. Everybody knows that it is important to find a responsible landlord. Rob and his wife love to cook together when both are free, so they need a spacious, well-equipped kitchen. Rob often looks for a place with an air conditioner because there are so many scorching days and nights in Arizona. Whenever Rob finds a new apartment, all of his concerns disappear. He felt relieved and called his mother. Someone understands!