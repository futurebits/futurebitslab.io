## Unit 20
# Power and Politics
### Working with words

  

#### A. Find the words from the text for these meanings. The first letters have been given for you.

  
a. **reluctance** unwillingness or disinclination to do something  
b. **repressive** preventing the expression or awareness of thoughts or desires  
c. **mutilation** the infliction of serious damage on something  
d. **ghettoized** restricted to an isolated or segregated place, group, or situation  
e. **history feminist** history viewed from a female or specifically feminist perspective  
f. **genital** relating to the human or animal reproductive organs  
g. **hysterectomies** surgical operations to remove all or part of the uterus  
h. **paranoid** obsessively anxious, suspicious, or mistrustful  
i. **eventuation** thrusting forward of abdominal organs through the abdominal wall  
  

#### B. Complete the chart.

  

| Verb | Noun | Adjective |
| --- | --- | --- |
| commit | commitment | committed |
| nurture | nurturance | nurturable |
| legitimatee | legitimization | legitimize |
| oppress | oppression | oppressive |
| defense | defensiveness | defensive |
| victimize | victimization | victimized |

  

#### C. Consult a dictionary and define these terms.

  
**Racism :** the belief that different races possess distinct characteristics, abilities, or qualities, especially so as to distinguish them as inferior or superior to one another.  
  
**Lesbian :** relating to gay women or to homosexuality in women.  
  
**Radical :** forming an inherent or fundamental part of the nature of someone or something.  
  
**Feminist :** the advocacy of women’s rights on the ground of the equality of the sexes.  
  
**Patriarchy :** a system of society or government in which men hold the power and women are largely excluded from it.  
  

#### D. Underline the consonant clusters in the following sentences.

  
a. Fluffy went flip-flop all over the floor.  
b. The whites sing a song about the striking stars.  
c. Here are some flowers and a flask of tea of your favourite flavour.  
d. Smita fell ill with flu.  
e. My friend is frightened of frogs.  
f. Thumbelina stumbled out of the green bush.  
g. She heard a rumble and a cry.  
h. ‘Climb up’, said Tom.  
i. Freedom needs to be strived for.  
j. Children were munching crunchy French fries.  
  



### Comprehension

  

#### Answer the following questions.

  

#### a. Why does Audre Lorde think that Mary got a sort of victory in the University of Boston?

Audre Lorde thought that Mary got a sort of victory in the University of Boston because she becomes so glad to find many women attending the speak out, and the show of joined power will provide more space for them to grow and be within.  
  

#### b. Why is Lorde thankful to Mary?

Lorde is thankful to Mary because Mary has provided her a book named Gyn/Ecology that is full of import, useful, generative, and provoking. The book is strengthening and helpful for her.  
  

#### c. What impression has the writer had about the way white women looked upon black women?

The writer has had a very bad impression about the way white women looked upon black women. According to her, the white women were unable to hear Black women’s words or to maintain dialogue with the black women. The act of white women is long and discouraging for her.  
  

#### d. Why did Mary not cast black women as goddesses in her book according to Lorde?

According to Lorde, Mary didn’t cast black women as goddesses in her book because she had made a conscious decision to limit her scope and deal only with the ecology of Western European women.  
  

#### e. Why did Lorde think that she misused her words?

Lorde thought that she misused her words because she utilized her words only to testify against Lorde as a woman of colour.  
  

#### f. For Lorde, how were women, especially from the black community, undervalued?

For Lorde, women, especially from the black community were undervalued as they were ghettoized by a white woman dealing only out of a patriarchal western European frame of reference.  
  

#### g. How does Mary take white women as? Does Lorde agree with Mary’s view?

Mary takes white women as superior to non-white women. Her assumption is that the herstory and the myth of white women is the legitimate and sole herstory and myth of all women to call upon for power and background, and that nonwhite women and their herstories are noteworthy only as decorations, or examples of female victimization.  
No, Lorde does not agree with Mary’s view. Lorde does not liked the supremacy of white feminism over black feminism shown by Mary in her views presenting blacks as decorative and useful things and ignoring their pain and oppression.  
  

#### h. What is Afrekete? How does it connect to this lesson?

Afrekete is the collective voice to the tradition of black lesbian writing. It connects to this lesson as the author wants to connect all the black women in the arena of black writing.  
  

### Critical thinking

  

#### a. “The oppression of women knows no ethnic or racial boundaries.” Do you agree or not? Justify with your reasons.

“The oppression of women knows no ethnic or racial boundaries.” Yes, I agree with this statement.  
  
Women have fought to be treated equal for many years and deserve same pay as men. Men receive better treatment, jobs, and better pay than women, which shows discrimination towards them. Women continue to fight against the discrimination worldwide. The oppression of women getting defined equality creates a form of intolerance. Women not having the rights they deserve can lead to uncertainty within oneself.  
  
In history, women did not have equality rights which connects to why some still face sexist discrimination against men. Despite the tremendous progress made in the struggle for gender equality, women still face violence, discrimination, and institutional barriers to equal participation in society. Women are treated less than men because of gender inequality, this show how women are not looked on the same social status. It should be equal pay for equal work, but unfortunately women are looked as less than men. Many countries do not give women the right to education because they believe women should not be educated. Many poor women cannot afford to pay for education, limiting them to receiving it. Many times it is not an option because governments prevent women from achieving their goal of education. Thus, due to all these things, women have been suffering a lot. Their oppression knows no ethnic and racial boundaries and they have been the victims for ages.  
  

#### b. Why is it important to question our beliefs and values? How are they set up in a person’s mind?

It is important to question our beliefs and values because it builds character, honesty, compassion, acceptance and strength. People usually tend to avoid questioning their own beliefs because they assume that it will diminish their beliefs. If we think this way it’s because we already doubt the veracity of our claims, and we are being dishonest with ourselves. Questioning beliefs through objectivity and observing as a third party/person helps us to gage not only if our beliefs are true, but what are the logical consequences of our ideas in the world (positive and negative). As soon as we question another person’s beliefs on a topic, we should evaluate our own. If we discover we are wrong, this fosters acceptance. If we discover we are wrong and modify our beliefs and admit our error, this fosters strength. Thus, it’s important to question our beliefs and values.  
  

### Writing

  

#### B. Write an article for a national daily on “The Status of Women in Nepali Society.”

The Status of Women in Nepali Society

**2021-01-26 :** Nepal is a country of great geographic, cultural, ethnic, religious diversity. Across the diversity, the majority of communities in Nepal are patriarchal. A women’s life is strongly influenced by her father, husband and son. Such patriarchal practices are further reinforced by the legal system. The status of women is determined by the patriarchal social system, values, and women’s right preserved and protected by the state, and state policy for the development of women.  
  
Women’s relative status, however, varied from one ethnic group to another. The economic contribution of women is substantial, but largely unnoticed because their traditional role was taken as for granted.  
  
The health status of Nepal’s people is one of the lowest in the South Asian region and this is particularly true for females. One fifth of women get married in the early age of 15-19. As a result of their premature pregnancy the deaths of women have been occurring in a very high.  
  
National statistics shows that women’s literacy rate is 30 percent while 66 percent to male. The enrollment of women in higher education is only 24.95 percent. Women’ involvement in technical and vocational education is also lower than men. This is due to the social norms and culture that we follow also. As in rural areas girls are considered as “paraya dhan”(others property) and they don’t get the opportunity to get education.  
  
A large part of women’s work is not considered as economic activity. As a result only 45.2 per cent of women as compared to 68.2 per cent of men are classified as economically active. There are very few women working in professional work in Nepal. They may study the law, but few are able to enter the profession.  
  

### Grammar

  

#### B. Choose the correct alternative.

  

#### a. an old foreign car/a foreign old car

an old foreign car  
  

#### b. a beautiful white dress/ a white beautiful dress

a beautiful white dress  
  

#### c. a nice tall young man/a tall nice young man

a nice tall young man  
  

#### d. a big black wooden desk/ a black wooden big desk

a big black wooden desk  
  

#### e. a delicious Italian pizza/ an Italian delicious pizza

a delicious Italian pizza  
  

#### f. a huge brown bear/a brown huge bear

a huge brown bear  
  

#### g. a purple cotton sleeping bag/ a cotton purple sleeping bag

a purple cotton sleeping bag  
  

#### h. a beautiful old Indian village/an old beautiful Indian village

an old beautiful Indian village  
  

#### i. a cute little kitten/ a little cute kitten

a cute little kitten  
  

#### j. an expensive antique /an antique expensive table

an antique expensive table  
  

#### C. Put the adjectives in the following sentences in the correct order.

  

#### a. I bought (red/ a /comfortable/new) scooter.

I bought a comfortable new scooter.  
  

#### b. She reached home and sat on (relaxing/old/her/wooden) chair.

She reached home and sat on her old relaxing wooden chair.  
  

#### c. We ate (Chinese/delicious/some) food.

we ate some delicious Chinese food.  
  

#### d. I need (golden/delicious/some/round) apples.

I need some delicious round golden apples.  
  

#### e. We like to live in a (calm/unmediated/remote) place for some time.

We like to live in a calm unmediated remote place for some time.  
  

#### f. She is wearing (silver/beautiful / a(n)/ old) ring.

She is wearing an old beautiful silver ring.  
  

#### g. I am looking for (golden/stylish/a/Japanese) watch.

I am looking for a stylish Japanese golden watch.  
  

#### h. She dropped (old/china/attractive/a(n)) cup and smashed it.

She dropped an old attractive China cup and smashed it.  
  

#### i. He wants to marry (young/pretty/educated/a(n)/rustic) girl.

He wants to marry a pretty young educated rustic girl.  
  

#### j. We stayed in a (luxurious/five star/new) hotel in Dubai.

We stayed in a new luxurious five-star hotel in Dubai.