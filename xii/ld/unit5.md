## Unit 5
# Education
### Working with words

  

#### A. Match the words in column ‘A’ with their meanings in column ‘B’.

#### a. expectantly

in a way that shows you are hoping for something, especially something good or exciting  
  

#### b. beckon

to give signal to somebody to move nearer or to follow you  
  

#### c. grapple

to try hard to find a solution to a problem  
  

#### d. texture

the way food or drink tastes or feels in your mouth  
  

#### e. cobbled

having a surface that is made of small round stones  
  

#### f. flimsy

thin and easily torn  
  

#### g. attire

clothes, especially fine or formal ones  
  

#### h. marvelous

extremely good; wonderful  
  

#### i. dynamics

the science of the forces involved in movement  
  

#### B. An autobiography is a story of a person’s life, written by himself/herself. Use a dictionary and find the meanings of the following words related to people’s life stories.

#### Hagiography

the writing of the lives of saints.  
  

#### Psychobiography

a biographical study focusing on psychological factors, as childhood traumas and unconscious motives.  
  

#### Pathography

the study of the life of an individual or the history of a community with regard to the influence of a particular disease or psychological disorder.  
  

#### Chronicle

a factual written account of important or historical events in the order of their occurrence.  
  

#### Obituary

a notice of a death, especially in a newspaper, typically including a brief biography of the deceased person.  
  

#### Character sketch

a brief written description of a person’s qualities.  
  

#### Profile

a short article giving a description of a person or organization.  
  

#### Memoir

a historical account or biography written from personal knowledge.  
  

 

### Comprehension

  

#### A. Put the following events in the life of Abdul Kalam in a chronological order.

  
c. Abdul Kalam attended an elementary school at Rameswaram.  
f. One day Abdul Kalam’s teacher visited them to share his pride and pleasure about his performance.  
a. They celebrated happiness with poli.  
g. A new teacher in the school forbade Abdul Kalam to sit together with his Bramhin friend.  
e. Many students did not understand well of Sivasubramania Iyer’s lecture.  
d. He then took the students to the seashore for a practical class.  
b. Abdul Kalam was determined that he would make a future study about flight and flight systems.  
  

#### B. Answer the following questions.

#### a. What were the causes of Abdul Kalam’s happiness?

The causes of Abdul Kalam’s happiness were his supportive father, loving mother, inspirational and motivational teachers at schools, his keen interest in mathematics and his labour and dedication to his study.  
  

#### b. Which two places did Abdul Kalam visit before going to school?

Abdul Kalam visited to his Arabic tutoring class and his mathematics teacher’s home before heading to school.  
  

#### c. What did he like about mathematics?

He liked to learn rules and principles of mathematics and solving complex number problems for hours.  
  

#### d. Why was the new teacher unhappy?

The new teacher was unhappy because he saw that a Brahmin boy Ramanadhan Sastry and a Muslim boy Abdul Kalam sitting together in a same bench in the classroom.  
  

#### e. Why did Abdul Kalam have to split with his intimate friend?

Abdul Kalam had to split with his intimate friend because of the discriminating behaviour of his new teacher in terms of religion in Rameswaram Elementary School.  
  

#### f. What was the topic of Sivasubramania Iyer’s class?

The topic of Sivasubramania Iyer’s class was how the birds could fly in the sky.  
  

#### g. How was the teacher’s reaction when the students told him that they did not understand his lecture?

Teacher didn’t become upset when the students told him that they didn’t understand his lecture.  
  

#### h. Why did Sivasubramania Iyer take his students to the seashore?

Sivasubramania Iyer took his students to the seashore because he wanted to demonstrate the mechanism of bird flight to the students.  
  

### Critical Thinking

  

#### a. APJ Abdul Kalam became a renowned aerospace scientist in his later life. Do you find any association of his childhood days in shaping his career? Explain with specific instances from the text.

APJ (Avul Pakir Jainulabdeeni) Abdul Kalam is a well-known aerospace scientist of India. He became a famous aerospace scientist because he was inspired and motivated by the explanation of the flight dynamics of birds practically given by his teacher Sivasubramania lyer in his school. There is the great associations of his childhood days in shaping his career. He was a keen student of mathematics and always scored full marks in it. He also scored full marks in Science in his study time. Beside this he would take extra classes of mathematics at his teacher’s home. He kept himself always busy in solving complex number problems. The most impressing thing to him was the practical lecture of his teacher Sivasubramania lyer. He, along with his friends, was taken to the seashore of Rameswaram to have a practical demonstration on flight dynamics of the birds. The real teaching of his teacher had left him an influential remarks. He learnt how birds could fly using their wings and tails in a proper balance. When he learnt flight systems of the birds he was determined to study flight dynamics and following every steps suggested by his teacher, and hence he became an aerospace scientist.  
  

#### b. Kalam mentions an instance of discrimination against him in his school life. What picture of society does he want to depict by mentioning the incident? Discuss.

In his memoir, he mentions a sad event of religious discrimination in his school. His new teacher showed his discriminating behaviour when he saw a Muslim boy (Abdul) and a Hindu boy (Rameswaram) sitting together in a same bench in the classroom. Rameswaram Sastry was an intimate friend of him belonging to Hindu community. Abdul was ignorant about the religious discrimination between Hindu and Muslim. During the time, there was extreme religious discrimination between Hindu and Muslim. The behaviour of the new teacher was a true picture of the then society. He could not digest the friendship between a Hindu boy and a Muslim boy, so he forbaded Abdul to sit close to his Brahmin friend. Abdul was totally upset by this behaviour. By this incident, Abdul wants to show the picture of the society that was parted between two sects – Hindu and Muslim. Few people denied such divisions, but most of the people, including educated people believed in partition.  
  

### Writing

  

#### Write a short autobiography featuring your childhood life using the following guidelines.

I was born in a small village of north – eastern part of Nepalgunj District. I’m from a middle class family. There are five members in my family including me. First of my respected Father who is tailor, then my lovely mother she is House wife. Other my family members are a eldest brother and a younger sister and both of them are studying too.  
  
Since I’m a student, my daily routine focuses on studying and classes. I usually wake up at 6:00 am and do my daily routines like brushing my teeth, taking a shower and getting ready for class. After which I check my notes and revise for the day. My classes usually used to start by 8:00 am and ends by 1:00 pm. I used to attend classes regularly. After classes, I hang out with my friends for a while and go back home. I reach home and then usually take a nap till 4:30 pm. After taking a nap, I go out and play. I usually come back by 6 and have a bath. After which I get ready to study and by 9:00, I go to the bed.  
  
I used to play a lot with my siblings. I remember very fondly the games we use to play. Especially, in the evenings, we used to go out in the park with our sports equipment. Each day we played different games, for example, football on one day and cricket on the other. These memories of playing in the park are very dear to me. Most importantly, I remember this instance very clearly when we went out for a picnic with my family. We paid a visit to the zoo and had an incredible day. My mother packed delectable dishes which we ate in the zoo. My father clicked so many pictures that day. When I look at these pictures, the memory is so clear, it seems like it happened just yesterday. Thus, my childhood memories are very dear to me and make me smile when I feel low.  
  

### Grammar

  

#### A. Study the following sentences and underline the connectives.

a. **Although** she spoke very fast, I understood what she meant to say.  
  
b. **In spite of** her hard labour, she failed her exam.  
  
c. **Though** he had all the required qualifications, he did not get the job.  
  
d. **Despite** having all the qualifications, he did not get the job.  
  

#### B. Join the following pairs of sentences twice, using although/though/even though and despite/in spite of as in the example.

  

#### a. He is a millionaire. He lives in a simple house.

– Although he is a millionaire, he lives in a simple house.  
– Despite of being a millionaire, he lives in a simple house.  
  

#### b. The weather was extremely bad. The pilot landed the plane safely.

– In spite of extremely bad weather, the pilot landed the plane safely.  
– Although the weather was extremely bad, the pilot landed the plane safely.  
  

#### c. We study in the same college. We hardly see each other.

– Despite studying in the same college, we hardly see each other.  
– Although we study in the same college, we hardly see each other.  
  

#### d. It rained heavily. We enjoyed our holiday.

– Although it rained heavily, we enjoyed our holiday.  
– In spite of the heavy rain, we enjoyed our holiday.  
  

#### e. I had an umbrella. I got wet in the rain.

– Despite having an umbrella, I got wet in the rain.  
– Although I had an umbrella, I got wet in the rain.  
  

#### f. I was really tired. I could not sleep for a moment.

– Although I was really tired, I could not sleep for a moment.  
– I couldn’t sleep despite being very tired.  
  

#### g. She has a very good accent in English. She failed the interview of a newsreader.

– Despite having a very good accent in English, she failed the interview of a newsreader.  
– Although she has a very good accent in English, she failed the interview of a newsreader.  
  

#### h. Lhasa has extremely cold weather in winter. Millions of tourists go there in January.

– Millions of tourists go to Lhasa in January, even though it has extremely cold weather in winter.  
– Despite having extremely cold weather in winter, millions of tourists go to Lhasa in January.  
  

#### i. He was badly injured in the first round of the boxing match. He was victorious in the third round.

– In spite of being badly injured in the first round of the boxing match, he was victorious in the third round.  
– Although he was badly injured in the first round of the boxing match, he was victorious in the third round.  
  

#### C. Complete each sentence in an appropriate way.

  
a. He passed the exam **although he hadn’t prepared well.**  
  
b. She climbed the mountain in spite of her **leg injury.**  
  
c. He did not give any alms to the beggars even though **he earned a lot.**  
  
d. In spite of his poor eyesight, **he is conducting online courses.**  
  
e **She does not go to parties**, though she is very sociable.  
  
f. **He could not pass the test**, in spite of his ten attempts.  
  
g. He refused to eat anything despite **having an appetite.**  
  
h. He could not score goods grades in the SEE exams in spite of **his hard with private coaching.**  
  
i. She accepted the job although **she couldn’t meet her salary expectation.**  
  
j. Even though we had planned everything carefully, **we could not complete our mission.**